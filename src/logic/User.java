package logic;

public class User {

	
	private String name;
	private String surname;
	private String telephone;
	private String DNI;
	private boolean isAdmin;
	private boolean isMonitor;
	private boolean isContable;
	private String username;
	private String password;
	
	private String datosInstalacionConflicto; //solo lo uso para la historia 3
	
	public User(String dni, String n, String surna, String tlph,boolean ad, String user, String password2, boolean monitor) {
		this.DNI=dni;
		this.name=n;
		this.surname=surna;
		this.telephone=tlph;
		this.isAdmin = ad;
		this.username=user;
		this.password=password2;
		this.isMonitor = monitor;
		this.isContable =false;
		setDatosInstalacionConflicto("");
	}
	//solo se usa para el contable
	public User(String dni, String n, String surna, String tlph,boolean ad, String user, String password2, boolean monitor,boolean contable) {
		this.DNI=dni;
		this.name=n;
		this.surname=surna;
		this.telephone=tlph;
		this.isAdmin = ad;
		this.username=user;
		this.password=password2;
		this.isMonitor = monitor;
		this.isContable = contable;
		setDatosInstalacionConflicto("");
	}
	
	//solo se usa para la historia 3 sprint 3 elena (crear actividad periodica)
		public User(String dni, String n, String surna, String tlph,String datos) {
			this.DNI=dni;
			this.name=n;
			this.surname=surna;
			this.telephone=tlph;
			this.isAdmin = false;
			this.username="";
			this.password="";
			this.isMonitor = false;
			this.isContable = false;
			setDatosInstalacionConflicto(datos);
		}

	protected String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	protected String getSurname() {
		return surname;
	}

	protected void setSurname(String surname) {
		this.surname = surname;
	}

	protected String getTelephone() {
		return telephone;
	}

	protected void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getDNI() {
		return DNI;
	}

	protected void setDNI(String dNI) {
		DNI = dNI;
	}

	protected boolean isAdmin() {
		return isAdmin;
	}

	protected void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setMonitor(boolean isMonitor){
		this.isMonitor =isMonitor;
	}
	
	public boolean isMonitor(){
		return isMonitor;
	}
	
	public void setContable(boolean isContable){
		this.isContable =isContable;
	}
	
	public boolean isContable(){
		return isContable;
	}
	public String getDatosInstalacionConflicto() {
		return datosInstalacionConflicto;
	}
	public void setDatosInstalacionConflicto(String datosInstalacionConflicto) {
		this.datosInstalacionConflicto = datosInstalacionConflicto;
	}
	
}
