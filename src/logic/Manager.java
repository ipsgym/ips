package logic;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Manager {

	private Database database;
	private User currentUser;

	protected static int monthBBDD = 11;
	private List<String> listConflictReservas;
	private List<String> daysActivity;
	private List<User> usersConflicts;
	private int[] daysOfMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public Manager() {
		database = new Database();
		currentUser = null;
	}

	/**
	 * This method returns all the installations names
	 * 
	 * @return installations names
	 */
	public ArrayList<String> getInstallations() {
		return database.getInstallations();
	}

	/**
	 * This method gets the schedule of a certain installation
	 * 
	 * @param actionCommand
	 *            the installation we are looking for
	 * @param fecha
	 *            the date we are looking at
	 * @return the list of activities of that installation in that certain date
	 */
	public ArrayList<Activity> getScheduleInstallation(String actionCommand, String fecha) {
		return database.getScheduleInstallation(actionCommand, fecha);
	}

	/**
	 * This method logs the user off the program
	 */
	public void logOff() {
		currentUser = null;
	}

	/**
	 * This method checks if the user and the password are correct fields, and
	 * signs the user in
	 * 
	 * @param text
	 *            the user
	 * @param password
	 *            the password of the account
	 * @return true if it was correct, false otherwise
	 */
	public boolean signIn(String text, char[] cs) {
		String aux = setArrayToString(cs);
		if (database.checkCorrectUserPass(text, aux)) {
			if (text.substring(0, 5).compareTo("admin") == 0) {
				currentUser = database.signInAdmin(text, aux);
			} else if (text.substring(0, 7).compareTo("monitor") == 0) {
				currentUser = database.signInMonitor(text, aux);
			} else if (text.substring(0, 8).compareTo("contable") == 0) {
				currentUser = database.signInContable(text, aux);
			} else {
				currentUser = database.signInMember(text, aux);
			}
			return true;
		}
		return false;
	}

	// Saul
	/**
	 * This method checks if the user and the password are correct fields, and
	 * signs the user in
	 * 
	 * @param text
	 *            the user
	 * @param password
	 *            the password of the account
	 * @return true if it was correct, false otherwise
	 */
	public boolean signIn(String text, String aux) {
		if (database.checkCorrectUserPass(text, aux)) {
			if (text.substring(0, 5).compareTo("admin") == 0) {
				currentUser = database.signInAdmin(text, aux);
			} else {
				currentUser = database.signInMember(text, aux);
			}
			return true;
		}
		return false;
	}

	/**
	 * This method does the conversion of an array of characters to a String
	 * 
	 * @param aux
	 *            the array of characters
	 * @return the new string formed
	 */
	private String setArrayToString(char[] aux) {
		String chain = "";
		for (int i = 0; i < aux.length; i++) {
			chain += aux[i];
		}
		return chain;
	}

	/**
	 * Checks whether the user that has logged in is an admin or not
	 * 
	 * @return true if the user is an admin, false otherwise
	 */
	public boolean currentUserIsAdmin() {
		return currentUser.isAdmin();
	}

	/**
	 * This method checks whether there is a user logged in
	 * 
	 * @return true if there is a user logged in, false otherwise
	 */
	public boolean isAUserLoggedIn() {
		if (currentUser != null)
			return true;
		return false;
	}

	/**
	 * This method returns all the activities names
	 * 
	 * @return activities names
	 */
	public ArrayList<String[]> getActivities() {
		return database.getActivities();
	}

	// ---------------------- Saul. ----------------------
	/**
	 * Method to check if activity booking is possible.
	 * 
	 * @param code
	 * @param activity,
	 *            activity to book.
	 * @return if the booking was possible or no.
	 */
	public boolean activityInstallationBookingAdm(String activity, String installation, String fecha, int hourI,
			int hourEnd, String dniUser, String rigisterCode, String code, String monitor, int plazas,
			boolean creandoActividad) {
		String type = "a";
		if (hourEnd == 0)
			hourEnd = 24;
		Activity newActivity = new Activity(installation, activity, fecha, hourI, hourEnd, type, code, monitor);
		// Se crea actividad
		if (!creandoActividad) {// si no se esta creando la actividad puntual y
								// no hay conflictos
			if (database.isInstallationFree(newActivity)) {
				return database.insertBooking(newActivity, dniUser, rigisterCode, plazas);
			}
		} else {
			return database.insertBooking(newActivity, dniUser, rigisterCode, plazas);
		}
		return false;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	// gonzi
	/**
	 * 
	 * @param installation.
	 *
	 * @return
	 */
	public boolean installationBookingAdmToMember(String installation, String fecha, int hourI, int hourEnd,
			String dniUser, String reservaCodigo, String code) {
		String type = "m";
		if (hourEnd == 0)
			hourEnd = 24;
		Activity newActivity = new Activity(installation, "Alquilado", fecha, hourI, hourEnd, type, code, null);
		if (database.isInstallationFree(newActivity))
			return database.insertBooking(newActivity, dniUser, reservaCodigo, -2);
		return false;
	}

	public boolean isUserCorrect(String userOnline) {
		return database.isUserCorrect(userOnline);
	}

	public boolean isUserValid(String fecha_usada, String dni, int hora_usada_inicio, int hora_usada_final) {
		return database.isUserValid(fecha_usada, dni, hora_usada_inicio, hora_usada_final);
	}

	// GONZI

	public String checkifInstallationIsOccupied(String installationName, String fecha, int bookingHour) {
		return database.checkifInstallationIsOccupied(installationName, fecha, bookingHour);
	}

	// Alvaro

	public boolean bookingInstallNotMember(String installation, String date, int hourI, int hourF, String dniUser,
			String coderegister, String code) {
		String type = "n";
		Activity actividad = new Activity(installation, "Alquilado", date, hourI, hourF, type, code, null);
		if (database.isInstallationFree(actividad))
			return database.insertBooking(actividad, dniUser, coderegister, -2);
		return false;
	}

	public boolean InsertUsers(String dni, String name, int phone, String surName) {
		if (database.InsertUser(dni, name, phone, surName))
			return true;
		return false;
	}

	/**
	 * Este metodo coge el precio de la instalacion y lo devuelve
	 * 
	 * @param currentInstallation
	 *            la instalacion
	 * @param socio
	 *            si es true, lo consideramos socio, y si pasamos false, no
	 *            socio
	 * @return el preio de la instalacion
	 */
	public int getPriceInstallation(String currentInstallation, boolean user) {
		if (user) {// si es socio
			return database.getPriceInstallationMember(currentInstallation);
		} else { // si es no socio
			return database.getPriceInstallationNotMember(currentInstallation);
		}
	}

	/**
	 * Este metodo llama a bbdd y crear� una nueva fila en la entidad pago,
	 * con los mismos atributos que los parametros
	 * 
	 * @param bookingCode
	 *            el codigo de la reserva
	 * @param b
	 *            si esta pagada(true si, false no)
	 * @param c
	 *            si esta pagada en efectivo (true si, false no(cuota))
	 * @param bill
	 */
	public void setBookingPayed(String bookingCode, boolean b, boolean c) {
		database.setBookingPayed(bookingCode, b, c);
	}

	// Saul
	public String checkifBookingExists(String installationName, String dni, int bookingHour, String fecha) {
		return database.checkifBookingExists(installationName, dni, bookingHour, fecha);
	}

	// Saul
	public boolean bookingInput(String id_reserva, int hour) {
		return database.bookingInput(id_reserva, hour);
	}

	// Saul
	public boolean bookingOutput(String id_reserva, int hour) {
		return database.bookingOutput(id_reserva, hour);
	}

	// Saul
	public boolean insertPay(String id_reserva, int pay) {
		return database.insertPay(id_reserva, pay);
	}

	// Saul
	public boolean isUserInput(String dni) {
		return database.isUserInput(dni);
	}

	// Saul
	public boolean userPayInput(String id_reserva) {
		return database.userPayInput(id_reserva);
	}

	public void saveBill(String dni, Date date, String instal, int i, String text) {
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = df.format(date);
			BufferedWriter file = new BufferedWriter(new FileWriter("files/" + dni + fecha + instal + i + ".txt"));
			file.write(text);
			file.close();
		} catch (FileNotFoundException f) {
			System.out.println("The file couldn't be saved");
		} catch (IOException e) {
			new RuntimeException("Input/output error");
		}
	}

	public String generateBillNM(String name, String surnames, String dni, String telephone, Date date,
			String hourStart, String hourEnd, String currentInstallation, boolean notUser, boolean cash,
			String moneyGiven) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String fecha = df.format(date);
		String line1 = "IPS GYM FACTURA \n";
		String line2 = "----------------------- \n";
		String line3 = "Nombre: " + name + " \n";
		line3 += "Apellidos: " + surnames + " \n";
		line3 += "DNI: " + dni + " \n";
		line3 += "Telefono: " + telephone + " \n";
		line3 += "Instalaci�n: " + currentInstallation + " \n";
		line3 += "Fecha: " + fecha + " \n";
		line3 += "Hora inicio: " + hourStart + " \n";
		line3 += "Hora final: " + hourEnd + " \n";
		String line4 = "Precio total: ";
		String aux = "Precio: ";
		int diff = Integer.parseInt(hourEnd) - Integer.parseInt(hourStart);
		int price = 0;
		if (notUser) {
			price = (database.getPriceInstallationNotMember(currentInstallation));
		} else {
			price = database.getPriceInstallationMember(currentInstallation);
		}
		aux += price + " �/h \n";
		price = price * diff;
		line4 += price + " � \n" + aux;
		if (cash) {
			line4 += "Pago: " + moneyGiven + "�\n";
			line4 += "Vuelta: ";
			int mon = Integer.parseInt(moneyGiven);
			int back = mon - price;
			line4 += back + "�\n";
		}
		String line5 = "GRACIAS POR CONFIAR EN NOSOTROS";
		return line1 + line2 + line3 + line4 + line5;
	}

	public String generateBillM(String dni, Date date, String hourStart, String hourEnd, String currentInstallation) {
		String[] data = database.searchForMemeberDataDNI(dni);
		return generateBillNM(data[0], data[1], dni, data[2], date, hourStart, hourEnd, currentInstallation, false,
				false, "");
	}

	public void saveBillInDatabase(String text, String bookingCode) {
		String[] data = text.split("\n");
		database.saveBillDatabase(data, bookingCode);
	}

	public String showBillInDatabase(String currentInstallation, Date date, int selectedRow) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date); // pasarlo al formato de fecha de bbd
		return database.getBillFromDatabase(currentInstallation, fecha, selectedRow);
	}

	public boolean doesUserHaveToPayCash(String id_reserva) {
		return database.doesUserHaveToPayCash(id_reserva);
	}

	public void saveBillAfterMemberPayedCash(int money, String id_reserva) {
		String originalBill = database.getBillFormBookingCode(id_reserva);
		int dineroAPagar = database.getMoneyToPay(id_reserva);
		originalBill += "Pago: " + money + "�\n";
		originalBill += "Vuelta: ";
		int back = money - dineroAPagar;
		originalBill += back + "�\n";
		saveBillInDatabase(originalBill, id_reserva);

	}

	// Saul
	public ArrayList<String[]> getUserBooking(String DNI) {
		return database.getUserBooking(DNI);
	}

	// Gonzi
	public ArrayList<String[]> getUserActivities(String DNI) {
		return database.getUserActivities(DNI);
	}

	public boolean deleteUserBooking(String id_reserva) {
		return database.deleteUserBooking(id_reserva);
	}

	public void crearActividad(String text, String plazas, String activityCode, String monitor) {
		String nombreActividad = text;
		int numeroPlazas = 0;
		if (plazas.equals("-1")) {
			numeroPlazas = -1;
		} else {
			numeroPlazas = Integer.parseInt(plazas);
		}
		database.createClass(nombreActividad, 0, 0, true, numeroPlazas, activityCode, monitor);
	}

	public String getDescripcionInstalacion(String inst) {
		return database.getDescripcionDeInstalcion(inst);
	}

	public ArrayList<String[]> getActivitiesLimit(String fecha) {
		return database.getActivitiesLimit(fecha);
	}

	public int getPlazasLibresAct(String id_reserva) {
		return database.getPlazasLibresAct(id_reserva);
	}

	public boolean insertUserInListAssistants(String id_reserva, String dni) {
		return database.insertUserInListAssistants(id_reserva, dni);
	}

	// public boolean insertUserInListWait(String id_reserva, String dni){
	// return database.insertUserInListWait(id_reserva, dni);
	// }
	public boolean isDniUser(String dni) {
		return database.isDNIUser(dni);
	}

	public boolean isUserRegisterToThisActivity(String dni, String id_reserva) {
		return database.isUserRegisterToThisActivity(dni, id_reserva);
	}

	// Gonzi
	public String getBookingToDelete(String installationName, String fecha, int bookingHour) {
		return database.getBookingToDelete(installationName, fecha, bookingHour);
	}

	//
	public boolean getIfBookingToDeleteIsN(String installationName, String fecha, int bookingHour) {
		return database.getIfBookingToDeleteIsN(installationName, fecha, bookingHour);
	}

	public void deleteAdminBooking(String id_reserva) {
		database.deleteAdminBooking(id_reserva);
	}

	public boolean updateCancelledBooking(int valor, String installationName, String fecha, int bookingHour) {
		return database.updateCancelledBooking(valor, installationName, fecha, bookingHour);
	}

	public boolean checkIfBookingIsCancelled(String installationName, String fecha, int bookingHour) {
		return database.checkIfBookingIsCancelled(installationName, fecha, bookingHour);
	}

	public int getBookingHourI(String id_reserva) {
		return database.getBookingHourI(id_reserva);
	}

	public ArrayList<String[]> seeAssistsList(String id_reserva, String dni_monitor) {
		return database.seeAssistsList(id_reserva, dni_monitor);
	}

	public ArrayList<String[]> seeActMonitor(String dni_monitor) {
		return database.seeActMonitor(dni_monitor);
	}

	public boolean markAssists(String dni_socio, String id_reserva, int assist) {
		return database.markAssists(dni_socio, id_reserva, assist);
	}

	// public boolean checkHourToAssists(String fecha, int hora, String
	// id_reserva){
	// return database.checkHourToAssists(fecha, hora, id_reserva);
	// }
	public boolean isValidSaveAssists(String id_reserva) {
		return database.isValidSaveAssists(id_reserva);
	}

	// Saul
	public boolean isUserInOtherActivity(String fecha, int hourI, String dni) {
		return database.isUserInOtherActivity(fecha, hourI, dni);
	}

	public String getBookingDate(String id_reserva) {
		return database.getBookingDate(id_reserva);
	}

	public boolean getAssistUser(String id_reserva, String dni) {
		return database.getAssistUser(id_reserva, dni);
	}

	public ArrayList<String> getMembersWithCuotaForPay(int month) {
		return database.getMembersWithCuotaForPay(month);
	}

	public ArrayList<Object[]> getReservasSinPagarCuotaSocio(String dni, int month) {
		return database.getReservasSinPagarCuotaSocio(dni, month);
	}

	public int getCuotaSocio(String valueOf, int month) {
		if (month == monthBBDD) {
			return database.getCuotaSocio(valueOf);
		} else {
			return database.getCuotaMesPasadoSocio(valueOf);
		}
	}

	public void updateCuotaSocio(String dni, int cuotaAPagar, int month) {
		ArrayList<Object[]> list = getReservasSinPagarCuotaSocio(dni, month);
		if (month == monthBBDD) {
			database.updateCuotaSocio(dni, cuotaAPagar);
		} else {
			database.updateCuotaMesPasadoSocio(dni, cuotaAPagar);
		}
		for (int i = 0; i < list.size(); i++) {
			database.updatePagoReservaCuotaSocio(list.get(i)[0]);
		}
	}

	public ArrayList<String[]> getAllActivitiesOfTheDay(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date); // pasarlo al formato de fecha de bbd
		return database.getAllActivitiesOfTheDay(fecha);
	}

	public List<String[]> getAllActivitiesOfTheDay(Date date, String inst) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date); // pasarlo al formato de fecha de bbd
		return database.getAllActivitiesOfTheDay(fecha, inst);
	}

	public boolean removeUserInListAssistants(String id_reserva, String dni) {
		return database.removeUserInListAssistants(id_reserva, dni);
	}

	public void lookMonthAndUpdateIfNecesario() {
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date);
		int anio = Integer.parseInt(fecha.substring(0, 4));
		int month = Integer.parseInt(fecha.substring(5, 7));
		int day = Integer.parseInt(fecha.substring(8));
		if ((day > 19 && month == monthBBDD) || (month - 1 == monthBBDD) || (anio > 2017)) {
			List<String> list = database.getMembers();
			for (int i = 0; i < list.size(); i++) {
				database.updateCuotaMesPasado(list.get(i));
			}
		}
	}

	/**
	 * Para checkear conflictos con otras reservas
	 * 
	 * @param dateAc
	 * @param semanas
	 * @param lunes
	 * @param martes
	 * @param miercoles
	 * @param jueves
	 * @param viernes
	 * @param sabado
	 * @param domingo
	 * @param horaInicio
	 * @param horaFin
	 * @param instalacion
	 * @param periodic
	 * @return
	 */
	public int checkConflictsWithTheNewActivity(Date dateAc, Date dateEnd, boolean lunes, boolean martes,
			boolean miercoles, boolean jueves, boolean viernes, boolean sabado, boolean domingo, int horaInicio,
			int horaFin, String instalacion, boolean periodic) {

		// ------------------------------------------------------------------
		// TODOS LOS DIAS QUE ESTA LA ACTIVIDAD
		List<Integer> days = diasQueSeHanSeleccionado(lunes, martes, miercoles, jueves, viernes, sabado, domingo);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		if (periodic) {
			// ------------------------------------------------------------------
			// D�AS DE INICIO
			@SuppressWarnings("deprecation")
			int numDayStart = dateAc.getDay();// dia inicio //1

			@SuppressWarnings("deprecation")
			int numDayEnd = dateEnd.getDay(); // 6

			boolean readyS = false;

			for (int d : days) {// 3 5
				if (!readyS && d >= numDayStart) { // 3 >=1
					numDayStart = d; // 3
					readyS = true;
				}
				if (d <= numDayEnd) { // 3<=6 5<=6
					numDayEnd = d; // 3 5
				}

			}

			@SuppressWarnings("deprecation")
			int month = dateAc.getMonth(); // 0 enero, 1 febrero ...11 diciembre

			int numberOfDaysMonth = daysOfMonths[month];

			@SuppressWarnings("deprecation")
			int monthEnd = dateEnd.getMonth();

			String fechaStart = df.format(dateAc);
			int dayStart = Integer.parseInt(fechaStart.substring(8));// el dia,
																		// si la
																		// fecha
																		// es
																		// 2017-11-16,
																		// el 16
			int year = Integer.parseInt(fechaStart.substring(0, 4));// 2017-11-16
																	// coge
			// 2017

			String fechaEnd = df.format(dateEnd);
			int dayEnd = Integer.parseInt(fechaEnd.substring(8));
			int yearEnd = Integer.parseInt(fechaEnd.substring(0, 4));

			if (dateAc.getDay() < numDayStart) { // 1 < 3
				dayStart += (numDayStart - dateAc.getDay()); // 14 += (3-1) --->
																// 16
				if (dayStart > numberOfDaysMonth) {
					dayStart = (dayStart - numberOfDaysMonth);
					month++;
					if (month == 12) {
						month = 0;
						year++;
					}
					numberOfDaysMonth = daysOfMonths[month];
				}
			}
			if (dateEnd.getDay() > numDayEnd) { // 6 > 5
				dayEnd -= (dateEnd.getDay() - numDayEnd); // 3 -= (6-5) --->2
				if (dayEnd < 1) { // menor que 1
					dayEnd = (daysOfMonths[monthEnd] + dayEnd);
					monthEnd--;
					if (monthEnd < 0) {// menor que enero
						month = 11;
						yearEnd--;
					}
				}
			}

			daysActivity = new ArrayList<String>(); // arraylist con todas las
													// fechas de la actividad
			int dayAux = dayStart;// para movernos a traves de los dias
									// suponemos
									// que el dia 16 es jueves, o sea 4
			int monthAux = month;

			// ----------------------------------------------------------------------------------
			// AQUI SE CALCULAN TODOS LOS DIAS

			if (days.size() == 1) {
				do {
					if (dayAux < 10) {// mira si es solo un numero para
										// a�adirle un 0
						if (monthAux < 10) {// mira si es solo un numero para
											// a�adirle un 0
							daysActivity.add(year + "-0" + (monthAux + 1) + "-0" + dayAux);
						} else {
							daysActivity.add(year + "-" + (monthAux + 1) + "-0" + dayAux);
						}

					} else {
						if (monthAux < 10) {// mira si es solo un numero para
											// a�adirle un 0
							daysActivity.add(year + "-0" + (monthAux + 1) + "-" + dayAux);
						} else {
							daysActivity.add(year + "-" + (monthAux + 1) + "-" + dayAux);
						}
					}
					if (dayAux + 7 <= numberOfDaysMonth) {
						dayAux += 7;
					} else {
						dayAux = (dayAux + 7) - numberOfDaysMonth;
						monthAux++;
						if (monthAux == 12) {// se pasa a enero
							monthAux = 0;
							year++;
						}
						numberOfDaysMonth = daysOfMonths[monthAux];
					}

				} while (dateLessThanFinal(dayAux, monthAux, year, dayEnd, monthEnd, yearEnd));
			} else {
				do {
					for (int i = 0; i < days.size(); i++) {// 0 4 6
						if (numDayStart == days.get(i)) {
							if (dayAux < 10) {// mira si es solo un numero para
												// a�adirle un 0
								if (monthAux < 10) {// mira si es solo un numero
													// para a�adirle un 0
									daysActivity.add(year + "-0" + (monthAux + 1) + "-0" + dayAux);
								} else {
									daysActivity.add(year + "-" + (monthAux + 1) + "-0" + dayAux);
								}
							} else {
								if (monthAux < 10) {// mira si es solo un numero
													// para a�adirle un 0
									daysActivity.add(year + "-0" + (monthAux + 1) + "-" + dayAux);
								} else {
									daysActivity.add(year + "-" + (monthAux + 1) + "-" + dayAux);
								}
							}

							int sum = 0;
							if (i + 1 < days.size()) { // que hay mas valores en
														// el arraylist
								sum = days.get(i + 1) - numDayStart;
								numDayStart = days.get(i + 1);
							} else {
								sum = (7 - numDayStart) + days.get(0);// (7 -
																		// diaEnElQueEstamos)
																		// +
																		// elPrimeroN�mero
																		// de la
																		// lista
								numDayStart = days.get(0);
							}

							if (dayAux + sum <= numberOfDaysMonth) {
								dayAux += sum;
							} else {
								dayAux = (dayAux + sum) - numberOfDaysMonth;
								monthAux++;
								if (monthAux == 12) {// se pasa a enero
									monthAux = 0;
									year++;
								}
								numberOfDaysMonth = daysOfMonths[monthAux];
							}
						}
					}
				} while (dateLessThanFinal(dayAux, monthAux, year, dayEnd, monthEnd, yearEnd));
			}
			listConflictReservas = database.getConflictsWithNewActivityPeriodic(daysActivity, horaInicio, horaFin,
					instalacion);
		} else {
			String fecha = df.format(dateAc);
			listConflictReservas = database.getConflictsWithNewActivityPuntual(fecha, horaInicio, horaFin, instalacion);
		}
		getDataFromReservasConflicts();

		return listConflictReservas.size();
	}

	public int checkConflictsWithTheNewActivityPuntual(Date date, int hI, int hF, String currentInstallation) {
		boolean[] days = lookForTheDaySelected(date);
		return checkConflictsWithTheNewActivity(date, date, days[0], days[1], days[2], days[3], days[4], days[5],
				days[6], hI, hF, currentInstallation, false);
	}

	private boolean[] lookForTheDaySelected(Date date) {
		boolean[] days = new boolean[7];
		int day = date.getDay();
		for (int i = 0; i < days.length; i++) {
			if (day == i) {
				days[i] = true;
			} else {
				days[i] = false;
			}
		}
		return days;
	}

	private boolean dateLessThanFinal(int dayAux, int monthAux, int year, int dayEnd, int monthEnd, int yearEnd) {
		if (year < yearEnd) { // 2017 < 2018
			return true;
		} else if (year == yearEnd) { // si es el mismo
			if (monthAux < monthEnd) {// noviembre < diciembre
				return true;
			} else if (monthAux == monthEnd) { // si es el mismo mes
				if (dayAux <= dayEnd) { // el dia ees anterior o el mismo
					return true;
				}
			}
		}
		return false;
	}

	private List<Integer> diasQueSeHanSeleccionado(boolean lunes, boolean martes, boolean miercoles, boolean jueves,
			boolean viernes, boolean sabado, boolean domingo) {
		List<Integer> list = new ArrayList<Integer>();
		if (domingo) {
			list.add(0);
		}
		if (lunes) {
			list.add(1);
		}
		if (martes) {
			list.add(2);
		}
		if (miercoles) {
			list.add(3);
		}
		if (jueves) {
			list.add(4);
		}
		if (viernes) {
			list.add(5);
		}
		if (sabado) {
			list.add(6);
		}
		return list;
	}

	private String generateCode() {
		String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String codigo = "";
		int longitudCodigo = 10;
		for (int i = 0; i < longitudCodigo; i++) {
			int numero = (int) (Math.random() * caracteres.length());
			codigo += caracteres.charAt(numero);
		}
		return codigo;
	}

	public void crearActividadPeriodica(String currentActivitiesName, String currentActivities,
			String currentInstallation, int hI, int hF, String dniMonitor, int plazas) {
		for (int i = 0; i < daysActivity.size(); i++) {
			String id_reserva = generateCode();
			String dni = currentUser.getDNI();
			Activity newBooking = new Activity(currentInstallation, currentActivitiesName, daysActivity.get(i), hI, hF,
					"a", currentActivities, dniMonitor);
			database.insertBooking(newBooking, dni, id_reserva, plazas);
			// Se crea actividad
		}
	}

	public void cancelAllTheBookingsConflict() {
		for (int i = 0; i < listConflictReservas.size(); i++) {
			database.cancelBookingConflict(listConflictReservas.get(i).substring(1));
		}
	}

	public void getDataFromReservasConflicts() {
		usersConflicts = new ArrayList<User>();
		for (int i = 0; i < listConflictReservas.size(); i++) {
			if (!listConflictReservas.get(i).substring(0, 1).equals("-")) {
				usersConflicts.add(database.getMemberFromReserva(listConflictReservas.get(i)));
			} else {
				usersConflicts.add(database.getReservaGimnasio(listConflictReservas.get(i).substring(1)));
			}
		}
	}

	public List<String> getUserConflictsString() {
		List<String> list = new ArrayList<String>();
		for (User user : usersConflicts) {
			if (!user.getDNI().equals("Gimnasio")) {
				list.add("Nombre: " + user.getName() + " Apellido: " + user.getSurname() + "\nDNI: " + user.getDNI()
						+ "\nTel�fono: " + user.getTelephone() + "\nDatos: " + user.getDatosInstalacionConflicto());
			} else {
				list.add(user.getName() + user.getSurname() + "\nDatos: " + user.getDatosInstalacionConflicto());
			}
		}
		return list;
	}

	public List<String> getDNIMonitores() {
		return database.getDNIMonitores();
	}

	public boolean checkMonitorOcupado(String dniMonitor, int horaInicio, int horaFin) {
		List<String> fechas = daysActivity;
		// if(fechas == null) {
		// //Seguir Aqui
		//
		// }
		List<HashMap<String, Object>> ocupacion = database.getFechasHorasMonitorOcupado(dniMonitor);
		// if(ocupacion.isEmpty()) {
		// return true;
		// }
		for (String fecha : fechas) {
			for (HashMap<String, Object> hs : ocupacion) {
				if (fecha.equals(hs.get("fecha"))) {
					if (areHorasEntreHoras(horaInicio, horaFin, (Integer) hs.get("horaI"), (Integer) hs.get("horaF"))) {
						return false; // Hay un conflicto
					}
				}
			}
		}
		return true;
	}

	public boolean checkMonitorOcupadoParaCambiar(String dniMonitor, int horaInicio, int horaFin, String id_clase) {
		List<String> fechas = daysActivity;
		if (fechas == null) {
			List<HashMap<String, Object>> infoMonitor = getFechaHoraIHoraF(id_clase);
			fechas = new ArrayList<String>();
			for (HashMap<String, Object> hs : infoMonitor) {
				fechas.add((String) hs.get("fecha"));
			}

		}
		List<HashMap<String, Object>> ocupacion = database.getFechasHorasMonitorOcupado(dniMonitor);
		// if(ocupacion.isEmpty()) {
		// return true;
		// }
		for (String fecha : fechas) {
			for (HashMap<String, Object> hs : ocupacion) {
				if (fecha.equals(hs.get("fecha"))) {
					if (areHorasEntreHoras(horaInicio, horaFin, (Integer) hs.get("horaI"), (Integer) hs.get("horaF"))) {
						return false; // Hay un conflicto
					}
				}
			}
		}
		return true;
	}

	public boolean checkMonitorOcupadoPuntual(String dniMonitor, int horaInicio, int horaFin, String fecha) {
		List<HashMap<String, Object>> ocupacion = database.getFechasHorasMonitorOcupado(dniMonitor);
		for (HashMap<String, Object> hs : ocupacion) {
			if (fecha.equals(hs.get("fecha"))) {
				if (areHorasEntreHoras(horaInicio, horaFin, (Integer) hs.get("horaI"), (Integer) hs.get("horaF"))) {
					return false; // Hay un conflicto
				}
			}
		}

		return true;
	}

	public void deleteclass(String id) {
		database.deleteclass(id);
	}

	public boolean isUserInOtherBooking(String fecha, int hourI, String dni) {
		return database.isUserInOtherBooking(fecha, hourI, dni);
	}

	public String showDataActivityInDatabase(String currentInstallation, Date date, int selectedRow) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date); // pasarlo al formato de fecha de bbd
		String aux = "DETALLES\n----------------\n";
		return aux + database.getDataAcitvityFromDatabase(currentInstallation, fecha, selectedRow);
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookings() {
		return database.getActivitiesWithFutureBookings();
	}

	public List<HashMap<String, Object>> getBookingsOfActivity(String id_clase) {
		return database.getBookingsOfActivity(id_clase);
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsPeriodicas() {
		return database.getActivitiesWithFutureBookingsPeriodicas();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsPuntual() {
		return database.getActivitiesWithFutureBookingsPuntual();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsLimitadas() {
		return database.getActivitiesWithFutureBookingsLimitadas();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsIlimitadas() {
		return database.getActivitiesWithFutureBookingsIlimitadas();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsLimitadasPeriodica() {
		return database.getActivitiesWithFutureBookingsLimitadasPeriodica();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsLimitadasPuntual() {
		return database.getActivitiesWithFutureBookingsLimitadasPuntual();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsIlimitadasPeriodica() {
		return database.getActivitiesWithFutureBookingsIlimitadasPeriodica();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsIlimitadasPuntual() {
		return database.getActivitiesWithFutureBookingsIlimitadasPuntual();
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsMonitorInfo(String monitor) {
		return database.getActivitiesWithFutureBookingsMonitorInfo(monitor);
	}

	public int getNumActMonitorInfo(String monitor) {
		return database.getNumActMonitorInfo(monitor);
	}

	public int getNumClassMonitorInfo(String monitor) {
		return database.getNumClassMonitorInfo(monitor);
	}

	public List<HashMap<String, Object>> getFechaHoraIHoraF(String id_clase) {
		return database.getFechaHoraIHoraF(id_clase);
	}

	public String getIDclaseFromIDreserva(String id_reserva) {
		return database.getIDclaseFromIDreserva(id_reserva);
	}

	public boolean cambiarMonito(String id_reserva, String monitor) {
		return database.cambiarMonito(id_reserva, monitor);
	}

	public String getNombreApellidosMonitorDNI(String monitor) {
		return database.getNombreApellidosMonitorDNI(monitor);
	}

	public String getNombreActIDClase(String id_clase) {
		return database.getNombreActIDClase(id_clase);
	}

	public List<HashMap<String, Object>> getHoraInicioHoraFinInstalacion(String nombre, String fecha) {
		return database.getHoraInicioHoraFinInstalacion(nombre, fecha);
	}

	public boolean checkInstalacionOcupada(String nombre, String fecha, int horaInicio, int horaFin) {
		List<HashMap<String, Object>> ocupacion = getHoraInicioHoraFinInstalacion(nombre, fecha);
		for (HashMap<String, Object> hs : ocupacion) {
			if (areHorasEntreHoras(horaInicio, horaFin, (Integer) hs.get("horaI"), (Integer) hs.get("horaF"))) {
				return false; // Hay un conflicto
			}
		}

		return true;
	}

	private boolean areHorasEntreHoras(int horaInicio, int horaFin, int hIM, int hFM) {
		for (int i = horaInicio; i < horaFin; i++) {
			for (int j = hIM; j < hFM; j++) {
				if (i == j)
					return true; // Se solapa alguna hora
			}
		}
		return false;
	}

	public ArrayList<String> getAllInst() {
		return database.getAllInst();
	}

	public String getNombreInstWithID_Reserva(String id_reserva) {
		return database.getNombreInstWithID_Reserva(id_reserva);
	}

	public String getHoraInicioWithID_Reserva(String id_reserva) {
		return database.getHoraInicioWithID_Reserva(id_reserva);
	}

	public String getHoraFinalWithID_Reserva(String id_reserva) {
		return database.getHoraFinalWithID_Reserva(id_reserva);
	}

	public String[] getFechaHorasWithID_Reserva(String id_reserva) {
		return database.getFechaHorasWithID_Reserva(id_reserva);
	}

	public boolean cambiarInstalacion(String id_reserva, String nuevaInst) {
		return database.cambiarInstalacion(id_reserva, nuevaInst);
	}

	public String borrarReservaCoindide(String actname, String currentInstallation, String fecha, int hI, int hF,
			String dni, String bookingCode, String currentActivities, String monitor, int plazas) {
		String type = "a";
		if (hF == 0)
			hF = 24;
		Activity newActivity = new Activity(currentInstallation, actname, fecha, hI, hF, type, currentActivities,
				monitor);
		return database.borrarReservaConflicto(newActivity, dni, bookingCode, plazas);
	}

	// Gonzi
	public void cancelarActividad(String idClase) {
		database.cancelarActividad(idClase);
	}

	public void cancelarClaseConcretaActividad(String idReserva) {
		database.cancelarClaseConcretaActividad(idReserva);
	}

	public List<String[]> obtenerAsistentesActividad(String idClase) {
		return database.obtenerAsistentesActividad(idClase);
	}

	public List<String[]> obtenerAsistentesClase(String idReserva) {
		return database.obtenerAsistentesClase(idReserva);
	}

	public void borrarActividadFromDatabase(String idClase) {
		database.borrarActividadFromDatabase(idClase);
	}//

	public ArrayList<HashMap<String, Object>> getActParaHorarioCentro(String inst) {
		return database.getActParaHorarioCentro(inst);
	}

	public String getIDreserva(String a, String ins, int dia, int mes, int anio, int horaI) {
		return database.getIDreserva(a, ins, dia, mes, anio, horaI);
	}

	// GonziYOLO
	public String getIDclase(String a, String ins, int dia, int mes, int anio, int horaI) {
		return database.getIDclase(a, ins, dia, mes, anio, horaI);
	}

	public void cambiarHoraInicio(int horaI, String id_reserva) {
		database.cambiarHoraInicio(horaI, id_reserva);
	}

	public ArrayList<HashMap<String, Object>> getActParaHorarioMonitor(String monitor) {

		return database.getActParaHorarioMonitor(monitor);
	}

	public boolean isClassIlimi(String id_clase) {
		return database.isClassIlimi(id_clase);
	}

	public boolean getNumAsistClass(String id_reserva) {
		return database.getNumAsistClass(id_reserva);
	}

	public HashMap<String, Object> getInfoBookingActCancel(String id_reserva) {
		return database.getInfoBookingActCancel(id_reserva);
	}

	public HashMap<String, Object> getInfoUsuario(String dni) {
		return database.getInfoUsuario(dni);
	}

	public int getHoraFinal(String instalacion, String fecha, int horaI) {
		return database.getHoraFinal(instalacion, fecha, horaI);
	}

	// Alvaro desapuntarSocio
	public ArrayList<String[]> getActivitiesSocio(String id_reserva) {
		return database.getActivitiesSocio(id_reserva);
	}

	public ArrayList<String> getIdReservaDesapuntarSocio(String dni) {
		return database.getIdReservaDesapuntarSocio(dni);
	}

	// Alvaro modificarPlazas
	public ArrayList<String[]> getAllActivitiesOfTheDayChoose(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date); // pasarlo al formato de fecha de bbd
		return database.getAllActivitiesOfTheDayChoose(fecha);
	}

	public List<String[]> getAllActivitiesOfTheDayChoose(Date date, String inst) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date); // pasarlo al formato de fecha de bbd
		return database.getAllActivitiesOfTheDayChoose(fecha, inst);
	}

	public boolean modificarPlazas(String id_reserva, String numeroModificar) {
		return database.modificarPlazas(id_reserva, numeroModificar);
	}

	public int getPlazasActividad(String id_reserva) {
		return database.getPlazasActividad(id_reserva);
	}

	public boolean seeActivitiesFecha(String id_reserva, String date) {
		return database.seeActivitiesFecha(id_reserva, date);
	}

	// Alvaro modificarHoras

	public boolean modificarHoras(String id_reserva, String nuevaHoraFinal) {
		return database.modificarHoras(id_reserva, nuevaHoraFinal);
	}

	public int getHoras(String id_reserva) {
		return database.getHoras(id_reserva);
	}

	public String getInstalacion(String id_reserva) {
		return database.getInstalacion(id_reserva);
	}

	public String getFechaInstalacion(String id_reserva) {
		return database.getFechaInstalacion(id_reserva);
	}

	public ArrayList<Integer> getHorasInstalacion(String instalacion, String fecha) {
		return database.getHorasInstalacion(instalacion, fecha);
	}

	public int getPlazasLibres(String id_reserva) {
		return database.getPlazasLibres(id_reserva);
	}

	// Alvaro modificarInstlacion
	public String getActividad(String id_reserva) {
		return database.getActividad(id_reserva);
	}

	public boolean getModInst(String nuevaInstalacion, String actividad, String fecha) {
		return database.getModInst(nuevaInstalacion, actividad, fecha);
	}
}
