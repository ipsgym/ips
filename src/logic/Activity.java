package logic;

public class Activity {
	
	private String instNombre;
	private String claseNombre;
	private String fecha;
	private int horaInit;
	private int horaFin;
	private String type;
	private String code;
	private String dniMonitor;
	
	public Activity(String in, String cn, String f, int h,int h2,String t,String c, String dniMonitor){
		this.instNombre = in;
		this.claseNombre = cn;
		this.fecha = f;
		this.horaInit = h;
		this.horaFin = h2;
		this.setType(t);
		this.setCode(c);
		this.dniMonitor=dniMonitor;
	}

	public String getmonitor() {
		return dniMonitor;
	}
	public String getInstNombre() {
		return instNombre;
	}

	public String getClaseNombre() {
		return claseNombre;
	}

	public String getFecha() {
		return fecha;
	}

	public int getHoraInit() {
		return horaInit;
	}

	public int getHoraFin() {
		return horaFin;
	}
	
	public String toString(){
		return getInstNombre() + " " + getClaseNombre()+ " " +getFecha()+ " " +getHoraInit()+ " " +getHoraFin();
	}
	
	public void print(){
		System.out.println(toString());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
