package logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Database {

	private static Connection conn;
	Calendar calendario = Calendar.getInstance();

	public Database() {
		connect();
	}

	private static void connect() {
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:DB\\database.db");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getInstallations() {

		ArrayList<String> lista = new ArrayList<String>();
		try {
			String sentence = "SELECT nombre_inst FROM instalacion ORDER BY nombre_inst DESC";

			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery(sentence);
			while (rs.next()) {
				lista.add(rs.getString("nombre_inst"));
			}
			rs.close();
			stat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;

	}

	public ArrayList<Activity> getScheduleInstallation(String inst, String fecha) {
		ArrayList<Activity> lista = new ArrayList<Activity>();

		try {
			String sentence = "SELECT nombre_inst,nombre_clase,fecha_usada,hora_usada_inicio,hora_usada_final,tipo,id_clase "
					+ "FROM reserva where nombre_inst = ? and fecha_usada = ? and cancelada=0";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, inst);
			psQuery.setString(2, fecha);
			ResultSet rs = psQuery.executeQuery();
			while (rs.next()) {
				int timeInit = rs.getInt(4);// hh
				int timeFin = rs.getInt(5);// hh
				lista.add(new Activity(rs.getString(1), rs.getString(2), rs.getString(3), timeInit, timeFin,
						rs.getString(6), rs.getString(7), ""));
				// nombre inst, nombre clase, fecha, hora inicio, hora final
			}
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

	public boolean checkCorrectUserPass(String user, String password) {
		boolean valid = false;
		try {
			String sentence = "SELECT * FROM usuarioOnline where nombre_usuarioOnline = ? and contrase�a = ?";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, user);
			psQuery.setString(2, password);
			ResultSet rs = psQuery.executeQuery();
			int count = 0;
			while (rs.next()) {
				count++;
			}
			if (count == 1)
				valid = true;
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return valid;
	}

	public User signInAdmin(String user, String password) {
		User aux = null;
		try {
			String sent = "SELECT * FROM usuarioOnline uo,usuario u,administrador a where uo.nombre_usuarioOnline = ? and uo.contrase�a = ? "
					+ " and ? = u.DNI and u.DNI=a.DNI";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, user);
			query.setString(2, password);
			query.setString(3, user.substring(5));
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				aux = new User(rs.getString("DNI"), rs.getString("nombre_usuario"), rs.getString("apellido_usuario"),
						rs.getString("telefono"), true, user, password, false);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public User signInMember(String user, String password) {
		User aux = null;
		try {
			String sent = "SELECT * FROM usuarioOnline uo,usuario u where nombre_usuarioOnline = ? and contrase�a = ? and"
					+ " uo.DNI=u.DNI";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, user);
			query.setString(2, password);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				aux = new User(rs.getString("DNI"), rs.getString("nombre_usuario"), rs.getString("apellido_usuario"),
						rs.getString("telefono"), false, user, password, false);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public ArrayList<String[]> getActivities() {
		ArrayList<String[]> lista = new ArrayList<String[]>();
		try {
			String sentence = "SELECT nombre_clase,id_clase FROM clase ORDER BY nombre_clase DESC";

			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery(sentence);
			while (rs.next()) {
				if (!rs.getString("nombre_clase").equals("Alquilado")) {
					String[] aux = new String[2];
					aux[0] = rs.getString("nombre_clase");
					aux[1] = rs.getString("id_clase");
					lista.add(aux);
				}
			}
			rs.close();
			stat.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	// Saul
	/**
	 * Method to insert new activity booking.
	 * 
	 * @param newBooking
	 * @param manager
	 * @return
	 */
	public boolean insertBooking(Activity newBooking, String dniUser, String id_reserva, int plazas) {
		boolean finish = false;

		try {
			String sentence = "INSERT INTO RESERVA VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sentence);
			ps.setString(1, newBooking.getInstNombre());
			ps.setString(2, newBooking.getClaseNombre());
			ps.setString(3, newBooking.getFecha());
			ps.setInt(4, newBooking.getHoraInit());
			ps.setInt(5, newBooking.getHoraFin());
			ps.setString(6, dniUser);
			ps.setString(7, id_reserva);
			ps.setString(8, newBooking.getType());
			// int plazas = getPlazasClase(newBooking.getCode());
			ps.setInt(9, plazas);
			// Cancelada
			ps.setInt(10, 0);
			ps.setString(11, newBooking.getCode());
			if (ps.executeUpdate() == 1)
				finish = true;
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finish;
	}

	/**
	 * Method to check if installation is free or occupied.
	 * 
	 * @param newBooking
	 * @return
	 */
	public boolean isInstallationFree(Activity newBooking) {
		try {
			ArrayList<int[]> horas = new ArrayList<int[]>();
			int[] newHour = { newBooking.getHoraInit(), newBooking.getHoraFin() };

			String sentence = "SELECT hora_usada_inicio horaI, hora_usada_final horaF FROM RESERVA "
					+ "WHERE fecha_usada=? AND nombre_inst=? and (cancelada=0 OR cancelada=2) " + "ORDER BY horaI ASC";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, newBooking.getFecha());
			pt.setString(2, newBooking.getInstNombre());
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				int[] aux = new int[2];
				aux[0] = rs.getInt("horaI");
				aux[1] = rs.getInt("horaF");
				horas.add(aux);
			}
			boolean result = checkHour(horas, newHour);
			rs.close();
			pt.close();
			return result;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private boolean checkHour(ArrayList<int[]> hoursOccupied, int[] newHours) {
		int hourSuax, hourFaux;
		for (int[] i : hoursOccupied) {
			hourSuax = i[0];
			hourFaux = i[1];
			for (int j = hourSuax; j < hourFaux; j++) {
				for (int k = newHours[0]; k < newHours[1]; k++) {
					if (k == j)
						return false;
				}
			}
		}
		return true;
	}

	// Gonzi
	/**
	 * Method to check if the user who is going to get the booking exists
	 * 
	 * @param userOnline
	 *            user
	 * @return
	 */
	public boolean isUserCorrect(String userOnline) {
		try {
			String sent = "SELECT Count(*) a FROM usuarioOnline WHERE nombre_usuarioOnline=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, userOnline);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("a");
			if (coin > 0) {
				return true;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	// Gonzi

	/**
	 * Method to check if the user already has a booking installation working at
	 * certain time
	 * 
	 * @param fecha_usada
	 * @param dni
	 * @param hora_usada_inicio
	 * @param hora_usada_final
	 * @return
	 */
	public boolean isUserValid(String fecha_usada, String dni, int hora_usada_inicio, int hora_usada_final) {
		try {
			String sent = "SELECT Count(*) a FROM RESERVA  WHERE fecha_usada=? "
					+ "AND dni=? AND ((hora_usada_inicio>=? AND hora_usada_inicio<=?) || "
					+ "(hora_usada_final>=? AND hora_usada_final<=?)) AND cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha_usada);
			query.setString(2, dni);
			query.setInt(3, hora_usada_inicio);
			query.setInt(4, hora_usada_final);
			query.setInt(5, hora_usada_inicio);
			query.setInt(6, hora_usada_final);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("a");
			if (coin == 0) {
				return true;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	// Alvaro
	/**
	 * Method to insert users
	 * 
	 * @param dni
	 * @param name
	 * @param phone
	 * @param surName
	 * @return
	 */
	public boolean InsertUser(String dni, String name, int phone, String surName) {
		boolean p = false;
		try {
			String sentence = "INSERT INTO USUARIO VALUES(?,?,?,?)";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, dni);
			pt.setString(2, name);
			pt.setInt(3, phone);
			pt.setString(4, surName);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	// SOCIO
	public int getPriceInstallationMember(String currentInstallation) {
		int dinero = 0;
		try {
			String sentence = "SELECT precioSocio FROM instalacion WHERE nombre_inst =? ";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, currentInstallation);
			ResultSet rs = pt.executeQuery();
			dinero = rs.getInt("precioSocio");
			rs.close();
			pt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dinero;
	}

	// NO SOCIO
	public int getPriceInstallationNotMember(String currentInstallation) {
		int dinero = 0;
		try {
			String sentence = "SELECT precioNoSocio FROM instalacion WHERE nombre_inst = ?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, currentInstallation);
			ResultSet rs = pt.executeQuery();
			dinero = rs.getInt("precioNoSocio");
			rs.close();
			pt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dinero;
	}

	public void setBookingPayed(String bookingCode, boolean b, boolean c) {
		try {
			String sentence = "INSERT INTO pago VALUES(?,?,?,'','','','')";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, bookingCode);
			pt.setBoolean(2, b);
			pt.setBoolean(3, c);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Saul
	public String checkifBookingExists(String installationName, String dni, int bookingHour, String fecha) {
		try {
			String sent = "SELECT COUNT(*) a, id_reserva FROM reserva  " + "WHERE dni=? and hora_usada_inicio=? and "
					+ "nombre_inst=? and fecha_usada=? and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			query.setString(3, installationName);
			query.setInt(2, bookingHour);
			query.setString(4, fecha);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("a");
			if (coin == 1) {
				return rs.getString("id_reserva");
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Saul
	public boolean bookingInput(String id_reserva, int hour) {
		boolean p = false;
		try {
			String sentence = "INSERT INTO registroES (id_reserva, horaEntrada) VALUES (?, ?)";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			pt.setInt(2, hour);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	// Saul
	public boolean bookingOutput(String id_reserva, int hour) {
		boolean p = false;
		try {
			String sentence = "UPDATE registroES SET horaSalida = ? WHERE id_reserva = ?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, hour);
			pt.setString(2, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	// Saul
	public boolean insertPay(String id_reserva, int pay) {
		boolean p = false;
		try {
			String sentence = "INSERT INTO pago (id_reserva, efectivo,bill1,bill2,bill3,bill4) VALUES (?, ?,'','','','')";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			pt.setInt(2, pay);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	// Saul
	public boolean isUserInput(String dni) {
		try {
			String sent = "select count(*) a from socio where dni=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("a");
			if (coin == 1) {
				return true;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	// Saul
	public boolean userPayInput(String id_reserva) {
		boolean p = false;
		try {
			String sentence = "UPDATE pago SET pagada = 1 WHERE efectivo=1 and id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	// Gonzi

	/**
	 * Method to use in the panel of installation state.
	 * 
	 * @param installationName
	 * @param fecha
	 * @param bookingHour
	 * @return
	 */
	public String checkifInstallationIsOccupied(String installationName, String fecha, int bookingHour) {
		try {

			String sent = "SELECT COUNT(*) a,DNI,hora_usada_inicio,hora_usada_final from reserva "
					+ "where nombre_inst=? and fecha_usada=? and cancelada=0 and (hora_usada_inicio<=? and hora_usada_final>=?);";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, installationName);
			query.setString(2, fecha);
			query.setInt(3, bookingHour);
			query.setInt(4, bookingHour);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("a");

			if (coin > 0) {
				return "INSTALACI�N OCUPADA\nDNI: " + rs.getString("DNI") + "\nHora de inicio: "
						+ rs.getInt("hora_usada_inicio") + "\nHora final: " + rs.getInt("hora_usada_final");
			} else if (coin == 0) {
				return "INSTALACI�N LIBRE";
			}

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] searchForMemeberDataDNI(String dni) {
		String[] data = new String[3];
		try {
			String sentence = "SELECT nombre_usuario,apellido_usuario,telefono FROM usuario " + "where DNI = ?";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, dni);
			ResultSet rs = psQuery.executeQuery();
			while (rs.next()) {
				data[0] = rs.getString("nombre_usuario");
				data[1] = rs.getString("apellido_usuario");
				data[2] = rs.getString("telefono");
			}
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}

	public String getBillFromDatabase(String currentInstallation, String fecha, int selectedRow) {
		String data1 = "";
		String data2 = "";
		String data3 = "";
		String data4 = "";
		boolean p = false;
		boolean ef = false;
		try {
			String sentence = "SELECT p.bill1,p.bill2,p.bill3,p.bill4,pagada,efectivo FROM reserva r,pago p where r.nombre_inst =? and r.hora_usada_inicio = ? "
					+ "and r.id_reserva = p.id_reserva and r.fecha_usada = ?";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, currentInstallation);
			psQuery.setInt(2, selectedRow);
			psQuery.setString(3, fecha);
			ResultSet rs = psQuery.executeQuery();
			while (rs.next()) {
				data1 = rs.getString("bill1");
				data2 = rs.getString("bill2");
				data3 = rs.getString("bill3");
				data4 = rs.getString("bill4");
				p = rs.getBoolean("pagada");
				ef = rs.getBoolean("efectivo");
			}
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (p && ef) {
			return data1 + data2 + data3 + data4 + "\nPAGADA";
		} else if (!p && ef) {
			return data1 + data2 + data3 + data4 + "\nNO PAGADA";
		} else if (!ef) {
			return data1 + data2 + data3 + data4;
		}
		return "";
	}

	/**
	 * Este metodo guarda la factura en la bbdd, y para ello, la divide en 4
	 * bloques(ya que si no la longitud del varchar no lo acepta en uno solo) al
	 * pasarle el parametro data, todo el txtpn se corto en saltos de linea, y
	 * por tanto tendremos coas como: IPS GYM FACTURA ------------ Nombre: Hola
	 * en las distintas posiciones del array, entonces se pasa a 4 primero, y
	 * luego se hace un update de cada vez para cada parte de la factura
	 * 
	 * @param data
	 *            el array con toda la factura
	 * @param bookingCode
	 */
	public void saveBillDatabase(String[] data, String bookingCode) {
		String[] aux = new String[4];
		for (int i = 0; i < 4; i++) {
			aux[i] = "";
		}
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			if (i >= 0 && i <= 2) {
				count = 0;
				aux[count] += data[i] + "\n";
			} else if (i >= 3 && i <= 5) {
				count = 1;
				aux[count] += data[i] + "\n";
			} else if (i >= 6 && i <= 9) {
				count = 2;
				aux[count] += data[i] + "\n";
			} else if (i >= 10) {
				count = 3;
				aux[count] += data[i] + "\n";
			}
		}
		try {
			String sentence = "UPDATE pago SET bill1=? WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, aux[0]);
			pt.setString(2, bookingCode);
			pt.executeUpdate();
			pt.close();

			sentence = "UPDATE pago SET bill2=? WHERE id_reserva=?";
			pt = conn.prepareStatement(sentence);
			pt.setString(1, aux[1]);
			pt.setString(2, bookingCode);
			pt.executeUpdate();
			pt.close();

			sentence = "UPDATE pago SET bill3=? WHERE id_reserva=?";
			pt = conn.prepareStatement(sentence);
			pt.setString(1, aux[2]);
			pt.setString(2, bookingCode);
			pt.executeUpdate();
			pt.close();

			sentence = "UPDATE pago SET bill4=? WHERE id_reserva=?";
			pt = conn.prepareStatement(sentence);
			pt.setString(1, aux[3]);
			pt.setString(2, bookingCode);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean doesUserHaveToPayCash(String id_reserva) {
		boolean p = false;
		try {
			String sentence = "select efectivo from pago WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			ResultSet rs = pt.executeQuery();
			p = rs.getBoolean("efectivo");
			rs.close();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	public String getBillFormBookingCode(String id_reserva) {
		String data1 = "";
		String data2 = "";
		String data3 = "";
		String data4 = "";
		try {
			String sentence = "SELECT bill1,bill2,bill3,bill4 FROM pago where id_reserva = ?";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, id_reserva);
			ResultSet rs = psQuery.executeQuery();
			while (rs.next()) {
				data1 = rs.getString("bill1");
				data2 = rs.getString("bill2");
				data3 = rs.getString("bill3");
				data4 = rs.getString("bill4");
			}
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return data1 + data2 + data3 + data4;
	}

	public int getMoneyToPay(String id_reserva) {
		int dinero = 0;
		int horaIn = 0;
		int horaFin = 0;
		int precio = 0;
		try {
			String sentence = "SELECT i.precioSocio,r.hora_usada_inicio,r.hora_usada_final FROM instalacion i,reserva r WHERE i.nombre_inst = r.nombre_inst "
					+ "and r.id_reserva = ?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			ResultSet rs = pt.executeQuery();
			precio = rs.getInt("precioSocio");
			horaIn = rs.getInt("hora_usada_inicio");
			horaFin = rs.getInt("hora_usada_final");
			rs.close();
			pt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dinero = precio * (horaFin - horaIn);
		return dinero;
	}

	public void createClass(String className, int pS, int pNS, boolean act, int numeroPlazas, String activityCode,
			String monitor) {
		try {
			String sentence = "INSERT INTO clase (nombre_clase, precioSocio,precioNoSocio,actividad,monitor,plazas,id_clase)"
					+ " VALUES (?,?,?,?,?,?,?)";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, className);
			pt.setInt(2, pS);
			pt.setInt(3, pNS);
			pt.setBoolean(4, act);
			pt.setString(5, monitor);
			pt.setInt(6, numeroPlazas);
			pt.setString(7, activityCode);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getDescripcionDeInstalcion(String inst) {
		String data1 = "";
		try {
			String sentence = "SELECT descripcion FROM instalacion where nombre_inst = ?";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, inst);
			ResultSet rs = psQuery.executeQuery();
			data1 = rs.getString("descripcion");
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return data1;
	}

	public ArrayList<String[]> getUserBooking(String DNI) {
		ArrayList<String[]> result = new ArrayList<String[]>();
		String nombreIns = "";
		String fecha = "";
		int horaIn = 0;
		int horaOut = 0;
		int pagado = 0;
		String id_reserva = "";

		try {
			String sentence = "SELECT nombre_inst, fecha_usada, hora_usada_inicio, hora_usada_final, pagada, reserva.id_reserva "
					+ "FROM reserva, pago " + "where reserva.id_reserva=pago.id_reserva and DNI=? and cancelada=0"
					+ " order by fecha_usada, hora_usada_inicio ASC";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, DNI);
			ResultSet rs = psQuery.executeQuery();
			while (rs.next()) {
				String[] datas = new String[6];
				nombreIns = rs.getString("nombre_inst");
				fecha = rs.getString("fecha_usada");
				horaIn = rs.getInt("hora_usada_inicio");
				horaOut = rs.getInt("hora_usada_final");
				pagado = rs.getInt("pagada");
				id_reserva = rs.getString("id_reserva");
				datas[0] = nombreIns;
				datas[1] = fecha;
				datas[2] = String.valueOf(horaIn);
				datas[3] = String.valueOf(horaOut);
				datas[4] = String.valueOf(pagado);
				datas[5] = id_reserva;
				result.add(datas);
			}
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public ArrayList<String[]> getUserActivities(String DNI) {
		ArrayList<String[]> result = new ArrayList<String[]>();
		String nombreIns = "";
		String nombreClase = "";
		String fecha = "";
		int horaIn = 0;
		int horaOut = 0;
		int asistencia = 0;
		String id_reserva = "";

		try {
			String sentence = "SELECT nombre_inst, nombre_clase, fecha_usada, hora_usada_inicio, hora_usada_final, asistencia,"
					+ " reserva.id_reserva FROM reserva, listaAsistentes where reserva.id_reserva=listaAsistentes.id_reserva and"
					+ " listaAsistentes.dni_socio=? and cancelada=0 order by fecha_usada, hora_usada_inicio ASC";
			PreparedStatement psQuery = conn.prepareStatement(sentence);
			psQuery.setString(1, DNI);
			ResultSet rs = psQuery.executeQuery();
			while (rs.next()) {
				String[] datas = new String[7];
				nombreIns = rs.getString("nombre_inst");
				nombreClase = rs.getString("nombre_clase");
				fecha = rs.getString("fecha_usada");
				horaIn = rs.getInt("hora_usada_inicio");
				horaOut = rs.getInt("hora_usada_final");
				asistencia = rs.getInt("asistencia");
				id_reserva = rs.getString("id_reserva");
				datas[0] = nombreIns;
				datas[1] = nombreClase;
				datas[2] = fecha;
				datas[3] = String.valueOf(horaIn);
				datas[4] = String.valueOf(horaOut);
				datas[5] = String.valueOf(asistencia);
				datas[6] = id_reserva;
				result.add(datas);
			}
			rs.close();
			psQuery.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public boolean deleteUserBooking(String id_reserva) {
		boolean p = false;
		try {
			String sentence = "DELETE FROM pago WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
			if (p) {
				p = false;
				String sentence2 = "DELETE FROM reserva WHERE id_reserva=?";
				PreparedStatement pt2 = conn.prepareStatement(sentence2);
				pt2.setString(1, id_reserva);
				if (pt2.executeUpdate() == 1)
					p = true;
				pt.close();
			}
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	// Gonzi

	public void deleteAdminBooking(String id_reserva) {
		try {
			String sentence = "DELETE FROM reserva WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getBookingToDelete(String installationName, String fecha, int bookingHour) {
		try {
			String sent = "Select DNI from reserva where nombre_inst=? "
					+ "and fecha_usada=? and cancelada=0 and (hora_usada_inicio<=? and hora_usada_final>=?)";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, installationName);
			query.setString(2, fecha);
			query.setInt(3, bookingHour);
			query.setInt(4, bookingHour);
			ResultSet rs = query.executeQuery();
			String coin = null;
			if (rs != null)
				coin = rs.getString("DNI");
			rs.close();
			query.close();
			if (coin != null) {
				rs.close();
				query.close();
				return coin;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	//
	public boolean getIfBookingToDeleteIsN(String installationName, String fecha, int bookingHour) {
		try {
			String sent = "Select count(DNI) a from reserva where nombre_inst=? "
					+ "and fecha_usada=? and cancelada=0 and tipo='n' and (hora_usada_inicio<=? and hora_usada_final>=?)";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, installationName);
			query.setString(2, fecha);
			query.setInt(3, bookingHour);
			query.setInt(4, bookingHour);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("a");
			if (coin != 0) {
				return true;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateCancelledBooking(int valor, String installationName, String fecha, int bookingHour) {
		boolean p = false;
		try {
			String sentence = "UPDATE reserva SET cancelada=? where nombre_inst=? and "
					+ "fecha_usada=? and (hora_usada_inicio<=? and hora_usada_final>=?)";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, valor);
			pt.setString(2, installationName);
			pt.setString(3, fecha);
			pt.setInt(4, bookingHour);
			pt.setInt(5, bookingHour);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	public boolean checkIfBookingIsCancelled(String installationName, String fecha, int bookingHour) {
		try {
			String sent = "Select count(*) a from reserva where nombre_inst=? "
					+ "and fecha_usada=? and (hora_usada_inicio<=? and hora_usada_final>=?) and cancelada=2";
			PreparedStatement pt = conn.prepareStatement(sent);
			pt.setString(1, installationName);
			pt.setString(2, fecha);
			pt.setInt(3, bookingHour);
			pt.setInt(4, bookingHour);
			ResultSet rs = pt.executeQuery();
			int coin = rs.getInt("a");
			if (coin == 1) {
				return true;
			}
			rs.close();
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return false;
	}

	/**
	 * M�todo que devuelve una lista con las diferentes actividades para un
	 * dia determinado.
	 * 
	 * @param fecha
	 * @return
	 */
	public ArrayList<String[]> getActivitiesLimit(String fecha) {
		ArrayList<String[]> lista = new ArrayList<String[]>();
		try {
			String sentence = "SELECT reserva.nombre_clase, reserva.id_reserva " + " FROM reserva "
					+ "WHERE reserva.fecha_usada=? and reserva.plazasLibres>=0 and cancelada=0 "
					+ " ORDER BY reserva.nombre_clase DESC";

			PreparedStatement stat = conn.prepareStatement(sentence);
			stat.setString(1, fecha);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {
				if (!rs.getString("nombre_clase").equals("Alquilado")) {// meter
																		// todos
																		// los
																		// nombres
																		// de
																		// las
																		// clases
																		// menos
																		// la de
																		// alquilado
					String[] str = new String[2];
					str[0] = rs.getString("nombre_clase");
					str[1] = rs.getString("id_reserva");
					lista.add(str);
				}
			}
			rs.close();
			stat.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	/**
	 * Actualiza las plazas libres en la tabla reserva e inserta una fila en la
	 * tabla listaAsistentes
	 * 
	 * @param id_reserva
	 * @param dni
	 * @return
	 */
	public boolean insertUserInListAssistants(String id_reserva, String dni) {
		boolean p = false;
		try {
			String sentence = "UPDATE reserva " + "SET plazasLibres = ? " + "WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, getPlazasLibresAct(id_reserva) - 1);
			pt.setString(2, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
			String sentence2 = "INSERT INTO listaAsistentes VALUES (?, ?,?)";
			PreparedStatement pt2 = conn.prepareStatement(sentence2);
			pt2.setString(1, id_reserva);
			pt2.setString(2, dni);
			pt2.setInt(3, -1);

			if (pt2.executeUpdate() == 1)
				p = true;
			pt2.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	// /**
	// * Inserta una fila en la listaEspera
	// * @param id_reserva
	// * @param dni
	// * @return
	// */
	// public boolean insertUserInListWait(String id_reserva, String dni){
	// boolean p=false;
	// try{
	// DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	// JCalendar calendar = new JCalendar();
	// String fecha = df.format(calendar .getDate());
	//
	// String sentence2="INSERT INTO listaEspera VALUES (?, ?,?,?)";
	// PreparedStatement pt2=conn.prepareStatement(sentence2);
	// pt2.setString(1,id_reserva);
	// pt2.setString(2,dni);
	// pt2.setString(3, fecha);
	// pt2.setInt(4, -1);
	// if(pt2.executeUpdate()==1)
	// p=true;
	// pt2.close();
	// }catch(SQLException e){
	// return false;
	// }
	// return p;
	// }

	/**
	 * M�todo para saber si el dni introducido es de un socio.
	 * 
	 * @param dni
	 * @return
	 */
	public boolean isDNIUser(String dni) {
		int coin = -1;
		try {

			String sent = "SELECT COUNT(*) a from socio where dni=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	/**
	 * M�todo para saber si un usuario est� ya apuntado a una determinada
	 * clase.
	 * 
	 * @param dni
	 * @param id_reserva
	 * @return
	 */
	public boolean isUserRegisterToThisActivity(String dni, String id_reserva) {
		int coin = -1;
		try {

			String sent = "SELECT COUNT(*) a from listaAsistentes "
					+ "where (listaAsistentes.dni_socio=? and listaAsistentes.id_reserva=?)";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			query.setString(2, id_reserva);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("a");
			rs.close();
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	/**
	 * M�todo para iniciar sesi�n con un monitor.
	 * 
	 * @param user
	 * @param password
	 * @return
	 */
	public User signInMonitor(String user, String password) {
		User aux = null;
		try {
			String sent = "SELECT * FROM usuarioOnline uo,usuario u,monitor a where uo.nombre_usuarioOnline = ? and uo.contrase�a = ? "
					+ " and ? = u.DNI and u.DNI=a.DNI";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, user);
			query.setString(2, password);
			query.setString(3, user.substring(7));
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				aux = new User(rs.getString("DNI"), rs.getString("nombre_usuario"), rs.getString("apellido_usuario"),
						rs.getString("telefono"), false, user, password, true);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	/**
	 * M�todo para que el monitor pueda ver la lista de asistentes que van a
	 * ir a una clase determinada.
	 * 
	 * @param id_reserva
	 * @param dni_monitor
	 * @return
	 */
	public ArrayList<String[]> seeAssistsList(String id_reserva, String dni_monitor) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());

		try {
			String sent = "select usuario.nombre_usuario nombre, usuario. apellido_usuario apellidos, usuario.DNI "
					+ "from listaAsistentes, reserva, usuario, clase "
					+ "where listaAsistentes.id_reserva=reserva.id_reserva and"
					+ " listaAsistentes.dni_socio=usuario.DNI and reserva.id_reserva= ? and clase.monitor=? and clase.id_clase=reserva.id_clase "
					+ "and fecha_usada>=? ";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			query.setString(2, dni_monitor);
			query.setString(3, fecha);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[4];
				str[0] = rs.getString("DNI");
				str[1] = rs.getString("nombre");
				str[2] = rs.getString("apellidos");
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * M�todo para mostrar al monitor las diferentes clase que tiene.
	 * 
	 * @param dni_monitor
	 * @return
	 */
	public ArrayList<String[]> seeActMonitor(String dni_monitor) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());

		try {
			String sent = "select id_reserva, nombre_inst, reserva.nombre_clase, fecha_usada, hora_usada_inicio, hora_usada_final "
					+ "from reserva, clase "
					+ "where clase.monitor=? and clase.id_clase=reserva.id_clase and fecha_usada>=? and plazasLibres>=0 and reserva.cancelada=0"
					+ " order by fecha_usada, hora_usada_inicio";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni_monitor);
			query.setString(2, fecha);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[6];
				str[0] = rs.getString("id_reserva");
				str[1] = rs.getString("nombre_inst");
				str[2] = rs.getString("nombre_clase");
				str[3] = rs.getString("fecha_usada");
				str[4] = String.valueOf(rs.getInt("hora_usada_inicio"));
				str[5] = String.valueOf(rs.getInt("hora_usada_final"));
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * M�todo para marcar en la tabla lista de asistentes las asistencias a
	 * una clase.
	 * 
	 * @param dni_socio
	 * @param id_reserva
	 * @param assist
	 * @return
	 */
	public boolean markAssists(String dni_socio, String id_reserva, int assist) {
		boolean p = false;
		try {
			String sentence = "UPDATE listaAsistentes SET asistencia = ? WHERE id_reserva = ? and dni_socio=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, assist);
			pt.setString(2, id_reserva);
			pt.setString(3, dni_socio);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();

			if (assist == 0) {
				String sentence2 = "UPDATE reserva set plazasLibres=plazasLibres-1 where id_reserva=?";
				PreparedStatement pt2 = conn.prepareStatement(sentence2);
				pt2.setString(1, id_reserva);
				pt2.close();
			}
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	/**
	 * M�todo para saber si ya se han apuntado las asistencias de una
	 * determinada clase.
	 * 
	 * @param id_reserva
	 * @return
	 */
	public boolean isValidSaveAssists(String id_reserva) {
		int coin = -1;
		try {

			String sent = "SELECT SUM(asistencia) a from listaAsistentes where id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	/**
	 * M�todo para saber si un socio ya esta apuntado a otra actividad a la
	 * misma hora y d�a.
	 */
	public boolean isUserInOtherActivity(String fecha, int hourI, String dni) {
		int coin = -1;
		try {

			String sent = "SELECT DISTINCT Count(dni_socio) a " + "from listaAsistentes "
					+ "where id_reserva IN (SELECT id_reserva " + "from reserva "
					+ "where fecha_usada=? and hora_usada_inicio=? and cancelada=0) and dni_socio=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setInt(2, hourI);
			query.setString(3, dni);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	public boolean isUserInOtherBooking(String fecha, int hourI, String dni) {
		int coin = -1;
		try {

			String sent = "SELECT DISTINCT Count(dni) a from reserva "
					+ " where id_reserva IN (SELECT id_reserva from reserva "
					+ " where fecha_usada=? and hora_usada_inicio=? and cancelada=0 and tipo=?) and dni=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setInt(2, hourI);
			query.setString(3, "m");
			query.setString(4, dni);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	// M�todos get Saul

	/**
	 * M�todo para coger la fecha de dado un id_reserva
	 */
	public String getBookingDate(String id_reserva) {
		String coin = "";
		try {
			String sent = "SELECT fecha_usada from reserva where id_reserva=?; ";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			coin = rs.getString("fecha_usada");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin;
	}

	/**
	 * M�todo para coger las plazas libres de una determinado clase.
	 * 
	 * @param id_reserva
	 * @return
	 */
	public int getPlazasLibresAct(String id_reserva) {
		int coin = -1;
		try {

			String sent = "SELECT plazasLibres " + "FROM reserva " + "WHERE id_reserva=?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("plazasLibres");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin;
	}

	/**
	 * M�todo para conseguir las plazas que tiene una determinada actividad.
	 * 
	 * @param nombreclase
	 * @return
	 */
	public int getPlazasClase(String nombreclase) {
		try {

			String sent = "SELECT plazas " + "FROM clase " + "where id_clase =?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, nombreclase);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("plazas");
			if (coin > 1) {
				return coin;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * M�todo para coger la hora de inicio de una clase con el id_reserva
	 * 
	 * @param id_reserva
	 * @return
	 */
	public int getBookingHourI(String id_reserva) {
		int coin = -1;
		try {
			String sent = "SELECT hora_usada_inicio from reserva where id_reserva=? ";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("hora_usada_inicio");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin;
	}

	/**
	 * M�todo para saber si un usuario ha ido ya a una clase.
	 * 
	 * @param id_reserva
	 * @param dni
	 * @return
	 */
	public boolean getAssistUser(String id_reserva, String dni) {
		int coin = -1;
		try {
			String sent = "SELECT asistencia from listaAsistentes where id_reserva=? and dni_socio=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			query.setString(2, dni);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("asistencia");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin == 1;
	}

	public User signInContable(String text, String aux) {
		User contable = null;
		try {
			String sent = "SELECT * FROM usuarioOnline uo,usuario u,contable a where uo.nombre_usuarioOnline = ? and uo.contrase�a = ? "
					+ " and ? = u.DNI and u.DNI=a.DNI";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, text);
			query.setString(2, aux);
			query.setString(3, text.substring(7));
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				contable = new User(rs.getString("DNI"), rs.getString("nombre_usuario"),
						rs.getString("apellido_usuario"), rs.getString("telefono"), false, text, aux, false, true);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return contable;
	}

	public ArrayList<String> getMembersWithCuotaForPay(int month) {
		ArrayList<String> list = new ArrayList<String>();
		boolean count = false;
		String m = String.valueOf(month);
		String pm = "";
		if (m.length() == 1) {
			m = "0" + m;
			pm = "0" + (month - 1);
		} else if (m.length() == 2 && month == 10) {
			pm = "09";
		} else {
			pm = String.valueOf(month - 1);
		}
		try {
			String sql = "select s.DNI,r.fecha_usada from socio s, usuario u, reserva r, pago p where s.DNI=u.DNI and u.DNI=r.DNI and r.id_reserva = p.id_reserva and p.efectivo = 0"
					+ " and p.pagada= 0 and r.cancelada = 0 and r.fecha_usada>=? and r.fecha_usada <=? "
					+ "and r.id_reserva in (select res.id_reserva from registroES res)";
			PreparedStatement stat = conn.prepareStatement(sql);
			stat.setString(1, "2017-" + pm + "-20");
			stat.setString(2, "2017-" + m + "-19");
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {
				if (!count) {
					list.add(rs.getString(1));
					count = true;
				} else {
					boolean repeat = false;
					for (int i = 0; i < list.size(); i++) {
						if (rs.getString(1).equals(list.get(i))) {
							repeat = true;
						}
					}
					if (!repeat) {
						list.add(rs.getString(1));
					}
				}

			}
			rs.close();
			stat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<Object[]> getReservasSinPagarCuotaSocio(String dni, int month) {
		ArrayList<Object[]> list = new ArrayList<Object[]>();
		String m = String.valueOf(month);
		String pm = "";
		if (m.length() == 1) {
			m = "0" + m;
			pm = "0" + (month - 1);
		} else if (m.length() == 2 && month == 10) {
			pm = "09";
		} else {
			pm = String.valueOf(month - 1);
		}
		try {
			// PRIMERO LAS RESERVAS DE ALQUILER (LO UNICO QUE HAY AHORA MISMO)
			String sent = "select DISTINCT r.id_reserva,r.fecha_usada,i.precioSocio as precioInstalacion, i.nombre_inst, r.hora_usada_inicio, r.hora_usada_final"
					+ " from socio s, usuario u, reserva r, pago p,clase c, instalacion i "
					+ "where ? = s.DNI and s.DNI = u.DNI and u.DNI=r.DNI and r.id_reserva = p.id_reserva and p.efectivo = 0 "
					+ "and p.pagada= 0 and r.nombre_clase = c.nombre_clase and r.nombre_inst = i.nombre_inst and c.nombre_clase = 'Alquilado' "
					+ "and r.fecha_usada>=? and r.fecha_usada <=? "
					+ "and r.id_reserva in (select res.id_reserva from registroES res)";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			query.setString(2, "2017-" + pm + "-20");
			query.setString(3, "2017-" + m + "-19");
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				Object[] str = new Object[6];
				str[0] = rs.getString("id_reserva");
				str[1] = rs.getString("fecha_usada");
				int cost = rs.getInt("precioInstalacion")
						* (rs.getInt("hora_usada_final") - rs.getInt("hora_usada_inicio"));
				str[2] = cost;
				str[3] = rs.getString("nombre_inst");
				str[4] = rs.getInt("hora_usada_inicio") + ":00";
				str[5] = rs.getInt("hora_usada_final") + ":00";
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public int getCuotaSocio(String dni) {
		int coin = 0;
		try {
			String sent = "select cuotaAPagar from socio where DNI = ?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("cuotaAPagar");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin;
	}

	public void updateCuotaSocio(String dni, int cuotaAPagar) {
		try {
			String sentence = "UPDATE socio SET cuotaAPagar = cuotaAPagar + ? WHERE DNI = ?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, cuotaAPagar);
			pt.setString(2, dni);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updatePagoReservaCuotaSocio(Object idreserva) {
		String reserva = String.valueOf(idreserva);
		try {
			String sentence = "UPDATE pago SET pagada = 1 WHERE id_reserva = ?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, reserva);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String[]> getAllActivitiesOfTheDay(String fecha) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		try {
			String sent = "select nombre_clase, nombre_inst, hora_usada_inicio, hora_usada_final from reserva where fecha_usada = ? and tipo = 'a' and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[4];
				str[0] = rs.getString("nombre_clase");
				str[1] = rs.getString("nombre_inst");
				str[2] = String.valueOf(rs.getInt("hora_usada_inicio")) + ":00";
				str[3] = String.valueOf(rs.getInt("hora_usada_final")) + ":00";
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<String[]> getAllActivitiesOfTheDay(String fecha, String inst) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		try {
			String sent = "select nombre_clase, nombre_inst, hora_usada_inicio, hora_usada_final from reserva where fecha_usada = ? and tipo = 'a' and nombre_inst = ? and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, inst);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[4];
				str[0] = rs.getString("nombre_clase");
				str[1] = rs.getString("nombre_inst");
				str[2] = String.valueOf(rs.getInt("hora_usada_inicio")) + ":00";
				str[3] = String.valueOf(rs.getInt("hora_usada_final")) + ":00";
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Actualiza las plazas libres en la tabla reserva y borra la fila
	 * correspondiente en la tabla listaAsistentes
	 * 
	 * @param id_reserva
	 * @param dni
	 * @return
	 */
	public boolean removeUserInListAssistants(String id_reserva, String dni) {
		boolean p = false;
		try {
			String sentence = "UPDATE reserva " + "SET plazasLibres = ? " + "WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, getPlazasLibresAct(id_reserva) + 1);
			pt.setString(2, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();

			String sentence2 = "DELETE FROM listaAsistentes WHERE id_reserva=? and dni_socio=?";
			PreparedStatement pt2 = conn.prepareStatement(sentence2);
			pt2.setString(1, id_reserva);
			pt2.setString(2, dni);

			if (pt2.executeUpdate() == 1)
				p = true;
			pt2.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	public void updateCuotaMesPasado(String dni) {
		try {
			String sentence = "UPDATE socio SET cuotaMesPasado = cuotaAPagar WHERE DNI = ?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, dni);
			pt.executeUpdate();
			pt.close();

			sentence = "UPDATE socio SET cuotaAPagar = cuota WHERE DNI = ?";
			pt = conn.prepareStatement(sentence);
			pt.setString(1, dni);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> getMembers() {
		ArrayList<String> list = new ArrayList<String>();
		try {
			String sent = "select DNI from socio";
			Statement query = conn.createStatement();
			ResultSet rs = query.executeQuery(sent);
			while (rs.next()) {
				list.add(rs.getString("DNI"));
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public int getCuotaMesPasadoSocio(String dni) {
		int coin = 0;
		try {
			String sent = "select cuotaMesPasado from socio where DNI = ?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("cuotaMesPasado");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin;
	}

	public void updateCuotaMesPasadoSocio(String dni, int cuotaAPagar) {
		String sentence = "UPDATE socio SET cuotaMesPasado = cuotaMesPasado + ? WHERE DNI = ?";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setInt(1, cuotaAPagar);
			pt.setString(2, dni);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public List<String> getConflictsWithNewActivityPeriodic(List<String> daysActivity, int horaInicio, int horaFin,
			String instalacion) {
		List<String> aux = new ArrayList<String>();
		for (int i = 0; i < daysActivity.size(); i++) {
			String fecha = daysActivity.get(i);
			try {
				String sent = "select id_reserva,hora_usada_inicio,hora_usada_final,tipo from reserva where nombre_inst = ? and fecha_usada = ?"
						+ " and (cancelada = 0 or cancelada=2)";
				PreparedStatement query = conn.prepareStatement(sent);
				query.setString(1, instalacion);
				query.setString(2, fecha);
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					String id = rs.getString("id_reserva");
					int hI = rs.getInt("hora_usada_inicio");
					int hF = rs.getInt("hora_usada_final");
					String tipo = rs.getString("tipo");
					if ((hI <= horaInicio && hF >= horaFin) || (hI >= horaInicio && hF <= horaFin)
							|| (hI >= horaInicio && hF > horaFin) || (hI <= horaInicio && hF < horaFin)) {
						// empieza antes y acaba mas tarde empieza mas tarde y
						// acaba antes empieza mas tarde y acaba despues empieza
						// antes u acaba antes
						if (tipo.equals("a")) {
							aux.add("-" + id);
						} else {
							aux.add(id);
						}
					}
				}
				rs.close();
				query.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return aux;
	}

	public void cancelBookingConflict(String id) {
		try {
			String sentence = "UPDATE reserva SET cancelada=2 where id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public User getMemberFromReserva(String id) {
		User aux = null;
		try {
			String sent = "SELECT u.DNI,u.nombre_usuario,u.apellido_usuario,u.telefono,r.fecha_usada,"
					+ "r.hora_usada_inicio,r.nombre_inst FROM usuario u,reserva r where r.id_reserva = ? and r.DNI = u.DNI";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				aux = new User(rs.getString("DNI"), rs.getString("nombre_usuario"), rs.getString("apellido_usuario"),
						rs.getString("telefono"), "\n\tInstalaci�n: " + rs.getString("nombre_inst") + "\n\tFecha: "
								+ rs.getString("fecha_usada") + "\n\t Hora inicio: " + rs.getInt("hora_usada_inicio"));
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public User getReservaGimnasio(String id) {
		User aux = null;
		try {
			String sent = "SELECT r.fecha_usada,r.hora_usada_inicio,r.nombre_inst,r.hora_usada_final,r.nombre_clase"
					+ " FROM usuario u,reserva r where r.id_reserva = ?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				aux = new User("Gimnasio", "\nInstalaci�n: " + rs.getString("nombre_inst"),
						"\nClase: " + rs.getString("nombre_clase"), "",
						"\n\tFecha: " + rs.getString("fecha_usada") + "\n\t Hora inicio: "
								+ rs.getInt("hora_usada_inicio") + "\n\t Hora final: " + rs.getInt("hora_usada_final"));
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	// -- Sprint 3 Saul

	public List<String> getDNIMonitores() {
		List<String> aux = new ArrayList<String>();
		try {
			String sentence = "select monitor.DNI, usuario.nombre_usuario, usuario.apellido_usuario from monitor, usuario where monitor.DNI=usuario.DNI";

			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery(sentence);
			while (rs.next()) {
				aux.add("<html><p>" + rs.getString("nombre_usuario") + " " + rs.getString("apellido_usuario") + "</p>"
						+ "<p>" + rs.getString("DNI") + "</p></html>");
			}
			rs.close();
			stat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getFechasHorasMonitorOcupado(String monitor) {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		try {
			String sent = "select hora_usada_inicio, hora_usada_final, fecha_usada  " + "from reserva "
					+ "where id_clase in ( select id_clase from clase where monitor=?) and reserva.cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, monitor);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				String fecha = rs.getString("fecha_usada");
				int horaI = rs.getInt("hora_usada_inicio");
				int horaF = rs.getInt("hora_usada_final");
				ocp.put("fecha", fecha);
				ocp.put("horaI", horaI);
				ocp.put("horaF", horaF);
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public void deleteclass(String id) {
		try {
			String sentence = "delete from clase where id_clase=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getDataAcitvityFromDatabase(String currentInstallation, String fecha, int selectedRow) {
		String aux = "";
		String after = "";
		String dni = "";
		try {
			String sent = "select r.nombre_clase,r.hora_usada_inicio, r.hora_usada_final,c.plazas,r.plazasLibres, c.monitor"
					+ " from reserva r, clase c where r.nombre_inst = ? and r.fecha_usada = ? and r.hora_usada_inicio <= ? "
					+ "and r.hora_usada_final >= ? and r.cancelada = 0" + " and r.id_clase = c.id_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, currentInstallation);
			query.setString(2, fecha);
			query.setInt(3, selectedRow);
			query.setInt(4, selectedRow);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				aux += "Instalaci�n: " + currentInstallation + "\nNombre clase: " + rs.getString("nombre_clase");
				int plazas = rs.getInt("plazas");
				aux += "\nPlazas totales: ";
				if (plazas == -1) {
					aux += "Ilimitadas";
				} else {
					aux += plazas + "\nPlazas libres: " + rs.getInt("plazasLibres");
				}
				dni = rs.getString("monitor");
				after += "\nFecha: " + fecha;
				after += "\nHora inicio: " + rs.getInt("hora_usada_inicio") + "\nHora final: "
						+ rs.getInt("hora_usada_final");
			}
			rs.close();
			query.close();

			String sql = "select nombre_usuario,apellido_usuario from usuario where dni = ?";
			PreparedStatement consul = conn.prepareStatement(sql);
			consul.setString(1, dni);
			rs = consul.executeQuery();
			while (rs.next()) {
				aux += "\nMonitor: " + rs.getString("nombre_usuario") + " " + rs.getString("apellido_usuario");
			}
			rs.close();
			consul.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux + after;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookings() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getBookingsOfActivity(String id_clase) {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select id_reserva, nombre_inst, fecha_usada from reserva where id_clase=? and fecha_usada>=? and hora_usada_inicio>=? and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(2, fecha);
			query.setString(1, id_clase);
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("id_reserva", rs.getString("id_reserva"));
				ocp.put("nombre_inst", rs.getString("nombre_inst"));
				ocp.put("fecha_usada", rs.getString("fecha_usada"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsPeriodicas() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and (select count(*) from reserva R where R.id_clase=reserva.id_clase)>1"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsPuntual() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and (select count(*) from reserva R where R.id_clase=reserva.id_clase)=1"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsIlimitadas() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and reserva.plazasLibres=-1" + " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsLimitadas() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and reserva.plazasLibres>=0" + " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsLimitadasPuntual() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and reserva.plazasLibres>=0 and (select count(*) from reserva R where R.id_clase=reserva.id_clase)=1"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsLimitadasPeriodica() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=?"
					+ "and reserva.plazasLibres>=0 and (select count(*) from reserva R where R.id_clase=reserva.id_clase)>1"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsIlimitadasPuntual() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and reserva.plazasLibres=-1 and (select count(*) from reserva R where R.id_clase=reserva.id_clase)=1"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsIlimitadasPeriodica() {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and hora_usada_inicio>=? "
					+ "and reserva.plazasLibres=-1 and (select count(*) from reserva R where R.id_clase=reserva.id_clase)>1"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getActivitiesWithFutureBookingsMonitorInfo(String monitor) {
		List<HashMap<String, Object>> aux = new ArrayList<HashMap<String, Object>>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "select DISTINCT clase.id_clase, clase.nombre_clase from clase, reserva "
					+ "where fecha_usada>=? and clase.id_clase=reserva.id_clase and reserva.tipo=? and clase.monitor=? "
					+ "and hora_usada_inicio>=?" + " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, "a");
			query.setString(3, monitor);
			query.setInt(4, hora);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre", rs.getString("nombre_clase"));
				ocp.put("id", rs.getString("id_clase"));
				aux.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public boolean isClassIlimi(String id_clase) {
		int coin = -1;
		try {

			String sent = "SELECT Count(*) b from reserva where tipo=? and plazasLibres=-1 and id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, "a");
			query.setString(2, id_clase);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("b");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	public boolean getNumAsistClass(String id_reserva) {
		int coin = -1;
		try {

			String sent = "SELECT COUNT(*) a from listaAsistentes where id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			coin = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coin > 0;
	}

	public int getNumClassMonitorInfo(String monitor) {
		int aux = 0;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "SELECT Count(*) a from reserva, clase where reserva.id_clase=clase.id_clase and monitor=? "
					+ "and fecha_usada>=? and hora_usada_inicio>=? " + "";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(2, fecha);
			query.setString(1, monitor);
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			aux = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public int getNumActMonitorInfo(String monitor) {
		int aux = 0;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(new Date());
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		try {
			String sent = "SELECT count(*) a " + "from clase "
					+ "where id_clase in (SELECT clase.id_clase a from reserva, clase where reserva.id_clase=clase.id_clase "
					+ "and monitor=? and fecha_usada>=? and hora_usada_inicio>=?)";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(2, fecha);
			query.setString(1, monitor);
			query.setInt(3, hora);
			ResultSet rs = query.executeQuery();
			aux = rs.getInt("a");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getFechaHoraIHoraF(String id_clase) {
		List<HashMap<String, Object>> ocps = new ArrayList<HashMap<String, Object>>();
		try {
			String sent = "select fecha_usada, hora_usada_inicio, hora_usada_final, monitor " + "from reserva, clase "
					+ "where clase.id_clase=reserva.id_clase and clase.id_clase=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_clase);

			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("fecha", rs.getString("fecha_usada"));
				ocp.put("horaI", rs.getInt("hora_usada_inicio"));
				ocp.put("horaF", rs.getInt("hora_usada_final"));
				ocp.put("monitor", rs.getString("monitor"));
				ocps.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ocps;
	}

	public boolean cambiarMonito(String id_clase, String monitor) {
		boolean p = false;
		try {
			String sentence = "UPDATE clase SET monitor=? WHERE id_clase=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(2, id_clase);
			pt.setString(1, monitor);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	public String getNombreApellidosMonitorDNI(String monitor) {
		String aux = "";
		try {
			String sent = "Select nombre_usuario, apellido_usuario from usuario where DNI =?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, monitor);
			ResultSet rs = query.executeQuery();
			String nombre = rs.getString("nombre_usuario");
			String apellido = rs.getString("apellido_usuario");
			aux = "" + nombre + " " + apellido + " - " + monitor + "";

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public String getNombreActIDClase(String id_clase) {
		String aux = "";
		try {
			String sent = "Select nombre_clase from clase where id_clase =?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_clase);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("nombre_clase");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public List<HashMap<String, Object>> getHoraInicioHoraFinInstalacion(String nombre, String fecha) {
		List<HashMap<String, Object>> ocps = new ArrayList<HashMap<String, Object>>();
		try {
			String sent = "select hora_usada_inicio, hora_usada_final, id_clase from reserva where fecha_usada=? and nombre_inst=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, nombre);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("horaI", rs.getInt("hora_usada_inicio"));
				ocp.put("horaF", rs.getInt("hora_usada_final"));
				ocp.put("id_clase", rs.getString("id_clase"));
				ocps.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ocps;
	}

	public ArrayList<String> getAllInst() {
		ArrayList<String> list = new ArrayList<String>();

		try {
			String sent = "select nombre_inst from instalacion";

			PreparedStatement query = conn.prepareStatement(sent);

			ResultSet rs = query.executeQuery();
			while (rs.next()) {

				list.add(rs.getString("nombre_inst"));
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public String getNombreInstWithID_Reserva(String id_reserva) {
		String aux = "";
		try {
			String sent = "select nombre_inst from reserva where id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("nombre_inst");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public String getHoraInicioWithID_Reserva(String id_reserva) {
		String aux = "";
		try {
			String sent = "select hora_usada_inicio from reserva where id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("hora_usada_inicio");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public String getHoraFinalWithID_Reserva(String id_reserva) {
		String aux = "";
		try {
			String sent = "select hora_usada_final from reserva where id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("hora_usada_final");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public String[] getFechaHorasWithID_Reserva(String id_reserva) {
		String[] aux = new String[5];
		try {
			String sent = "select fecha_usada, hora_usada_inicio, hora_usada_final, reserva.nombre_clase from reserva, clase "
					+ "where id_reserva=? and clase.id_clase=reserva.id_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			aux[0] = "Cambio de instalaci�n para actividad " + rs.getString("nombre_clase") + " ("
					+ rs.getString("fecha_usada") + " de " + rs.getInt("hora_usada_inicio") + "h a "
					+ rs.getInt("hora_usada_final") + "h)";
			aux[1] = rs.getString("fecha_usada");
			aux[2] = String.valueOf(rs.getInt("hora_usada_inicio"));
			aux[3] = String.valueOf(rs.getInt("hora_usada_final"));
			aux[4] = "Cambio de hora de inicio para actividad " + rs.getString("nombre_clase") + " ("
					+ rs.getString("fecha_usada") + " de " + rs.getInt("hora_usada_inicio") + "h a "
					+ rs.getInt("hora_usada_final") + "h)";
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public boolean cambiarInstalacion(String id_reserva, String nuevaInst) {
		boolean p = false;
		try {
			String sentence = "UPDATE reserva SET nombre_inst=? WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(2, id_reserva);
			pt.setString(1, nuevaInst);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	public String borrarReservaConflicto(Activity newActivity, String dni, String bookingCode, int plazas) {
		String aux = "";
		String id = "";
		try {
			String sentence = "SELECT r.id_reserva, r.hora_usada_inicio, r.hora_usada_final, r.nombre_clase, u.nombre_usuario,"
					+ "u.apellido_usuario,u.telefono FROM RESERVA r,usuario u "
					+ "WHERE r.fecha_usada=? AND r.nombre_inst=? and cancelada=0 and ((r.hora_usada_inicio > ? and "
					+ "r.hora_usada_inicio < ?) or (r.hora_usada_final > ? and r.hora_usada_final < ?)) and r.DNI = u.DNI";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, newActivity.getFecha());
			pt.setString(2, newActivity.getInstNombre());
			pt.setInt(3, newActivity.getHoraInit());
			pt.setInt(4, newActivity.getHoraFin());
			pt.setInt(5, newActivity.getHoraInit());
			pt.setInt(6, newActivity.getHoraFin());
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				id = rs.getString("id_reserva");
				String clase = rs.getString("nombre_clase");
				aux += "Instalaci�n:" + newActivity.getInstNombre() + "\n Reserva: " + clase + "\nFecha: "
						+ newActivity.getFecha() + "\nHora inicio: " + rs.getInt("hora_usada_inicio") + "\nHora final: "
						+ rs.getInt("hora_usada_final") + "\n";
				if (clase.equals("Alquilado")) {
					aux += "\tNombre usuario: " + rs.getString("nombre_usuario") + "\n\tApellidos usuario: "
							+ rs.getString("apellido_usuario") + "\n\tTel�fono: " + rs.getString("telefono") + "\n";
				}
			}
			rs.close();
			pt.close();

			String sql = "update reserva set cancelada = 2 where id_reserva=?";
			pt = conn.prepareStatement(sql);
			pt.setString(1, id);
			pt.executeUpdate();
			pt.close();
			String sent = "INSERT INTO RESERVA VALUES (?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sent);
			ps.setString(1, newActivity.getInstNombre());
			ps.setString(2, newActivity.getClaseNombre());
			ps.setString(3, newActivity.getFecha());
			ps.setInt(4, newActivity.getHoraInit());
			ps.setInt(5, newActivity.getHoraFin());
			ps.setString(6, dni);
			ps.setString(7, bookingCode);
			ps.setString(8, "a");
			ps.setInt(9, plazas);
			// Cancelada
			ps.setInt(10, 0);
			ps.setString(11, newActivity.getCode());
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aux;
	}

	// Gonzi

	public void cancelarActividad(String idClase) {
		String sentence = "update reserva set cancelada=1 where id_clase=?";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setString(1, idClase);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void cancelarClaseConcretaActividad(String idReserva) {
		String sentence = "update reserva set cancelada=1 where id_reserva=?";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setString(1, idReserva);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<String[]> obtenerAsistentesActividad(String idClase) {
		List<String[]> list = new ArrayList<String[]>();
		String sentence = "select distinct(usuario.nombre_usuario) as nombre,usuario.apellido_usuario as apellido,usuario.DNI as dni,usuario.telefono as telefono"
				+ " from listaAsistentes,reserva,usuario where listaAsistentes.id_reserva=reserva.id_reserva and "
				+ "reserva.id_clase=? and usuario.dni=listaAsistentes.dni_socio";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setString(1, idClase);
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				String[] str = new String[5];
				str[0] = rs.getString("nombre");
				str[1] = rs.getString("apellido");
				str[2] = rs.getString("dni");
				str[3] = rs.getString("telefono");
				list.add(str);
			}
			rs.close();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<String[]> obtenerAsistentesClase(String idReserva) {
		List<String[]> list = new ArrayList<String[]>();
		String sentence = "select distinct(usuario.nombre_usuario) as nombre,usuario.apellido_usuario as apellido,usuario.DNI as dni,usuario.telefono as telefono"
				+ " from listaAsistentes,reserva,usuario where listaAsistentes.id_reserva=reserva.id_reserva and "
				+ "reserva.id_reserva=? and usuario.dni=listaAsistentes.dni_socio";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setString(1, idReserva);
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				String[] str = new String[5];
				str[0] = rs.getString("nombre");
				str[1] = rs.getString("apellido");
				str[2] = rs.getString("dni");
				str[3] = rs.getString("telefono");
				list.add(str);
			}
			rs.close();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void borrarActividadFromDatabase(String idClase) {
		String sentence = "delete from clase where id_clase=?";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setString(1, idClase);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//

	public List<String> getConflictsWithNewActivityPuntual(String fecha, int horaInicio, int horaFin,
			String instalacion) {
		List<String> aux = new ArrayList<String>();
		try {
			String sent = "select id_reserva,hora_usada_inicio,hora_usada_final,tipo from reserva where nombre_inst = ? and fecha_usada = ?"
					+ " and (cancelada = 0 or cancelada=2)";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, instalacion);
			query.setString(2, fecha);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String id = rs.getString("id_reserva");
				int hI = rs.getInt("hora_usada_inicio");
				int hF = rs.getInt("hora_usada_final");
				String tipo = rs.getString("tipo");
				if ((hI <= horaInicio && hF >= horaFin) || (hI >= horaInicio && hF <= horaFin)
						|| (hI >= horaInicio && hF > horaFin) || (hI <= horaInicio && hF < horaFin)) {
					// empieza antes y acaba mas tarde empieza mas tarde y acaba
					// antes empieza mas tarde y acaba despues empieza antes u
					// acaba antes
					if (tipo.equals("a")) {
						aux.add("-" + id);
					} else {
						aux.add(id);
					}
				}
			}
			rs.close();
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public ArrayList<HashMap<String, Object>> getActParaHorarioCentro(String inst) {
		ArrayList<HashMap<String, Object>> clasesSemana = new ArrayList<HashMap<String, Object>>();
		try {
			String sent = "select nombre_inst,nombre_clase, fecha_usada, hora_usada_inicio, hora_usada_final from reserva where cancelada=0 and nombre_inst=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, inst);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("fecha", rs.getString("fecha_usada"));
				ocp.put("actividad", "<html><p>" + rs.getString("nombre_clase") + "</p><p>"
						+ rs.getString("nombre_inst") + "</p></html>");
				ocp.put("horas", rs.getInt("hora_usada_inicio") + "-" + rs.getInt("hora_usada_final"));
				clasesSemana.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clasesSemana;
	}

	public String getIDreserva(String a, String ins, int dia, int mes, int anio, int horaI) {
		String fecha = "";
		String aux = "";
		if (mes >= 10 && dia >= 10)
			fecha = anio + "-" + mes + "-" + dia;
		else if (dia < 10)
			fecha = anio + "-" + mes + "-0" + dia;
		else if (mes < 10)
			fecha = anio + "-0" + mes + "-" + dia;
		else
			fecha = anio + "-0" + mes + "-0" + dia;

		try {
			String sent = "select id_reserva from reserva where nombre_clase =? and nombre_inst=? and fecha_usada=? and hora_usada_inicio=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, a);
			query.setString(2, ins);
			query.setString(3, fecha);
			query.setInt(4, horaI);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("id_reserva");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	// GonziYOLO
	public String getIDclase(String a, String ins, int dia, int mes, int anio, int horaI) {
		String fecha = "";
		String aux = "";
		if (mes >= 10 && dia >= 10)
			fecha = anio + "-" + mes + "-" + dia;
		else if (dia < 10)
			fecha = anio + "-" + mes + "-0" + dia;
		else if (mes < 10)
			fecha = anio + "-0" + mes + "-" + dia;
		else
			fecha = anio + "-0" + mes + "-0" + dia;

		try {
			String sent = "select id_clase from reserva where nombre_clase =? and nombre_inst=? and fecha_usada=? and hora_usada_inicio=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, a);
			query.setString(2, ins);
			query.setString(3, fecha);
			query.setInt(4, horaI);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("id_clase");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public void cambiarHoraInicio(int horaI, String idReserva) {
		String sentence = "update reserva set hora_usada_inicio=? where id_reserva=?";
		PreparedStatement pt;
		try {
			pt = conn.prepareStatement(sentence);
			pt.setInt(1, horaI);
			pt.setString(2, idReserva);
			pt.executeUpdate();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getIDclaseFromIDreserva(String id_reserva) {
		String aux = "";

		try {
			String sent = "select id_clase from reserva where id_reserva =?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			aux = rs.getString("id_clase");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public ArrayList<HashMap<String, Object>> getActParaHorarioMonitor(String monitor) {
		ArrayList<HashMap<String, Object>> clasesSemana = new ArrayList<HashMap<String, Object>>();
		try {
			String sent = "select nombre_inst,reserva.nombre_clase, fecha_usada, hora_usada_inicio, hora_usada_final from reserva, clase where cancelada=0 "
					+ "and clase.id_clase=reserva.id_clase and clase.monitor=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, monitor);

			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("fecha", rs.getString("fecha_usada"));
				ocp.put("actividad", "<html><p>" + rs.getString("nombre_clase") + "</p><p>"
						+ rs.getString("nombre_inst") + "</p></html>");
				ocp.put("horas", rs.getInt("hora_usada_inicio") + "-" + rs.getInt("hora_usada_final"));
				clasesSemana.add(ocp);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clasesSemana;
	}

	public HashMap<String, Object> getInfoBookingActCancel(String id_reserva) {
		HashMap<String, Object> aux = new HashMap<String, Object>();
		try {
			String sent = "select reserva.nombre_inst, reserva.fecha_usada, reserva.hora_usada_inicio, reserva.hora_usada_final, clase.monitor, "
					+ "usuario.nombre_usuario, usuario.apellido_usuario, reserva.plazasLibres, clase.nombre_clase "
					+ "from reserva, clase, usuario "
					+ "where id_reserva=? and clase.id_clase=reserva.id_clase and clase.monitor=usuario.DNI"
					+ " order by clase.nombre_clase";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre_inst", rs.getString("nombre_inst"));
				ocp.put("fecha_usada", rs.getString("fecha_usada"));
				ocp.put("hora_usada_inicio", rs.getInt("hora_usada_inicio"));
				ocp.put("hora_usada_final", rs.getInt("hora_usada_final"));
				ocp.put("monitorDNI", rs.getString("monitor"));
				ocp.put("nombre_monitor", rs.getString("nombre_usuario"));
				ocp.put("apellido_monitor", rs.getString("apellido_usuario"));
				ocp.put("plazasLibres", rs.getInt("plazasLibres"));
				ocp.put("nombre_clase", rs.getString("nombre_clase"));
				aux = ocp;
			}

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public HashMap<String, Object> getInfoUsuario(String dni) {
		HashMap<String, Object> aux = new HashMap<String, Object>();
		try {
			String sent = "select nombre_usuario, apellido_usuario, telefono from usuario where dni=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				HashMap<String, Object> ocp = new HashMap<>();
				ocp.put("nombre_usuario", rs.getString("nombre_usuario"));
				ocp.put("apellido_usuario", rs.getString("apellido_usuario"));
				ocp.put("telefono", rs.getInt("telefono"));
				aux = ocp;
			}

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public int getHoraFinal(String instalacion, String fecha, int horaI) {
		int aux = 0;

		try {
			String sent = "select hora_usada_final from reserva where nombre_inst=? and fecha_usada=? and hora_usada_inicio=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, instalacion);
			query.setString(2, fecha);
			query.setInt(3, horaI);
			ResultSet rs = query.executeQuery();
			aux = rs.getInt("hora_usada_final");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public ArrayList<String> getIdReservaDesapuntarSocio(String dni) {
		ArrayList<String> lista = new ArrayList<String>();
		String id_reserva = "";
		try {
			String sent = "select id_reserva from listaAsistentes where dni_socio=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, dni);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				id_reserva = rs.getString("id_reserva");
				lista.add(id_reserva);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public ArrayList<String[]> getActivitiesSocio(String id_reserva) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		try {
			String sent = "select nombre_clase, id_reserva from reserva where id_reserva=? and tipo='a' and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[2];
				str[0] = rs.getString("nombre_clase");
				str[1] = rs.getString("id_reserva");
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	// Alvaro modificarPlazas
	public ArrayList<String[]> getAllActivitiesOfTheDayChoose(String fecha) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		try {
			String sent = "select nombre_clase, nombre_inst, hora_usada_inicio, hora_usada_final, id_reserva from reserva where fecha_usada = ? and tipo = 'a' and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[5];
				str[0] = rs.getString("nombre_clase");
				str[1] = rs.getString("nombre_inst");
				str[2] = String.valueOf(rs.getInt("hora_usada_inicio")) + ":00";
				str[3] = String.valueOf(rs.getInt("hora_usada_final")) + ":00";
				str[4] = rs.getString("id_reserva");
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<String[]> getAllActivitiesOfTheDayChoose(String fecha, String inst) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		try {
			String sent = "select nombre_clase, nombre_inst, hora_usada_inicio, hora_usada_final, id_reserva from reserva where fecha_usada = ? and tipo = 'a' and nombre_inst = ? and cancelada=0";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, fecha);
			query.setString(2, inst);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				String[] str = new String[5];
				str[0] = rs.getString("nombre_clase");
				str[1] = rs.getString("nombre_inst");
				str[2] = String.valueOf(rs.getInt("hora_usada_inicio")) + ":00";
				str[3] = String.valueOf(rs.getInt("hora_usada_final")) + ":00";
				str[4] = rs.getString("id_reserva");
				list.add(str);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean modificarPlazas(String id_reserva, String numeroModificar) {
		boolean p = false;
		try {
			String sentence = "UPDATE reserva " + "SET plazasLibres = plazasLibres + ? " + "WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, Integer.parseInt(numeroModificar));
			pt.setString(2, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	/**
	 * M�todo para conseguir las plazas que tiene una determinada actividad.
	 * 
	 * @param id_reserva
	 * @return
	 */

	public int getPlazasActividad(String id_reserva) {
		try {

			String sent = "SELECT plazasLibres " + "FROM reserva " + "where id_reserva =?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("plazasLibres");
			if (coin >= 0) {
				return coin;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * Compara fechas
	 * 
	 * @param id_reserva
	 * @param fecha
	 * @return
	 */
	public boolean seeActivitiesFecha(String id_reserva, String fecha) {
		boolean estado = false;
		try {
			String sentence = "select fecha_usada from reserva where id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setString(1, id_reserva);
			ResultSet rs = pt.executeQuery();
			while (rs.next()) {
				String date = rs.getString("fecha_usada");
				if (fecha.compareTo(date) <= 0)
					estado = true;
			}
			rs.close();
			pt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return estado;
	}

	// Alvaro ModificarHoras

	public boolean modificarHoras(String id_reserva, String nuevaHoraFinal) {
		boolean p = false;
		try {
			String sentence = "UPDATE reserva " + "SET hora_usada_final = ? " + "WHERE id_reserva=?";
			PreparedStatement pt = conn.prepareStatement(sentence);
			pt.setInt(1, Integer.parseInt(nuevaHoraFinal));
			pt.setString(2, id_reserva);
			if (pt.executeUpdate() == 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

	public int getHoras(String id_reserva) {
		try {

			String sent = "SELECT hora_usada_final " + "FROM reserva " + "where id_reserva =?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			int coin = rs.getInt("hora_usada_final");
			if (coin >= 0) {
				return coin;
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public String getInstalacion(String id_reserva) {
		String instalacion = "";
		try {
			String sent = "SELECT nombre_inst " + "FROM reserva " + "where id_reserva = ?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			instalacion = rs.getString("nombre_inst");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instalacion;

	}

	public String getFechaInstalacion(String id_reserva) {
		String fechaInstalacion = "";
		try {
			String sent = "SELECT fecha_usada " + "FROM reserva " + "where id_reserva = ?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			fechaInstalacion = rs.getString("fecha_usada");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return fechaInstalacion;

	}

	public ArrayList<Integer> getHorasInstalacion(String instalacion, String fecha) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		try {
			String sent = "SELECT hora_usada_final " + "FROM reserva " + "where nombre_inst=? and fecha_usada=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, instalacion);
			query.setString(2, fecha);
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				int hora = rs.getInt("hora_usada_final");
				list.add(hora);
			}
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public int getPlazasLibres(String id_reserva) {
		int aux = 0;

		try {
			String sent = "select plazasLibres from reserva where id_reserva=?";
			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			aux = rs.getInt("plazasLibres");

			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return aux;
	}

	// Alvaro modificarInstlacion

	public String getActividad(String id_reserva) {
		String instalacion = "";
		try {
			String sent = "SELECT nombre_clase " + "FROM reserva " + "where id_reserva = ?";

			PreparedStatement query = conn.prepareStatement(sent);
			query.setString(1, id_reserva);
			ResultSet rs = query.executeQuery();
			instalacion = rs.getString("nombre_clase");
			rs.close();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instalacion;
	}

	public boolean getModInst(String nuevaInstalacion, String actividad, String fecha) {
		boolean p = false;
		try {
			String sent = "UPDATE reserva " + "SET nombre_inst = ? " + "WHERE nombre_clase=? and fecha_usada>=?";

			PreparedStatement pt = conn.prepareStatement(sent);
			pt.setString(1, nuevaInstalacion);
			pt.setString(2, actividad);
			pt.setString(3, fecha);
			if (pt.executeUpdate() != 1)
				p = true;
			pt.close();
		} catch (SQLException e) {
			return false;
		}
		return p;
	}

}
