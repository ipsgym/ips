package igu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import logic.Activity;
import logic.Manager;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private Manager manager;
	protected String errorEmpty = "Hay campos vac�os ";
	protected String errorUser = "El usuario o contrase�a es inv�lido";
	private String currentInstallation = "";
	private String currentActivities = "";
	private String currentActivitiesName = "";
	private String currentBookingToDelete = "";
	private String currentActivityToDelete = "";
	private String currentClaseSeleccionadaParaPasarLista = "";
	private String currentClaseSeleccionadaParaCancelar = "";
	private String currentReservaSeleccionadaParaCancelarAct = "";
	private String currentInfoMonitorActividadSeleccionada = "";
	private String currentInfoMonitorClaseConcretaSeleccionada = "";

	private JScrollPane scrollPaneHorarioCentral;
	private JTable tableHorarioCentro;
	private JButton btnComprobarVerActividadesHorarioCentro;
	private JDateChooser dateChooserHorarioCentro;

	private Date inicialHorarioCentro;
	private Date finalHorarioCentro;
	ArrayList<int[]> semanaEnInts;
	public static String currentClaseSeleccionadaHorario = "";
	public static String currentClaseSeleccionadaHorarioIdClase = "";
	private Date diaFinalSemanaHorario;
	private Date diaInicialSemanaHorario;

	private JPanel pnNorteHorarioActCentro;
	private JLabel lbSemanaHorario;
	private JPanel pnBotonesHorarioCentro;
	private JButton btnSemanaSiguiente;
	private JButton btnSemanaAnterior;
	private JButton btnCambiar;

	private JButton btnVerHorarioMonitor;
	private JButton btnPasarListaDesdeHorario;
	private int isHorarioMonitoroAdm = -1; // 0-> Adm, 1-> Monitor
	private JPanel pnHorarioActividadesCentro;

	private int dineroDevolver = 0;
	private Date inicial;
	private Date finaL;
	private String currentActivitiesToUser = "";
	private boolean checkIfBookingIsOfUser = false;
	private Date dateInicial = new Date();
	protected CardLayout card;// pnBase cardlayout
	protected CardLayout cardInstallation; // pnInstallation cardlayoutt
	protected CardLayout cardPeriodica; // p cardlayoutt
	protected CardLayout cardCancelarCambiarActClass;

	protected int cuotaAPagar = 0;

	protected ButtonGroup groupAdminInstallation = new ButtonGroup();
	protected ButtonGroup groupMemberInstallation = new ButtonGroup();
	protected ButtonGroup groupRegisterInputOutput = new ButtonGroup();
	protected ButtonGroup groupCrearActividadPlazas = new ButtonGroup();
	protected ButtonGroup groupCrearActividadPeriodicidad = new ButtonGroup();
	protected ButtonGroup groupCancelarActividadFiltroPlazas = new ButtonGroup();
	protected ButtonGroup groupCancelarActividadFiltroPeriodicidad = new ButtonGroup();

	private boolean crearActividad = false;
	private boolean actividadPeriodica = false;

	private JPanel contentPane;
	private JPanel pnNorth;
	private JPanel pnBase;
	private JPanel pnCalendarView;
	private JCalendar calendar;
	private JButton btnTitle;
	private JPanel pnHomeView;
	private JButton btnGoToCalendar;
	private JPanel pnInstallationSelection;
	private JPanel pnAgenda;
	private JScrollPane scrllInstallationSlect;
	private JPanel pnInstallations;
	private JScrollPane scrollAgenda;
	private JTable tableAgenda;
	private JPanel pnCalendarSelect;
	private JPanel pnCalendarButtons;
	private JButton btnIdentificarse;
	private JPanel pnIdentificationView;
	private JLabel lblUsersignin;
	private JLabel lblPasswordsignin;
	private JTextField txtUsersignin;
	private JPasswordField pwdPasswordSignIn;
	private JLabel lblErrorsSignin;
	private JButton btnSignInView;
	private JButton btnReservar;
	private JPanel pnInstallationBookingView;
	private JPanel pnChooserInstallationBooking;
	private JRadioButton rdbtnChooserActivityInstallation;
	private JRadioButton rdbtnChooserMemberInstallation;
	private JRadioButton rdbtnChooserNotMemberInstal;
	private JButton btnSiguienteChooserInstallation;
	private JPanel pnNotMemberInstallationBooking;
	private JLabel lblNameNotMemberInstal;
	private JLabel lblSurnameNotMemberInstal;
	private JLabel lblDNINotMemberInstal;
	private JLabel lblTelephoneNotMemberInstal;
	private JTextField txtTelephoneNotMemberInstal;
	private JTextField txtDNINotMemberInstal;
	private JTextField txtSurnameNotMemberInstal;
	private JTextField txtNameNotMemberInstal;
	private JButton btnBackNotMemberInstal;
	private JButton btnNextNotMemberInstal;
	private JDateChooser dateChooserNotMember;
	private JLabel lblHoraNotMemberInstal;
	private JLabel lblDateNotMemberInstal;
	private JLabel lblDateMemberInstal;
	private JLabel lblHoraMemberInstal;
	private JDateChooser dateChooserMember;
	private JButton btnNextMemberInstal;
	private JButton btnBackMemberInstal;
	private JTextField txtDNIMemberInstal;
	private JPanel pnMemberInstallationBooking;
	private JLabel lblDNIMemberInstal;
	private JPanel pnActivityInstallationBooking;
	private JPanel pnActivitiesInstallation;
	private JScrollPane scrllActivities;
	private JPanel pnActivitiesContent;
	private JCalendar calendarActivities;
	private JLabel lblHoraActivityInstal;
	private JButton btnNextActivityInstal;
	private JLabel lblHourActivityEnd;
	private JLabel lbBookingInstallation;
	private JLabel lblHoraFin;
	private JPanel pnIdent;
	private JButton btnAdmin;
	private JButton btnUsuario;
	private JLabel lblHoraFinal;

	private JButton btnBackAdminInstal;
	private JLabel lblTipoDePago;
	private JRadioButton rdbtnPagoConMensualidad;
	private JRadioButton rdbtnPagoAlEntrar;
	private JComboBox<String> cbHourActivityInstallationI;
	private JComboBox<String> cbHourActivityInstallationF;
	private JComboBox<String> cbHourInstallationMemberS;
	private JComboBox<String> cbHourInstallationMemberF;
	private JComboBox<String> cbHourInstallationNotMemberI;
	private JComboBox<String> cbHourInstallationNotMemberF;
	private JButton btnRegistroDeEntradasalida;
	private JPanel pnRegisterInputOutput;
	private JLabel lblDniRegister;
	private JTextField txtDniRegister;
	private JLabel lblInstallationToInputOutput;
	private JRadioButton rdbtnEntrada;
	private JRadioButton rdbtnSalida;
	private JButton btnRegisterInputOuput;
	private JComboBox<String> cBInstallationToInputOutput;
	private JLabel lblHoraDeReserva;
	private JComboBox<String> cBHourBookingToInputOutput;
	private JLabel lblTipoDeRegistro;
	private JButton btnEstadoInstalaciones;
	private JPanel pnEstadoInstallation;
	private JLabel lblEstadoInstalacion;
	private JComboBox<String> cbEstadoInstalacion;
	private JTextPane txtDatosOcupante;
	private JButton btnBooking24h;

	private JPanel pnTicketDevolucion;

	private JPanel pnPayBye;

	private JPanel pnByeAlways;

	private JScrollPane scrollBill;

	private JTextPane txtpnByeMessage;

	private JTextPane txtpnAllBill;

	private JPanel pnByeSouth;

	private JButton btnEndPuchase;

	private JPanel pnSaveBill;

	private JButton btnSaveTheBill;
	private JButton btnVerFactura;
	private JButton btnDeleteBooking;
	private JButton btnComprobar;
	private JButton btnVerMisReservas;

	private JButton btnVerMisActividades;
	private JPanel pnBookingView;

	private JPanel pnVerMisActividades;

	private JPanel pnBookingSelection;
	private JScrollPane scrollPnBookingSelection;
	private JPanel pnBookings;

	private JPanel pnActivitiesSelection;
	private JScrollPane scrollPnActivitiesSelection;
	private JPanel pnActivities;

	private JLabel lblSusReservasSon;
	private JLabel lblNoHayReservas;
	private JLabel lblNoHay;
	private JButton btnCancelarReserva;
	private JButton btnBackHomeFromBookingView;

	private JButton btnCrearNuevaActividad;
	private JButton btnVerActividades;
	private JPanel pnCrearActividad;
	private JButton btnBackCrearActividad;
	private JLabel lblNombreActividad;
	private JLabel lblPlazas;
	private JRadioButton rdbtnIlimitadas;
	private JRadioButton rdbtnLimitadas;
	private JLabel lblNmeroDePlazas;
	private JTextField txtNumeroDePlazas;
	private JButton btnConfirmarCrearActividad;
	private JTextPane txtpnInformacionCrearActividad;
	private JTextField txtNombreAtividad;
	private JButton btnInformacinSobreLas;
	private JPanel pnInfoInstallations;
	private JPanel pnInfoInstalSelectView;

	private JScrollPane scrllInfoInstallationSlect;

	private JPanel pnInfoInstallationsSelect;
	private JLabel lblPhotoInstallation;
	private JButton btnBackInfoInstal;
	private JTextPane txtpnInfoInstallation;
	private JButton btnApuntarSocioActividad;
	private JPanel pnAddUserToActivity;
	private JCalendar calendarAddUserToActivity;
	private JPanel pnActivitiesForUsersSelection;
	private JScrollPane scrollPnActivitiesForUsers;
	private JPanel pnActivitiesForUsers;
	private JLabel lblDniDelSocio;
	private JTextField txtDNISocioApuntarAct;
	private JButton btnApuntarSocioAAct;
	private JLabel lblNoHayActividades;
	private JTextField txtInfPlazasAddUserToActivity;

	private JButton btnMonitor;
	private JButton btnVerListaAsistentes;
	private JPanel pnSeeAssistsList;
	private JPanel pnListAssistsSelection;
	private JScrollPane scrollPnListAssistsSelection;
	private JPanel pnListAssists;
	private JButton btnBackfromseeassistantslist;
	private JButton btnGuardarAsistencias;
	private JPanel pnAcciones;
	private JLabel lblIps;
	private JButton btnContable;

	private JButton btnTranferirACuota;
	private JPanel pnTransferirACuota;
	private JLabel lblDniSocioTransferir;
	private JComboBox<String> cmbBoxDNITranferir;
	private JButton btnMostrarPagosPendientes;
	private JPanel pnPagosATransferir;
	private JLabel lblCuotaFija;
	private JLabel lblCuotaFijaNumero;
	private JLabel lblCuotaAPagar;
	private JLabel lblCuotaAPagarNumero;
	private JButton btnPasarTodosLos;
	private JScrollPane scrollTranferir;
	private JPanel pnInfoTransferir;
	private JPanel pnActivitiesOfTheDayAdmin;
	private JCalendar calendarViewActivities;
	private JButton btnEnsearAvtividades;
	private JScrollPane scrollPnActivitiesView;
	private JPanel pnActivitiesViewInfo;
	private JButton btnBackActivitiesView;

	private JButton btnVerActividadesDeUnDia;
	private JLabel lblFechaActivitiesView;
	private JComboBox<String> cmbBoxInstActivitiesView;
	private JLabel lblVerActividadesDel;
	private JPanel pnVerActividades;
	private JButton btnAtrasVerActividades;
	private JPanel panel;
	private JPanel panelLunes;
	private JScrollPane scrollPnLunes;
	private JPanel panelMartes;
	private JScrollPane scrollPnMartes;
	private JPanel panelMiercoles;
	private JScrollPane scrollPnMiercoles;
	private JPanel panelJueves;
	private JScrollPane scrollPnJueves;
	private JPanel panelViernes;
	private JScrollPane scrollPnViernes;
	private JPanel panelSabado;
	private JScrollPane scrollPnSabado;
	private JPanel panelDomingo;
	private JScrollPane scrollPnDomingo;
	private JButton btnComprobarVerActividades;
	private JDateChooser dateChooser;
	private JLabel labelSemana;
	private JButton btnBackVerMisActividades;
	private JLabel labelActividadesApuntado;
	// Alvaro
	private JButton btnDesapuntarSocioActividad;
	private JPanel pnActivitesAlvaro;
	private JPanel pnRemoveUserToActivity;
	private JButton btnBackHomeForRemoveUserToActivity;
	private JCalendar calendarRemoveUserToActivity;
	private JButton btnComprobarActividadesRemoveUser;
	private JPanel pnActivitiesSelectionAlvaro;
	private JScrollPane scrollPnActivitiesSelectionAlvaro;
	private JTextField txtRemovePlazasUser;
	private JLabel lblRemoveUserToActivity;
	private JTextField txtApuntarDniSocio;
	private JButton btnDesapuntarSocio;
	private JComboBox<Integer> cmbBoxChooseMonthTransferir;
	private JLabel lblMes;
	private JRadioButton rdbtnPuntual;
	private JRadioButton rdbtnPeriodica;
	private JLabel lblPeriodicidad;
	private JPanel pnActividadPeriodicaCrear;
	private JLabel lblDasEnLos;
	private JPanel pnDatosAcitividadPeriodica;
	private JPanel pnInstalActividadPeriodicaAux;
	private JScrollPane scrollInstalActividadPeriodica;
	private JPanel pnInstalacionActividadPeriodica;
	private JPanel pnDiasParaActividadPeriodica;
	private JCheckBox chckbxLu;
	private JCheckBox chckbxMa;
	private JCheckBox chckbxMie;
	private JCheckBox chckbxJue;
	private JCheckBox chckbxVie;
	private JCheckBox chckbxSa;
	private JCheckBox chckbxDo;
	private JLabel lblDiaParaEmpezar;
	private JDateChooser dateActividadPeriodicaEmpezar;
	private JLabel lblNmeroDeSemanas;
	private JLabel lblHoraDeInicio;
	private JLabel lblHoraDeFinal;
	private JComboBox<String> cmbBoxHoraDeInicio;
	private JComboBox<String> cmbBoxHoraDeFinal;
	private JLabel lblActividad;
	private JLabel lblNombreActividadPeriodica;
	private JLabel lblNmeroDePlazasPeriodica;
	private JLabel lblPlazasActividadPeriodica;
	private JButton btnCrearActividad;
	private JPanel pnDataConflictsActivity;
	private JLabel lblUsuariosQueSufrieron;
	private JPanel pnUsuariosCancelacionesAux;
	private JButton btnFinalizarPeriodicas;
	private JScrollPane scrollUsuariosAvisar;
	private JPanel pnUsuariosAvisar;
	private JLabel lblMonitor;
	private JComboBox<String> cBMonitores;
	private JLabel lblMonitor_1;
	private JComboBox<String> cBMonitorPuntual;
	private JPanel pnListaClasesMonitor;
	private JScrollPane scrollPane;
	private JPanel pnListaClasesMonitorView;
	private JPanel pnCancelarActividad;
	private JPanel pnActividadesCanc;
	private JScrollPane scrollPane_1;
	private JPanel pnActividadesCancView;
	private JPanel pnFiltros;
	private JPanel pnFiltrosTipoPlazas;
	private JPanel pnFiltrosPeriodicidad;
	private JRadioButton rdbtnPlazasLimitadas;
	private JRadioButton rdbtnPlazasIlimitadas;
	private JRadioButton rdbtnActPuntual;
	private JRadioButton rdbtnActPeridica;
	private JPanel pnReservasCancelarAct;
	private JScrollPane scrollPane_2;
	private JPanel pnReservasCancelarActView;
	private JPanel pnInformacionReservaActCancelar;
	private JScrollPane scrollPane_3;
	private JTextArea txtInfoReservaConcreta;
	private JButton btnCancelarActividad;
	private JButton btnCancelarClaseConcreto;
	private JButton btnAtrs;
	private JButton btnCancelarActividad_1;
	private JPanel pnQuitarFiltros;
	private JButton btnQuitarFiltros;
	private JLabel lblNoHayActividades_1;
	private JPanel pnBotonesActConcreta;
	private JButton btnCambiarMonitorClase;
	private JPanel pnBotonesAct;
	private JScrollPane scrollPane_4;
	private JPanel pnBotenesActConcretaView;
	private JScrollPane scrollPane_5;
	private JPanel pnBotonesActView;

	private JPanel pnActMonitorInfo;
	private JPanel pnInformacionGeneralActClassMonitorInfo;
	private JScrollPane scrollPaneActMonitorInf;
	private JPanel pnActMonitorInfView;
	private JPanel pnClasesConcretasInfoMonitor;
	private JScrollPane scrollPaneClasesConcretasInfoMonitor;
	private JPanel pnClasesConcretasMonitorView;

	private JPanel pnInfClaseConcretaInfoMonitor;
	private JButton btnIrAPasar;

	private JPanel pnListaAsistClaseConcretaInfoMonitorView;
	private JScrollPane scrollPaneInfClaseConcretaInfoMonitor;
	private JTextArea txtInfoClassMonitorInfo;
	private JPanel pnInfoClaseConcretaGeneral;
	private JLabel lblTieneXActividades;
	private JLabel lblUnTotalDe;
	private JPanel pnCambiosActANDClass;
	private JPanel pnCambioMonitorNuevo;
	private JLabel lblMonitorAnterior;
	private JTextField txtNombreMonitor;
	private JLabel lblMonitorNuevo;
	private JComboBox<String> cBNuevoMonitorCambio;
	private JButton btnCambiarMonitor;
	private JLabel lblNewLabel;
	private JButton btnCambiarInstalacin;
	private JPanel pnCambioInstalacionNuevaClaseConcreta;
	private JLabel lblInstalacinAnterior;
	private JLabel lblInstalacinNueva;
	private JTextField txtInstalacionAnterior;
	private JComboBox<String> cBNuevasInstalacionesAEscoger;
	private JButton btnCambiarInstalacin_1;
	private JLabel lblCambioDeInstalacin;

	private JPanel pnActivitiesViewActivities;
	private JButton btnDisminuirPlazas;
	private JButton btnAumentarPlazas;
	private JLabel lblPlazasAModificar;
	private JTextField txModificarPlazas;
	private JPanel pnModificar;
	private JPanel pnAvisarAsistentes;
	private JLabel lblAvisarALos;
	private JPanel pnAvisarAsis;
	private JTextArea txtAreaAvisarAsistentes;
	private JButton btnVolverAvisarAsistentes;
	private JButton btnCancelarPeriodicaConflicto;
	private JDateChooser dateActividadPeriodicaAcabar;
	private JButton btnVerHorarioAct;

	protected String currentBookingCodePuntual;

	protected String currentMessagePuntual;
	private JButton btnAtrasDesdeHorarioCentro;
	private JPanel pnCenter;
	private JPanel pnCambiosDesdeHorario;
	private JButton btnVolverAHorario;
	private JPanel pnCambios;
	private JScrollPane scrollPaneCambiosClaseConcreta;
	private JPanel pnContenedorDeTablaCrearActividad;
	private JScrollPane scrollParaTablaCrearActividadPuntual;
	private JTable tableAgendaActividades;

	protected boolean creandoActividadPuntual;
	private JTextArea lblLaClaseSeleccionada;
	private JPanel pnCambioHoraInicio;
	private JLabel lblCambioHoraInicio;
	private JLabel lblHoraInicioAnterior;
	private JTextField txtHoraInicioAnterior;
	private JComboBox<String> cbNuevaHoraInicio;
	private JButton btnCambiarHoraInicio;
	private JLabel lblPrueba;

	private boolean createdCombo;
	private JLabel lblTicketDeDevolucion;
	private JTextPane txtPaneTicketDevolucion;
	private JButton btnFinalizarTicketDevolucion;

	private JPanel pnModificarPlazas;
	private JLabel lblModificarPlazas;
	private JComboBox<String> cbxPlazas;
	private JButton btnModificarPlazas;
	private JLabel lblPlazasActuales;
	private JTextField tfAumentarPlazas;

	private JPanel pnModificarHoras;
	private JLabel lblHoraFinalAnterior;
	private JButton btnCambiarHora;
	private JLabel lblNuevaHoraFinal;
	private JComboBox<String> cbxHoraFinal;
	private JTextField tfHoraActual;
	private JComboBox<String> cBInstalacionesAfiltrar;

	private JPanel pnInstalacionActividad;
	private JTextField tfActividadEscogida;
	private JComboBox<String> cbxNuevaInstalacion;
	private JLabel lblNuevaInstalacion;
	private JLabel lblActividadEscogida;
	private JButton btnCambiarInstalacionDe;
	private JLabel lblInstalacionActual;
	private JTextField tfInstalacionActual;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		manager = getManager();
		setTitle("IPS GYM");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/img/Title.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 762, 620);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPnNorth(), BorderLayout.NORTH);
		contentPane.add(getPnBase(), BorderLayout.CENTER);
		card = ((CardLayout) pnBase.getLayout());
		cardInstallation = ((CardLayout) pnInstallationBookingView.getLayout());
		cardPeriodica = ((CardLayout) pnActividadPeriodicaCrear.getLayout());
		cardCancelarCambiarActClass = ((CardLayout) pnCambiosActANDClass.getLayout());
	}

	private Manager getManager() {
		if (manager != null)
			return manager;
		manager = new Manager();
		return manager;
	}

	/**
	 * This method adapts the image we want to the JButton specified
	 * 
	 * @param button
	 *            the button in which we want to put the image
	 * @param imagePath
	 *            the resource path to the image
	 */
	private void setAdaptedImageButton(JButton button, String imagePath) {
		this.setVisible(true);
		Image originalImg = new ImageIcon(getClass().getResource(imagePath)).getImage();
		Image scaledImg = originalImg.getScaledInstance(button.getWidth(), button.getHeight(), Image.SCALE_FAST);
		ImageIcon icon = new ImageIcon(scaledImg);
		button.setIcon(icon);
		button.setDisabledIcon(icon);
	}

	/**
	 * This method adapts the image we want to the JLabel specified
	 * 
	 * @param label
	 *            the label in which we want to put the image
	 * @param imagePath
	 *            the resource path to the image
	 */
	protected void setAdaptedImageLabel(JLabel label, String imagePath) {
		this.setVisible(true);
		Image originalImg = new ImageIcon(getClass().getResource(imagePath)).getImage();
		Image scaledImg = originalImg.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_FAST);
		ImageIcon icon = new ImageIcon(scaledImg);
		label.setIcon(icon);
		label.setDisabledIcon(icon);
	}

	// NORTH PANEL

	private JPanel getPnNorth() {
		if (pnNorth == null) {
			pnNorth = new JPanel();
			pnNorth.setBackground(contentPane.getBackground());
			pnNorth.setLayout(new BorderLayout(0, 0));
			pnNorth.add(getBtnTitle(), BorderLayout.WEST);
			pnNorth.add(getPnIdent(), BorderLayout.EAST);
		}
		return pnNorth;
	}

	private JButton getBtnTitle() {
		if (btnTitle == null) {
			btnTitle = new JButton("");
			btnTitle.setMnemonic('H');
			btnTitle.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
					pnInstallations.removeAll();
					resetAreasSignIn();
					txtpnAllBill.setText("");
					cardInstallation.show(pnInstallationBookingView, "pnChooser");
					resetFieldMember();
					// clearCancelarActi();
				}
			});
			btnTitle.setBounds(0, 0, 200, 80);
			btnTitle.setBorder(null);
			btnTitle.setBackground(contentPane.getBackground());
			setAdaptedImageButton(btnTitle, "/img/Title.jpg");
		}
		return btnTitle;
	}

	// BASE PANEL

	private JPanel getPnBase() {
		if (pnBase == null) {
			pnBase = new JPanel();
			pnBase.setBackground(contentPane.getBackground());
			pnBase.setLayout(new CardLayout(0, 0));
			pnBase.add(getPnHomeView(), "pnHomeView");
			pnBase.add(getPnCalendarView(), "pnCalendarView");
			pnBase.add(getPnIdentificationView(), "pnIdentificationView");
			pnBase.add(getPnInstallationBookingView(), "pnInstallationView");
			pnBase.add(getPnRegisterInputOutput(), "pnRegister");
			pnBase.add(getPnPayBye(), "pnBill");
			// Gonzi
			pnBase.add(getPnTicketDevolucion(), "pnTicketDevolucion");
			pnBase.add(getPnEstadoInstallation(), "pnEstado");
			pnBase.add(getPnBookingView(), "pnViewBooking");
			//
			pnBase.add(getPnVerMisActividades(), "pnVerMisActividades");
			pnBase.add(getPnCrearActividad(), "pnCrearActividad");
			pnBase.add(getPnVerActividades(), "pnVerActividades");
			pnBase.add(getPnInfoInstallations(), "pnInfoInstallations");
			pnBase.add(getPnAddUserToActivity(), "pnAddUserToActivity");
			pnBase.add(getPnSeeAssistsList(), "pnSeeAssistantsList");
			pnBase.add(getPnTransferirACuota(), "pnTransferirACuota");
			pnBase.add(getPnActivitiesOfTheDayAdmin(), "pnVerActividadesDeUnDia");
			// Alvaro
			pnBase.add(getPnRemoveUserToActivity(), "pnRemoveUserToActivity");
			pnBase.add(getPnActividadPeriodicaCrear(), "pnActividadPeriodica");
			// pnBase.add(getPnCancelarActividad(), "pnCancelarActividad");
			pnBase.add(getPnCambiosActANDClass(), "pnCambiosCancelarActClass");
			pnBase.add(getPnHorarioActividadesCentro(), "pnHorarioActividadesCentro");
			pnBase.add(getPnCambiosDesdeHorario(), "pnModificacionesActClase");

		}
		return pnBase;
	}

	private JPanel getPnHomeView() {
		if (pnHomeView == null) {
			pnHomeView = new JPanel();
			pnHomeView.setBackground(contentPane.getBackground());
			GridBagLayout gbl_pnHomeView = new GridBagLayout();
			gbl_pnHomeView.columnWidths = new int[] { 30, 215, 30, 172, 40, 202, 40, 0 };
			gbl_pnHomeView.rowHeights = new int[] { 40, 40, 40, 40, 40, 40, 40, 40, 40, 0, 40, 0 };
			gbl_pnHomeView.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnHomeView.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnHomeView.setLayout(gbl_pnHomeView);
			GridBagConstraints gbc_pnAcciones = new GridBagConstraints();
			gbc_pnAcciones.gridwidth = 3;
			gbc_pnAcciones.gridheight = 9;
			gbc_pnAcciones.insets = new Insets(0, 0, 5, 5);
			gbc_pnAcciones.fill = GridBagConstraints.BOTH;
			gbc_pnAcciones.gridx = 1;
			gbc_pnAcciones.gridy = 1;
			pnHomeView.add(getPnAcciones(), gbc_pnAcciones);
			GridBagConstraints gbc_lblIps = new GridBagConstraints();
			gbc_lblIps.gridheight = 9;
			gbc_lblIps.gridwidth = 2;
			gbc_lblIps.insets = new Insets(0, 0, 5, 5);
			gbc_lblIps.gridx = 4;
			gbc_lblIps.gridy = 1;
			pnHomeView.add(getLblIps(), gbc_lblIps);
		}
		return pnHomeView;
	}

	private JButton getBtnGoToCalendar() {
		if (btnGoToCalendar == null) {
			btnGoToCalendar = new JButton("Horario de las instalaciones");
			btnGoToCalendar.setBackground(Color.WHITE);
			btnGoToCalendar.setEnabled(false);
			btnGoToCalendar.setVisible(true);
			btnGoToCalendar.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnGoToCalendar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					creandoActividadPuntual = false;
					pnInstallations.removeAll();
					crearActividad = false;
					currentInstallation = "";
					card.show(pnBase, "pnCalendarView");
					showAllInstallations(pnInstallations);
					btnReservar.setEnabled(false);
					btnVerFactura.setVisible(false);
					btnDeleteBooking.setVisible(false);
				}
			});
		}
		return btnGoToCalendar;
	}

	private JPanel getPnCalendarView() {
		if (pnCalendarView == null) {
			pnCalendarView = new JPanel();
			pnCalendarView.setBackground(contentPane.getBackground());
			pnCalendarView.setLayout(new BorderLayout(0, 0));
			pnCalendarView.add(getPnCalendarSelect(), BorderLayout.CENTER);
			pnCalendarView.add(getPnCalendarButtons(), BorderLayout.SOUTH);
		}
		return pnCalendarView;
	}

	private JPanel getPnCalendarSelect() {
		if (pnCalendarSelect == null) {
			pnCalendarSelect = new JPanel();
			pnCalendarSelect.setBackground(pnCalendarView.getBackground());
			pnCalendarSelect.setLayout(new GridLayout(1, 0, 0, 0));
			pnCalendarSelect.add(getCalendar());
			pnCalendarSelect.add(getPnInstallationSelection());
			pnCalendarSelect.add(getPnAgenda());
		}
		return pnCalendarSelect;
	}

	private JPanel getPnInstallationSelection() {
		if (pnInstallationSelection == null) {
			pnInstallationSelection = new JPanel();
			pnInstallationSelection.setBackground(contentPane.getBackground());
			pnInstallationSelection.setLayout(new BorderLayout(0, 0));
			pnInstallationSelection.add(getScrllInstallationSelect());
		}
		return pnInstallationSelection;
	}

	private JScrollPane getScrllInstallationSelect() {
		if (scrllInstallationSlect == null) {
			scrllInstallationSlect = new JScrollPane();
			scrllInstallationSlect.setBackground(pnInstallationSelection.getBackground());
			scrllInstallationSlect.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrllInstallationSlect.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrllInstallationSlect.setViewportView(getPnInstallations());
		}
		return scrllInstallationSlect;
	}

	private JPanel getPnInstallations() {
		if (pnInstallations == null) {
			pnInstallations = new JPanel();
			pnInstallations.setBackground(pnInstallationSelection.getBackground());
			pnInstallations.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnInstallations.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnInstallations;
	}

	/**
	 * This method shows all the installations and creates all the buttons
	 */
	protected void showAllInstallations(JPanel panel) {
		ArrayList<String> installations = manager.getInstallations();
		for (String act : installations) {
			showInstallationUnique(act, panel);
		}
	}

	/**
	 * This method shows a unique activity
	 * 
	 * @param inst
	 *            the installation we want to show
	 */
	protected void showInstallationUnique(String inst, JPanel panel) {
		JButton boton = new JButton();
		boton.setText(inst);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(inst);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boton.setBackground(new Color(135, 206, 250));
				currentInstallation = boton.getActionCommand();
				lbBookingInstallation.setText("Ha seleccionado la instalaci�n: " + currentInstallation); // Saul
				Component[] comp = panel.getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}
				btnReservar.setEnabled(true);
				btnVerFactura.setVisible(false);
				btnDeleteBooking.setVisible(false);
				if (creandoActividadPuntual) {
					showScheduleInstActividadPuntual(boton.getActionCommand());
				} else {
					showScheduleInstallation(boton.getActionCommand());
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(pnInstallations.getBackground());
		panel.add(boton);
	}

	private JCalendar getCalendar() {
		if (calendar == null) {
			calendar = new JCalendar();
			calendar.getDayChooser().addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent arg0) {
					if (currentInstallation != "")
						showScheduleInstallation(currentInstallation);

				}
			});

		}
		return calendar;
	}

	private JPanel getPnAgenda() {
		if (pnAgenda == null) {
			pnAgenda = new JPanel();
			pnAgenda.setBackground(pnCalendarView.getBackground());
			pnAgenda.setLayout(new BorderLayout(0, 0));
			pnAgenda.add(getScrollAgenda(), BorderLayout.CENTER);
		}
		return pnAgenda;
	}

	private JScrollPane getScrollAgenda() {
		if (scrollAgenda == null) {
			scrollAgenda = new JScrollPane();
			scrollAgenda.setBackground(pnAgenda.getBackground());
			scrollAgenda.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollAgenda.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		}
		return scrollAgenda;
	}

	private JTable getTableAgenda() {
		if (tableAgenda == null) {
			tableAgenda = new JTable();
			tableAgenda.setVisible(true);
			tableAgenda.setOpaque(true);
		}
		return tableAgenda;
	}

	/**
	 * This method shows in the table, all the schedule of the installation
	 * 
	 * @param boton
	 *            the installation clicked
	 */
	protected void showScheduleInstallation(String boton) {
		ArrayList<Activity> list = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(calendar.getDate());

		ArrayList<Activity> auxlist = manager.getScheduleInstallation(boton, fecha);

		String[] columns = { "Hora", "      " };
		Object[][] data = new Object[24][2];

		if (auxlist.size() > 0) {
			for (Activity a : auxlist) {
				list.add(a);
			}
			// putting data into the table when it has any kind of activity
			// scheduled
			for (int i = 0; i < 24; i++) {
				data[i][0] = i;
				for (int j = 0; j < list.size(); j++) {
					Activity ac = list.get(j);
					if (ac.getHoraInit() <= i && ac.getHoraFin() > i) {
						if (ac.getType().compareTo("a") == 0) { // admin
							data[i][1] = ">" + ac.getClaseNombre();
						} else if (manager.checkIfBookingIsCancelled(currentInstallation, fecha, i)) {// cancelada
							data[i][1] = "*" + "Cancelada";
						} else if (ac.getType().compareTo("n") == 0) {// not
																		// member
							data[i][1] = "-" + ac.getClaseNombre();
						} else if (ac.getType().compareTo("m") == 0) {// member
							data[i][1] = "+" + ac.getClaseNombre();
						}
						j = list.size() - 1;
					} else {
						data[i][1] = "";
					}
				}
			}
		} else if (auxlist.size() == 0) {
			// putting data into the table when it is empty
			for (int i = 0; i < 24; i++) {
				data[i][0] = i;
				data[i][1] = new String("");
			}
		}

		DefaultTableModel model = new DefaultTableModel(data, columns) {

			private static final long serialVersionUID = 1L;

			@Override
			public Class<? extends Object> getColumnClass(int column) {
				return getValueAt(0, column).getClass();
			}
		};

		tableAgenda = new JTable(model) {
			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				if (!isRowSelected(row)) {
					if (tableAgenda.getColumnCount() >= 0) {
						String type = (String) getModel().getValueAt(row, 1);
						// colores en funcion del tipo de actividad que haya en
						// la instalacion y dependiendo del usuario que este
						// conectado, si es admin o si es un member
						if (manager.currentUserIsAdmin()) {// cuando el loggeado
															// es un
															// administrador
															// verde para sus
															// cosas, naranja
															// para las reservas
															// de usuario
							if (type.equals("")) {
								c.setBackground(Color.white);
							} else if (type.substring(0, 1).compareTo(">") == 0) {// el
																					// ultimo
																					// espacio
																					// sololo
																					// tienen
																					// las
																					// cosas
																					// del
																					// gimnasio
								c.setBackground(Color.yellow);
							} else if (type.substring(0, 1).compareTo("+") == 0) {
								c.setBackground(Color.lightGray);// si es de
																	// socio
							} else if (type.substring(0, 1).compareTo("-") == 0) {// si
																					// es
																					// no
																					// socio
								c.setBackground(SystemColor.activeCaption);
							} else if (type.substring(0, 1).compareTo("*") == 0) { // si
																					// es
																					// cancelada
								c.setBackground(Color.red);
							}

						} else {// cuando el loggeado es un usuario verde para
								// sus cosas, y naranja para los del centro
							if (type.equals("")) {
								c.setBackground(Color.white);
							} else if (type.substring(0, 1).compareTo(">") == 0) {
								c.setBackground(Color.lightGray);
							} else if (type.substring(0, 1).compareTo("+") == 0) {
								c.setBackground(Color.yellow);// si es de socio
							} else if (type.substring(0, 1).compareTo("-") == 0) {// si
																					// es
																					// no
																					// socio
								c.setBackground(Color.lightGray);
							} else if (type.substring(0, 1).compareTo("*") == 0) { // si
																					// es
																					// cancelada
								c.setBackground(Color.red);
							}
						}
					}
				}
				return c;
			}
		};
		// centrar el texto de dentro de la tabla
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		tableAgenda.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		tableAgenda.setDefaultRenderer(String.class, centerRenderer);

		tableAgenda.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent event) {
				if (tableAgenda.getSelectedRow() > -1) {
					if (tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().compareTo("") == 0) {
						btnVerFactura.setVisible(false);
						btnDeleteBooking.setVisible(false);
					} else if ((tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().substring(0, 1)
							.compareTo("+") == 0
							|| tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().substring(0, 1)
									.compareTo("-") == 0)
							&& manager.currentUserIsAdmin()) {// si es de socio
																// o de no socio
																// y estamos en
																// la ventana
																// administrador
						btnVerFactura.setVisible(true);
						btnDeleteBooking.setVisible(true);
						setChechIfBookingIsOfUser(true);
					} else if ((tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().substring(0, 1)
							.compareTo(">") == 0) && manager.currentUserIsAdmin()) {// si
																					// es
																					// de
																					// gimnasio
																					// y
																					// eres
																					// administrador
						btnVerFactura.setVisible(true);
						btnDeleteBooking.setVisible(true);
						setChechIfBookingIsOfUser(false);
					} else if ((tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().substring(0, 1)
							.compareTo("*") == 0) && manager.currentUserIsAdmin()) {
						btnVerFactura.setVisible(false);
						btnDeleteBooking.setVisible(false);
					} else if (tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().substring(0, 1)
							.compareTo("+") == 0 && !manager.currentUserIsAdmin()) {
						btnVerFactura.setVisible(true);
						btnDeleteBooking.setVisible(false);
					}
				}
			}
		});

		// a�adir al scroll pane la tabla
		scrollAgenda.setViewportView(getTableAgenda());

	}

	private JPanel getPnCalendarButtons() {
		if (pnCalendarButtons == null) {
			pnCalendarButtons = new JPanel();
			pnCalendarButtons.setBackground(pnCalendarView.getBackground());
			GridBagLayout gbl_pnCalendarButtons = new GridBagLayout();
			gbl_pnCalendarButtons.columnWidths = new int[] { 101, 144, 107, 134, 137, 108, 0 };
			gbl_pnCalendarButtons.rowHeights = new int[] { 25, 0 };
			gbl_pnCalendarButtons.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnCalendarButtons.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
			pnCalendarButtons.setLayout(gbl_pnCalendarButtons);
			GridBagConstraints gbc_btnVerFactura = new GridBagConstraints();
			gbc_btnVerFactura.anchor = GridBagConstraints.NORTHWEST;
			gbc_btnVerFactura.insets = new Insets(0, 0, 0, 5);
			gbc_btnVerFactura.gridx = 0;
			gbc_btnVerFactura.gridy = 0;
			pnCalendarButtons.add(getBtnVerFactura(), gbc_btnVerFactura);
			GridBagConstraints gbc_btnDeleteBooking = new GridBagConstraints();
			gbc_btnDeleteBooking.fill = GridBagConstraints.BOTH;
			gbc_btnDeleteBooking.insets = new Insets(0, 0, 0, 5);
			gbc_btnDeleteBooking.gridx = 1;
			gbc_btnDeleteBooking.gridy = 0;
			pnCalendarButtons.add(getBtnDeleteBooking(), gbc_btnDeleteBooking);
			GridBagConstraints gbc_btnReservar = new GridBagConstraints();
			gbc_btnReservar.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnReservar.anchor = GridBagConstraints.NORTH;
			gbc_btnReservar.gridx = 5;
			gbc_btnReservar.gridy = 0;
			pnCalendarButtons.add(getBtnReservar(), gbc_btnReservar);
		}
		return pnCalendarButtons;
	}

	private JPanel getPnIdentificationView() {
		if (pnIdentificationView == null) {
			pnIdentificationView = new JPanel();
			pnIdentificationView.setBackground(pnBase.getBackground());
			GridBagLayout gbl_pnSignInView = new GridBagLayout();
			gbl_pnSignInView.columnWidths = new int[] { 10, 180, 128, 157, 114, 188, 0 };
			gbl_pnSignInView.rowHeights = new int[] { 0, 33, 33, 33, 33, 33, 0, 0, 0 };
			gbl_pnSignInView.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnSignInView.rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnIdentificationView.setLayout(gbl_pnSignInView);
			GridBagConstraints gbc_lblUsersignin = new GridBagConstraints();
			gbc_lblUsersignin.fill = GridBagConstraints.VERTICAL;
			gbc_lblUsersignin.anchor = GridBagConstraints.EAST;
			gbc_lblUsersignin.insets = new Insets(0, 0, 5, 5);
			gbc_lblUsersignin.gridx = 2;
			gbc_lblUsersignin.gridy = 2;
			pnIdentificationView.add(getLblUsersignin(), gbc_lblUsersignin);
			GridBagConstraints gbc_txtUsersignin = new GridBagConstraints();
			gbc_txtUsersignin.fill = GridBagConstraints.BOTH;
			gbc_txtUsersignin.insets = new Insets(0, 0, 5, 5);
			gbc_txtUsersignin.gridx = 3;
			gbc_txtUsersignin.gridy = 2;
			pnIdentificationView.add(getTxtUsersignin(), gbc_txtUsersignin);
			GridBagConstraints gbc_lblPasswordsignin = new GridBagConstraints();
			gbc_lblPasswordsignin.fill = GridBagConstraints.VERTICAL;
			gbc_lblPasswordsignin.anchor = GridBagConstraints.EAST;
			gbc_lblPasswordsignin.insets = new Insets(0, 0, 5, 5);
			gbc_lblPasswordsignin.gridx = 2;
			gbc_lblPasswordsignin.gridy = 3;
			pnIdentificationView.add(getLblPasswordsignin(), gbc_lblPasswordsignin);
			GridBagConstraints gbc_pwdPasswordSignIn = new GridBagConstraints();
			gbc_pwdPasswordSignIn.fill = GridBagConstraints.BOTH;
			gbc_pwdPasswordSignIn.insets = new Insets(0, 0, 5, 5);
			gbc_pwdPasswordSignIn.gridx = 3;
			gbc_pwdPasswordSignIn.gridy = 3;
			pnIdentificationView.add(getPwdPasswordSignIn(), gbc_pwdPasswordSignIn);
			GridBagConstraints gbc_lblErrorsSignin = new GridBagConstraints();
			gbc_lblErrorsSignin.insets = new Insets(0, 0, 5, 5);
			gbc_lblErrorsSignin.gridx = 3;
			gbc_lblErrorsSignin.gridy = 4;
			pnIdentificationView.add(getLblErrorsSignin(), gbc_lblErrorsSignin);
			GridBagConstraints gbc_btnSignInView = new GridBagConstraints();
			gbc_btnSignInView.insets = new Insets(0, 0, 5, 5);
			gbc_btnSignInView.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnSignInView.gridx = 3;
			gbc_btnSignInView.gridy = 5;
			pnIdentificationView.add(getBtnSignInView(), gbc_btnSignInView);
		}
		return pnIdentificationView;
	}

	private JLabel getLblUsersignin() {
		if (lblUsersignin == null) {
			lblUsersignin = new JLabel();
			lblUsersignin.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblUsersignin.setText("Usuario:");
		}
		return lblUsersignin;
	}

	private JLabel getLblPasswordsignin() {
		if (lblPasswordsignin == null) {
			lblPasswordsignin = new JLabel();
			lblPasswordsignin.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblPasswordsignin.setText("Contrase\u00F1a:");
		}
		return lblPasswordsignin;
	}

	private JTextField getTxtUsersignin() {
		if (txtUsersignin == null) {
			txtUsersignin = new JTextField();
			txtUsersignin.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtUsersignin.setBorder(new LineBorder(Color.LIGHT_GRAY));
			txtUsersignin.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					Integer i = e.getKeyCode();
					if (i.equals(KeyEvent.VK_ENTER))
						signIn();
				}

				@Override
				public void keyTyped(KeyEvent e) {
					char c = e.getKeyChar();
					if (Character.isWhitespace(c))
						e.consume();
				}
			});
			txtUsersignin.setColumns(10);
		}
		return txtUsersignin;
	}

	private JPasswordField getPwdPasswordSignIn() {
		if (pwdPasswordSignIn == null) {
			pwdPasswordSignIn = new JPasswordField();
			pwdPasswordSignIn.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			pwdPasswordSignIn.setBorder(new LineBorder(Color.LIGHT_GRAY));
			pwdPasswordSignIn.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					Integer i = e.getKeyCode();
					if (i.equals(KeyEvent.VK_ENTER))
						signIn();
				}
			});
		}
		return pwdPasswordSignIn;
	}

	private JButton getBtnSignInView() {
		if (btnSignInView == null) {
			btnSignInView = new JButton();
			btnSignInView.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnSignInView.setText("Iniciar Sesi\u00F3n");
			btnSignInView.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					signIn();
				}
			});
		}
		return btnSignInView;
	}

	/**
	 * This method is used to sign in to the sell point
	 */
	protected void signIn() {
		if (txtUsersignin.getText() == null || pwdPasswordSignIn.getPassword().length == 0) {
			lblErrorsSignin.setText(errorEmpty);
			txtUsersignin.setBorder(new LineBorder(Color.RED));
			pwdPasswordSignIn.setBorder(new LineBorder(Color.RED));
		} else if (manager.signIn(txtUsersignin.getText(), pwdPasswordSignIn.getPassword())) {
			card.show(pnBase, "pnHomeView");
			resetAreasSignIn();
			btnIdentificarse.setText("Salir");
		} else {
			txtUsersignin.setBorder(new LineBorder(Color.RED));
			pwdPasswordSignIn.setBorder(new LineBorder(Color.RED));
			lblErrorsSignin.setText(errorUser);
		}

	}

	/**
	 * This method resets all the default values of the text areas
	 */
	protected void resetAreasSignIn() {
		txtUsersignin.setText("");
		pwdPasswordSignIn.setText("");
		txtUsersignin.setBorder(new LineBorder(Color.LIGHT_GRAY));
		pwdPasswordSignIn.setBorder(new LineBorder(Color.LIGHT_GRAY));
		lblErrorsSignin.setText("");
	}

	private JLabel getLblErrorsSignin() {
		if (lblErrorsSignin == null) {
			lblErrorsSignin = new JLabel("");
			lblErrorsSignin.setForeground(Color.RED);
			lblErrorsSignin.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblErrorsSignin;
	}

	private JButton getBtnReservar() {
		if (btnReservar == null) {
			btnReservar = new JButton("Reservar");
			btnReservar.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnReservar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnBase, "pnInstallationView");
					btnBackAdminInstal.setVisible(true);
					if (manager.currentUserIsAdmin()) {
						rdbtnChooserNotMemberInstal.setVisible(true);
						rdbtnChooserMemberInstallation.setText("Reservar instalaci�n para un socio");// Saul
					} else {
						rdbtnChooserNotMemberInstal.setVisible(false);
						rdbtnChooserMemberInstallation.setText("Reservar instalaci�n");// Saul
					}
					Date today = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					if (Integer.parseInt(sdf.format(today).substring(6, 8)) <= (Integer
							.parseInt(sdf.format(calendar.getDate()).substring(6, 8)))) {
						dateChooserMember.setDate(calendar.getDate());
						dateChooserNotMember.setDate(calendar.getDate());
						calendarActivities.setDate(calendar.getDate());
					} else {
						dateChooserMember.setDate(today);
						dateChooserNotMember.setDate(today);
						calendarActivities.setDate(today);
					}
				}
			});
		}
		return btnReservar;
	}

	// PANEL VISTA DE LAS ACCIONES DEL INSTALACIONES

	private JPanel getPnInstallationBookingView() {
		if (pnInstallationBookingView == null) {
			pnInstallationBookingView = new JPanel();
			pnInstallationBookingView.setBackground(pnBase.getBackground());
			pnInstallationBookingView.setLayout(new CardLayout(0, 0));
			pnInstallationBookingView.add(getPnChooserInstallationBooking(), "pnChooser");
			pnInstallationBookingView.add(getPnNotMemberInstallationBooking(), "pnNotMember");
			pnInstallationBookingView.add(getPnMemberInstallationBooking(), "pnMember");
			pnInstallationBookingView.add(getPnActivityInstallationBooking(), "pnActivity");
		}
		return pnInstallationBookingView;
	}

	// Saul
	private JPanel getPnChooserInstallationBooking() {
		if (pnChooserInstallationBooking == null) {
			pnChooserInstallationBooking = new JPanel();
			pnChooserInstallationBooking.setBackground(pnInstallationBookingView.getBackground());
			groupAdminInstallation.add(getRdbtnAdminactivityinstallation());
			groupAdminInstallation.add(getRdbtnAdminmemberinstallation());
			groupAdminInstallation.add(getRdbtnAdminNotMemberInstallation());
			GridBagLayout gbl_pnChooserInstallationBooking = new GridBagLayout();
			gbl_pnChooserInstallationBooking.columnWidths = new int[] { 163, 0, 334, 0, 0, 0 };
			gbl_pnChooserInstallationBooking.rowHeights = new int[] { 67, 23, 23, 23, 46, 34, 0, 0 };
			gbl_pnChooserInstallationBooking.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnChooserInstallationBooking.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnChooserInstallationBooking.setLayout(gbl_pnChooserInstallationBooking);
			GridBagConstraints gbc_rdbtnChooserActivityInstallation = new GridBagConstraints();
			gbc_rdbtnChooserActivityInstallation.gridwidth = 2;
			gbc_rdbtnChooserActivityInstallation.fill = GridBagConstraints.BOTH;
			gbc_rdbtnChooserActivityInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnChooserActivityInstallation.gridx = 1;
			gbc_rdbtnChooserActivityInstallation.gridy = 1;
			pnChooserInstallationBooking.add(getRdbtnAdminactivityinstallation(), gbc_rdbtnChooserActivityInstallation);
			GridBagConstraints gbc_rdbtnChooserMemberInstallation = new GridBagConstraints();
			gbc_rdbtnChooserMemberInstallation.gridwidth = 2;
			gbc_rdbtnChooserMemberInstallation.fill = GridBagConstraints.BOTH;
			gbc_rdbtnChooserMemberInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnChooserMemberInstallation.gridx = 1;
			gbc_rdbtnChooserMemberInstallation.gridy = 2;
			pnChooserInstallationBooking.add(getRdbtnAdminmemberinstallation(), gbc_rdbtnChooserMemberInstallation);
			GridBagConstraints gbc_rdbtnChooserNotMemberInstal = new GridBagConstraints();
			gbc_rdbtnChooserNotMemberInstal.gridwidth = 2;
			gbc_rdbtnChooserNotMemberInstal.fill = GridBagConstraints.BOTH;
			gbc_rdbtnChooserNotMemberInstal.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnChooserNotMemberInstal.gridx = 1;
			gbc_rdbtnChooserNotMemberInstal.gridy = 3;
			pnChooserInstallationBooking.add(getRdbtnAdminNotMemberInstallation(), gbc_rdbtnChooserNotMemberInstal);
			GridBagConstraints gbc_btnSiguienteChooserInstallation = new GridBagConstraints();
			gbc_btnSiguienteChooserInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_btnSiguienteChooserInstallation.fill = GridBagConstraints.BOTH;
			gbc_btnSiguienteChooserInstallation.gridx = 3;
			gbc_btnSiguienteChooserInstallation.gridy = 5;
			pnChooserInstallationBooking.add(getBtnSiguienteChooserInstallation(), gbc_btnSiguienteChooserInstallation);
			groupMemberInstallation.add(getRdbtnPagoConMensualidad());
			groupMemberInstallation.add(getRdbtnPagoAlEntrar());
		}
		return pnChooserInstallationBooking;
	}

	private JRadioButton getRdbtnAdminactivityinstallation() {
		if (rdbtnChooserActivityInstallation == null) {
			rdbtnChooserActivityInstallation = new JRadioButton("Reservar actividad en el centro");
			rdbtnChooserActivityInstallation.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			rdbtnChooserActivityInstallation.setBackground(pnChooserInstallationBooking.getBackground());
			rdbtnChooserActivityInstallation.setEnabled(false);
			rdbtnChooserActivityInstallation.setVisible(false);
		}
		return rdbtnChooserActivityInstallation;
	}

	private JRadioButton getRdbtnAdminmemberinstallation() {
		if (rdbtnChooserMemberInstallation == null) {
			rdbtnChooserMemberInstallation = new JRadioButton("Reservar instalaci\u00F3n para un socio");
			rdbtnChooserMemberInstallation.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			rdbtnChooserMemberInstallation.setBackground(pnChooserInstallationBooking.getBackground());
		}
		return rdbtnChooserMemberInstallation;
	}

	private JRadioButton getRdbtnAdminNotMemberInstallation() {
		if (rdbtnChooserNotMemberInstal == null) {
			rdbtnChooserNotMemberInstal = new JRadioButton("Reserva instalaci\u00F3n para un no socio");
			rdbtnChooserNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			rdbtnChooserNotMemberInstal.setBackground(pnChooserInstallationBooking.getBackground());
		}
		return rdbtnChooserNotMemberInstal;
	}

	// Saul
	private JButton getBtnSiguienteChooserInstallation() {
		if (btnSiguienteChooserInstallation == null) {
			btnSiguienteChooserInstallation = new JButton("Siguiente");
			btnSiguienteChooserInstallation.setBackground(Color.WHITE);
			btnSiguienteChooserInstallation.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnSiguienteChooserInstallation.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (rdbtnChooserNotMemberInstal.isSelected()) {
						cardInstallation.show(pnInstallationBookingView, "pnNotMember");
					} else if (rdbtnChooserMemberInstallation.isSelected()) {
						cardInstallation.show(pnInstallationBookingView, "pnMember");
						if (!manager.currentUserIsAdmin()) {
							txtDNIMemberInstal.setText(manager.getCurrentUser().getDNI());
							txtDNIMemberInstal.setEditable(false);
						}
					} else if (rdbtnChooserActivityInstallation.isSelected()) {
						pnActivitiesContent.removeAll();
						cardInstallation.show(pnInstallationBookingView, "pnActivity");
						showActivities();
					}
				}
			});
		}
		return btnSiguienteChooserInstallation;
	}

	private JPanel getPnNotMemberInstallationBooking() {
		if (pnNotMemberInstallationBooking == null) {
			pnNotMemberInstallationBooking = new JPanel();
			pnNotMemberInstallationBooking.setBackground(pnInstallationBookingView.getBackground());
			GridBagLayout gbl_pnPayData = new GridBagLayout();
			gbl_pnPayData.columnWidths = new int[] { 41, 0, 33, 37, 63, 122, 62, 171, 101, 57, 56, 0 };
			gbl_pnPayData.rowHeights = new int[] { 64, 0, 30, 30, 30, 30, 30, 30, 30, 30, 0, 0 };
			gbl_pnPayData.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			gbl_pnPayData.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnNotMemberInstallationBooking.setLayout(gbl_pnPayData);
			GridBagConstraints gbc_lblIdPayment = new GridBagConstraints();
			gbc_lblIdPayment.fill = GridBagConstraints.BOTH;
			gbc_lblIdPayment.insets = new Insets(0, 0, 5, 5);
			gbc_lblIdPayment.gridx = 5;
			gbc_lblIdPayment.gridy = 2;
			pnNotMemberInstallationBooking.add(getLblDNINotMemberInstal(), gbc_lblIdPayment);
			GridBagConstraints gbc_txtIdPayment = new GridBagConstraints();
			gbc_txtIdPayment.gridwidth = 2;
			gbc_txtIdPayment.fill = GridBagConstraints.BOTH;
			gbc_txtIdPayment.insets = new Insets(0, 0, 5, 5);
			gbc_txtIdPayment.gridx = 6;
			gbc_txtIdPayment.gridy = 2;
			pnNotMemberInstallationBooking.add(getTxtDNINotMemberInstal(), gbc_txtIdPayment);
			GridBagConstraints gbc_lblNamePayment = new GridBagConstraints();
			gbc_lblNamePayment.fill = GridBagConstraints.BOTH;
			gbc_lblNamePayment.insets = new Insets(0, 0, 5, 5);
			gbc_lblNamePayment.gridx = 5;
			gbc_lblNamePayment.gridy = 3;
			pnNotMemberInstallationBooking.add(getLblNameNotMemberInstal(), gbc_lblNamePayment);
			GridBagConstraints gbc_txtNamePayment = new GridBagConstraints();
			gbc_txtNamePayment.gridwidth = 2;
			gbc_txtNamePayment.fill = GridBagConstraints.BOTH;
			gbc_txtNamePayment.insets = new Insets(0, 0, 5, 5);
			gbc_txtNamePayment.gridx = 6;
			gbc_txtNamePayment.gridy = 3;
			pnNotMemberInstallationBooking.add(getTxtNameNotMemberInstal(), gbc_txtNamePayment);
			GridBagConstraints gbc_lblSurnamePayment = new GridBagConstraints();
			gbc_lblSurnamePayment.fill = GridBagConstraints.BOTH;
			gbc_lblSurnamePayment.insets = new Insets(0, 0, 5, 5);
			gbc_lblSurnamePayment.gridx = 5;
			gbc_lblSurnamePayment.gridy = 4;
			pnNotMemberInstallationBooking.add(getLblSurnameNotMemberInstal(), gbc_lblSurnamePayment);
			GridBagConstraints gbc_txtSurnamePayment = new GridBagConstraints();
			gbc_txtSurnamePayment.gridwidth = 2;
			gbc_txtSurnamePayment.fill = GridBagConstraints.BOTH;
			gbc_txtSurnamePayment.insets = new Insets(0, 0, 5, 5);
			gbc_txtSurnamePayment.gridx = 6;
			gbc_txtSurnamePayment.gridy = 4;
			pnNotMemberInstallationBooking.add(getTxtSurnameNotMemberInstal(), gbc_txtSurnamePayment);
			GridBagConstraints gbc_lblTelephonePayment = new GridBagConstraints();
			gbc_lblTelephonePayment.fill = GridBagConstraints.BOTH;
			gbc_lblTelephonePayment.insets = new Insets(0, 0, 5, 5);
			gbc_lblTelephonePayment.gridx = 5;
			gbc_lblTelephonePayment.gridy = 5;
			pnNotMemberInstallationBooking.add(getLblTelephoneNotMemberInstal(), gbc_lblTelephonePayment);
			GridBagConstraints gbc_txtTelephonePayment = new GridBagConstraints();
			gbc_txtTelephonePayment.gridwidth = 2;
			gbc_txtTelephonePayment.fill = GridBagConstraints.BOTH;
			gbc_txtTelephonePayment.insets = new Insets(0, 0, 5, 5);
			gbc_txtTelephonePayment.gridx = 6;
			gbc_txtTelephonePayment.gridy = 5;
			pnNotMemberInstallationBooking.add(getTxtTelephoneNotMemberInstal(), gbc_txtTelephonePayment);
			GridBagConstraints gbc_btnBackPayment = new GridBagConstraints();
			gbc_btnBackPayment.gridheight = 3;
			gbc_btnBackPayment.gridwidth = 3;
			gbc_btnBackPayment.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackPayment.anchor = GridBagConstraints.WEST;
			gbc_btnBackPayment.fill = GridBagConstraints.VERTICAL;
			gbc_btnBackPayment.gridx = 1;
			gbc_btnBackPayment.gridy = 1;
			pnNotMemberInstallationBooking.add(getBtnBackNotMemberInstal(), gbc_btnBackPayment);
			GridBagConstraints gbc_lblDateNotMemberInstal = new GridBagConstraints();
			gbc_lblDateNotMemberInstal.fill = GridBagConstraints.BOTH;
			gbc_lblDateNotMemberInstal.insets = new Insets(0, 0, 5, 5);
			gbc_lblDateNotMemberInstal.gridx = 5;
			gbc_lblDateNotMemberInstal.gridy = 6;
			pnNotMemberInstallationBooking.add(getLblDateNotMemberInstal(), gbc_lblDateNotMemberInstal);
			GridBagConstraints gbc_dateChooserNotMember = new GridBagConstraints();
			gbc_dateChooserNotMember.gridwidth = 2;
			gbc_dateChooserNotMember.insets = new Insets(0, 0, 5, 5);
			gbc_dateChooserNotMember.fill = GridBagConstraints.BOTH;
			gbc_dateChooserNotMember.gridx = 6;
			gbc_dateChooserNotMember.gridy = 6;
			pnNotMemberInstallationBooking.add(getDateChooserNotMember(), gbc_dateChooserNotMember);
			GridBagConstraints gbc_lblHoraNotMemberInstal = new GridBagConstraints();
			gbc_lblHoraNotMemberInstal.fill = GridBagConstraints.VERTICAL;
			gbc_lblHoraNotMemberInstal.anchor = GridBagConstraints.EAST;
			gbc_lblHoraNotMemberInstal.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraNotMemberInstal.gridx = 5;
			gbc_lblHoraNotMemberInstal.gridy = 7;
			pnNotMemberInstallationBooking.add(getLblHoraNotMemberInstal(), gbc_lblHoraNotMemberInstal);
			GridBagConstraints gbc_cbHourInstallationNotMemberI = new GridBagConstraints();
			gbc_cbHourInstallationNotMemberI.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourInstallationNotMemberI.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourInstallationNotMemberI.gridx = 6;
			gbc_cbHourInstallationNotMemberI.gridy = 7;
			pnNotMemberInstallationBooking.add(getCbHourInstallationNotMemberI(), gbc_cbHourInstallationNotMemberI);
			GridBagConstraints gbc_txtHoraNotMemberInstali = new GridBagConstraints();
			gbc_txtHoraNotMemberInstali.insets = new Insets(0, 0, 5, 5);
			gbc_txtHoraNotMemberInstali.fill = GridBagConstraints.BOTH;
			gbc_txtHoraNotMemberInstali.gridx = 7;
			gbc_txtHoraNotMemberInstali.gridy = 7;

			GridBagConstraints gbc_lblHoraFinal = new GridBagConstraints();
			gbc_lblHoraFinal.anchor = GridBagConstraints.EAST;
			gbc_lblHoraFinal.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraFinal.gridx = 5;
			gbc_lblHoraFinal.gridy = 8;
			pnNotMemberInstallationBooking.add(getLblHoraFinal(), gbc_lblHoraFinal);
			GridBagConstraints gbc_cbHourInstallationNotMemberF = new GridBagConstraints();
			gbc_cbHourInstallationNotMemberF.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourInstallationNotMemberF.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourInstallationNotMemberF.gridx = 6;
			gbc_cbHourInstallationNotMemberF.gridy = 8;
			pnNotMemberInstallationBooking.add(getCbHourInstallationNotMemberF(), gbc_cbHourInstallationNotMemberF);
			GridBagConstraints gbc_txtHoraNotMemberInstalF = new GridBagConstraints();
			gbc_txtHoraNotMemberInstalF.insets = new Insets(0, 0, 5, 5);
			gbc_txtHoraNotMemberInstalF.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHoraNotMemberInstalF.gridx = 7;
			gbc_txtHoraNotMemberInstalF.gridy = 8;

			GridBagConstraints gbc_btnNextPayment = new GridBagConstraints();
			gbc_btnNextPayment.insets = new Insets(0, 0, 5, 5);
			gbc_btnNextPayment.anchor = GridBagConstraints.WEST;
			gbc_btnNextPayment.fill = GridBagConstraints.VERTICAL;
			gbc_btnNextPayment.gridx = 8;
			gbc_btnNextPayment.gridy = 9;
			pnNotMemberInstallationBooking.add(getBtnNextNotMemberInstal(), gbc_btnNextPayment);
		}
		return pnNotMemberInstallationBooking;
	}

	private JLabel getLblNameNotMemberInstal() {
		if (lblNameNotMemberInstal == null) {
			lblNameNotMemberInstal = new JLabel();
			lblNameNotMemberInstal.setText("Nombre:");
			lblNameNotMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNameNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblNameNotMemberInstal;
	}

	private JLabel getLblSurnameNotMemberInstal() {
		if (lblSurnameNotMemberInstal == null) {
			lblSurnameNotMemberInstal = new JLabel();
			lblSurnameNotMemberInstal.setText("Apellidos:");
			lblSurnameNotMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblSurnameNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblSurnameNotMemberInstal;
	}

	private JLabel getLblDNINotMemberInstal() {
		if (lblDNINotMemberInstal == null) {
			lblDNINotMemberInstal = new JLabel();
			lblDNINotMemberInstal.setText("DNI:");
			lblDNINotMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblDNINotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblDNINotMemberInstal;
	}

	private JLabel getLblTelephoneNotMemberInstal() {
		if (lblTelephoneNotMemberInstal == null) {
			lblTelephoneNotMemberInstal = new JLabel();
			lblTelephoneNotMemberInstal.setText("Telefono:");
			lblTelephoneNotMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblTelephoneNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblTelephoneNotMemberInstal;
	}

	private JTextField getTxtNameNotMemberInstal() {
		if (txtNameNotMemberInstal == null) {
			txtNameNotMemberInstal = new JTextField();
			txtNameNotMemberInstal.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char c = e.getKeyChar();
					if (!Character.isLetter(c) || txtNameNotMemberInstal.getText().length() > 15)
						e.consume();
				}
			});
			txtNameNotMemberInstal.setHorizontalAlignment(SwingConstants.LEFT);
			txtNameNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtNameNotMemberInstal.setColumns(10);
		}
		return txtNameNotMemberInstal;
	}

	private JTextField getTxtSurnameNotMemberInstal() {
		if (txtSurnameNotMemberInstal == null) {
			txtSurnameNotMemberInstal = new JTextField();
			txtSurnameNotMemberInstal.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char c = e.getKeyChar();
					if (!Character.isLetter(c) || txtSurnameNotMemberInstal.getText().length() > 40)
						e.consume();
				}
			});
			txtSurnameNotMemberInstal.setHorizontalAlignment(SwingConstants.LEFT);
			txtSurnameNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtSurnameNotMemberInstal.setColumns(10);
		}
		return txtSurnameNotMemberInstal;
	}

	private JTextField getTxtDNINotMemberInstal() {
		if (txtDNINotMemberInstal == null) {
			txtDNINotMemberInstal = new JTextField();
			txtDNINotMemberInstal.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) {
					if (manager.isUserCorrect(getTxtDNINotMemberInstal().getText())) {
						JOptionPane.showMessageDialog(null,
								"El DNI introducido pertenece a un socio \n(Se va a redirigir al sitio correcto para hacer esta reserva)",
								"", JOptionPane.INFORMATION_MESSAGE);
						getTxtDNIMemberInstal().setText(getTxtDNINotMemberInstal().getText());
						getTxtDNINotMemberInstal().setText("");
						pnInstallations.removeAll();
						cardInstallation.show(pnInstallationBookingView, "pnMember");
					}
				}
			});
			txtDNINotMemberInstal.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent a) {
					char c = a.getKeyChar();
					if (Character.isWhitespace(c)
							|| (countNumbersDNI(txtDNINotMemberInstal) == 8 && Character.isDigit(c))
							|| (countLettersDNI(txtDNINotMemberInstal) == 1 && Character.isLetter(c))
							|| notCorrectDNI(txtDNINotMemberInstal)) {
						a.consume();
					}
				}
			});
			txtDNINotMemberInstal.setHorizontalAlignment(SwingConstants.LEFT);
			txtDNINotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtDNINotMemberInstal.setColumns(10);
		}
		return txtDNINotMemberInstal;
	}

	private JTextField getTxtTelephoneNotMemberInstal() {
		if (txtTelephoneNotMemberInstal == null) {
			txtTelephoneNotMemberInstal = new JTextField();
			txtTelephoneNotMemberInstal.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent a) {
					char c = a.getKeyChar();
					if (!Character.isDigit(c) || txtTelephoneNotMemberInstal.getText().length() >= 9) {
						a.consume();
					}
				}
			});
			txtTelephoneNotMemberInstal.setHorizontalAlignment(SwingConstants.LEFT);
			txtTelephoneNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtTelephoneNotMemberInstal.setColumns(10);
		}
		return txtTelephoneNotMemberInstal;
	}

	/**
	 * This method checks the correct order of the id, being 8 numbers and 1
	 * letter at the end
	 * 
	 * @return true if it is not correct, false otherwise
	 */
	protected boolean notCorrectDNI(JTextField text) {
		char[] nif = text.getText().toCharArray();
		boolean correct = false;
		for (int i = 0; i < nif.length; i++) {
			if (i <= 8) {
				if (Character.isLetter(nif[i])) {
					correct = true;
				} else {
					correct = false;
				}
			}
		}
		return correct;
	}

	/**
	 * This method counts the number of letters of the text area of the nif
	 * 
	 * @return the number of letters of the text area nif
	 */
	protected int countLettersDNI(JTextField text) {
		char[] id = text.getText().toCharArray();
		int count = 0;
		for (int i = 0; i < id.length; i++) {
			if (Character.isLetter(id[i]))
				count++;
		}
		return count;
	}

	/**
	 * This method counts the number of digits of the text area of the nif
	 * 
	 * @return the number of digits of the text area nif
	 */
	protected int countNumbersDNI(JTextField text) {
		char[] id = text.getText().toCharArray();
		int count = 0;
		for (int i = 0; i < id.length; i++) {
			if (Character.isDigit(id[i]))
				count++;
		}
		return count;
	}

	private JButton getBtnBackNotMemberInstal() {
		if (btnBackNotMemberInstal == null) {
			btnBackNotMemberInstal = new JButton();
			btnBackNotMemberInstal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardInstallation.show(pnInstallationBookingView, "pnChooser");
				}
			});
			btnBackNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackNotMemberInstal.setBackground(pnNotMemberInstallationBooking.getBackground());
			btnBackNotMemberInstal.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackNotMemberInstal, "/img/arrow.png");
			btnBackNotMemberInstal.setBorder(null);
		}
		return btnBackNotMemberInstal;
	}

	private JButton getBtnNextNotMemberInstal() {
		if (btnNextNotMemberInstal == null) {
			btnNextNotMemberInstal = new JButton();
			btnNextNotMemberInstal.setText("Siguiente");
			btnNextNotMemberInstal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String date = df.format(getDateChooserNotMember().getDate());
					String bookingCode = generateCode();
					if (!notEmptyField())
						JOptionPane.showMessageDialog(null, "No se han de completado todos los campos de texto",
								"Error al reservar", JOptionPane.ERROR_MESSAGE);
					else if (!isDateValid())
						JOptionPane.showMessageDialog(null, "No se han seleccionado las horas", "Error al reservar",
								JOptionPane.ERROR_MESSAGE);
					else {
						if (manager.bookingInstallNotMember(currentInstallation, date,
								Integer.parseInt((String) getCbHourInstallationNotMemberI().getSelectedItem()),
								Integer.parseInt((String) getCbHourInstallationNotMemberF().getSelectedItem()),
								getTxtDNINotMemberInstal().getText(), bookingCode, "alquilado")) {

							if (bookInstallationNotMember(getTxtDNINotMemberInstal().getText(),
									getTxtNameNotMemberInstal().getText(),
									Integer.parseInt(getTxtTelephoneNotMemberInstal().getText()),
									getTxtSurnameNotMemberInstal().getText())) {
								int dinero = manager.getPriceInstallation(currentInstallation, false);
								int in = Integer.parseInt((String) getCbHourInstallationNotMemberI().getSelectedItem());
								int fin = Integer
										.parseInt((String) getCbHourInstallationNotMemberF().getSelectedItem());
								dineroDevolver = dinero * (fin - in);

								JOptionPane
										.showMessageDialog(contentPane,
												"La reserva se ha realizado con exito,\n el importe a abonar es: "
														+ dinero * (fin - in) + " �",
												"", JOptionPane.INFORMATION_MESSAGE);
								txtpnByeMessage.setText("Ticket de pago:");
								showFinalPage(true);
								manager.setBookingPayed(bookingCode, true, true); // tabla
																					// pago,
																					// codigo
																					// booking,
																					// pagada,
																					// efectivo
								manager.saveBillInDatabase(txtpnAllBill.getText(), bookingCode);
								btnSaveTheBill.setVisible(true);
								pnInstallations.removeAll();
								cardInstallation.show(pnInstallationBookingView, "pnChooser");
								clearPanelNotMember();

							} else
								JOptionPane.showMessageDialog(null, "La instalacion ya se encuentra seleccionada",
										"Error al reservar", JOptionPane.ERROR_MESSAGE);
						}
					}

				}
			});
			btnNextNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnNextNotMemberInstal;
	}

	private void clearPanelNotMember() {
		getTxtNameNotMemberInstal().setText("");
		getTxtDNINotMemberInstal().setText("");
		getTxtSurnameNotMemberInstal().setText("");
		getTxtTelephoneNotMemberInstal().setText("");
		getCbHourInstallationNotMemberI().setSelectedIndex(0);
		getCbHourInstallationNotMemberF().setEnabled(false);

	}

	private JDateChooser getDateChooserNotMember() {
		if (dateChooserNotMember == null) {
			dateChooserNotMember = new JDateChooser();
			dateChooserNotMember.setMinSelectableDate(new Date());
			dateChooserNotMember.setDate(new Date());
		}
		return dateChooserNotMember;
	}

	private JLabel getLblHoraNotMemberInstal() {
		if (lblHoraNotMemberInstal == null) {
			lblHoraNotMemberInstal = new JLabel("Hora Inicio:");
			lblHoraNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblHoraNotMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblHoraNotMemberInstal;
	}

	private JLabel getLblDateNotMemberInstal() {
		if (lblDateNotMemberInstal == null) {
			lblDateNotMemberInstal = new JLabel("Fecha:");
			lblDateNotMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblDateNotMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblDateNotMemberInstal;
	}

	private JPanel getPnMemberInstallationBooking() {
		if (pnMemberInstallationBooking == null) {
			pnMemberInstallationBooking = new JPanel();
			pnMemberInstallationBooking.setBackground(pnInstallationBookingView.getBackground());
			GridBagLayout gbl_pnPayData = new GridBagLayout();
			gbl_pnPayData.columnWidths = new int[] { 41, 0, 33, 37, 41, 0, 0, 108, 63, 122, 0, 101, 57, 56, 0 };
			gbl_pnPayData.rowHeights = new int[] { 32, 0, 30, 30, 30, 30, 30, 30, 0, 30, 0, 0 };
			gbl_pnPayData.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0,
					1.0, 1.0, Double.MIN_VALUE };
			gbl_pnPayData.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnMemberInstallationBooking.setLayout(gbl_pnPayData);
			GridBagConstraints gbc_lblIdPayment = new GridBagConstraints();
			gbc_lblIdPayment.fill = GridBagConstraints.BOTH;
			gbc_lblIdPayment.insets = new Insets(0, 0, 5, 5);
			gbc_lblIdPayment.gridx = 7;
			gbc_lblIdPayment.gridy = 2;
			pnMemberInstallationBooking.add(getLblDNIMemberInstal(), gbc_lblIdPayment);
			GridBagConstraints gbc_txtIdPayment = new GridBagConstraints();
			gbc_txtIdPayment.gridwidth = 2;
			gbc_txtIdPayment.fill = GridBagConstraints.BOTH;
			gbc_txtIdPayment.insets = new Insets(0, 0, 5, 5);
			gbc_txtIdPayment.gridx = 8;
			gbc_txtIdPayment.gridy = 2;
			pnMemberInstallationBooking.add(getTxtDNIMemberInstal(), gbc_txtIdPayment);
			GridBagConstraints gbc_btnBackPayment = new GridBagConstraints();
			gbc_btnBackPayment.gridheight = 3;
			gbc_btnBackPayment.gridwidth = 3;
			gbc_btnBackPayment.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackPayment.anchor = GridBagConstraints.WEST;
			gbc_btnBackPayment.fill = GridBagConstraints.VERTICAL;
			gbc_btnBackPayment.gridx = 1;
			gbc_btnBackPayment.gridy = 1;
			pnMemberInstallationBooking.add(getBtnBackMemberInstal(), gbc_btnBackPayment);
			GridBagConstraints gbc_lblDateNotMemberInstal = new GridBagConstraints();
			gbc_lblDateNotMemberInstal.fill = GridBagConstraints.BOTH;
			gbc_lblDateNotMemberInstal.insets = new Insets(0, 0, 5, 5);
			gbc_lblDateNotMemberInstal.gridx = 7;
			gbc_lblDateNotMemberInstal.gridy = 3;
			pnMemberInstallationBooking.add(getLblDateMemberInstal(), gbc_lblDateNotMemberInstal);
			GridBagConstraints gbc_dateChooser = new GridBagConstraints();
			gbc_dateChooser.gridwidth = 2;
			gbc_dateChooser.insets = new Insets(0, 0, 5, 5);
			gbc_dateChooser.fill = GridBagConstraints.BOTH;
			gbc_dateChooser.gridx = 8;
			gbc_dateChooser.gridy = 3;
			pnMemberInstallationBooking.add(getDateChooserMember(), gbc_dateChooser);
			GridBagConstraints gbc_lblHoraNotMemberInstal = new GridBagConstraints();
			gbc_lblHoraNotMemberInstal.fill = GridBagConstraints.VERTICAL;
			gbc_lblHoraNotMemberInstal.anchor = GridBagConstraints.EAST;
			gbc_lblHoraNotMemberInstal.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraNotMemberInstal.gridx = 7;
			gbc_lblHoraNotMemberInstal.gridy = 4;
			pnMemberInstallationBooking.add(getLblHoraMemberInstal(), gbc_lblHoraNotMemberInstal);
			GridBagConstraints gbc_cbHourInstallationMemberS = new GridBagConstraints();
			gbc_cbHourInstallationMemberS.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourInstallationMemberS.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourInstallationMemberS.gridx = 8;
			gbc_cbHourInstallationMemberS.gridy = 4;
			pnMemberInstallationBooking.add(getCbHourInstallationMemberS(), gbc_cbHourInstallationMemberS);
			GridBagConstraints gbc_txtHoraNotMemberInstal = new GridBagConstraints();
			gbc_txtHoraNotMemberInstal.insets = new Insets(0, 0, 5, 5);
			gbc_txtHoraNotMemberInstal.fill = GridBagConstraints.BOTH;
			gbc_txtHoraNotMemberInstal.gridx = 9;
			gbc_txtHoraNotMemberInstal.gridy = 4;
			GridBagConstraints gbc_lblHoraFin = new GridBagConstraints();
			gbc_lblHoraFin.anchor = GridBagConstraints.EAST;
			gbc_lblHoraFin.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraFin.gridx = 7;
			gbc_lblHoraFin.gridy = 5;
			pnMemberInstallationBooking.add(getLblHoraFin(), gbc_lblHoraFin);
			GridBagConstraints gbc_cbHourInstallationMemberF = new GridBagConstraints();
			gbc_cbHourInstallationMemberF.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourInstallationMemberF.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourInstallationMemberF.gridx = 8;
			gbc_cbHourInstallationMemberF.gridy = 5;
			pnMemberInstallationBooking.add(getCbHourInstallationMemberF(), gbc_cbHourInstallationMemberF);
			GridBagConstraints gbc_txtHourMemberEnd = new GridBagConstraints();
			gbc_txtHourMemberEnd.insets = new Insets(0, 0, 5, 5);
			gbc_txtHourMemberEnd.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHourMemberEnd.gridx = 9;
			gbc_txtHourMemberEnd.gridy = 5;
			GridBagConstraints gbc_lblTipoDePago = new GridBagConstraints();
			gbc_lblTipoDePago.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblTipoDePago.gridwidth = 4;
			gbc_lblTipoDePago.insets = new Insets(0, 0, 5, 5);
			gbc_lblTipoDePago.gridx = 1;
			gbc_lblTipoDePago.gridy = 7;
			pnMemberInstallationBooking.add(getLblTipoDePago(), gbc_lblTipoDePago);
			GridBagConstraints gbc_rdbtnPagoConMensualidad = new GridBagConstraints();
			gbc_rdbtnPagoConMensualidad.anchor = GridBagConstraints.WEST;
			gbc_rdbtnPagoConMensualidad.gridwidth = 4;
			gbc_rdbtnPagoConMensualidad.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnPagoConMensualidad.gridx = 4;
			gbc_rdbtnPagoConMensualidad.gridy = 8;
			pnMemberInstallationBooking.add(getRdbtnPagoConMensualidad(), gbc_rdbtnPagoConMensualidad);
			GridBagConstraints gbc_rdbtnPagoAlEntrar = new GridBagConstraints();
			gbc_rdbtnPagoAlEntrar.anchor = GridBagConstraints.WEST;
			gbc_rdbtnPagoAlEntrar.gridwidth = 4;
			gbc_rdbtnPagoAlEntrar.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnPagoAlEntrar.gridx = 4;
			gbc_rdbtnPagoAlEntrar.gridy = 9;
			pnMemberInstallationBooking.add(getRdbtnPagoAlEntrar(), gbc_rdbtnPagoAlEntrar);
			GridBagConstraints gbc_btnNextPayment = new GridBagConstraints();
			gbc_btnNextPayment.insets = new Insets(0, 0, 5, 5);
			gbc_btnNextPayment.anchor = GridBagConstraints.WEST;
			gbc_btnNextPayment.fill = GridBagConstraints.VERTICAL;
			gbc_btnNextPayment.gridx = 11;
			gbc_btnNextPayment.gridy = 9;
			pnMemberInstallationBooking.add(getBtnNextMemberInstal(), gbc_btnNextPayment);
		}
		return pnMemberInstallationBooking;
	}

	private JLabel getLblDNIMemberInstal() {
		if (lblDNIMemberInstal == null) {
			lblDNIMemberInstal = new JLabel();
			lblDNIMemberInstal.setText("DNI:");
			lblDNIMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblDNIMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblDNIMemberInstal;
	}

	private JTextField getTxtDNIMemberInstal() {
		if (txtDNIMemberInstal == null) {
			txtDNIMemberInstal = new JTextField();
			txtDNIMemberInstal.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent a) {
					char c = a.getKeyChar();
					if (Character.isWhitespace(c) || (countNumbersDNI(txtDNIMemberInstal) == 8 && Character.isDigit(c))
							|| (countLettersDNI(txtDNIMemberInstal) == 1 && Character.isLetter(c))
							|| notCorrectDNI(txtDNIMemberInstal)) {
						a.consume();
					}
				}
			});
			txtDNIMemberInstal.setHorizontalAlignment(SwingConstants.LEFT);
			txtDNIMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtDNIMemberInstal.setColumns(10);
		}
		return txtDNIMemberInstal;
	}

	private JButton getBtnBackMemberInstal() {
		if (btnBackMemberInstal == null) {
			btnBackMemberInstal = new JButton();
			btnBackMemberInstal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardInstallation.show(pnInstallationBookingView, "pnChooser");
				}
			});
			btnBackMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackMemberInstal.setBackground(pnNotMemberInstallationBooking.getBackground());
			btnBackMemberInstal.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackMemberInstal, "/img/arrow.png");
			btnBackMemberInstal.setBorder(null);
		}
		return btnBackMemberInstal;
	}

	// Gonzi
	private JButton getBtnNextMemberInstal() {
		if (btnNextMemberInstal == null) {
			btnNextMemberInstal = new JButton();
			btnNextMemberInstal.setText("Siguiente");
			btnNextMemberInstal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (isEmptyDateBookingAdminToMember()) {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						String fecha = df.format(getDateChooserMember().getDate());
						String bookingCode = generateCode();
						int days = 15;
						if (manager.currentUserIsAdmin())
							days = 7;
						if (checkBookingAdminToMember(days)) {
							if (manager.installationBookingAdmToMember(currentInstallation, fecha,
									Integer.parseInt((String) cbHourInstallationMemberS.getSelectedItem()),
									Integer.parseInt((String) cbHourInstallationMemberF.getSelectedItem()),
									getTxtDNIMemberInstal().getText(), bookingCode, "alquilado")) {
								JOptionPane.showMessageDialog(null, "La reserva se ha completado con �xito",
										"Reserva completada", JOptionPane.INFORMATION_MESSAGE);
								txtpnByeMessage.setText("Ticket de reserva:");
								showFinalPage(false);
								insertPay(bookingCode);
								manager.saveBillInDatabase(txtpnAllBill.getText(), bookingCode);
								btnSaveTheBill.setVisible(true);
								pnInstallations.removeAll();
								cardInstallation.show(pnInstallationBookingView, "pnChooser");
							} else
								JOptionPane.showMessageDialog(null,
										"Lo sentimos. \nLa instalaci�n seleccionada ya est� ocupada para la fecha y horas introducidas",
										"Error al reservar", JOptionPane.ERROR_MESSAGE);
						}
					}

				}
			});
			btnNextMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnNextMemberInstal;
	}

	// Saul
	private boolean checkIsHourBefore() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(getDateChooserMember().getDate());
		String fechaAct = df.format(new Date());
		if (fecha.equals(fechaAct)) {
			Calendar calendario = Calendar.getInstance();
			int hour = calendario.get(Calendar.HOUR_OF_DAY);
			if (hour > Integer.parseInt((String) getCbHourInstallationMemberS().getSelectedItem())) {
				JOptionPane.showMessageDialog(null, "La hora seleccionada es inv�lida", "Error al reservar",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
			if (hour - 1 > Integer.parseInt((String) getCbHourInstallationMemberS().getSelectedItem())) {
				JOptionPane.showMessageDialog(null, "La reserva se debe hacer con, al menos, una hora de antelaci�n",
						"Error al reservar", JOptionPane.ERROR_MESSAGE);
				return false;
			}

		}
		return true;
	}

	// Saul
	private void resetFieldMember() {
		getTxtDNIMemberInstal().setText("");
		cbHourInstallationMemberS.setSelectedIndex(0);
		cbHourInstallationMemberF.setEnabled(false);
		cbHourInstallationMemberF.setSelectedIndex(0);
		getRdbtnPagoAlEntrar().setSelected(false);
		getRdbtnPagoConMensualidad().setSelected(false);
	}

	// Saul
	private void insertPay(String id_reserva) {
		if (getRdbtnPagoAlEntrar().isSelected())
			manager.insertPay(id_reserva, 1);
		else if (getRdbtnPagoConMensualidad().isSelected())
			manager.insertPay(id_reserva, 0);
	}

	// Gonzi
	private boolean checkBookingAdminToMember(int days) {
		if (checkTxtHoursBookingAdminToMember())
			return checkDniBookingAdminToMember() && checkDateBookingAdminToMember(days) && isSelectedPay()
					&& checkIsHourBefore();
		return false;
	}

	// Saul
	private boolean isSelectedPay() {
		if (getRdbtnPagoAlEntrar().isSelected() || getRdbtnPagoConMensualidad().isSelected())
			return true;
		JOptionPane.showMessageDialog(null, "No ha seleccionado la forma de pago", "Error al reservar",
				JOptionPane.ERROR_MESSAGE);
		return false;
	}

	private boolean isEmptyDateBookingAdminToMember() {
		if (getDateChooserMember() == null) {
			JOptionPane.showMessageDialog(null, "No se ha introduccido la fecha", "Error al reservar",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	// Gonzi
	private boolean checkDniBookingAdminToMember() {
		if (!manager.isUserCorrect(getTxtDNIMemberInstal().getText())) {
			JOptionPane.showMessageDialog(null, "El DNI introducido no existe", "Error al reservar",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(getDateChooserMember().getDate());
		if (!manager.isUserValid(fecha, getTxtDNIMemberInstal().getText(),
				Integer.parseInt((String) cbHourInstallationMemberS.getSelectedItem()),
				Integer.parseInt((String) cbHourInstallationMemberF.getSelectedItem()))) {
			JOptionPane.showMessageDialog(null,
					"Este usuario ya tiene una reserva en curso para la fecha y horas seleccionadas",
					"Error al reservar", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	// Gonzi
	private boolean checkTxtHoursBookingAdminToMember() {

		if (!cbHourInstallationMemberF.isEnabled()) {
			JOptionPane.showMessageDialog(null, "No has seleccionado las horas de inicio y final", "Error al reservar",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	// Gonzi
	private boolean checkDateBookingAdminToMember(int days) {
		Date ahora = new Date();
		long timeAhora = ahora.getTime();
		long timeReserva = getDateChooserMember().getDate().getTime();
		long diferencia = timeReserva - timeAhora;
		double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
		if (((int) dias) > days) {
			JOptionPane.showMessageDialog(null,
					"La reserva solo puede realizarse ,como m�ximo, con " + days + " d�as de antelaci�n",
					"Error al reservar", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	private JDateChooser getDateChooserMember() {
		if (dateChooserMember == null) {
			dateChooserMember = new JDateChooser();
			dateChooserMember.setMinSelectableDate(new Date());
			dateChooserMember.setDate(new Date());
		}
		return dateChooserMember;
	}

	private JLabel getLblHoraMemberInstal() {
		if (lblHoraMemberInstal == null) {
			lblHoraMemberInstal = new JLabel("Hora inicio:");
			lblHoraMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblHoraMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblHoraMemberInstal;
	}

	// Gonzi
	// private JTextField getTxtHoraMemberInstal() {
	// if (txtHoraMemberInstal == null) {
	// txtHoraMemberInstal = new JTextField();
	// txtHoraMemberInstal.addKeyListener(new KeyAdapter() {
	// public void keyTyped(KeyEvent e) {
	// char caracter = e.getKeyChar();
	// if (((caracter < '0') || (caracter > '9'))
	// && (caracter != '\b' /* corresponde a BACK_SPACE */)
	// || (txtHoraMemberInstal.getText().length() > 1)) {
	// e.consume();
	// }
	// }
	// });
	// txtHoraMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
	// txtHoraMemberInstal.setColumns(10);
	// }
	// return txtHoraMemberInstal;
	// }

	private JLabel getLblDateMemberInstal() {
		if (lblDateMemberInstal == null) {
			lblDateMemberInstal = new JLabel("Fecha:");
			lblDateMemberInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblDateMemberInstal.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblDateMemberInstal;
	}

	// Saul
	private JPanel getPnActivityInstallationBooking() {
		if (pnActivityInstallationBooking == null) {
			pnActivityInstallationBooking = new JPanel();
			pnActivityInstallationBooking.setBackground(pnInstallationBookingView.getBackground());
			GridBagLayout gbl_pnActivityInstallationBooking = new GridBagLayout();
			gbl_pnActivityInstallationBooking.columnWidths = new int[] { 87, 0, 57, 43, 0, 81, 84, 58, 0, 90, 0, 0, 0 };
			gbl_pnActivityInstallationBooking.rowHeights = new int[] { 0, 0, 0, 0, 76, 0, 40, 40, 35, 32, 0, 0 };
			gbl_pnActivityInstallationBooking.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnActivityInstallationBooking.rowWeights = new double[] { 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0,
					1.0, 1.0, Double.MIN_VALUE };
			pnActivityInstallationBooking.setLayout(gbl_pnActivityInstallationBooking);
			GridBagConstraints gbc_pnContenedorDeTablaCrearActividad = new GridBagConstraints();
			gbc_pnContenedorDeTablaCrearActividad.gridwidth = 3;
			gbc_pnContenedorDeTablaCrearActividad.gridheight = 6;
			gbc_pnContenedorDeTablaCrearActividad.insets = new Insets(0, 0, 5, 5);
			gbc_pnContenedorDeTablaCrearActividad.fill = GridBagConstraints.BOTH;
			gbc_pnContenedorDeTablaCrearActividad.gridx = 8;
			gbc_pnContenedorDeTablaCrearActividad.gridy = 1;
			pnActivityInstallationBooking.add(getPnContenedorDeTablaCrearActividad(),
					gbc_pnContenedorDeTablaCrearActividad);
			GridBagConstraints gbc_calendarActivities = new GridBagConstraints();
			gbc_calendarActivities.gridheight = 3;
			gbc_calendarActivities.fill = GridBagConstraints.BOTH;
			gbc_calendarActivities.insets = new Insets(0, 0, 5, 5);
			gbc_calendarActivities.gridwidth = 2;
			gbc_calendarActivities.gridx = 5;
			gbc_calendarActivities.gridy = 2;
			pnActivityInstallationBooking.add(getCalendarActivities(), gbc_calendarActivities);
			GridBagConstraints gbc_pnActivitiesInstallation = new GridBagConstraints();
			gbc_pnActivitiesInstallation.gridwidth = 4;
			gbc_pnActivitiesInstallation.fill = GridBagConstraints.BOTH;
			gbc_pnActivitiesInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_pnActivitiesInstallation.gridheight = 7;
			gbc_pnActivitiesInstallation.gridx = 0;
			gbc_pnActivitiesInstallation.gridy = 3;
			pnActivityInstallationBooking.add(getPnActivitiesInstallation(), gbc_pnActivitiesInstallation);
			GridBagConstraints gbc_cbHourActivityInstallationI = new GridBagConstraints();
			gbc_cbHourActivityInstallationI.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourActivityInstallationI.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourActivityInstallationI.gridx = 6;
			gbc_cbHourActivityInstallationI.gridy = 5;
			GridBagConstraints gbc_lblHoraActivityInstal = new GridBagConstraints();
			gbc_lblHoraActivityInstal.gridwidth = 2;
			gbc_lblHoraActivityInstal.anchor = GridBagConstraints.EAST;
			gbc_lblHoraActivityInstal.fill = GridBagConstraints.VERTICAL;
			gbc_lblHoraActivityInstal.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraActivityInstal.gridx = 4;
			gbc_lblHoraActivityInstal.gridy = 6;
			pnActivityInstallationBooking.add(getLblHoraActivityInstal(), gbc_lblHoraActivityInstal);
			GridBagConstraints gbc_txtHoraActivityInicio = new GridBagConstraints();
			gbc_txtHoraActivityInicio.fill = GridBagConstraints.BOTH;
			gbc_txtHoraActivityInicio.insets = new Insets(0, 0, 5, 5);
			gbc_txtHoraActivityInicio.gridx = 7;
			gbc_txtHoraActivityInicio.gridy = 5;
			GridBagConstraints gbc_cbHourActivityInstallationS = new GridBagConstraints();
			gbc_cbHourActivityInstallationS.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourActivityInstallationS.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourActivityInstallationS.gridx = 6;
			gbc_cbHourActivityInstallationS.gridy = 6;
			pnActivityInstallationBooking.add(getCbHourActivityInstallationI(), gbc_cbHourActivityInstallationS);
			// pnActivityInstallationBooking.add(getTxtHoraActivityInicio(),
			// gbc_txtHoraActivityInicio);
			GridBagConstraints gbc_lblHourActivityEnd = new GridBagConstraints();
			gbc_lblHourActivityEnd.anchor = GridBagConstraints.EAST;
			gbc_lblHourActivityEnd.insets = new Insets(0, 0, 5, 5);
			gbc_lblHourActivityEnd.gridx = 5;
			gbc_lblHourActivityEnd.gridy = 7;
			pnActivityInstallationBooking.add(getLblHourActivityEnd(), gbc_lblHourActivityEnd);
			GridBagConstraints gbc_cbHourActivityInstallation = new GridBagConstraints();
			gbc_cbHourActivityInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourActivityInstallation.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourActivityInstallation.gridx = 6;
			gbc_cbHourActivityInstallation.gridy = 7;
			pnActivityInstallationBooking.add(getCbHourActivityInstallationF(), gbc_cbHourActivityInstallation);
			GridBagConstraints gbc_cbHourActivityInstallationF = new GridBagConstraints();
			gbc_cbHourActivityInstallationF.insets = new Insets(0, 0, 5, 5);
			gbc_cbHourActivityInstallationF.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbHourActivityInstallationF.gridx = 6;
			gbc_cbHourActivityInstallationF.gridy = 6;
			GridBagConstraints gbc_txtHourActivityEnd = new GridBagConstraints();
			gbc_txtHourActivityEnd.anchor = GridBagConstraints.WEST;
			gbc_txtHourActivityEnd.insets = new Insets(0, 0, 5, 5);
			gbc_txtHourActivityEnd.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHourActivityEnd.gridx = 7;
			gbc_txtHourActivityEnd.gridy = 6;
			GridBagConstraints gbc_btnBooking24h = new GridBagConstraints();
			gbc_btnBooking24h.gridwidth = 2;
			gbc_btnBooking24h.fill = GridBagConstraints.VERTICAL;
			gbc_btnBooking24h.insets = new Insets(0, 0, 5, 5);
			gbc_btnBooking24h.gridx = 5;
			gbc_btnBooking24h.gridy = 8;
			pnActivityInstallationBooking.add(getBtnBooking24h(), gbc_btnBooking24h);
			GridBagConstraints gbc_lblMonitor_1 = new GridBagConstraints();
			gbc_lblMonitor_1.fill = GridBagConstraints.VERTICAL;
			gbc_lblMonitor_1.anchor = GridBagConstraints.EAST;
			gbc_lblMonitor_1.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonitor_1.gridx = 8;
			gbc_lblMonitor_1.gridy = 8;
			pnActivityInstallationBooking.add(getLblMonitor_1(), gbc_lblMonitor_1);
			GridBagConstraints gbc_cBMonitorPuntual = new GridBagConstraints();
			gbc_cBMonitorPuntual.gridwidth = 2;
			gbc_cBMonitorPuntual.insets = new Insets(0, 0, 5, 5);
			gbc_cBMonitorPuntual.fill = GridBagConstraints.BOTH;
			gbc_cBMonitorPuntual.gridx = 9;
			gbc_cBMonitorPuntual.gridy = 8;
			pnActivityInstallationBooking.add(getCBMonitorPuntual(), gbc_cBMonitorPuntual);
			// pnActivityInstallationBooking.add(getTxtHourActivityEnd(),
			// gbc_txtHourActivityEnd);
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.gridwidth = 3;
			gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
			gbc_lblNewLabel.gridx = 5;
			gbc_lblNewLabel.gridy = 10;
			pnActivityInstallationBooking.add(getLbBookingInstallation(), gbc_lblNewLabel);
			GridBagConstraints gbc_btnBackAdminInstal = new GridBagConstraints();
			gbc_btnBackAdminInstal.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackAdminInstal.anchor = GridBagConstraints.EAST;
			gbc_btnBackAdminInstal.fill = GridBagConstraints.VERTICAL;
			gbc_btnBackAdminInstal.gridheight = 2;
			gbc_btnBackAdminInstal.gridx = 0;
			gbc_btnBackAdminInstal.gridy = 1;
			pnActivityInstallationBooking.add(getBtnBackAdminInstal(), gbc_btnBackAdminInstal);
			GridBagConstraints gbc_btnNextActivityInstal = new GridBagConstraints();
			gbc_btnNextActivityInstal.insets = new Insets(0, 0, 0, 5);
			gbc_btnNextActivityInstal.fill = GridBagConstraints.BOTH;
			gbc_btnNextActivityInstal.gridx = 9;
			gbc_btnNextActivityInstal.gridy = 10;
			pnActivityInstallationBooking.add(getBtnNextActivityInstal(), gbc_btnNextActivityInstal);
		}
		return pnActivityInstallationBooking;
	}

	private JPanel getPnActivitiesInstallation() {
		if (pnActivitiesInstallation == null) {
			pnActivitiesInstallation = new JPanel();
			pnActivitiesInstallation.setBackground(pnInstallationBookingView.getBackground());
			pnActivitiesInstallation.setLayout(new BorderLayout(0, 0));
			pnActivitiesInstallation.add(getScrllActivities());
		}
		return pnActivitiesInstallation;
	}

	private JScrollPane getScrllActivities() {
		if (scrllActivities == null) {
			scrllActivities = new JScrollPane();
			scrllActivities.setViewportView(getPnActvitiesContent());
			scrllActivities.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrllActivities.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		}
		return scrllActivities;
	}

	private JPanel getPnActvitiesContent() {
		if (pnActivitiesContent == null) {
			pnActivitiesContent = new JPanel();
			pnActivitiesContent.setBackground(pnInstallationBookingView.getBackground());
			pnActivitiesContent.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnActivitiesContent.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnActivitiesContent;
	}

	protected void showActivities() {
		pnActivitiesContent.removeAll();
		ArrayList<String[]> activities = manager.getActivities();
		for (String[] act : activities) {
			showActivityUnique(act[0], act[1]);
		}
	}

	protected void showActivityUnique(String name, String code) {
		JButton boton = new JButton();
		boton.setText(name);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(code);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentActivities = boton.getActionCommand();
				currentActivitiesName = boton.getText();
				boton.setBackground(new Color(135, 206, 250));
				Component[] comp = pnActivitiesContent.getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(pnActivitiesContent.getBackground());
		pnActivitiesContent.add(boton);
	}

	private JCalendar getCalendarActivities() {
		if (calendarActivities == null) {
			calendarActivities = new JCalendar();
			calendarActivities.setMinSelectableDate(new Date());
			calendarActivities.getDayChooser().addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent arg0) {
					if (currentInstallation != "")
						showScheduleInstActividadPuntual(currentInstallation);

				}
			});
		}
		return calendarActivities;
	}

	// Saul
	private JLabel getLblHoraActivityInstal() {
		if (lblHoraActivityInstal == null) {
			lblHoraActivityInstal = new JLabel("Hora inicio:");
			lblHoraActivityInstal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblHoraActivityInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblHoraActivityInstal;
	}

	// Saul
	private JButton getBtnNextActivityInstal() {
		if (btnNextActivityInstal == null) {
			btnNextActivityInstal = new JButton("Reservar");
			btnNextActivityInstal.setBackground(Color.WHITE);
			btnNextActivityInstal.addActionListener(new ActionListener() {
				@SuppressWarnings("unused")
				public void actionPerformed(ActionEvent e) {
					String bookingCode = generateCode();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String fecha = df.format(getCalendarActivities().getDate());
					String message = "";
					String activityCode = generateCode();
					int plazas = -1;
					if (crearActividad) {// si se va crear una actividad puntual
						actividadPeriodica = false;
						message = "\nCon plazas ";
						if (rdbtnIlimitadas.isSelected()) {
							message += "ilimitadas\n";
							manager.crearActividad(txtNombreAtividad.getText(), "-1", activityCode,
									getDNImonitor((String) getCBMonitores().getSelectedItem()));
							plazas = -1;
						} else if (rdbtnLimitadas.isSelected()) {
							message += "limitadas a " + txtNumeroDePlazas.getText() + "participantes\n";
							manager.crearActividad(txtNombreAtividad.getText(), txtNumeroDePlazas.getText(),
									activityCode, getDNImonitor((String) getCBMonitores().getSelectedItem()));
							plazas = Integer.parseInt((String) txtNumeroDePlazas.getText());
						}
						currentActivities = activityCode;
						currentActivitiesName = txtNombreAtividad.getText();
						currentBookingCodePuntual = bookingCode;
						currentMessagePuntual = message;
					}
					if (currentData()) {
						if (!manager.checkMonitorOcupadoPuntual(
								getDNImonitor((String) getCBMonitorPuntual().getSelectedItem()),
								Integer.parseInt((String) cbHourActivityInstallationI.getSelectedItem()),
								Integer.parseInt((String) cbHourActivityInstallationF.getSelectedItem()), fecha)) {
							JOptionPane.showMessageDialog(null,
									"El monitor seleccionado tiene otra actividad asignada en la misma fecha \n"
											+ "y en horas incompatibles con las seleccionadas",
									"Error en monitor", JOptionPane.ERROR_MESSAGE);
						} else {
							if (manager.activityInstallationBookingAdm(currentActivitiesName, currentInstallation,
									fecha, Integer.parseInt((String) cbHourActivityInstallationI.getSelectedItem()),
									Integer.parseInt((String) cbHourActivityInstallationF.getSelectedItem()),
									manager.getCurrentUser().getDNI(), bookingCode, currentActivities, null, plazas,
									false)) {
								JOptionPane.showMessageDialog(null,
										"Se ha reservado correctamente la actividad: " + currentActivitiesName + ".\n"
												+ "En la instalaci�n: " + currentInstallation + ".\nDesde: "
												+ cbHourActivityInstallationI.getSelectedItem() + "h hasta: "
												+ cbHourActivityInstallationF.getSelectedItem() + "h.\n" + "El dia: "
												+ fecha + "." + message,
										"Informaci�n sobre reserva", JOptionPane.INFORMATION_MESSAGE);
								pnInstallations.removeAll();
								initializeAdmToActivityBooking();
								card.show(pnBase, "pnHomeView");
								cardInstallation.show(pnInstallationBookingView, "pnChooser");
							} else {
								if (crearActividad) {
									manager.checkConflictsWithTheNewActivityPuntual(getCalendarActivities().getDate(),
											Integer.parseInt((String) cbHourActivityInstallationI.getSelectedItem()),
											Integer.parseInt((String) cbHourActivityInstallationF.getSelectedItem()),
											currentInstallation);
									fillUsuariosALosQueAvisar();
									card.show(pnBase, "pnActividadPeriodica");
									cardPeriodica.show(pnActividadPeriodicaCrear, "avisarTlfnos");
								}
							}
						}
					}
				}

			});
			btnNextActivityInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnNextActivityInstal;
	}

	private void initializeAdmToActivityBooking() {
		currentActivities = "";
		currentInstallation = "";
		// getTxtHourActivityEnd().setText("");
		// getTxtHoraActivityInicio().setText("");
	}

	// Saul
	/**
	 * method to check if datas are correct.
	 * 
	 * @return
	 */
	private boolean currentData() {
		String error = "";
		boolean correct = true;
		if (currentActivities.equals("")) {
			error += "No has seleccionado ninguna actividad \n";
			correct = false;
		}
		if (!cbHourActivityInstallationF.isEnabled()) {
			error += "No has seleccionado las horas de inicio y fin \n";
			correct = false;
		}

		if (currentInstallation.equals("")) {
			error += "No hay ninguna instalaci�n seleccionada";
			correct = false;
		}

		if (!error.equals("")) {
			JOptionPane.showMessageDialog(null, error, "Fallo al reservar", JOptionPane.ERROR_MESSAGE);
			correct = false;
		}

		return correct;
	}

	// Saul
	private JLabel getLblHourActivityEnd() {
		if (lblHourActivityEnd == null) {
			lblHourActivityEnd = new JLabel("Hora fin:");
			lblHourActivityEnd.setHorizontalAlignment(SwingConstants.RIGHT);
			lblHourActivityEnd.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblHourActivityEnd;
	}

	private JLabel getLbBookingInstallation() {
		if (lbBookingInstallation == null) {
			lbBookingInstallation = new JLabel("");
			lbBookingInstallation.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
		}
		return lbBookingInstallation;
	}

	// Fin Saul.

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora fin:");
			lblHoraFin.setHorizontalAlignment(SwingConstants.RIGHT);
			lblHoraFin.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblHoraFin;
	}

	// Gonzi
	// private JTextField getTxtHourMemberEnd() {
	// if (txtHourMemberEnd == null) {
	// txtHourMemberEnd = new JTextField();
	// txtHourMemberEnd.addKeyListener(new KeyAdapter() {
	// public void keyTyped(KeyEvent e) {
	// char caracter = e.getKeyChar();
	// if (((caracter < '0') || (caracter > '9'))
	// && (caracter != '\b' /* corresponde a BACK_SPACE */)
	// || (txtHourMemberEnd.getText().length() > 1)) {
	// e.consume();
	// }
	// }
	// });
	// txtHourMemberEnd.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
	// txtHourMemberEnd.setColumns(10);
	// }
	// return txtHourMemberEnd;
	// }

	// Saul
	private JPanel getPnIdent() {
		if (pnIdent == null) {
			pnIdent = new JPanel();
			pnIdent.setBackground(Color.WHITE);
			GridBagLayout gbl_pnIdent = new GridBagLayout();
			gbl_pnIdent.columnWidths = new int[] { 99, 1, 3, 0 };
			gbl_pnIdent.rowHeights = new int[] { 23, 19, 23, 0 };
			gbl_pnIdent.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
			gbl_pnIdent.rowWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnIdent.setLayout(gbl_pnIdent);
			GridBagConstraints gbc_btnMonitor = new GridBagConstraints();
			gbc_btnMonitor.fill = GridBagConstraints.BOTH;
			gbc_btnMonitor.insets = new Insets(0, 0, 5, 5);
			gbc_btnMonitor.gridx = 0;
			gbc_btnMonitor.gridy = 0;
			pnIdent.add(getBtnMonitor(), gbc_btnMonitor);
			// pnIdent.add(getBtnIdentificarse(), BorderLayout.WEST);
			GridBagConstraints gbc_btnAdmin = new GridBagConstraints();
			gbc_btnAdmin.fill = GridBagConstraints.VERTICAL;
			gbc_btnAdmin.anchor = GridBagConstraints.WEST;
			gbc_btnAdmin.insets = new Insets(0, 0, 5, 0);
			gbc_btnAdmin.gridx = 2;
			gbc_btnAdmin.gridy = 0;
			pnIdent.add(getBtnAdmin(), gbc_btnAdmin);
			GridBagConstraints gbc_btnContable = new GridBagConstraints();
			gbc_btnContable.fill = GridBagConstraints.BOTH;
			gbc_btnContable.insets = new Insets(0, 0, 0, 5);
			gbc_btnContable.gridx = 0;
			gbc_btnContable.gridy = 2;
			pnIdent.add(getBtnContable(), gbc_btnContable);
			GridBagConstraints gbc_btnUsuario = new GridBagConstraints();
			gbc_btnUsuario.fill = GridBagConstraints.BOTH;
			gbc_btnUsuario.gridx = 2;
			gbc_btnUsuario.gridy = 2;
			pnIdent.add(getBtnUsuario(), gbc_btnUsuario);
		}
		return pnIdent;
	}

	// Saul
	private JButton getBtnAdmin() {
		if (btnAdmin == null) {
			btnAdmin = new JButton("Administrador");
			btnAdmin.setFont(new Font("Tw Cen MT", Font.PLAIN, 13));
			btnAdmin.setBackground(Color.WHITE);
			btnAdmin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					getPnAcciones().removeAll();
					getPnAcciones().repaint();
					getPnAcciones().paintAll(pnAcciones.getGraphics());

					pnAcciones.add(getBtnInformacinSobreLas());

					getPnAcciones().remove(getBtnVerListaAsistentes());
					btnVerListaAsistentes.setEnabled(false);
					btnVerListaAsistentes.setVisible(false);

					getPnAcciones().add(getBtnRegistroDeEntradasalida());
					btnRegistroDeEntradasalida.setEnabled(true);
					btnRegistroDeEntradasalida.setVisible(true);

					// getPnAcciones().remove(getBtnVerActividadesAsignadas());
					// getBtnVerActividadesAsignadas().setEnabled(false);
					// getBtnVerActividadesAsignadas().setVisible(false);

					getPnAcciones().add(getBtnEstadoInstalaciones());
					btnEstadoInstalaciones.setEnabled(true);
					btnEstadoInstalaciones.setVisible(true);

					getPnAcciones().add(getBtnHorariosActividadCentro());
					btnVerHorarioAct.setEnabled(true);
					btnVerHorarioAct.setVisible(true);

					getPnAcciones().remove(getBtnHorariosActividadMonitor());
					getBtnHorariosActividadMonitor().setEnabled(false);
					getBtnHorariosActividadMonitor().setVisible(false);

					getPnAcciones().remove(getBtnVerActividades());
					btnVerActividades.setEnabled(false);
					btnVerActividades.setVisible(false);

					btnBooking24h.setVisible(true);
					btnBooking24h.setEnabled(true);

					getPnAcciones().add(getBtnCrearNuevaActividad());
					btnCrearNuevaActividad.setEnabled(true);
					btnCrearNuevaActividad.setVisible(true);

					getRdbtnAdminactivityinstallation().setVisible(false);
					getRdbtnAdminactivityinstallation().setEnabled(false);

					getPnAcciones().remove(getBtnVerMisReservas());
					getBtnVerMisReservas().setEnabled(false);
					getBtnVerMisReservas().setVisible(false);

					getPnAcciones().remove(getBtnVerMisActividades());
					btnVerMisActividades.setEnabled(false);
					btnVerMisActividades.setVisible(false);

					getPnAcciones().remove(getTransferirACuota());
					btnTranferirACuota.setEnabled(false);
					btnTranferirACuota.setVisible(false);

					getPnAcciones().add(getBtnApuntarSocioActividad());
					getBtnApuntarSocioActividad().setEnabled(true);
					getBtnApuntarSocioActividad().setVisible(true);

					// getPnAcciones().add(getBtnCancelarActividad_1());
					// getBtnCancelarActividad_1().setEnabled(true);
					// getBtnCancelarActividad_1().setVisible(true);

					getPnAcciones().add(getVerActividadesDeUnDia());
					getVerActividadesDeUnDia().setEnabled(true);
					getVerActividadesDeUnDia().setVisible(true);

					calendar.setMinSelectableDate(null);

					getBtnGoToCalendar().setEnabled(true);
					getBtnGoToCalendar().setVisible(true);
					getPnAcciones().add(getBtnGoToCalendar());

					getPnAcciones().add(getBtnDesapuntarSocioActividad());
					getBtnDesapuntarSocioActividad().setEnabled(true);
					getBtnDesapuntarSocioActividad().setVisible(true);

					signIn("admin95874563C", "admin");
					// clearCancelarActi();

				}
			});
		}
		return btnAdmin;
	}

	// Saul
	private JButton getBtnUsuario() {
		if (btnUsuario == null) {
			btnUsuario = new JButton("Usuario");
			btnUsuario.setFont(new Font("Tw Cen MT", Font.PLAIN, 13));
			btnUsuario.setBackground(Color.WHITE);
			btnUsuario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					getPnAcciones().removeAll();
					getPnAcciones().repaint();
					getPnAcciones().paintAll(pnAcciones.getGraphics());

					pnAcciones.add(getBtnInformacinSobreLas());

					getPnAcciones().remove(getBtnRegistroDeEntradasalida());
					btnRegistroDeEntradasalida.setEnabled(false);
					btnRegistroDeEntradasalida.setVisible(false);

					getPnAcciones().remove(getBtnEstadoInstalaciones());
					btnEstadoInstalaciones.setEnabled(false);
					btnEstadoInstalaciones.setVisible(false);

					getPnAcciones().add(getBtnVerActividades());
					btnVerActividades.setEnabled(true);
					btnVerActividades.setVisible(true);

					getPnAcciones().remove(getBtnHorariosActividadCentro());
					btnVerHorarioAct.setEnabled(false);
					btnVerHorarioAct.setVisible(false);

					getPnAcciones().remove(getBtnHorariosActividadMonitor());
					getBtnHorariosActividadMonitor().setEnabled(false);
					getBtnHorariosActividadMonitor().setVisible(false);

					getPnAcciones().remove(getVerActividadesDeUnDia());
					getVerActividadesDeUnDia().setEnabled(false);
					getVerActividadesDeUnDia().setVisible(false);

					// getPnAcciones().remove(getBtnVerActividadesAsignadas());
					// getBtnVerActividadesAsignadas().setEnabled(false);
					// getBtnVerActividadesAsignadas().setVisible(false);

					getPnAcciones().remove(getBtnCrearNuevaActividad());
					btnCrearNuevaActividad.setEnabled(false);
					btnCrearNuevaActividad.setVisible(false);

					getRdbtnAdminactivityinstallation().setVisible(false);
					getRdbtnAdminactivityinstallation().setEnabled(false);

					calendar.setMinSelectableDate(new Date());

					getPnAcciones().add(getBtnVerMisReservas());
					getBtnVerMisReservas().setEnabled(true);
					getBtnVerMisReservas().setVisible(true);

					getPnAcciones().add(getBtnVerMisActividades());
					btnVerMisActividades.setEnabled(true);
					btnVerMisActividades.setVisible(true);

					getPnAcciones().remove(getTransferirACuota());
					btnTranferirACuota.setEnabled(false);
					btnTranferirACuota.setVisible(false);

					getPnAcciones().remove(getBtnApuntarSocioActividad());
					getBtnApuntarSocioActividad().setEnabled(false);
					getBtnApuntarSocioActividad().setVisible(false);

					getPnAcciones().add(getBtnGoToCalendar());
					getBtnGoToCalendar().setEnabled(true);
					getBtnGoToCalendar().setVisible(true);

					getPnAcciones().remove(getBtnVerListaAsistentes());
					btnVerListaAsistentes.setEnabled(false);
					btnVerListaAsistentes.setVisible(false);

					// getPnAcciones().remove(getBtnCancelarActividad_1());
					// getBtnCancelarActividad_1().setEnabled(false);
					// getBtnCancelarActividad_1().setVisible(false);

					// Alvaro
					getPnAcciones().add(getBtnApuntarSocioActividad());
					getBtnApuntarSocioActividad().setEnabled(true);
					getBtnApuntarSocioActividad().setVisible(true);

					getPnAcciones().add(getBtnDesapuntarSocioActividad());
					getBtnDesapuntarSocioActividad().setEnabled(true);
					getBtnDesapuntarSocioActividad().setVisible(true);

					signIn("56565656A", "user");
				}
			});
		}
		return btnUsuario;
	}

	// Saul
	/**
	 * This method is used to sign in to the sell point
	 */
	protected void signIn(String usuario, String password) {
		if (manager.signIn(usuario, password)) {
			card.show(pnBase, "pnHomeView");
			resetAreasSignIn();
			// btnIdentificarse.setText("Salir");
			btnGoToCalendar.setEnabled(true);
			btnRegistroDeEntradasalida.setEnabled(true);
		}
	}

	private JButton getBtnBackAdminInstal() {
		if (btnBackAdminInstal == null) {
			btnBackAdminInstal = new JButton();
			btnBackAdminInstal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cardInstallation.show(pnInstallationBookingView, "pnChooser");
				}
			});
			btnBackAdminInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackAdminInstal.setBackground(pnNotMemberInstallationBooking.getBackground());
			btnBackAdminInstal.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackAdminInstal, "/img/arrow.png");
			btnBackAdminInstal.setBorder(null);
		}
		return btnBackAdminInstal;
	}

	// Alvaro
	private boolean bookInstallationNotMember(String dni, String name, int phone, String surName) {
		if (manager.InsertUsers(dni, name, phone, surName))
			return true;
		return false;
	}

	private boolean notEmptyField() {
		if (txtNameNotMemberInstal.getText().equals("") || txtSurnameNotMemberInstal.getText().equals("")
				|| txtDNINotMemberInstal.equals("") || txtTelephoneNotMemberInstal.equals("")
		// ||txtHoraNotMemberInstali.equals("")||txtHoraNotMemberInstalF.equals("")
		)
			return false;
		return true;
	}

	private boolean isDateValid() {
		// if(Integer.parseInt(txtHoraNotMemberInstali.getText()) < 0 ||
		// Integer.parseInt(txtHoraNotMemberInstali.getText()) >23 ||
		// Integer.parseInt(txtHoraNotMemberInstalF.getText()) < 0 ||
		// Integer.parseInt(txtHoraNotMemberInstalF.getText()) >23)
		if (!getCbHourInstallationNotMemberF().isEnabled())
			return false;
		return true;
	}

	private JLabel getLblHoraFinal() {
		if (lblHoraFinal == null) {
			lblHoraFinal = new JLabel("Hora Final:");
			lblHoraFinal.setHorizontalAlignment(SwingConstants.RIGHT);
			lblHoraFinal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblHoraFinal;
	}

	// private JTextField getTxtHoraNotMemberInstalF() {
	// if (txtHoraNotMemberInstalF == null) {
	// txtHoraNotMemberInstalF = new JTextField();
	// txtHoraNotMemberInstalF.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
	// txtHoraNotMemberInstalF.setColumns(10);
	// txtHoraNotMemberInstalF.addKeyListener(new KeyAdapter() {
	// public void keyTyped(KeyEvent e) {
	// char caracter = e.getKeyChar();
	// if (((caracter < '0') || (caracter > '9'))
	// && (caracter != '\b' /* corresponde a BACK_SPACE */)
	// || (txtHoraNotMemberInstalF.getText().length() > 1)) {
	// e.consume();
	// }
	// }
	// });
	// }
	// return txtHoraNotMemberInstalF;
	// }

	// Saul
	private JLabel getLblTipoDePago() {
		if (lblTipoDePago == null) {
			lblTipoDePago = new JLabel("Tipo de pago:");
			lblTipoDePago.setFont(new Font("Tw Cen MT Condensed", Font.BOLD, 17));
		}
		return lblTipoDePago;
	}

	// Saul
	private JRadioButton getRdbtnPagoConMensualidad() {
		if (rdbtnPagoConMensualidad == null) {
			rdbtnPagoConMensualidad = new JRadioButton("Pago con mensualidad.");
			rdbtnPagoConMensualidad.setBackground(Color.WHITE);
			rdbtnPagoConMensualidad.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
		}
		return rdbtnPagoConMensualidad;
	}

	// Saul
	private JRadioButton getRdbtnPagoAlEntrar() {
		if (rdbtnPagoAlEntrar == null) {
			rdbtnPagoAlEntrar = new JRadioButton("Pago en efectivo al entrar.");
			rdbtnPagoAlEntrar.setBackground(Color.WHITE);
			rdbtnPagoAlEntrar.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
		}
		return rdbtnPagoAlEntrar;
	}

	private JComboBox<String> getCbHourActivityInstallationI() {
		if (cbHourActivityInstallationI == null) {
			cbHourActivityInstallationI = new JComboBox<String>();
			generarHoras(cbHourActivityInstallationI, 0, 23);
			cbHourActivityInstallationI.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					generarHoras(cbHourActivityInstallationF,
							Integer.parseInt((String) cbHourActivityInstallationI.getSelectedItem()) + 1, 23);
					cbHourActivityInstallationF.setEnabled(true);
				}
			});
		}
		return cbHourActivityInstallationI;
	}

	private JComboBox<String> getCbHourActivityInstallationF() {
		if (cbHourActivityInstallationF == null) {
			cbHourActivityInstallationF = new JComboBox<String>();
			cbHourActivityInstallationF.setEnabled(false);
		}
		return cbHourActivityInstallationF;
	}

	private void generarHoras(JComboBox<String> combo, int inicio, int end) {
		ArrayList<String> horas = new ArrayList<String>();
		for (int i = inicio; i <= end; i++) {
			if (i == 24)
				horas.add(String.valueOf(0));
			else if (i > 24)
				break;
			else
				horas.add(String.valueOf(i));
		}
		if (end == 23)
			horas.add("0");
		String[] model = horas.toArray(new String[horas.size()]);
		combo.setModel(new DefaultComboBoxModel<String>(model));
	}

	private JComboBox<String> getCbHourInstallationMemberS() {
		if (cbHourInstallationMemberS == null) {
			cbHourInstallationMemberS = new JComboBox<String>();
			generarHoras(cbHourInstallationMemberS, 0, 23);
			cbHourInstallationMemberS.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					generarHoras(cbHourInstallationMemberF,
							Integer.parseInt((String) cbHourInstallationMemberS.getSelectedItem()) + 1,
							Integer.parseInt((String) cbHourInstallationMemberS.getSelectedItem()) + 2);
					cbHourInstallationMemberF.setEnabled(true);
				}
			});
		}
		return cbHourInstallationMemberS;
	}

	private JComboBox<String> getCbHourInstallationMemberF() {
		if (cbHourInstallationMemberF == null) {
			cbHourInstallationMemberF = new JComboBox<String>();
			cbHourInstallationMemberF.setEnabled(false);
		}
		return cbHourInstallationMemberF;
	}

	private JComboBox<String> getCbHourInstallationNotMemberI() {
		if (cbHourInstallationNotMemberI == null) {
			cbHourInstallationNotMemberI = new JComboBox<String>();
			generarHoras(cbHourInstallationNotMemberI, 0, 23);
			cbHourInstallationNotMemberI.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					generarHoras(cbHourInstallationNotMemberF,
							Integer.parseInt((String) cbHourInstallationNotMemberI.getSelectedItem()) + 1,
							Integer.parseInt((String) cbHourInstallationNotMemberI.getSelectedItem()) + 2);
					cbHourInstallationNotMemberF.setEnabled(true);
				}
			});
		}
		return cbHourInstallationNotMemberI;
	}

	private JComboBox<String> getCbHourInstallationNotMemberF() {
		if (cbHourInstallationNotMemberF == null) {
			cbHourInstallationNotMemberF = new JComboBox<String>();
			cbHourInstallationNotMemberF.setEnabled(false);
		}
		return cbHourInstallationNotMemberF;
	}

	// Gonzi
	private JButton getBtnRegistroDeEntradasalida() {
		if (btnRegistroDeEntradasalida == null) {
			btnRegistroDeEntradasalida = new JButton("Registro de entrada/salida");
			btnRegistroDeEntradasalida.setBackground(Color.WHITE);
			btnRegistroDeEntradasalida.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnRegistroDeEntradasalida.setVisible(false);
			btnRegistroDeEntradasalida.setEnabled(false);
			btnRegistroDeEntradasalida.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnBase, "pnRegister");
					btnRegisterInputOuput.setEnabled(false);
					getTxtDniRegister().setText("");
				}
			});
		}
		return btnRegistroDeEntradasalida;
	}

	// Saul
	private JPanel getPnRegisterInputOutput() {
		if (pnRegisterInputOutput == null) {
			pnRegisterInputOutput = new JPanel();
			pnRegisterInputOutput.setBackground(Color.WHITE);
			GridBagLayout gbl_pnRegisterInputOutput = new GridBagLayout();
			gbl_pnRegisterInputOutput.columnWidths = new int[] { 143, 81, 17, 6, 35, 90, 0, 0, 0 };
			gbl_pnRegisterInputOutput.rowHeights = new int[] { 77, 40, 40, 40, 40, 23, 39, 0, 23, 0 };
			gbl_pnRegisterInputOutput.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			gbl_pnRegisterInputOutput.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnRegisterInputOutput.setLayout(gbl_pnRegisterInputOutput);
			GridBagConstraints gbc_lblDniRegister = new GridBagConstraints();
			gbc_lblDniRegister.anchor = GridBagConstraints.EAST;
			gbc_lblDniRegister.insets = new Insets(0, 0, 5, 5);
			gbc_lblDniRegister.gridx = 1;
			gbc_lblDniRegister.gridy = 1;
			pnRegisterInputOutput.add(getLblDniRegister(), gbc_lblDniRegister);
			GridBagConstraints gbc_txtDniRegister = new GridBagConstraints();
			gbc_txtDniRegister.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDniRegister.insets = new Insets(0, 0, 5, 5);
			gbc_txtDniRegister.gridwidth = 3;
			gbc_txtDniRegister.gridx = 2;
			gbc_txtDniRegister.gridy = 1;
			pnRegisterInputOutput.add(getTxtDniRegister(), gbc_txtDniRegister);
			GridBagConstraints gbc_lblInstallationToInputOutput = new GridBagConstraints();
			gbc_lblInstallationToInputOutput.anchor = GridBagConstraints.EAST;
			gbc_lblInstallationToInputOutput.insets = new Insets(0, 0, 5, 5);
			gbc_lblInstallationToInputOutput.gridx = 1;
			gbc_lblInstallationToInputOutput.gridy = 2;
			pnRegisterInputOutput.add(getLblInstallationToInputOutput(), gbc_lblInstallationToInputOutput);
			GridBagConstraints gbc_cBInstallationToInputOutput = new GridBagConstraints();
			gbc_cBInstallationToInputOutput.gridwidth = 3;
			gbc_cBInstallationToInputOutput.insets = new Insets(0, 0, 5, 5);
			gbc_cBInstallationToInputOutput.fill = GridBagConstraints.HORIZONTAL;
			gbc_cBInstallationToInputOutput.gridx = 2;
			gbc_cBInstallationToInputOutput.gridy = 2;
			pnRegisterInputOutput.add(getCBInstallationToInputOutput(), gbc_cBInstallationToInputOutput);
			GridBagConstraints gbc_lblTipoDeRegistro = new GridBagConstraints();
			gbc_lblTipoDeRegistro.anchor = GridBagConstraints.EAST;
			gbc_lblTipoDeRegistro.insets = new Insets(0, 0, 5, 5);
			gbc_lblTipoDeRegistro.gridx = 1;
			gbc_lblTipoDeRegistro.gridy = 3;
			pnRegisterInputOutput.add(getLblTipoDeRegistro(), gbc_lblTipoDeRegistro);
			GridBagConstraints gbc_rdbtnEntrada = new GridBagConstraints();
			gbc_rdbtnEntrada.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnEntrada.gridx = 2;
			gbc_rdbtnEntrada.gridy = 3;
			pnRegisterInputOutput.add(getRdbtnEntrada(), gbc_rdbtnEntrada);
			GridBagConstraints gbc_rdbtnSalida = new GridBagConstraints();
			gbc_rdbtnSalida.gridwidth = 2;
			gbc_rdbtnSalida.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnSalida.gridx = 3;
			gbc_rdbtnSalida.gridy = 3;
			pnRegisterInputOutput.add(getRdbtnSalida(), gbc_rdbtnSalida);
			GridBagConstraints gbc_lblHoraDeReserva = new GridBagConstraints();
			gbc_lblHoraDeReserva.anchor = GridBagConstraints.EAST;
			gbc_lblHoraDeReserva.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraDeReserva.gridx = 1;
			gbc_lblHoraDeReserva.gridy = 4;
			pnRegisterInputOutput.add(getLblHoraDeReserva(), gbc_lblHoraDeReserva);
			GridBagConstraints gbc_cBHourBookingToInputOutput = new GridBagConstraints();
			gbc_cBHourBookingToInputOutput.fill = GridBagConstraints.HORIZONTAL;
			gbc_cBHourBookingToInputOutput.insets = new Insets(0, 0, 5, 5);
			gbc_cBHourBookingToInputOutput.gridx = 2;
			gbc_cBHourBookingToInputOutput.gridy = 4;
			pnRegisterInputOutput.add(getCBHourBookingToInputOutput(), gbc_cBHourBookingToInputOutput);
			groupRegisterInputOutput.add(getRdbtnEntrada());
			groupRegisterInputOutput.add(getRdbtnSalida());
			GridBagConstraints gbc_btnRegisterInputOuput = new GridBagConstraints();
			gbc_btnRegisterInputOuput.fill = GridBagConstraints.VERTICAL;
			gbc_btnRegisterInputOuput.insets = new Insets(0, 0, 5, 5);
			gbc_btnRegisterInputOuput.gridwidth = 2;
			gbc_btnRegisterInputOuput.gridx = 2;
			gbc_btnRegisterInputOuput.gridy = 7;
			pnRegisterInputOutput.add(getBtnRegisterInputOuput(), gbc_btnRegisterInputOuput);
		}
		return pnRegisterInputOutput;
	}

	// Gonzi
	private JLabel getLblDniRegister() {
		if (lblDniRegister == null) {
			lblDniRegister = new JLabel("DNI:");
			lblDniRegister.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblDniRegister;
	}

	// Gonzi
	private JTextField getTxtDniRegister() {
		if (txtDniRegister == null) {
			txtDniRegister = new JTextField();
			txtDniRegister.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent a) {
					char c = a.getKeyChar();
					if (Character.isWhitespace(c) || (countNumbersDNI(txtDniRegister) == 8 && Character.isDigit(c))
							|| (countLettersDNI(txtDniRegister) == 1 && Character.isLetter(c))
							|| notCorrectDNI(txtDniRegister)) {
						a.consume();
					}
				}
			});
			txtDniRegister.setColumns(10);
		}
		return txtDniRegister;
	}

	// Gonzi
	private JLabel getLblInstallationToInputOutput() {
		if (lblInstallationToInputOutput == null) {
			lblInstallationToInputOutput = new JLabel("Instalaci\u00F3n:");
			lblInstallationToInputOutput.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblInstallationToInputOutput;
	}

	// Gonzi
	private JRadioButton getRdbtnEntrada() {
		if (rdbtnEntrada == null) {
			rdbtnEntrada = new JRadioButton("Entrada");
			rdbtnEntrada.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			rdbtnEntrada.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnRegisterInputOuput.setEnabled(true);
				}
			});
			rdbtnEntrada.setBackground(Color.WHITE);
		}
		return rdbtnEntrada;
	}

	// Gonzi
	private JRadioButton getRdbtnSalida() {
		if (rdbtnSalida == null) {
			rdbtnSalida = new JRadioButton("Salida");
			rdbtnSalida.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			rdbtnSalida.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnRegisterInputOuput.setEnabled(true);
				}
			});
			rdbtnSalida.setBackground(Color.WHITE);
		}
		return rdbtnSalida;
	}

	// Saul
	private JButton getBtnRegisterInputOuput() {
		if (btnRegisterInputOuput == null) {
			btnRegisterInputOuput = new JButton("Registrar");
			btnRegisterInputOuput.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnRegisterInputOuput.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (checkFieldsInputOutput()) {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						String fecha = df.format(new Date());
						String id_reserva = manager.checkifBookingExists(
								(String) cBInstallationToInputOutput.getSelectedItem(), txtDniRegister.getText(),
								Integer.parseInt((String) cBHourBookingToInputOutput.getSelectedItem()), fecha);
						if (id_reserva != null) {
							JOptionPane.showMessageDialog(null, "Se ha encontrado la reserva", "",
									JOptionPane.INFORMATION_MESSAGE);
							if (getRdbtnEntrada().isSelected()) {
								if (manager.bookingInput(id_reserva, getSystemHour())) {
									if (manager.isUserInput(txtDniRegister.getText())) {
										if (manager.doesUserHaveToPayCash(id_reserva)) {
											manager.userPayInput(id_reserva);
											int money = Integer.parseInt(JOptionPane.showInputDialog(contentPane,
													"Dinero entregado por el cliente"));
											txtpnByeMessage.setText("Ticket de pago:");
											showBillAfterMemberPayedCash(money, id_reserva);
										}
									}
									JOptionPane.showMessageDialog(null,
											"El registro de la entrada se ha realizado correctamente", "",
											JOptionPane.INFORMATION_MESSAGE);
								} else {
									JOptionPane.showMessageDialog(null,
											"No se ha podido completar el registro de la entrada\nPuede que esta entrada"
													+ " ya haya sido registrada antes",
											"Error al reservar", JOptionPane.ERROR_MESSAGE);
								}
							} else if (getRdbtnSalida().isSelected()) {
								if (manager.bookingOutput(id_reserva, getSystemHour())) {
									JOptionPane.showMessageDialog(null,
											"El registro de la salida se ha realizado correctamente", "",
											JOptionPane.INFORMATION_MESSAGE);
									card.show(pnBase, "pnHomeView");
								} else {
									JOptionPane.showMessageDialog(null,
											"No se ha podido completar el registro de la salida\nPuede que no se haya "
													+ "registrado la entrada",
											"Error al reservar", JOptionPane.ERROR_MESSAGE);

								}
							}
						} else
							JOptionPane.showMessageDialog(null, "No se ha encontrado la reserva", "Error",
									JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnRegisterInputOuput.setEnabled(false);
		}
		return btnRegisterInputOuput;
	}

	protected void showBillAfterMemberPayedCash(int money, String id_reserva) {
		// guardar la nueva factura en la bbdd, coger la que hay, y a�adir
		// pago:
		// dinero, y vuelta: dinero
		manager.saveBillAfterMemberPayedCash(money, id_reserva);
		txtpnAllBill.setText(manager.showBillInDatabase((String) cBInstallationToInputOutput.getSelectedItem(),
				new Date(), Integer.parseInt((String) cBHourBookingToInputOutput.getSelectedItem())));
		btnSaveTheBill.setVisible(false);
		card.show(pnBase, "pnBill");

	}

	private int getSystemHour() {
		Calendar calendario = Calendar.getInstance();
		return calendario.get(Calendar.HOUR_OF_DAY);
	}

	// Saul
	private boolean checkFieldsInputOutput() {
		if (getTxtDniRegister().getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "No se han rellenado todos los campos de texto", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	// Saul
	private String generateCode() {
		String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String codigo = "";
		int longitudCodigo = 10;
		for (int i = 0; i < longitudCodigo; i++) {
			int numero = (int) (Math.random() * caracteres.length());
			codigo += caracteres.charAt(numero);
		}
		return codigo;
	}

	// Saul
	private JComboBox<String> getCBInstallationToInputOutput() {
		if (cBInstallationToInputOutput == null) {
			cBInstallationToInputOutput = new JComboBox<String>();
			cBInstallationToInputOutput.setBackground(Color.WHITE);
			String[] model = manager.getInstallations().toArray(new String[manager.getInstallations().size()]);
			cBInstallationToInputOutput.setModel(new DefaultComboBoxModel<String>(model));
		}
		return cBInstallationToInputOutput;
	}

	// Saul
	private JLabel getLblHoraDeReserva() {
		if (lblHoraDeReserva == null) {
			lblHoraDeReserva = new JLabel("Hora de reserva:");
			lblHoraDeReserva.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblHoraDeReserva;
	}

	// Saul
	private JComboBox<String> getCBHourBookingToInputOutput() {
		if (cBHourBookingToInputOutput == null) {
			cBHourBookingToInputOutput = new JComboBox<String>();
			cBHourBookingToInputOutput.setBackground(Color.WHITE);
			generarHoras(cBHourBookingToInputOutput, 1, 23);
		}
		return cBHourBookingToInputOutput;
	}

	// Saul
	private JLabel getLblTipoDeRegistro() {
		if (lblTipoDeRegistro == null) {
			lblTipoDeRegistro = new JLabel("Tipo de Registro:");
			lblTipoDeRegistro.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblTipoDeRegistro;
	}

	// Gonzii
	private JButton getBtnEstadoInstalaciones() {
		if (btnEstadoInstalaciones == null) {
			btnEstadoInstalaciones = new JButton("Estado de las instalaciones");
			btnEstadoInstalaciones.setBackground(Color.WHITE);
			btnEstadoInstalaciones.setEnabled(false);
			btnEstadoInstalaciones.setVisible(false);
			btnEstadoInstalaciones.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnEstadoInstalaciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtDatosOcupante.setText("");
					card.show(pnBase, "pnEstado");
				}
			});
		}
		return btnEstadoInstalaciones;
	}

	// Gonzi
	private JPanel getPnEstadoInstallation() {
		if (pnEstadoInstallation == null) {
			pnEstadoInstallation = new JPanel();
			pnEstadoInstallation.setBackground(Color.WHITE);
			GridBagLayout gbl_pnEstadoInstallation = new GridBagLayout();
			gbl_pnEstadoInstallation.columnWidths = new int[] { 129, 127, 19, 172, 108, 0, 0, 0 };
			gbl_pnEstadoInstallation.rowHeights = new int[] { 148, 44, 19, 114, 0, 0 };
			gbl_pnEstadoInstallation.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			gbl_pnEstadoInstallation.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnEstadoInstallation.setLayout(gbl_pnEstadoInstallation);
			GridBagConstraints gbc_lblEstadoInstalacion = new GridBagConstraints();
			gbc_lblEstadoInstalacion.fill = GridBagConstraints.VERTICAL;
			gbc_lblEstadoInstalacion.insets = new Insets(0, 0, 5, 5);
			gbc_lblEstadoInstalacion.gridx = 1;
			gbc_lblEstadoInstalacion.gridy = 1;
			pnEstadoInstallation.add(getLblEstadoInstalacion(), gbc_lblEstadoInstalacion);
			GridBagConstraints gbc_cbEstadoInstalacion = new GridBagConstraints();
			gbc_cbEstadoInstalacion.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbEstadoInstalacion.insets = new Insets(0, 0, 5, 5);
			gbc_cbEstadoInstalacion.gridx = 3;
			gbc_cbEstadoInstalacion.gridy = 1;
			pnEstadoInstallation.add(getCbEstadoInstalacion(), gbc_cbEstadoInstalacion);
			GridBagConstraints gbc_btnComprobar = new GridBagConstraints();
			gbc_btnComprobar.insets = new Insets(0, 0, 5, 5);
			gbc_btnComprobar.gridx = 4;
			gbc_btnComprobar.gridy = 1;
			pnEstadoInstallation.add(getBtnComprobar(), gbc_btnComprobar);
			GridBagConstraints gbc_txtDatosOcupante = new GridBagConstraints();
			gbc_txtDatosOcupante.insets = new Insets(0, 0, 5, 5);
			gbc_txtDatosOcupante.fill = GridBagConstraints.BOTH;
			gbc_txtDatosOcupante.gridx = 3;
			gbc_txtDatosOcupante.gridy = 3;
			pnEstadoInstallation.add(getTxtDatosOcupante(), gbc_txtDatosOcupante);

		}
		return pnEstadoInstallation;
	}

	private JLabel getLblEstadoInstalacion() {
		if (lblEstadoInstalacion == null) {
			lblEstadoInstalacion = new JLabel("Instalaci\u00F3n: ");
			lblEstadoInstalacion.setFont(new Font("Tw Cen MT", Font.BOLD, 18));
		}
		return lblEstadoInstalacion;
	}

	private JComboBox<String> getCbEstadoInstalacion() {
		if (cbEstadoInstalacion == null) {
			cbEstadoInstalacion = new JComboBox<String>();
			cbEstadoInstalacion.setBackground(Color.WHITE);
			String[] model = manager.getInstallations().toArray(new String[manager.getInstallations().size()]);
			cbEstadoInstalacion.setModel(new DefaultComboBoxModel<String>(model));
		}
		return cbEstadoInstalacion;
	}

	private JTextPane getTxtDatosOcupante() {
		if (txtDatosOcupante == null) {
			txtDatosOcupante = new JTextPane();
			txtDatosOcupante.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			txtDatosOcupante.setEditable(false);
		}
		return txtDatosOcupante;
	}

	private JButton getBtnBooking24h() {
		if (btnBooking24h == null) {
			btnBooking24h = new JButton("Seleccionar todo el d\u00EDa");
			btnBooking24h.setBackground(Color.WHITE);
			btnBooking24h.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					getCbHourActivityInstallationI().setSelectedItem("0");
					getCbHourActivityInstallationF().setEnabled(true);
					getCbHourActivityInstallationF().setSelectedItem("0");
				}
			});
			btnBooking24h.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnBooking24h.setVisible(false);
			btnBooking24h.setEnabled(false);
		}
		return btnBooking24h;
	}

	// Gonzi
	private JPanel getPnTicketDevolucion() {
		if (pnTicketDevolucion == null) {
			pnTicketDevolucion = new JPanel();
			pnTicketDevolucion.setBackground(pnBase.getBackground());
			GridBagLayout gbl_pnTicketDevolucion = new GridBagLayout();
			gbl_pnTicketDevolucion.columnWidths = new int[] { 33, 548, 33, 95, 0 };
			gbl_pnTicketDevolucion.rowHeights = new int[] { 67, 367, 44, 0 };
			gbl_pnTicketDevolucion.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnTicketDevolucion.rowWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnTicketDevolucion.setLayout(gbl_pnTicketDevolucion);
			GridBagConstraints gbc_lblTicketDeDevolucion = new GridBagConstraints();
			gbc_lblTicketDeDevolucion.anchor = GridBagConstraints.WEST;
			gbc_lblTicketDeDevolucion.fill = GridBagConstraints.VERTICAL;
			gbc_lblTicketDeDevolucion.insets = new Insets(0, 0, 5, 5);
			gbc_lblTicketDeDevolucion.gridx = 1;
			gbc_lblTicketDeDevolucion.gridy = 0;
			pnTicketDevolucion.add(getLblTicketDeDevolucion(), gbc_lblTicketDeDevolucion);
			GridBagConstraints gbc_txtPaneTicketDevolucion = new GridBagConstraints();
			gbc_txtPaneTicketDevolucion.fill = GridBagConstraints.BOTH;
			gbc_txtPaneTicketDevolucion.insets = new Insets(0, 0, 5, 5);
			gbc_txtPaneTicketDevolucion.gridx = 1;
			gbc_txtPaneTicketDevolucion.gridy = 1;
			pnTicketDevolucion.add(getTxtPaneTicketDevolucion(), gbc_txtPaneTicketDevolucion);
			GridBagConstraints gbc_btnFinalizarTicketDevolucion = new GridBagConstraints();
			gbc_btnFinalizarTicketDevolucion.fill = GridBagConstraints.BOTH;
			gbc_btnFinalizarTicketDevolucion.gridx = 3;
			gbc_btnFinalizarTicketDevolucion.gridy = 2;
			pnTicketDevolucion.add(getBtnFinalizarTicketDevolucion(), gbc_btnFinalizarTicketDevolucion);
		}
		return pnTicketDevolucion;
	}

	// Elena
	private JPanel getPnPayBye() {
		if (pnPayBye == null) {
			pnPayBye = new JPanel();
			pnPayBye.setBackground(pnBase.getBackground());
			pnPayBye.setLayout(new BorderLayout(0, 0));
			pnPayBye.add(getPnByeAlways(), BorderLayout.CENTER);
			pnPayBye.add(getPnByeSouth(), BorderLayout.SOUTH);
		}
		return pnPayBye;
	}

	private JPanel getPnByeAlways() {
		if (pnByeAlways == null) {
			pnByeAlways = new JPanel();
			pnByeAlways.setBackground(pnPayBye.getBackground());
			GridBagLayout gbl_pnByeAlways = new GridBagLayout();
			gbl_pnByeAlways.columnWidths = new int[] { 0, 0, 0, 796, 0, 0, 0, 0 };
			gbl_pnByeAlways.rowHeights = new int[] { 0, 68, 268, 0, 0 };
			gbl_pnByeAlways.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnByeAlways.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnByeAlways.setLayout(gbl_pnByeAlways);
			GridBagConstraints gbc_txtpnByeMessage = new GridBagConstraints();
			gbc_txtpnByeMessage.gridwidth = 3;
			gbc_txtpnByeMessage.fill = GridBagConstraints.BOTH;
			gbc_txtpnByeMessage.insets = new Insets(0, 0, 5, 5);
			gbc_txtpnByeMessage.gridx = 2;
			gbc_txtpnByeMessage.gridy = 1;
			pnByeAlways.add(getTxtpnByeMessage(), gbc_txtpnByeMessage);
			GridBagConstraints gbc_scrollBill = new GridBagConstraints();
			gbc_scrollBill.insets = new Insets(0, 0, 5, 5);
			gbc_scrollBill.fill = GridBagConstraints.BOTH;
			gbc_scrollBill.gridx = 3;
			gbc_scrollBill.gridy = 2;
			pnByeAlways.add(getScrollBill(), gbc_scrollBill);
		}
		return pnByeAlways;
	}

	private JScrollPane getScrollBill() {
		if (scrollBill == null) {
			scrollBill = new JScrollPane();
			scrollBill.setBackground(pnPayBye.getBackground());
			scrollBill.setViewportView(getTxtpnAllBill());
		}
		return scrollBill;
	}

	private JTextPane getTxtpnByeMessage() {
		if (txtpnByeMessage == null) {
			txtpnByeMessage = new JTextPane();
			txtpnByeMessage.setText("Ticket de reserva:");
			txtpnByeMessage.setForeground(new Color(255, 0, 0));
			txtpnByeMessage.setFont(new Font("Tw Cen MT", Font.BOLD, 40));
			txtpnByeMessage.setEditable(false);
		}
		return txtpnByeMessage;
	}

	private JTextPane getTxtpnAllBill() {
		if (txtpnAllBill == null) {
			txtpnAllBill = new JTextPane();
			txtpnAllBill.setFont(new Font("Tw Cen MT", Font.PLAIN, 18));
			txtpnAllBill.setEditable(false);
		}
		return txtpnAllBill;
	}

	private JPanel getPnByeSouth() {
		if (pnByeSouth == null) {
			pnByeSouth = new JPanel();
			pnByeSouth.setBackground(pnPayBye.getBackground());
			GridBagLayout gbl_pnByeSouth = new GridBagLayout();
			gbl_pnByeSouth.columnWidths = new int[] { 0, 78, 300, 122, 119, 25, 0 };
			gbl_pnByeSouth.rowHeights = new int[] { 27, 0, 0 };
			gbl_pnByeSouth.columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnByeSouth.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
			pnByeSouth.setLayout(gbl_pnByeSouth);
			GridBagConstraints gbc_pnSaveBill = new GridBagConstraints();
			gbc_pnSaveBill.gridwidth = 2;
			gbc_pnSaveBill.fill = GridBagConstraints.HORIZONTAL;
			gbc_pnSaveBill.insets = new Insets(0, 0, 5, 5);
			gbc_pnSaveBill.gridx = 1;
			gbc_pnSaveBill.gridy = 0;
			pnByeSouth.add(getPnSaveBill(), gbc_pnSaveBill);
			GridBagConstraints gbc_btnEndPuchase = new GridBagConstraints();
			gbc_btnEndPuchase.gridwidth = 2;
			gbc_btnEndPuchase.insets = new Insets(0, 0, 5, 5);
			gbc_btnEndPuchase.anchor = GridBagConstraints.EAST;
			gbc_btnEndPuchase.gridx = 3;
			gbc_btnEndPuchase.gridy = 0;
			pnByeSouth.add(getBtnEndPuchase(), gbc_btnEndPuchase);
		}
		return pnByeSouth;
	}

	private JButton getBtnEndPuchase() {
		if (btnEndPuchase == null) {
			btnEndPuchase = new JButton();
			btnEndPuchase.setText("Finalizar");
			btnEndPuchase.setMnemonic('P');
			btnEndPuchase.setBackground(Color.WHITE);
			btnEndPuchase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtpnAllBill.setText("");
					card.show(pnBase, "pnHomeView");
					resetFieldMember();
					calendar.setDate(new Date());
				}
			});
			btnEndPuchase.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return btnEndPuchase;
	}

	private JPanel getPnSaveBill() {
		if (pnSaveBill == null) {
			pnSaveBill = new JPanel();
			pnSaveBill.setBackground(pnByeSouth.getBackground());
			pnSaveBill.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
			pnSaveBill.add(getBtnSaveTheBill());
		}
		return pnSaveBill;
	}

	private JButton getBtnSaveTheBill() {
		if (btnSaveTheBill == null) {
			btnSaveTheBill = new JButton();
			btnSaveTheBill.setText("Guardar factura");
			btnSaveTheBill.setMnemonic('A');
			btnSaveTheBill.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (rdbtnChooserMemberInstallation.isSelected()) {// si la
																		// opcion
																		// se
																		// fue a
																		// reservar
																		// para
																		// usuario
						manager.saveBill(txtDNIMemberInstal.getText(), dateChooserMember.getDate(), currentInstallation,
								cbHourInstallationMemberS.getSelectedIndex(), txtpnAllBill.getText());
					} else if (rdbtnChooserNotMemberInstal.isSelected()) {
						manager.saveBill(txtDNINotMemberInstal.getText(), dateChooserNotMember.getDate(),
								currentInstallation, cbHourInstallationNotMemberI.getSelectedIndex(),
								txtpnAllBill.getText());
					}
				}
			});
			btnSaveTheBill.setVerticalAlignment(SwingConstants.BOTTOM);
			btnSaveTheBill.setBounds(0, 0, 40, 40);
			btnSaveTheBill.setBorder(null);
			btnSaveTheBill.setBackground(pnByeSouth.getBackground());
			setAdaptedImageButton(btnSaveTheBill, "/img/saveFile.png");
			btnSaveTheBill.setFont(new Font("Tw Cen MT", Font.BOLD, 17));
		}
		return btnSaveTheBill;
	}

	/**
	 * This method shows the final ppage depending on the user is logged in or
	 * not, and the check box for saving bill
	 * 
	 * @param notUser
	 *            if the user is register or not
	 */
	protected void showFinalPage(boolean notUser) {
		String money = "";
		if (rdbtnChooserNotMemberInstal.isSelected()) {
			money = JOptionPane.showInputDialog(contentPane, "Dinero entregado por el cliente");
		}
		card.show(pnBase, "pnBill");
		btnSaveTheBill.setVisible(true);
		if (notUser) {
			txtpnAllBill.setText(manager.generateBillNM(txtNameNotMemberInstal.getText(),
					txtSurnameNotMemberInstal.getText(), txtDNINotMemberInstal.getText(),
					txtTelephoneNotMemberInstal.getText(), dateChooserNotMember.getDate(),
					cbHourInstallationNotMemberI.getSelectedItem().toString(),
					cbHourInstallationNotMemberF.getSelectedItem().toString(), currentInstallation, true, true, money));
		} else {
			boolean payment = false;
			if (rdbtnPagoAlEntrar.isSelected()) {
				payment = true;
			}
			String text = manager.generateBillM(txtDNIMemberInstal.getText(), dateChooserMember.getDate(),
					cbHourInstallationMemberS.getSelectedItem().toString(),
					cbHourInstallationMemberF.getSelectedItem().toString(), currentInstallation);
			if (payment) {
				text += "\nLa cantidad a pagar ser� abonada en el momento";
			} else {
				text += "\nLa cantidad ser� a�adida a la cuota mensual";
			}
			txtpnAllBill.setText(text);
		}
		scrollBill.getViewport().setViewPosition(new Point(0, 0));
	}

	private JButton getBtnVerFactura() {
		if (btnVerFactura == null) {
			btnVerFactura = new JButton("Ver detalles");
			btnVerFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (tableAgenda.getValueAt(tableAgenda.getSelectedRow(), 1).toString().substring(0, 1)
							.compareTo(">") == 0) {
						txtpnAllBill.setText(manager.showDataActivityInDatabase(currentInstallation, calendar.getDate(),
								tableAgenda.getSelectedRow()));
						txtpnByeMessage.setText("Detalles de la actividad:");
					} else {
						txtpnAllBill.setText(manager.showBillInDatabase(currentInstallation, calendar.getDate(),
								tableAgenda.getSelectedRow()));
						txtpnByeMessage.setText("Detalles de la reserva:");
					}
					card.show(pnBase, "pnBill");
					btnSaveTheBill.setVisible(false);
				}
			});
			btnVerFactura.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnVerFactura;
	}

	// GONZI
	private JButton getBtnComprobar() {
		if (btnComprobar == null) {
			btnComprobar = new JButton("Comprobar");
			btnComprobar.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnComprobar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String fecha = df.format(new Date());
					Date fe = new Date();
					@SuppressWarnings("deprecation")
					int hora = fe.getHours();
					txtDatosOcupante.setText(manager.checkifInstallationIsOccupied(
							(String) cbEstadoInstalacion.getSelectedItem(), fecha, hora));
				}
			});
		}
		return btnComprobar;
	}

	// Saul
	private JButton getBtnVerMisReservas() {
		if (btnVerMisReservas == null) {
			btnVerMisReservas = new JButton("Ver mis reservas");
			btnVerMisReservas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						card.show(pnBase, "pnViewBooking");
						showAllBookings();
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			});
			btnVerMisReservas.setBackground(Color.WHITE);
			btnVerMisReservas.setEnabled(false);
			btnVerMisReservas.setVisible(false);
			btnVerMisReservas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnVerMisReservas;
	}

	// Gonzi
	private JButton getBtnVerMisActividades() {
		if (btnVerMisActividades == null) {
			btnVerMisActividades = new JButton("Ver mis actividades");
			btnVerMisActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					pnActivities.removeAll();
					getPnActivities().repaint();
					card.show(pnBase, "pnVerMisActividades");
					showAllActivities();
				}
			});
			btnVerMisActividades.setBackground(Color.WHITE);
			btnVerMisActividades.setEnabled(false);
			btnVerMisActividades.setVisible(false);
			btnVerMisActividades.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnVerMisActividades;
	}

	private JPanel getPnBookingView() {
		if (pnBookingView == null) {
			pnBookingView = new JPanel();
			pnBookingView.setBackground(Color.WHITE);
			GridBagLayout gbl_pnBookingView = new GridBagLayout();
			gbl_pnBookingView.columnWidths = new int[] { 65, 43, 247, 40, 204, 40, 0 };
			gbl_pnBookingView.rowHeights = new int[] { 50, 80, 116, 91, 128, 0, 0 };
			gbl_pnBookingView.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnBookingView.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
			pnBookingView.setLayout(gbl_pnBookingView);
			GridBagConstraints gbc_btnBackHomeFromBookingView = new GridBagConstraints();
			gbc_btnBackHomeFromBookingView.gridwidth = 2;
			gbc_btnBackHomeFromBookingView.fill = GridBagConstraints.BOTH;
			gbc_btnBackHomeFromBookingView.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackHomeFromBookingView.gridheight = 3;
			gbc_btnBackHomeFromBookingView.gridx = 0;
			gbc_btnBackHomeFromBookingView.gridy = 0;
			pnBookingView.add(getBtnBackHomeFromBookingView(), gbc_btnBackHomeFromBookingView);
			GridBagConstraints gbc_lblSusReservasSon = new GridBagConstraints();
			gbc_lblSusReservasSon.anchor = GridBagConstraints.SOUTHWEST;
			gbc_lblSusReservasSon.insets = new Insets(0, 0, 5, 5);
			gbc_lblSusReservasSon.gridx = 2;
			gbc_lblSusReservasSon.gridy = 0;
			pnBookingView.add(getLblSusReservasSon(), gbc_lblSusReservasSon);
			GridBagConstraints gbc_pnBookingSelection = new GridBagConstraints();
			gbc_pnBookingSelection.fill = GridBagConstraints.BOTH;
			gbc_pnBookingSelection.insets = new Insets(0, 0, 5, 5);
			gbc_pnBookingSelection.gridheight = 4;
			gbc_pnBookingSelection.gridx = 2;
			gbc_pnBookingSelection.gridy = 1;
			pnBookingView.add(getPnBookingSelection(), gbc_pnBookingSelection);
			GridBagConstraints gbc_btnCancelarReserva = new GridBagConstraints();
			gbc_btnCancelarReserva.fill = GridBagConstraints.BOTH;
			gbc_btnCancelarReserva.insets = new Insets(0, 0, 5, 5);
			gbc_btnCancelarReserva.gridx = 4;
			gbc_btnCancelarReserva.gridy = 3;
			pnBookingView.add(getBtnCancelarReserva(), gbc_btnCancelarReserva);
		}
		return pnBookingView;
	}

	private JPanel getPnBookingSelection() {
		if (pnBookingSelection == null) {
			pnBookingSelection = new JPanel();
			pnBookingSelection.setBackground(Color.WHITE);
			pnBookingSelection.setLayout(new BorderLayout(0, 0));
			pnBookingSelection.add(getScrollPnBookingSelection(), BorderLayout.CENTER);
		}
		return pnBookingSelection;
	}

	private JScrollPane getScrollPnBookingSelection() {
		if (scrollPnBookingSelection == null) {
			scrollPnBookingSelection = new JScrollPane();
			scrollPnBookingSelection.setBackground(pnBookingSelection.getBackground());
			scrollPnBookingSelection.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPnBookingSelection.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPnBookingSelection.setViewportView(getPnBookings());
		}
		return scrollPnBookingSelection;
	}

	private JPanel getPnBookings() {
		if (pnBookings == null) {
			pnBookings = new JPanel();
			pnBookings.setBackground(Color.WHITE);
			pnBookings.setLayout(new GridLayout(0, 1, 0, 0));

		}
		return pnBookings;
	}

	private void showAllBookings() throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date now = calendar.getDate();
		String text = "";
		ArrayList<String[]> bookings = manager.getUserBooking(manager.getCurrentUser().getDNI());
		int i = 0;
		for (String[] act : bookings) {
			Date actDate = df.parse(act[1]);
			if (now.before(actDate)) {
				if (Integer.parseInt(act[4]) == 1) {
					text = "<html><p>Reserva de: " + act[0] + "</p>" + "<p>El d�a: " + act[1] + "</p>" + "<p>De "
							+ act[2] + " a " + act[3] + "</p>" + "<p>Est� pagada</p></html>";
				} else if (Integer.parseInt(act[4]) == 0) {
					text = "<html><p>Reserva de: " + act[0] + "</p>" + "<p>El d�a: " + act[1] + "</p>" + "<p>De "
							+ act[2] + " a " + act[3] + "</p>" + "<p>No est� pagada</p></html>";
				}
				showUserBooking(act[5], text);
			}
			i++;
		}
		if (i == 0) {
			pnBookings.add(getLblNoHayReservas());
		}
	}

	private void showAllActivities() {
		String text = "";
		ArrayList<String[]> bookings = manager.getUserActivities(manager.getCurrentUser().getDNI());
		int i = 0;
		for (String[] act : bookings) {
			text = "<html><p>Reserva de: " + act[1] + "</p>" + "<p>En: " + act[0] + "</p>" + "<p>El d�a: " + act[2]
					+ "</p>" + "<p>De " + act[3] + " a " + act[4] + "</p>";
			showUserActivity(act[6], text);
			i++;

		}
		if (i == 0) {
			pnActivities.add(getLblNoHay());
		}
	}

	private void showUserActivity(String id_booking, String text) {
		JButton boton = new JButton();
		boton.setText(text); // Hay que cambiar
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(id_booking);// Hay que cambiar
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boton.setBackground(new Color(135, 206, 250));
				currentActivityToDelete = boton.getActionCommand();
				Component[] comp = pnActivities.getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(pnActivities.getBackground());
		pnActivities.add(boton);
	}

	private void showUserBooking(String id_booking, String text) {
		JButton boton = new JButton();
		boton.setText(text); // Hay que cambiar
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(id_booking);// Hay que cambiar
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boton.setBackground(new Color(135, 206, 250));
				currentBookingToDelete = boton.getActionCommand();
				Component[] comp = pnBookings.getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(pnBookings.getBackground());
		pnBookings.add(boton);
	}

	private JLabel getLblSusReservasSon() {
		if (lblSusReservasSon == null) {
			lblSusReservasSon = new JLabel("Sus reservas son:");
			lblSusReservasSon.setBackground(Color.WHITE);
			lblSusReservasSon.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblSusReservasSon;
	}

	private JLabel getLblNoHayReservas() {
		if (lblNoHayReservas == null) {
			lblNoHayReservas = new JLabel("No hay reservas");
			lblNoHayReservas.setFont(new Font("Tw Cen MT", Font.ITALIC, 35));
		}
		return lblNoHayReservas;
	}

	private JLabel getLblNoHay() {
		if (lblNoHay == null) {
			lblNoHay = new JLabel("No hay actividades reservadas");
			lblNoHay.setFont(new Font("Tw Cen MT", Font.ITALIC, 35));
		}
		return lblNoHay;
	}

	private JButton getBtnCancelarReserva() {
		if (btnCancelarReserva == null) {
			btnCancelarReserva = new JButton("Cancelar reserva");
			btnCancelarReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (currentBookingToDelete == "")
						JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna reserva", "Error",
								JOptionPane.ERROR_MESSAGE);
					else {
						if (manager.deleteUserBooking(currentBookingToDelete)) {
							JOptionPane.showMessageDialog(null, "Se ha completado la cancelaci�n correctamente");
							currentBookingToDelete = "";
							pnBookings.removeAll();
							card.show(pnBase, "pnHomeView");
						} else
							JOptionPane.showMessageDialog(null, "No se ha podido completar la cancelaci�n", "Error",
									JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnCancelarReserva.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnCancelarReserva.setBackground(Color.WHITE);
		}
		return btnCancelarReserva;
	}

	private JButton getBtnBackHomeFromBookingView() {
		if (btnBackHomeFromBookingView == null) {
			btnBackHomeFromBookingView = new JButton();
			btnBackHomeFromBookingView.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentBookingToDelete = "";
					card.show(pnBase, "pnHomeView");
				}
			});
			btnBackHomeFromBookingView.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackHomeFromBookingView.setBackground(pnBookingView.getBackground());
			btnBackHomeFromBookingView.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackHomeFromBookingView, "/img/arrow.png");
			btnBackHomeFromBookingView.setBorder(null);
		}
		return btnBackHomeFromBookingView;
	}

	private JButton getBtnCrearNuevaActividad() {
		if (btnCrearNuevaActividad == null) {
			btnCrearNuevaActividad = new JButton("Crear nueva actividad");
			btnCrearNuevaActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					crearActividad = true;
					currentInstallation = "";
					card.show(pnBase, "pnCrearActividad");
					lblNmeroDePlazas.setVisible(false);
					txtNumeroDePlazas.setText("");
					txtNumeroDePlazas.setVisible(false);
					txtNombreAtividad.setText("");
					rdbtnLimitadas.setSelected(false);
					rdbtnIlimitadas.setSelected(true);
					rdbtnPuntual.setSelected(true);
					rdbtnPeriodica.setSelected(false);
					dateActividadPeriodicaEmpezar.setDate(null);
					dateActividadPeriodicaEmpezar.setMinSelectableDate(new Date());
					dateActividadPeriodicaAcabar.setDate(null);
					dateActividadPeriodicaAcabar.setEnabled(false);
				}
			});
			btnCrearNuevaActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnCrearNuevaActividad.setBackground(Color.WHITE);
			btnCrearNuevaActividad.setEnabled(false);
			btnCrearNuevaActividad.setVisible(false);
		}
		return btnCrearNuevaActividad;
	}

	private JButton getBtnVerActividades() {
		if (btnVerActividades == null) {
			btnVerActividades = new JButton("Ver Actividades");
			btnVerActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnBase, "pnVerActividades");
					actualizarPaneles();
					labelSemana.setText("");
				}
			});
			btnVerActividades.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnVerActividades.setBackground(Color.WHITE);
			btnVerActividades.setEnabled(false);
			btnVerActividades.setVisible(false);
		}
		return btnVerActividades;

	}

	private JPanel getPnCrearActividad() {
		if (pnCrearActividad == null) {
			pnCrearActividad = new JPanel();
			pnCrearActividad.setBackground(pnBase.getBackground());
			GridBagLayout gbl_pnCrearActividad = new GridBagLayout();
			gbl_pnCrearActividad.columnWidths = new int[] { 0, 81, 55, 142, 23, 0, 148, 30, 123, 0, 0 };
			gbl_pnCrearActividad.rowHeights = new int[] { 52, 65, 31, 38, 23, 39, 50, 0, 21, 0, 0 };
			gbl_pnCrearActividad.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			gbl_pnCrearActividad.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnCrearActividad.setLayout(gbl_pnCrearActividad);
			GridBagConstraints gbc_btnBackCrearActividad = new GridBagConstraints();
			gbc_btnBackCrearActividad.fill = GridBagConstraints.BOTH;
			gbc_btnBackCrearActividad.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackCrearActividad.gridx = 1;
			gbc_btnBackCrearActividad.gridy = 1;
			pnCrearActividad.add(getBtnBackCrearActividad(), gbc_btnBackCrearActividad);
			GridBagConstraints gbc_lblNombreActividad = new GridBagConstraints();
			gbc_lblNombreActividad.fill = GridBagConstraints.BOTH;
			gbc_lblNombreActividad.insets = new Insets(0, 0, 5, 5);
			gbc_lblNombreActividad.gridx = 3;
			gbc_lblNombreActividad.gridy = 2;
			pnCrearActividad.add(getLblNombreActividad(), gbc_lblNombreActividad);
			GridBagConstraints gbc_txtNombreAtividad = new GridBagConstraints();
			gbc_txtNombreAtividad.gridwidth = 3;
			gbc_txtNombreAtividad.fill = GridBagConstraints.BOTH;
			gbc_txtNombreAtividad.insets = new Insets(0, 0, 5, 5);
			gbc_txtNombreAtividad.gridx = 4;
			gbc_txtNombreAtividad.gridy = 2;
			pnCrearActividad.add(getTxtNombreAtividad(), gbc_txtNombreAtividad);
			GridBagConstraints gbc_lblPlazas = new GridBagConstraints();
			gbc_lblPlazas.fill = GridBagConstraints.BOTH;
			gbc_lblPlazas.insets = new Insets(0, 0, 5, 5);
			gbc_lblPlazas.gridx = 3;
			gbc_lblPlazas.gridy = 3;
			pnCrearActividad.add(getLblPlazas(), gbc_lblPlazas);
			GridBagConstraints gbc_rdbtnIlimitadas = new GridBagConstraints();
			gbc_rdbtnIlimitadas.fill = GridBagConstraints.BOTH;
			gbc_rdbtnIlimitadas.gridwidth = 2;
			gbc_rdbtnIlimitadas.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnIlimitadas.gridx = 4;
			gbc_rdbtnIlimitadas.gridy = 3;
			pnCrearActividad.add(getRdbtnIlimitadas(), gbc_rdbtnIlimitadas);
			GridBagConstraints gbc_rdbtnLimitadas = new GridBagConstraints();
			gbc_rdbtnLimitadas.gridwidth = 2;
			gbc_rdbtnLimitadas.fill = GridBagConstraints.BOTH;
			gbc_rdbtnLimitadas.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnLimitadas.gridx = 4;
			gbc_rdbtnLimitadas.gridy = 4;
			pnCrearActividad.add(getRdbtnLimitadas(), gbc_rdbtnLimitadas);
			groupCrearActividadPlazas.add(getRdbtnIlimitadas());
			groupCrearActividadPlazas.add(getRdbtnLimitadas());
			GridBagConstraints gbc_lblNmeroDePlazas = new GridBagConstraints();
			gbc_lblNmeroDePlazas.fill = GridBagConstraints.BOTH;
			gbc_lblNmeroDePlazas.insets = new Insets(0, 0, 5, 5);
			gbc_lblNmeroDePlazas.gridx = 3;
			gbc_lblNmeroDePlazas.gridy = 5;
			pnCrearActividad.add(getLblNmeroDePlazas(), gbc_lblNmeroDePlazas);
			GridBagConstraints gbc_txtNumeroDePlazas = new GridBagConstraints();
			gbc_txtNumeroDePlazas.gridwidth = 2;
			gbc_txtNumeroDePlazas.fill = GridBagConstraints.BOTH;
			gbc_txtNumeroDePlazas.insets = new Insets(0, 0, 5, 5);
			gbc_txtNumeroDePlazas.gridx = 4;
			gbc_txtNumeroDePlazas.gridy = 5;
			pnCrearActividad.add(getTxtNumeroDePlazas(), gbc_txtNumeroDePlazas);
			GridBagConstraints gbc_txtpnInformacionCrearActividad = new GridBagConstraints();
			gbc_txtpnInformacionCrearActividad.gridheight = 3;
			gbc_txtpnInformacionCrearActividad.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtpnInformacionCrearActividad.insets = new Insets(0, 0, 0, 5);
			gbc_txtpnInformacionCrearActividad.gridwidth = 2;
			gbc_txtpnInformacionCrearActividad.gridx = 1;
			gbc_txtpnInformacionCrearActividad.gridy = 7;
			pnCrearActividad.add(getTxtpnInformacionCrearActividad(), gbc_txtpnInformacionCrearActividad);
			GridBagConstraints gbc_lblPeriodicidad = new GridBagConstraints();
			gbc_lblPeriodicidad.fill = GridBagConstraints.BOTH;
			gbc_lblPeriodicidad.insets = new Insets(0, 0, 5, 5);
			gbc_lblPeriodicidad.gridx = 3;
			gbc_lblPeriodicidad.gridy = 7;
			pnCrearActividad.add(getLblPeriodicidad(), gbc_lblPeriodicidad);
			GridBagConstraints gbc_rdbtnPuntual = new GridBagConstraints();
			gbc_rdbtnPuntual.fill = GridBagConstraints.BOTH;
			gbc_rdbtnPuntual.gridwidth = 2;
			gbc_rdbtnPuntual.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnPuntual.gridx = 4;
			gbc_rdbtnPuntual.gridy = 7;
			pnCrearActividad.add(getRdbtnPuntual(), gbc_rdbtnPuntual);
			GridBagConstraints gbc_rdbtnPeriodica = new GridBagConstraints();
			gbc_rdbtnPeriodica.fill = GridBagConstraints.BOTH;
			gbc_rdbtnPeriodica.gridwidth = 2;
			gbc_rdbtnPeriodica.insets = new Insets(0, 0, 5, 5);
			gbc_rdbtnPeriodica.gridx = 4;
			gbc_rdbtnPeriodica.gridy = 8;
			pnCrearActividad.add(getRdbtnPeriodica(), gbc_rdbtnPeriodica);
			groupCrearActividadPeriodicidad.add(getRdbtnPuntual());
			groupCrearActividadPeriodicidad.add(getRdbtnPeriodica());
			GridBagConstraints gbc_btnConfirmarCrearActividad = new GridBagConstraints();
			gbc_btnConfirmarCrearActividad.insets = new Insets(0, 0, 5, 5);
			gbc_btnConfirmarCrearActividad.fill = GridBagConstraints.BOTH;
			gbc_btnConfirmarCrearActividad.gridx = 8;
			gbc_btnConfirmarCrearActividad.gridy = 8;
			pnCrearActividad.add(getBtnConfirmarCrearActividad(), gbc_btnConfirmarCrearActividad);
		}
		return pnCrearActividad;
	}

	private JButton getBtnBackCrearActividad() {
		if (btnBackCrearActividad == null) {
			btnBackCrearActividad = new JButton("");
			btnBackCrearActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
				}
			});
			btnBackCrearActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackCrearActividad.setBackground(Color.WHITE);
			btnBackCrearActividad.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackCrearActividad, "/img/arrow.png");
			btnBackCrearActividad.setBorder(null);
		}
		return btnBackCrearActividad;
	}

	private JLabel getLblNombreActividad() {
		if (lblNombreActividad == null) {
			lblNombreActividad = new JLabel("Nombre actividad:");
			lblNombreActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblNombreActividad.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblNombreActividad;
	}

	private JLabel getLblPlazas() {
		if (lblPlazas == null) {
			lblPlazas = new JLabel("Plazas:");
			lblPlazas.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblPlazas.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblPlazas;
	}

	private JRadioButton getRdbtnIlimitadas() {
		if (rdbtnIlimitadas == null) {
			rdbtnIlimitadas = new JRadioButton("Ilimitadas");
			rdbtnIlimitadas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblNmeroDePlazas.setVisible(false);
					txtNumeroDePlazas.setVisible(false);
				}
			});
			rdbtnIlimitadas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			rdbtnIlimitadas.setBackground(Color.WHITE);
		}
		return rdbtnIlimitadas;
	}

	private JRadioButton getRdbtnLimitadas() {
		if (rdbtnLimitadas == null) {
			rdbtnLimitadas = new JRadioButton("Limitadas");
			rdbtnLimitadas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					lblNmeroDePlazas.setVisible(true);
					txtNumeroDePlazas.setVisible(true);
				}
			});
			rdbtnLimitadas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			rdbtnLimitadas.setBackground(Color.WHITE);
		}
		return rdbtnLimitadas;
	}

	private JLabel getLblNmeroDePlazas() {
		if (lblNmeroDePlazas == null) {
			lblNmeroDePlazas = new JLabel("N\u00FAmero de plazas:");
			lblNmeroDePlazas.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblNmeroDePlazas.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblNmeroDePlazas;
	}

	private JTextField getTxtNumeroDePlazas() {
		if (txtNumeroDePlazas == null) {
			txtNumeroDePlazas = new JTextField();
			txtNumeroDePlazas.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char c = e.getKeyChar();
					if (!Character.isDigit(c)) {
						e.consume();
					}
				}
			});
			txtNumeroDePlazas.setHorizontalAlignment(SwingConstants.CENTER);
			txtNumeroDePlazas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			txtNumeroDePlazas.setColumns(10);
		}
		return txtNumeroDePlazas;
	}

	private JButton getBtnConfirmarCrearActividad() {
		if (btnConfirmarCrearActividad == null) {
			btnConfirmarCrearActividad = new JButton("Confirmar");
			btnConfirmarCrearActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!txtNombreAtividad.getText().equals("")) {
						if (rdbtnPuntual.isSelected()) {
							creandoActividadPuntual = true;
							card.show(pnBase, "pnInstallationView");
							btnBackAdminInstal.setVisible(false);
							cardInstallation.show(pnInstallationBookingView, "pnActivity");
							pnActivitiesContent.removeAll();
							showAllInstallations(pnActivitiesContent);
						} else if (rdbtnPeriodica.isSelected()) {
							creandoActividadPuntual = false;
							if (rdbtnIlimitadas.isSelected()) {
								lblPlazasActividadPeriodica.setText("Ilimitadas");
							} else {// si son limitadas
								lblPlazasActividadPeriodica.setText(txtNumeroDePlazas.getText());
							}
							dateActividadPeriodicaAcabar.setEnabled(false);
							lblNombreActividadPeriodica.setText(txtNombreAtividad.getText());
							card.show(pnBase, "pnActividadPeriodica");
							cardPeriodica.show(pnActividadPeriodicaCrear, "datos");// que
																					// salga
																					// el
																					// panel
																					// de
																					// datos
							eraseDataPrev();// borrar todas las selecciones que
											// habia
							showAllInstallations(pnInstalacionActividadPeriodica);
						}
					} else {
						String message = "NO SE HA ENCONTRADO UN NOMBRE PARA LA ACTIVIDAD";
						JOptionPane.showMessageDialog(pnBase, message, "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnConfirmarCrearActividad.setBackground(Color.WHITE);
			btnConfirmarCrearActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnConfirmarCrearActividad;
	}

	protected void eraseDataPrev() {
		pnInstalacionActividadPeriodica.removeAll();
		cmbBoxHoraDeInicio.setSelectedIndex(1);
		cmbBoxHoraDeFinal.setSelectedIndex(1);
		cmbBoxHoraDeFinal.setEnabled(false);
		chckbxLu.setSelected(false);
		chckbxMa.setSelected(false);
		chckbxMie.setSelected(false);
		chckbxJue.setSelected(false);
		chckbxVie.setSelected(false);
		chckbxSa.setSelected(false);
		chckbxDo.setSelected(false);
	}

	private JTextPane getTxtpnInformacionCrearActividad() {
		if (txtpnInformacionCrearActividad == null) {
			txtpnInformacionCrearActividad = new JTextPane();
			txtpnInformacionCrearActividad.setText(
					"Solamente los socios podr\u00E1n apuntarse a esta actividad. El precio por apuntarse ser\u00E1 0 "
							+ "\u20AC.");
			txtpnInformacionCrearActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 12));
			txtpnInformacionCrearActividad.setEditable(false);
		}
		return txtpnInformacionCrearActividad;
	}

	private JTextField getTxtNombreAtividad() {
		if (txtNombreAtividad == null) {
			txtNombreAtividad = new JTextField();
			txtNombreAtividad.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char c = e.getKeyChar();
					if (!Character.isLetter(c) && !Character.isWhitespace(c))
						e.consume();
				}
			});
			txtNombreAtividad.setHorizontalAlignment(SwingConstants.CENTER);
			txtNombreAtividad.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			txtNombreAtividad.setColumns(10);
		}
		return txtNombreAtividad;
	}

	private JButton getBtnInformacinSobreLas() {
		if (btnInformacinSobreLas == null) {
			btnInformacinSobreLas = new JButton("Informaci\u00F3n sobre las instalaciones");
			btnInformacinSobreLas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnInfoInstallations");
					txtpnInfoInstallation.setText("");
					lblPhotoInstallation.setIcon(null);
					showAllInstallationsInfo();
				}
			});
			btnInformacinSobreLas.setBackground(Color.WHITE);
			btnInformacinSobreLas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnInformacinSobreLas;
	}

	private JPanel getPnInfoInstallations() {
		if (pnInfoInstallations == null) {
			pnInfoInstallations = new JPanel();
			pnInfoInstallations.setBackground(pnBase.getBackground());
			GridBagLayout gbl_pnInfoInstallations = new GridBagLayout();
			gbl_pnInfoInstallations.columnWidths = new int[] { 31, 241, 49, 0, 285, 81, 0, 0 };
			gbl_pnInfoInstallations.rowHeights = new int[] { 44, 117, 117, 0, 30, 0, 0, 0 };
			gbl_pnInfoInstallations.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			gbl_pnInfoInstallations.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnInfoInstallations.setLayout(gbl_pnInfoInstallations);
			GridBagConstraints gbc_pnInfoInstalSelectView = new GridBagConstraints();
			gbc_pnInfoInstalSelectView.fill = GridBagConstraints.BOTH;
			gbc_pnInfoInstalSelectView.insets = new Insets(0, 0, 5, 5);
			gbc_pnInfoInstalSelectView.gridheight = 5;
			gbc_pnInfoInstalSelectView.gridx = 1;
			gbc_pnInfoInstalSelectView.gridy = 1;
			pnInfoInstallations.add(getPnInfoInstalSelectView(), gbc_pnInfoInstalSelectView);
			GridBagConstraints gbc_lblPhotoInstallation = new GridBagConstraints();
			gbc_lblPhotoInstallation.fill = GridBagConstraints.BOTH;
			gbc_lblPhotoInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_lblPhotoInstallation.gridx = 4;
			gbc_lblPhotoInstallation.gridy = 1;
			pnInfoInstallations.add(getLblPhotoInstallation(), gbc_lblPhotoInstallation);
			GridBagConstraints gbc_txtpnInfoInstallation = new GridBagConstraints();
			gbc_txtpnInfoInstallation.gridwidth = 2;
			gbc_txtpnInfoInstallation.gridheight = 3;
			gbc_txtpnInfoInstallation.fill = GridBagConstraints.BOTH;
			gbc_txtpnInfoInstallation.insets = new Insets(0, 0, 5, 5);
			gbc_txtpnInfoInstallation.gridx = 4;
			gbc_txtpnInfoInstallation.gridy = 2;
			pnInfoInstallations.add(getTxtpnInfoInstallation(), gbc_txtpnInfoInstallation);
			GridBagConstraints gbc_btnBackInfoInstal = new GridBagConstraints();
			gbc_btnBackInfoInstal.gridheight = 2;
			gbc_btnBackInfoInstal.insets = new Insets(0, 0, 0, 5);
			gbc_btnBackInfoInstal.fill = GridBagConstraints.BOTH;
			gbc_btnBackInfoInstal.gridx = 5;
			gbc_btnBackInfoInstal.gridy = 5;
			pnInfoInstallations.add(getBtnBackInfoInstal(), gbc_btnBackInfoInstal);
		}
		return pnInfoInstallations;
	}

	private JPanel getPnInfoInstalSelectView() {
		if (pnInfoInstalSelectView == null) {
			pnInfoInstalSelectView = new JPanel();
			pnInfoInstalSelectView.setLayout(new BorderLayout(0, 0));
			pnInfoInstalSelectView.setBackground(pnInfoInstallations.getBackground());
			pnInfoInstalSelectView.add(getScrllInfoInstallationSelect());
		}
		return pnInfoInstalSelectView;
	}

	private JScrollPane getScrllInfoInstallationSelect() {
		if (scrllInfoInstallationSlect == null) {
			scrllInfoInstallationSlect = new JScrollPane();
			scrllInfoInstallationSlect.setBackground(pnInstallationSelection.getBackground());
			scrllInfoInstallationSlect.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrllInfoInstallationSlect.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrllInfoInstallationSlect.setViewportView(getPnInfoInstallationsSelect());
		}
		return scrllInfoInstallationSlect;
	}

	private JPanel getPnInfoInstallationsSelect() {
		if (pnInfoInstallationsSelect == null) {
			pnInfoInstallationsSelect = new JPanel();
			pnInfoInstallationsSelect.setBackground(pnInstallationSelection.getBackground());
			pnInfoInstallationsSelect.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnInfoInstallationsSelect.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnInfoInstallationsSelect;
	}

	private JLabel getLblPhotoInstallation() {
		if (lblPhotoInstallation == null) {
			lblPhotoInstallation = new JLabel("");
			lblPhotoInstallation.setBounds(0, 0, 100, 100);
		}
		return lblPhotoInstallation;
	}

	private JButton getBtnBackInfoInstal() {
		if (btnBackInfoInstal == null) {
			btnBackInfoInstal = new JButton("");
			btnBackInfoInstal.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
				}
			});
			btnBackInfoInstal.setBounds(0, 0, 50, 50);
			btnBackInfoInstal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackInfoInstal.setBackground(pnBase.getBackground());
			setAdaptedImageButton(btnBackInfoInstal, "/img/arrow.png");
			btnBackInfoInstal.setBorder(null);
		}
		return btnBackInfoInstal;
	}

	private JTextPane getTxtpnInfoInstallation() {
		if (txtpnInfoInstallation == null) {
			txtpnInfoInstallation = new JTextPane();
			txtpnInfoInstallation.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			txtpnInfoInstallation.setEditable(false);
		}
		return txtpnInfoInstallation;
	}

	/**
	 * This method shows all the installations and creates all the buttons
	 */
	protected void showAllInstallationsInfo() {
		pnInfoInstallationsSelect.removeAll();
		ArrayList<String> installations = manager.getInstallations();
		for (String act : installations) {
			showInstallationUniqueInfo(act);
		}
	}

	/**
	 * This method shows a unique activity
	 * 
	 * @param inst
	 *            the installation we want to show
	 */
	protected void showInstallationUniqueInfo(String inst) {
		JButton boton = new JButton();
		boton.setText(inst);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(inst);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boton.setBackground(new Color(135, 206, 250));
				setAdaptedImageLabel(lblPhotoInstallation, "/img/" + boton.getActionCommand() + ".jpg");
				txtpnInfoInstallation.setText(manager.getDescripcionInstalacion(boton.getActionCommand()));
				Component[] comp = pnInfoInstallationsSelect.getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(pnInfoInstalSelectView.getBackground());
		pnInfoInstallationsSelect.add(boton);
	}

	private void pintarTicketDevolucion(String dni, String insta, String fecha, int horaI) {
		HashMap<String, Object> info = manager.getInfoUsuario(dni);
		int horaF = manager.getHoraFinal(insta, fecha, horaI);
		int dif = horaF - horaI;
		int precioH = manager.getPriceInstallation(insta, false);
		int precio = precioH * dif;
		txtPaneTicketDevolucion.setText("");
		String txt = "";
		txt += "Usuario: " + info.get("nombre_usuario") + " " + info.get("apellidos_usuario") + "\nTel�fono: "
				+ info.get("telefono") + "\nDNI:" + dni + "\n";
		txt += "Su reserva en la instalaci�n " + insta + " para el d�a " + fecha + "  a las " + horaI
				+ " horas\n ha sido CANCELADA.\n";
		txt += "Se le devuelve el importe de " + precio + " $ abonados por la reserva";
		txtPaneTicketDevolucion.setText(txt);
	}

	//
	// Gonzi
	private JButton getBtnDeleteBooking() {
		if (btnDeleteBooking == null) {
			btnDeleteBooking = new JButton("Cancelar reserva");
			btnDeleteBooking.addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent arg0) {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String fecha = df.format(calendar.getDate());
					String reserva = manager.getBookingToDelete(currentInstallation, fecha,
							tableAgenda.getSelectedRow());
					if (checkIfBookingIsOfUser == true) {
						String[] options = { "Petici�n del usuario", "Necesidades del centro", "Cancelar" };
						int seleccion = JOptionPane.showOptionDialog(null, "Seleccione el motivo de la cancelaci�n",
								"Cancelar", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
								options[0]);
						if (seleccion == 0) {
							Date hoy = new Date();
							if (calendar.getDate().getDay() - hoy.getDay() >= 1) {
								if (manager.getIfBookingToDeleteIsN(currentInstallation, fecha,
										tableAgenda.getSelectedRow())) {
									manager.updateCancelledBooking(1, currentInstallation, fecha,
											tableAgenda.getSelectedRow());
									JOptionPane.showMessageDialog(null,
											"La reserva ha sido cancelada con antelaci�n suficiente.\n"
													+ "Se genera ticket de reserva");
									card.show(pnBase, "pnTicketDevolucion");
									pintarTicketDevolucion(reserva, currentInstallation, fecha,
											tableAgenda.getSelectedRow());
								} else {
									manager.updateCancelledBooking(1, currentInstallation, fecha,
											tableAgenda.getSelectedRow());
									JOptionPane.showMessageDialog(null,
											"La reserva ha sido cancelada con �xito\nSin cargos para el socio.");
								}
							} else {
								manager.updateCancelledBooking(1, currentInstallation, fecha,
										tableAgenda.getSelectedRow());
								JOptionPane.showMessageDialog(null,
										"La reserva ha sido cancelada con �xito\nSin ticket de devoluci�n por"
												+ "falta de antelaci�n.");
							}
						} else if (seleccion == 1) {
							if (manager.getIfBookingToDeleteIsN(currentInstallation, fecha,
									tableAgenda.getSelectedRow())) {
								manager.updateCancelledBooking(2, currentInstallation, fecha,
										tableAgenda.getSelectedRow());
								JOptionPane.showMessageDialog(null,
										"La reserva ha sido cancelada con �xito.\n Se genera ticket de devoluci�n");
								card.show(pnBase, "pnTicketDevolucion");
								pintarTicketDevolucion(reserva, currentInstallation, fecha,
										tableAgenda.getSelectedRow());
							} else {
								manager.updateCancelledBooking(2, currentInstallation, fecha,
										tableAgenda.getSelectedRow());
								JOptionPane.showMessageDialog(null,
										"La reserva ha sido cancelada con �xito\nSin cargos para el socio.");
							}
						}
					} else {
						int resp = JOptionPane.showConfirmDialog(null,
								"�Est� seguro de que desea cancelar esta reserva del centro?", "",
								JOptionPane.YES_NO_OPTION);
						if (resp == JOptionPane.YES_OPTION) {
							manager.updateCancelledBooking(1, currentInstallation, fecha, tableAgenda.getSelectedRow());
							JOptionPane.showMessageDialog(null, "La reserva ha sido cancelada con �xito.");
						}
					}
				}
			});
			btnDeleteBooking.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnDeleteBooking;
	}

	private void setChechIfBookingIsOfUser(boolean bol) {
		checkIfBookingIsOfUser = bol;
	}

	// ------------------------------ Historia apuntar socio a una actividad
	// ---------------------
	private JButton getBtnApuntarSocioActividad() {
		if (btnApuntarSocioActividad == null) {
			btnApuntarSocioActividad = new JButton("Apuntar a socio a un actividad");
			btnApuntarSocioActividad.setBackground(Color.WHITE);
			btnApuntarSocioActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					pnActivitiesForUsers.removeAll();
					card.show(pnBase, "pnAddUserToActivity");
					showActivitiesToUser();
					pnActivitiesForUsers.paintAll(pnActivitiesForUsers.getGraphics());
					if (!manager.currentUserIsAdmin()) {
						txtDNISocioApuntarAct.setText(manager.getCurrentUser().getDNI());
						txtDNISocioApuntarAct.setEditable(false);
					} else {
						txtDNISocioApuntarAct.setText("");
						txtDNISocioApuntarAct.setEditable(true);
					}
				}
			});
			btnApuntarSocioActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnApuntarSocioActividad.setEnabled(false);
			btnApuntarSocioActividad.setVisible(false);
		}
		return btnApuntarSocioActividad;
	}

	private JPanel getPnAddUserToActivity() {
		if (pnAddUserToActivity == null) {
			pnAddUserToActivity = new JPanel();
			pnAddUserToActivity.setBackground(Color.WHITE);
			GridBagLayout gbl_pnAddUserToActivity = new GridBagLayout();
			gbl_pnAddUserToActivity.columnWidths = new int[] { 245, 198, 0, 149, 0 };
			gbl_pnAddUserToActivity.rowHeights = new int[] { 0, 41, 21, 72, 19, 20, 82, 0, 0 };
			gbl_pnAddUserToActivity.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnAddUserToActivity.rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnAddUserToActivity.setLayout(gbl_pnAddUserToActivity);
			GridBagConstraints gbc_calendarAddUserToActivity = new GridBagConstraints();
			gbc_calendarAddUserToActivity.fill = GridBagConstraints.BOTH;
			gbc_calendarAddUserToActivity.insets = new Insets(0, 0, 5, 5);
			gbc_calendarAddUserToActivity.gridheight = 7;
			gbc_calendarAddUserToActivity.gridx = 0;
			gbc_calendarAddUserToActivity.gridy = 1;
			pnAddUserToActivity.add(getCalendarAddUserToActivity(), gbc_calendarAddUserToActivity);
			GridBagConstraints gbc_btnComprobarActividades = new GridBagConstraints();
			gbc_btnComprobarActividades.anchor = GridBagConstraints.SOUTH;
			gbc_btnComprobarActividades.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnComprobarActividades.insets = new Insets(0, 0, 5, 5);
			gbc_btnComprobarActividades.gridx = 1;
			gbc_btnComprobarActividades.gridy = 1;
			// pnAddUserToActivity.add(getBtnComprobarActividades(),
			// gbc_btnComprobarActividades);
			GridBagConstraints gbc_pnActivitiesForUsersSelection = new GridBagConstraints();
			gbc_pnActivitiesForUsersSelection.fill = GridBagConstraints.BOTH;
			gbc_pnActivitiesForUsersSelection.insets = new Insets(0, 0, 5, 5);
			gbc_pnActivitiesForUsersSelection.gridheight = 7;
			gbc_pnActivitiesForUsersSelection.gridx = 1;
			gbc_pnActivitiesForUsersSelection.gridy = 1;
			pnAddUserToActivity.add(getPnActivitiesForUsersSelection(), gbc_pnActivitiesForUsersSelection);
			GridBagConstraints gbc_txtInfPlazasAddUserToActivity = new GridBagConstraints();
			gbc_txtInfPlazasAddUserToActivity.insets = new Insets(0, 0, 5, 0);
			gbc_txtInfPlazasAddUserToActivity.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtInfPlazasAddUserToActivity.gridx = 3;
			gbc_txtInfPlazasAddUserToActivity.gridy = 2;
			pnAddUserToActivity.add(getTxtInfPlazasAddUserToActivity(), gbc_txtInfPlazasAddUserToActivity);
			GridBagConstraints gbc_lblDniDelSocio = new GridBagConstraints();
			gbc_lblDniDelSocio.fill = GridBagConstraints.BOTH;
			gbc_lblDniDelSocio.insets = new Insets(0, 0, 5, 0);
			gbc_lblDniDelSocio.gridx = 3;
			gbc_lblDniDelSocio.gridy = 4;
			pnAddUserToActivity.add(getLblDniDelSocio(), gbc_lblDniDelSocio);
			GridBagConstraints gbc_txtDNISocioApuntarAct = new GridBagConstraints();
			gbc_txtDNISocioApuntarAct.anchor = GridBagConstraints.NORTH;
			gbc_txtDNISocioApuntarAct.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDNISocioApuntarAct.insets = new Insets(0, 0, 5, 0);
			gbc_txtDNISocioApuntarAct.gridx = 3;
			gbc_txtDNISocioApuntarAct.gridy = 5;
			pnAddUserToActivity.add(getTxtDNISocioApuntarAct(), gbc_txtDNISocioApuntarAct);
			GridBagConstraints gbc_btnApuntarSocioAAct = new GridBagConstraints();
			gbc_btnApuntarSocioAAct.insets = new Insets(0, 0, 5, 0);
			gbc_btnApuntarSocioAAct.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnApuntarSocioAAct.gridx = 3;
			gbc_btnApuntarSocioAAct.gridy = 6;
			pnAddUserToActivity.add(getBtnApuntarSocioAAct(), gbc_btnApuntarSocioAAct);
		}
		return pnAddUserToActivity;
	}

	private JCalendar getCalendarAddUserToActivity() {
		if (calendarAddUserToActivity == null) {
			calendarAddUserToActivity = new JCalendar();
			calendarAddUserToActivity.getDayChooser().addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent arg0) {
					try {
						resetPanelActivitiesForUsers();
						pnActivitiesForUsers.removeAll();
						getPnActivitiesForUsers().repaint();
						showActivitiesToUser();
						pnActivitiesForUsers.paintAll(pnActivitiesForUsers.getGraphics());
					} catch (Exception e) {
					}
				}
			});
			calendarAddUserToActivity.setMinSelectableDate(new Date());
		}
		return calendarAddUserToActivity;
	}

	private void resetPanelActivitiesForUsers() {
		pnActivitiesForUsers.removeAll();
		getPnActivitiesForUsers().repaint();
		pnActivitiesForUsers.paintAll(pnActivitiesForUsers.getGraphics());
		currentActivitiesToUser = "";
		if (getManager().currentUserIsAdmin())
			getTxtDNISocioApuntarAct().setText("");
		getTxtInfPlazasAddUserToActivity().setText("");
		getTxtInfPlazasAddUserToActivity().setForeground(Color.BLACK);
		getBtnApuntarSocioAAct().setText("Apuntar");
	}

	private JPanel getPnActivitiesForUsersSelection() {
		if (pnActivitiesForUsersSelection == null) {
			pnActivitiesForUsersSelection = new JPanel();
			pnActivitiesForUsersSelection.setBorder(new TitledBorder(
					new TitledBorder(new LineBorder(new Color(171, 173, 179)), "Actividades disponibles",
							TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)),
					"Actividades disponibles", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnActivitiesForUsersSelection.setBackground(Color.WHITE);
			pnActivitiesForUsersSelection.setLayout(new BorderLayout(0, 0));
			pnActivitiesForUsersSelection.add(getScrollPnActivitiesForUsers(), BorderLayout.CENTER);

		}
		return pnActivitiesForUsersSelection;
	}

	private JScrollPane getScrollPnActivitiesForUsers() {
		if (scrollPnActivitiesForUsers == null) {
			scrollPnActivitiesForUsers = new JScrollPane();
			scrollPnActivitiesForUsers.setBackground(pnActivitiesForUsersSelection.getBackground());
			scrollPnActivitiesForUsers.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesForUsers.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesForUsers.setViewportView(getPnActivitiesForUsers());
		}
		return scrollPnActivitiesForUsers;
	}

	private JPanel getPnActivitiesForUsers() {
		if (pnActivitiesForUsers == null) {
			pnActivitiesForUsers = new JPanel();
			pnActivitiesForUsers.setBackground(Color.WHITE);
			// pnActivitiesForUsers.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnActivitiesForUsers.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnActivitiesForUsers;
	}

	protected void showActivitiesToUser() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(getCalendarAddUserToActivity().getDate());

		ArrayList<String[]> activities = manager.getActivitiesLimit(fecha);
		for (String act[] : activities) {
			showActivityUniqueToUser("<html><p>" + act[0] + "</p>" + "<p>Fecha: " + manager.getBookingDate(act[1])
					+ "</p>" + "<p>Hora de inicio: " + manager.getBookingHourI(act[1]) + "h</p></html>", act[1]);

		}
		if (activities.isEmpty())
			pnActivitiesForUsers.add(getLblNoHayActividades());
	}

	protected void showActivityUniqueToUser(String act, String id_reserva) {
		JButton boton = new JButton();
		boton.setText(act);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(id_reserva);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getTxtInfPlazasAddUserToActivity().setForeground(Color.BLACK);
				currentActivitiesToUser = boton.getActionCommand(); // Saul
				boton.setBackground(new Color(135, 206, 250));
				Component[] comp = getPnActivitiesForUsers().getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}

				if (manager.getPlazasLibresAct(currentActivitiesToUser) > 0) {
					getTxtInfPlazasAddUserToActivity()
							.setText("Quedan: " + manager.getPlazasLibresAct(currentActivitiesToUser) + " plazas");
				} else if (manager.getPlazasLibresAct(currentActivitiesToUser) < 0) {
					getTxtInfPlazasAddUserToActivity().setText("Plazas ilimitadas");
					getTxtInfPlazasAddUserToActivity().setForeground(Color.RED);
				} else {
					getTxtInfPlazasAddUserToActivity().setText("No quedan plazas");
					getTxtInfPlazasAddUserToActivity().setForeground(Color.RED);
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(getPnActivitiesForUsers().getBackground());
		getPnActivitiesForUsers().add(boton);
	}

	private JLabel getLblDniDelSocio() {
		if (lblDniDelSocio == null) {
			lblDniDelSocio = new JLabel("DNI del socio:");
		}
		return lblDniDelSocio;
	}

	private JTextField getTxtDNISocioApuntarAct() {
		if (txtDNISocioApuntarAct == null) {
			txtDNISocioApuntarAct = new JTextField();
			txtDNISocioApuntarAct.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent a) {
					char c = a.getKeyChar();
					if (Character.isWhitespace(c)
							|| (countNumbersDNI(txtDNISocioApuntarAct) == 8 && Character.isDigit(c))
							|| (countLettersDNI(txtDNISocioApuntarAct) == 1 && Character.isLetter(c))
							|| notCorrectDNI(txtDNISocioApuntarAct)) {
						a.consume();
					}
				}
			});
			txtDNISocioApuntarAct.setColumns(10);
		}
		return txtDNISocioApuntarAct;
	}

	private JButton getBtnApuntarSocioAAct() {
		if (btnApuntarSocioAAct == null) {
			btnApuntarSocioAAct = new JButton("Apuntar");
			btnApuntarSocioAAct.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (checkAddUserToAct()) {
						if (manager.isDniUser(getTxtDNISocioApuntarAct().getText())) { // el
																						// dni
																						// pertence
																						// a
																						// un
																						// usuario
							if (!manager.isUserInOtherActivity(manager.getBookingDate(currentActivitiesToUser),
									manager.getBookingHourI(currentActivitiesToUser),
									getTxtDNISocioApuntarAct().getText())) {// el
																			// usuario
																			// tiene
																			// otra
																			// actividad
								if (!manager.isUserRegisterToThisActivity(getTxtDNISocioApuntarAct().getText(),
										currentActivitiesToUser)) {// El usuario
																	// ya esta
																	// registrado
																	// en esta
																	// actividad
									if (!manager.isUserInOtherBooking(manager.getBookingDate(currentActivitiesToUser),
											manager.getBookingHourI(currentActivitiesToUser),
											getTxtDNISocioApuntarAct().getText())) {// El
																					// usuario
																					// tiene
																					// una
																					// reserva
																					// a
																					// la
																					// misma
																					// hora.
										if (manager.getPlazasLibresAct(currentActivitiesToUser) > 0) {
											if (manager.insertUserInListAssistants(currentActivitiesToUser,
													getTxtDNISocioApuntarAct().getText())) {
												JOptionPane.showMessageDialog(null, "Se ha completado correctamente",
														"", JOptionPane.INFORMATION_MESSAGE);
												resetPanelActivitiesForUsers();
												card.show(pnBase, "pnHomeView");

											}
										} else
											JOptionPane.showMessageDialog(null,
													"No hay mas plazas para la actividad seleccionada\n"
															+ "Si asiste a la clase y alguna de las plazas ha quedado "
															+ "libre podr� asistir",
													"", JOptionPane.INFORMATION_MESSAGE);

									} else
										JOptionPane.showMessageDialog(null,
												"Este socio tiene una instalaci�n " + "reservada a la misma hora",
												"Error", JOptionPane.ERROR_MESSAGE);
								} else
									JOptionPane.showMessageDialog(null, "Este socio ya se ha apuntado a esta actividad",
											"Error", JOptionPane.ERROR_MESSAGE);
							} else
								JOptionPane.showMessageDialog(null,
										"Este socio ya se ha apuntado a otra actividad que comienza a la misma hora que "
												+ "la seleccionada",
										"Error", JOptionPane.ERROR_MESSAGE);
						} else
							JOptionPane.showMessageDialog(null, "El DNI no pertenece a ning�n socio", "Error",
									JOptionPane.ERROR_MESSAGE);
					}
				}

			});
			btnApuntarSocioAAct.setBackground(Color.WHITE);
		}
		return btnApuntarSocioAAct;
	}

	private boolean checkAddUserToAct() {
		boolean valid = true;
		if (getTxtDNISocioApuntarAct().getText().isEmpty()) {
			valid = false;
			JOptionPane.showMessageDialog(null, "No ha introducido el DNI del socio", "Error",
					JOptionPane.ERROR_MESSAGE);
		} else if (currentActivitiesToUser.equals("")) {
			valid = false;
			JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna actividad", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return valid;
	}

	private JLabel getLblNoHayActividades() {
		if (lblNoHayActividades == null) {
			lblNoHayActividades = new JLabel("No hay actividades");
			lblNoHayActividades.setFont(new Font("Tw Cen MT", Font.BOLD, 20));
		}
		return lblNoHayActividades;
	}

	private JTextField getTxtInfPlazasAddUserToActivity() {
		if (txtInfPlazasAddUserToActivity == null) {
			txtInfPlazasAddUserToActivity = new JTextField();
			txtInfPlazasAddUserToActivity.setForeground(Color.BLACK);
			txtInfPlazasAddUserToActivity.setBackground(Color.WHITE);
			txtInfPlazasAddUserToActivity.setHorizontalAlignment(SwingConstants.CENTER);
			txtInfPlazasAddUserToActivity.setFont(new Font("Tw Cen MT", Font.BOLD | Font.ITALIC, 14));
			txtInfPlazasAddUserToActivity.setEditable(false);
			txtInfPlazasAddUserToActivity.setColumns(10);
		}
		return txtInfPlazasAddUserToActivity;
	}
	// ------------------------------------------------------------------------

	// Saul
	private JButton getBtnMonitor() {
		if (btnMonitor == null) {
			btnMonitor = new JButton("Monitor");
			btnMonitor.setFont(new Font("Tw Cen MT", Font.PLAIN, 13));
			btnMonitor.setBackground(Color.WHITE);
			btnMonitor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					getPnAcciones().removeAll();
					getPnAcciones().repaint();
					getPnAcciones().paintAll(pnAcciones.getGraphics());

					pnAcciones.add(getBtnInformacinSobreLas());

					getPnAcciones().remove(getBtnVerActividades());
					btnVerActividades.setEnabled(false);
					btnVerActividades.setVisible(false);

					getPnAcciones().remove(getBtnHorariosActividadCentro());
					btnVerHorarioAct.setEnabled(false);
					btnVerHorarioAct.setVisible(false);

					getPnAcciones().add(getBtnHorariosActividadMonitor());
					getBtnHorariosActividadMonitor().setEnabled(true);
					getBtnHorariosActividadMonitor().setVisible(true);

					getPnAcciones().remove(getBtnRegisterInputOuput());
					getBtnRegistroDeEntradasalida().setEnabled(false);
					getBtnRegistroDeEntradasalida().setVisible(false);

					getPnAcciones().remove(getBtnEstadoInstalaciones());
					btnEstadoInstalaciones.setEnabled(false);
					btnEstadoInstalaciones.setVisible(false);

					getPnAcciones().remove(getBtnCrearNuevaActividad());
					btnCrearNuevaActividad.setEnabled(false);
					btnCrearNuevaActividad.setVisible(false);

					getPnAcciones().remove(getVerActividadesDeUnDia());
					getVerActividadesDeUnDia().setEnabled(false);
					getVerActividadesDeUnDia().setVisible(false);

					getRdbtnAdminactivityinstallation().setVisible(false);
					getRdbtnAdminactivityinstallation().setEnabled(false);

					calendar.setMinSelectableDate(new Date());

					getPnAcciones().remove(getBtnVerMisReservas());
					getBtnVerMisReservas().setEnabled(false);
					getBtnVerMisReservas().setVisible(false);

					// getPnAcciones().add(getBtnVerActividadesAsignadas());
					// getBtnVerActividadesAsignadas().setEnabled(true);
					// getBtnVerActividadesAsignadas().setVisible(true);

					getPnAcciones().remove(getBtnApuntarSocioActividad());
					getBtnApuntarSocioActividad().setEnabled(false);
					getBtnApuntarSocioActividad().setVisible(false);

					getPnAcciones().remove(getTransferirACuota());
					btnTranferirACuota.setEnabled(false);
					btnTranferirACuota.setVisible(false);

					getPnAcciones().remove(getBtnGoToCalendar());
					getBtnGoToCalendar().setEnabled(false);
					getBtnGoToCalendar().setVisible(false);

					getPnAcciones().add(getBtnVerListaAsistentes());
					btnVerListaAsistentes.setEnabled(true);
					btnVerListaAsistentes.setVisible(true);

					getPnAcciones().remove(getBtnDesapuntarSocioActividad());
					getBtnDesapuntarSocioActividad().setEnabled(false);
					getBtnDesapuntarSocioActividad().setVisible(false);

					// getPnAcciones().remove(getBtnCancelarActividad_1());
					// getBtnCancelarActividad_1().setEnabled(false);
					// getBtnCancelarActividad_1().setVisible(false);

					signIn("monitor10101010M", "monitor");
				}
			});
		}
		return btnMonitor;
	}

	private JButton getBtnVerListaAsistentes() {
		if (btnVerListaAsistentes == null) {
			btnVerListaAsistentes = new JButton("Ver lista de asistentes a actividad");
			btnVerListaAsistentes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					pnListAssists.setVisible(false);
					pnListAssists.removeAll();
					pnListAssists.repaint();
					pnListAssists.setVisible(true);
					getPnListaClasesMonitorView().setVisible(false);
					getPnListaClasesMonitorView().removeAll();
					getPnListaClasesMonitorView().repaint();
					getPnListaClasesMonitorView().setVisible(true);
					showClassMonitorList();
					getLblLaClaseSeleccionada().setText("");
					currentClaseSeleccionadaParaPasarLista = "";
					card.show(pnBase, "pnSeeAssistantsList");

					pnListAssists.setVisible(false);
					pnListAssists.paintAll(pnListAssists.getGraphics());
					pnListAssists.setVisible(true);

				}
			});
			btnVerListaAsistentes.setBackground(Color.WHITE);
			btnVerListaAsistentes.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnVerListaAsistentes.setEnabled(false);
			btnVerListaAsistentes.setVisible(false);
		}
		return btnVerListaAsistentes;
	}

	private JPanel getPnSeeAssistsList() {
		if (pnSeeAssistsList == null) {
			pnSeeAssistsList = new JPanel();
			pnSeeAssistsList.setBackground(Color.WHITE);
			GridBagLayout gbl_pnSeeAssistsList = new GridBagLayout();
			gbl_pnSeeAssistsList.columnWidths = new int[] { 70, 313, 67, 224, 0, 0 };
			gbl_pnSeeAssistsList.rowHeights = new int[] { 0, 70, 127, 49, 0, 33, 0, 0 };
			gbl_pnSeeAssistsList.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnSeeAssistsList.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnSeeAssistsList.setLayout(gbl_pnSeeAssistsList);
			GridBagConstraints gbc_btnBackfromseeassistantslist = new GridBagConstraints();
			gbc_btnBackfromseeassistantslist.fill = GridBagConstraints.BOTH;
			gbc_btnBackfromseeassistantslist.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackfromseeassistantslist.gridx = 0;
			gbc_btnBackfromseeassistantslist.gridy = 1;
			pnSeeAssistsList.add(getBtnBackfromseeassistantslist(), gbc_btnBackfromseeassistantslist);
			GridBagConstraints gbc_pnListaClasesMonitor = new GridBagConstraints();
			gbc_pnListaClasesMonitor.gridheight = 3;
			gbc_pnListaClasesMonitor.insets = new Insets(0, 0, 5, 5);
			gbc_pnListaClasesMonitor.fill = GridBagConstraints.BOTH;
			gbc_pnListaClasesMonitor.gridx = 1;
			gbc_pnListaClasesMonitor.gridy = 1;
			pnSeeAssistsList.add(getPnListaClasesMonitor(), gbc_pnListaClasesMonitor);
			// pnSeeAssistantsList.add(getTxtListaAsistentes());
			GridBagConstraints gbc_pnListAssistsSelection = new GridBagConstraints();
			gbc_pnListAssistsSelection.insets = new Insets(0, 0, 5, 5);
			gbc_pnListAssistsSelection.fill = GridBagConstraints.BOTH;
			gbc_pnListAssistsSelection.gridheight = 5;
			gbc_pnListAssistsSelection.gridx = 3;
			gbc_pnListAssistsSelection.gridy = 1;
			pnSeeAssistsList.add(getPnListAssistsSelection(), gbc_pnListAssistsSelection);
			GridBagConstraints gbc_cBActListMonitor = new GridBagConstraints();
			gbc_cBActListMonitor.fill = GridBagConstraints.BOTH;
			gbc_cBActListMonitor.insets = new Insets(0, 0, 5, 5);
			gbc_cBActListMonitor.gridx = 1;
			gbc_cBActListMonitor.gridy = 1;
			GridBagConstraints gbc_lblLaClaseSeleccionada = new GridBagConstraints();
			gbc_lblLaClaseSeleccionada.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblLaClaseSeleccionada.insets = new Insets(0, 0, 5, 5);
			gbc_lblLaClaseSeleccionada.gridx = 1;
			gbc_lblLaClaseSeleccionada.gridy = 4;
			pnSeeAssistsList.add(getLblLaClaseSeleccionada(), gbc_lblLaClaseSeleccionada);
			// pnSeeAssistsList.add(getCBActListMonitor(),
			// gbc_cBActListMonitor);
			GridBagConstraints gbc_btnGuardarAsistencias = new GridBagConstraints();
			gbc_btnGuardarAsistencias.fill = GridBagConstraints.BOTH;
			gbc_btnGuardarAsistencias.insets = new Insets(0, 0, 5, 5);
			gbc_btnGuardarAsistencias.gridx = 1;
			gbc_btnGuardarAsistencias.gridy = 5;
			pnSeeAssistsList.add(getBtnGuardarAsistencias(), gbc_btnGuardarAsistencias);
		}
		return pnSeeAssistsList;
	}

	private JPanel getPnListAssistsSelection() {
		if (pnListAssistsSelection == null) {
			pnListAssistsSelection = new JPanel();
			pnListAssistsSelection.setBorder(new TitledBorder(new LineBorder(new Color(171, 173, 179)),
					"Lista de asistentes", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnListAssistsSelection.setBackground(Color.WHITE);
			pnListAssistsSelection.setLayout(new BorderLayout(0, 0));
			pnListAssistsSelection.add(getScrollPnListAssistsSelection(), BorderLayout.CENTER);
		}
		return pnListAssistsSelection;
	}

	private JScrollPane getScrollPnListAssistsSelection() {
		if (scrollPnListAssistsSelection == null) {
			scrollPnListAssistsSelection = new JScrollPane();
			scrollPnListAssistsSelection.setBackground(pnListAssistsSelection.getBackground());
			scrollPnListAssistsSelection
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPnListAssistsSelection.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPnListAssistsSelection.setViewportView(getPnListAssists());
		}
		return scrollPnListAssistsSelection;
	}

	private JPanel getPnListAssists() {
		if (pnListAssists == null) {
			pnListAssists = new JPanel();
			pnListAssists.setBackground(Color.WHITE);
			pnListAssists.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnListAssists.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnListAssists;
	}

	protected void showAssistantList() {
		ArrayList<String[]> assistants = manager.seeAssistsList(currentClaseSeleccionadaParaPasarLista,
				manager.getCurrentUser().getDNI());

		for (String act[] : assistants) {
			showAssistantListUniqueToUser(act[1] + " " + act[2], act[0]);
		}

		if (!manager.getNumAsistClass(currentClaseSeleccionadaParaPasarLista)) {
			JButton boton = new JButton();
			boton.setText("<html><p>No hay</p><p>socios apuntados</p></html>");
			boton.setFont(new Font("Tw Cen MT", Font.BOLD, 20));
			boton.setEnabled(false);
			getPnListAssists().add(boton);
		}
	}

	protected void showAssistantListUniqueToUser(String text, String dni) {
		JCheckBox boton = new JCheckBox();
		boton.setText(text);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
		boton.setActionCommand(dni);
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(getPnListAssists().getBackground());
		boton.setVisible(true);
		boton.setHorizontalAlignment(SwingConstants.LEFT);
		String id_reserva = currentClaseSeleccionadaParaPasarLista;
		if (manager.getAssistUser(id_reserva, dni)) {
			boton.setSelected(true);
		}
		if (manager.isValidSaveAssists(id_reserva)) {
			boton.setEnabled(false);
		}
		getPnListAssists().add(boton);
	}

	private JButton getBtnBackfromseeassistantslist() {
		if (btnBackfromseeassistantslist == null) {
			btnBackfromseeassistantslist = new JButton("");
			btnBackfromseeassistantslist.setBackground(Color.WHITE);

			btnBackfromseeassistantslist.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					pnListAssists.removeAll();
					pnListAssists.repaint();
					card.show(pnBase, "pnHomeView");
					pnListAssists.paintAll(pnListAssists.getGraphics());
					getPnListaClasesMonitorView().setVisible(false);
					getPnListaClasesMonitorView().removeAll();
					getPnListaClasesMonitorView().repaint();
					getPnListaClasesMonitorView().setVisible(true);
					getLblLaClaseSeleccionada().setText("");

				}
			});
			btnBackfromseeassistantslist.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackfromseeassistantslist.setBackground(pnSeeAssistsList.getBackground());
			btnBackfromseeassistantslist.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackfromseeassistantslist, "/img/arrow.png");
			btnBackfromseeassistantslist.setBorder(null);
		}
		return btnBackfromseeassistantslist;
	}

	private JButton getBtnGuardarAsistencias() {
		if (btnGuardarAsistencias == null) {
			btnGuardarAsistencias = new JButton("Guardar asistencias");
			btnGuardarAsistencias.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (currentClaseSeleccionadaParaPasarLista != "") {
						String id_reserva = currentClaseSeleccionadaParaPasarLista;
						if (checkHourToAssistant()) {
							if (!manager.isValidSaveAssists(id_reserva)) {
								Component[] comp = pnListAssists.getComponents();
								for (Component c : comp) {
									JCheckBox cb = (JCheckBox) c;
									String dni = cb.getActionCommand();
									if (cb.isSelected()) {
										manager.markAssists(dni, id_reserva, 1);
									} else {
										manager.markAssists(dni, id_reserva, 0);
									}
								}
								JOptionPane.showMessageDialog(null, "Se han guardado correctamente", "",
										JOptionPane.INFORMATION_MESSAGE);
								String dni = "";
								while (manager.getPlazasLibresAct(id_reserva) > 0 && dni != null) {
									dni = JOptionPane.showInputDialog("Introducir DNI del socio: ");
									if (manager.isDniUser(dni)) {
										// Segunda condici�n
										manager.insertUserInListAssistants(id_reserva, dni);
										manager.markAssists(dni, id_reserva, 1);
									}
								}
								JOptionPane.showMessageDialog(null, "Se ha acabado de pasar lista", "",
										JOptionPane.INFORMATION_MESSAGE);
								pnListAssists.removeAll();
								pnListAssists.repaint();
								card.show(pnBase, "pnHomeView");
								pnListAssists.paintAll(pnListAssists.getGraphics());
							} else
								JOptionPane.showMessageDialog(null,
										"Ya se ha realizado el guardado la asistencia de esta actividad", "Error",
										JOptionPane.ERROR_MESSAGE);

						} else
							JOptionPane.showMessageDialog(null,
									"No se pueden guardar las asistencias de \nesta actividad en este momento", "Error",
									JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna clase", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnGuardarAsistencias.setBackground(Color.WHITE);
			btnGuardarAsistencias.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
		}
		return btnGuardarAsistencias;
	}

	private boolean checkHourToAssistant() {
		String id_reverva = currentClaseSeleccionadaParaPasarLista;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(calendar.getDate());
		Calendar calendario = Calendar.getInstance();
		int hour = calendario.get(Calendar.HOUR_OF_DAY);
		int hourBooking = manager.getBookingHourI(id_reverva);
		int min = calendario.get(Calendar.MINUTE);
		// int min = 55;
		if (min >= 55 && hour == (hourBooking - 1) && fecha.equals(manager.getBookingDate(id_reverva)))
			return true;
		return false;
	}

	private JPanel getPnAcciones() {
		if (pnAcciones == null) {
			pnAcciones = new JPanel();
			pnAcciones.setBackground(pnHomeView.getBackground());
			pnAcciones.setBorder(new LineBorder(new Color(0, 0, 0)));
			pnAcciones.setLayout(new GridLayout(0, 1, 0, 0));
			// pnAcciones.add(getBtnVerActividadesAsignadas());
			pnAcciones.add(getBtnInformacinSobreLas());
			// pnAcciones.add(getBtnCancelarActividad_1());
			// pnAcciones.add(getBtnGoToCalendar());
			// pnAcciones.add(getBtnEstadoInstalaciones());
			// pnAcciones.add(getBtnRegistroDeEntradasalida());
			// pnAcciones.add(getBtnVerMisReservas());
			// pnAcciones.add(getBtnVerListaAsistentes());
			// pnAcciones.add(getBtnApuntarSocioActividad());
			// pnAcciones.add(getBtnCrearNuevaActividad());
		}
		return pnAcciones;
	}

	private JLabel getLblIps() {
		if (lblIps == null) {
			lblIps = new JLabel("<html><p>   IPS</p><p>Gym</p>");
			lblIps.setHorizontalAlignment(SwingConstants.CENTER);
			lblIps.setFont(new Font("Tw Cen MT", Font.BOLD, 76));
		}
		return lblIps;
	}

	private JButton getBtnContable() {
		if (btnContable == null) {
			btnContable = new JButton("Contable");
			btnContable.setFont(new Font("Tw Cen MT", Font.PLAIN, 13));
			btnContable.setBackground(Color.WHITE);
			btnContable.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					getPnAcciones().removeAll();
					getPnAcciones().repaint();
					getPnAcciones().paintAll(pnAcciones.getGraphics());
					pnAcciones.add(getBtnInformacinSobreLas());

					getPnAcciones().remove(getBtnRegistroDeEntradasalida());
					btnRegistroDeEntradasalida.setEnabled(false);
					btnRegistroDeEntradasalida.setVisible(false);

					getPnAcciones().remove(getBtnHorariosActividadCentro());
					btnVerHorarioAct.setEnabled(false);
					btnVerHorarioAct.setVisible(false);

					getPnAcciones().remove(getBtnHorariosActividadMonitor());
					getBtnHorariosActividadMonitor().setEnabled(false);
					getBtnHorariosActividadMonitor().setVisible(false);

					getPnAcciones().remove(getBtnVerActividades());
					btnVerActividades.setEnabled(false);
					btnVerActividades.setVisible(false);

					getPnAcciones().remove(getBtnEstadoInstalaciones());
					btnEstadoInstalaciones.setEnabled(false);
					btnEstadoInstalaciones.setVisible(false);

					// getPnAcciones().remove(getBtnVerActividadesAsignadas());
					// getBtnVerActividadesAsignadas().setEnabled(false);
					// getBtnVerActividadesAsignadas().setVisible(false);

					getPnAcciones().remove(getBtnCrearNuevaActividad());
					btnCrearNuevaActividad.setEnabled(false);
					btnCrearNuevaActividad.setVisible(false);

					getRdbtnAdminactivityinstallation().setVisible(false);
					getRdbtnAdminactivityinstallation().setEnabled(false);

					calendar.setMinSelectableDate(new Date());

					getPnAcciones().remove(getBtnVerMisReservas());
					getBtnVerMisReservas().setEnabled(false);
					getBtnVerMisReservas().setVisible(false);

					getPnAcciones().remove(getBtnApuntarSocioActividad());
					getBtnApuntarSocioActividad().setEnabled(false);
					getBtnApuntarSocioActividad().setVisible(false);

					getPnAcciones().remove(getBtnDesapuntarSocioActividad());
					getBtnDesapuntarSocioActividad().setEnabled(false);
					getBtnDesapuntarSocioActividad().setVisible(false);

					getPnAcciones().remove(getBtnGoToCalendar());
					getBtnGoToCalendar().setEnabled(false);
					getBtnGoToCalendar().setVisible(false);

					getPnAcciones().remove(getBtnVerListaAsistentes());
					btnVerListaAsistentes.setEnabled(false);
					btnVerListaAsistentes.setVisible(false);

					getPnAcciones().remove(getVerActividadesDeUnDia());
					getVerActividadesDeUnDia().setEnabled(false);
					getVerActividadesDeUnDia().setVisible(false);

					getPnAcciones().add(getTransferirACuota());
					btnTranferirACuota.setEnabled(true);
					btnTranferirACuota.setVisible(true);

					// getPnAcciones().remove(getBtnCancelarActividad_1());
					// getBtnCancelarActividad_1().setEnabled(false);
					// getBtnCancelarActividad_1().setVisible(false);

					signIn("contable12121212C", "contable");
					manager.lookMonthAndUpdateIfNecesario();
				}
			});
		}
		return btnContable;
	}

	private JButton getTransferirACuota() {
		if (btnTranferirACuota == null) {
			btnTranferirACuota = new JButton("Transferir cobros a la cuota de un socio");
			btnTranferirACuota.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnBase, "pnTransferirACuota");
					setEverythingToNullTransferir();
					fillMonthComboBox();
				}
			});
			btnTranferirACuota.setBackground(Color.WHITE);
			btnTranferirACuota.setEnabled(false);
			btnTranferirACuota.setVisible(false);
			btnTranferirACuota.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnTranferirACuota;
	}

	protected void fillMonthComboBox() {
		createdCombo = false;
		cmbBoxChooseMonthTransferir.removeAllItems();
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(date);
		int month = Integer.parseInt(fecha.substring(5, 7));
		int prevMonth = month - 1;
		cmbBoxChooseMonthTransferir.addItem(month);
		cmbBoxChooseMonthTransferir.addItem(prevMonth);
		createdCombo = true;
	}

	protected void fillComboBox() {
		cmbBoxDNITranferir.removeAllItems();
		if (createdCombo) {
			List<String> listMembers = manager
					.getMembersWithCuotaForPay((int) cmbBoxChooseMonthTransferir.getSelectedItem());
			for (String socio : listMembers) {
				cmbBoxDNITranferir.addItem(socio);
			}
		}
	}

	private JPanel getPnTransferirACuota() {
		if (pnTransferirACuota == null) {
			pnTransferirACuota = new JPanel();
			pnTransferirACuota.setBackground(pnBase.getBackground());
			GridBagLayout gbl_pnTransferirACuota = new GridBagLayout();
			gbl_pnTransferirACuota.columnWidths = new int[] { 42, 110, 116, 106, 152, 123, 0, 0 };
			gbl_pnTransferirACuota.rowHeights = new int[] { 39, 34, 0, 33, 34, 34, 58, 0, 0, 0 };
			gbl_pnTransferirACuota.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnTransferirACuota.rowWeights = new double[] { 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0,
					Double.MIN_VALUE };
			pnTransferirACuota.setLayout(gbl_pnTransferirACuota);
			GridBagConstraints gbc_lblMes = new GridBagConstraints();
			gbc_lblMes.fill = GridBagConstraints.VERTICAL;
			gbc_lblMes.anchor = GridBagConstraints.EAST;
			gbc_lblMes.insets = new Insets(0, 0, 5, 5);
			gbc_lblMes.gridx = 1;
			gbc_lblMes.gridy = 1;
			pnTransferirACuota.add(getLblMes(), gbc_lblMes);
			GridBagConstraints gbc_cmbBoxChooseMonthTransferir = new GridBagConstraints();
			gbc_cmbBoxChooseMonthTransferir.insets = new Insets(0, 0, 5, 5);
			gbc_cmbBoxChooseMonthTransferir.fill = GridBagConstraints.BOTH;
			gbc_cmbBoxChooseMonthTransferir.gridx = 2;
			gbc_cmbBoxChooseMonthTransferir.gridy = 1;
			pnTransferirACuota.add(getCmbBoxChooseMonthTransferir(), gbc_cmbBoxChooseMonthTransferir);
			GridBagConstraints gbc_lblDniSocioTransferir = new GridBagConstraints();
			gbc_lblDniSocioTransferir.anchor = GridBagConstraints.EAST;
			gbc_lblDniSocioTransferir.fill = GridBagConstraints.VERTICAL;
			gbc_lblDniSocioTransferir.insets = new Insets(0, 0, 5, 5);
			gbc_lblDniSocioTransferir.gridx = 3;
			gbc_lblDniSocioTransferir.gridy = 1;
			pnTransferirACuota.add(getLblDniSocioTransferir(), gbc_lblDniSocioTransferir);
			GridBagConstraints gbc_cmbBoxDNITranferir = new GridBagConstraints();
			gbc_cmbBoxDNITranferir.fill = GridBagConstraints.BOTH;
			gbc_cmbBoxDNITranferir.insets = new Insets(0, 0, 5, 5);
			gbc_cmbBoxDNITranferir.gridx = 4;
			gbc_cmbBoxDNITranferir.gridy = 1;
			pnTransferirACuota.add(getCmbBoxDNITranferir(), gbc_cmbBoxDNITranferir);
			GridBagConstraints gbc_btnMostrarPagosPendientes = new GridBagConstraints();
			gbc_btnMostrarPagosPendientes.fill = GridBagConstraints.BOTH;
			gbc_btnMostrarPagosPendientes.insets = new Insets(0, 0, 5, 5);
			gbc_btnMostrarPagosPendientes.gridx = 5;
			gbc_btnMostrarPagosPendientes.gridy = 1;
			pnTransferirACuota.add(getBtnMostrarPagosPendientes(), gbc_btnMostrarPagosPendientes);
			GridBagConstraints gbc_pnPagosATransferir = new GridBagConstraints();
			gbc_pnPagosATransferir.fill = GridBagConstraints.BOTH;
			gbc_pnPagosATransferir.insets = new Insets(0, 0, 5, 5);
			gbc_pnPagosATransferir.gridheight = 5;
			gbc_pnPagosATransferir.gridwidth = 3;
			gbc_pnPagosATransferir.gridx = 1;
			gbc_pnPagosATransferir.gridy = 3;
			pnTransferirACuota.add(getPnPagosATransferir(), gbc_pnPagosATransferir);
			GridBagConstraints gbc_lblCuotaFija = new GridBagConstraints();
			gbc_lblCuotaFija.fill = GridBagConstraints.VERTICAL;
			gbc_lblCuotaFija.anchor = GridBagConstraints.EAST;
			gbc_lblCuotaFija.insets = new Insets(0, 0, 5, 5);
			gbc_lblCuotaFija.gridx = 4;
			gbc_lblCuotaFija.gridy = 4;
			pnTransferirACuota.add(getLblCuotaFija(), gbc_lblCuotaFija);
			GridBagConstraints gbc_lblCuotaFijaNumero = new GridBagConstraints();
			gbc_lblCuotaFijaNumero.fill = GridBagConstraints.BOTH;
			gbc_lblCuotaFijaNumero.insets = new Insets(0, 0, 5, 5);
			gbc_lblCuotaFijaNumero.gridx = 5;
			gbc_lblCuotaFijaNumero.gridy = 4;
			pnTransferirACuota.add(getLblCuotaFijaNumero(), gbc_lblCuotaFijaNumero);
			GridBagConstraints gbc_lblCuotaAPagar = new GridBagConstraints();
			gbc_lblCuotaAPagar.anchor = GridBagConstraints.EAST;
			gbc_lblCuotaAPagar.fill = GridBagConstraints.VERTICAL;
			gbc_lblCuotaAPagar.insets = new Insets(0, 0, 5, 5);
			gbc_lblCuotaAPagar.gridx = 4;
			gbc_lblCuotaAPagar.gridy = 5;
			pnTransferirACuota.add(getLblCuotaAPagar(), gbc_lblCuotaAPagar);
			GridBagConstraints gbc_lblCuotaAPagarNumero = new GridBagConstraints();
			gbc_lblCuotaAPagarNumero.fill = GridBagConstraints.BOTH;
			gbc_lblCuotaAPagarNumero.insets = new Insets(0, 0, 5, 5);
			gbc_lblCuotaAPagarNumero.gridx = 5;
			gbc_lblCuotaAPagarNumero.gridy = 5;
			pnTransferirACuota.add(getLblCuotaAPagarNumero(), gbc_lblCuotaAPagarNumero);
			GridBagConstraints gbc_btnPasarTodosLos = new GridBagConstraints();
			gbc_btnPasarTodosLos.fill = GridBagConstraints.VERTICAL;
			gbc_btnPasarTodosLos.insets = new Insets(0, 0, 5, 5);
			gbc_btnPasarTodosLos.anchor = GridBagConstraints.EAST;
			gbc_btnPasarTodosLos.gridwidth = 2;
			gbc_btnPasarTodosLos.gridx = 4;
			gbc_btnPasarTodosLos.gridy = 7;
			pnTransferirACuota.add(getBtnPasarTodosLos(), gbc_btnPasarTodosLos);
		}
		return pnTransferirACuota;
	}

	private JLabel getLblDniSocioTransferir() {
		if (lblDniSocioTransferir == null) {
			lblDniSocioTransferir = new JLabel("DNI del socio:");
			lblDniSocioTransferir.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblDniSocioTransferir.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblDniSocioTransferir;
	}

	private JComboBox<String> getCmbBoxDNITranferir() {
		if (cmbBoxDNITranferir == null) {
			cmbBoxDNITranferir = new JComboBox<String>();
			cmbBoxDNITranferir.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			cmbBoxDNITranferir.setMaximumRowCount(100);
			cmbBoxDNITranferir.setModel(new DefaultComboBoxModel<String>(new String[] {}));
		}
		return cmbBoxDNITranferir;
	}

	private JButton getBtnMostrarPagosPendientes() {
		if (btnMostrarPagosPendientes == null) {
			btnMostrarPagosPendientes = new JButton("Mostrar pendientes");
			btnMostrarPagosPendientes.setBackground(Color.WHITE);
			btnMostrarPagosPendientes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (cmbBoxDNITranferir.getSelectedIndex() != -1
							&& cmbBoxChooseMonthTransferir.getSelectedIndex() != -1) {
						int apagar = showReservasSinPagarTransferir();
						int cuota = manager.getCuotaSocio(String.valueOf(cmbBoxDNITranferir.getSelectedItem()),
								(int) cmbBoxChooseMonthTransferir.getSelectedItem());
						lblCuotaFijaNumero.setText(cuota + " �");
						cuotaAPagar = apagar;
						lblCuotaAPagarNumero.setText(apagar + " �");
						btnPasarTodosLos.setEnabled(true);
					} else {
						JOptionPane.showMessageDialog(pnTransferirACuota, "No hay ningun DNI seleccionado");
					}
				}
			});
			btnMostrarPagosPendientes.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnMostrarPagosPendientes;
	}

	protected int showReservasSinPagarTransferir() {
		pnInfoTransferir.removeAll();
		List<Object[]> list = manager.getReservasSinPagarCuotaSocio(
				String.valueOf(cmbBoxDNITranferir.getSelectedItem()),
				(int) cmbBoxChooseMonthTransferir.getSelectedItem());
		int importe = 0;
		for (Object[] objects : list) {
			importe += showReservasSinPagarTransferirUnique(objects);
		}
		return importe;
	}

	private int showReservasSinPagarTransferirUnique(Object[] objects) {
		JTextPane label = new JTextPane();
		String text = "";
		text += "Reserva: " + objects[0];
		text += "\nFecha: " + objects[1];
		text += "\nPer�odo: " + objects[4] + " - " + objects[5];
		text += "\nAlquiler de la instalaci�n: " + objects[3];
		text += "\nPrecio: " + objects[2] + "�";
		label.setText(text);
		label.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		label.setBounds(0, 0, 100, 100);
		label.setBackground(pnInfoTransferir.getBackground());
		label.setEditable(false);
		pnInfoTransferir.add(label);
		return (int) objects[2];
	}

	private JPanel getPnPagosATransferir() {
		if (pnPagosATransferir == null) {
			pnPagosATransferir = new JPanel();
			pnPagosATransferir.setBackground(pnTransferirACuota.getBackground());
			pnPagosATransferir.setLayout(new BorderLayout(0, 0));
			pnPagosATransferir.add(getScrollTranferir());
		}
		return pnPagosATransferir;
	}

	private JLabel getLblCuotaFija() {
		if (lblCuotaFija == null) {
			lblCuotaFija = new JLabel("Cuota de el mes:");
			lblCuotaFija.setFont(new Font("Tw Cen MT", Font.PLAIN, 14));
			lblCuotaFija.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblCuotaFija;
	}

	private JLabel getLblCuotaFijaNumero() {
		if (lblCuotaFijaNumero == null) {
			lblCuotaFijaNumero = new JLabel("");
			lblCuotaFijaNumero.setFont(new Font("Tw Cen MT", Font.BOLD, 18));
		}
		return lblCuotaFijaNumero;
	}

	private JLabel getLblCuotaAPagar() {
		if (lblCuotaAPagar == null) {
			lblCuotaAPagar = new JLabel("A�adir a la cuota:");
			lblCuotaAPagar.setHorizontalAlignment(SwingConstants.RIGHT);
			lblCuotaAPagar.setFont(new Font("Tw Cen MT", Font.PLAIN, 14));
		}
		return lblCuotaAPagar;
	}

	private JLabel getLblCuotaAPagarNumero() {
		if (lblCuotaAPagarNumero == null) {
			lblCuotaAPagarNumero = new JLabel("");
			lblCuotaAPagarNumero.setFont(new Font("Tw Cen MT", Font.BOLD, 18));
		}
		return lblCuotaAPagarNumero;
	}

	private JButton getBtnPasarTodosLos() {
		if (btnPasarTodosLos == null) {
			btnPasarTodosLos = new JButton("Pasar todos los pendientes a la cuota");
			btnPasarTodosLos.setBackground(Color.WHITE);
			btnPasarTodosLos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					manager.updateCuotaSocio(String.valueOf(cmbBoxDNITranferir.getSelectedItem()), cuotaAPagar,
							(int) cmbBoxChooseMonthTransferir.getSelectedItem());
					JOptionPane.showMessageDialog(pnTransferirACuota,
							"�Se ha actualizado la cuota mensual correctamente!");
					setEverythingToNullTransferir();
					fillMonthComboBox();
				}
			});
			btnPasarTodosLos.setEnabled(false);
			btnPasarTodosLos.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnPasarTodosLos;
	}

	protected void setEverythingToNullTransferir() {
		lblCuotaAPagarNumero.setText("");
		lblCuotaFijaNumero.setText("");
		pnInfoTransferir.removeAll();
		pnInfoTransferir.setVisible(false);
		pnInfoTransferir.setVisible(true);
		// cmbBoxChooseMonthTransferir.removeAllItems();
		cmbBoxDNITranferir.removeAllItems();
		btnPasarTodosLos.setEnabled(false);
	}

	private JScrollPane getScrollTranferir() {
		if (scrollTranferir == null) {
			scrollTranferir = new JScrollPane();
			scrollTranferir.setViewportView(getPnInfoTransferir());
		}
		return scrollTranferir;
	}

	private JPanel getPnInfoTransferir() {
		if (pnInfoTransferir == null) {
			pnInfoTransferir = new JPanel();
			pnInfoTransferir.setBackground(pnPagosATransferir.getBackground());
			pnInfoTransferir.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnInfoTransferir;
	}

	private JButton getVerActividadesDeUnDia() {
		if (btnVerActividadesDeUnDia == null) {
			btnVerActividadesDeUnDia = new JButton("Ver todas las actividades");
			btnVerActividadesDeUnDia.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnBase, "pnVerActividadesDeUnDia");
					pnActivitiesViewInfo.removeAll();
					calendarViewActivities.setDate(new Date());
					lblFechaActivitiesView.setText("");
					fillComboBoxInstalacionesActivitiesView();
				}
			});
			btnVerActividadesDeUnDia.setBackground(Color.WHITE);
			btnVerActividadesDeUnDia.setEnabled(false);
			btnVerActividadesDeUnDia.setVisible(false);
			btnVerActividadesDeUnDia.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnVerActividadesDeUnDia;
	}

	protected void fillComboBoxInstalacionesActivitiesView() {
		cmbBoxInstActivitiesView.removeAllItems();
		List<String> list = manager.getInstallations();
		list.add(0, "Todas");
		for (String string : list) {
			cmbBoxInstActivitiesView.addItem(string);
		}
	}

	private JPanel getPnVerActividades() {
		if (pnVerActividades == null) {
			pnVerActividades = new JPanel();
			pnVerActividades.setBackground(Color.WHITE);
			GridBagLayout gbl_pnVerActividades = new GridBagLayout();
			gbl_pnVerActividades.columnWidths = new int[] { 73, 128, 176, 67, 272, 0 };
			gbl_pnVerActividades.rowHeights = new int[] { 66, 3, 269, 0, 0 };
			gbl_pnVerActividades.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnVerActividades.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnVerActividades.setLayout(gbl_pnVerActividades);
			GridBagConstraints gbc_btnAtrasVerActividades = new GridBagConstraints();
			gbc_btnAtrasVerActividades.fill = GridBagConstraints.BOTH;
			gbc_btnAtrasVerActividades.insets = new Insets(0, 0, 5, 5);
			gbc_btnAtrasVerActividades.gridx = 0;
			gbc_btnAtrasVerActividades.gridy = 0;
			pnVerActividades.add(getBtnAtrasVerActividades(), gbc_btnAtrasVerActividades);
			GridBagConstraints gbc_dateChooser = new GridBagConstraints();
			gbc_dateChooser.fill = GridBagConstraints.HORIZONTAL;
			gbc_dateChooser.insets = new Insets(0, 0, 5, 5);
			gbc_dateChooser.gridx = 2;
			gbc_dateChooser.gridy = 0;
			pnVerActividades.add(getDateChooser(), gbc_dateChooser);
			GridBagConstraints gbc_btnComprobarVerActividades = new GridBagConstraints();
			gbc_btnComprobarVerActividades.anchor = GridBagConstraints.WEST;
			gbc_btnComprobarVerActividades.insets = new Insets(0, 0, 5, 0);
			gbc_btnComprobarVerActividades.gridx = 4;
			gbc_btnComprobarVerActividades.gridy = 0;
			pnVerActividades.add(getButtonComprobarVerActividades(), gbc_btnComprobarVerActividades);
			GridBagConstraints gbc_labelSemana = new GridBagConstraints();
			gbc_labelSemana.insets = new Insets(0, 0, 5, 5);
			gbc_labelSemana.gridx = 2;
			gbc_labelSemana.gridy = 1;
			pnVerActividades.add(getLabelSemana(), gbc_labelSemana);
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridwidth = 5;
			gbc_panel.gridx = 0;
			gbc_panel.gridy = 2;
			pnVerActividades.add(getPanel(), gbc_panel);
		}
		return pnVerActividades;
	}

	private JButton getBtnAtrasVerActividades() {
		if (btnAtrasVerActividades == null) {
			btnAtrasVerActividades = new JButton("");
			btnAtrasVerActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
				}
			});
			;
			btnAtrasVerActividades.setBackground(pnVerActividades.getBackground());
			btnAtrasVerActividades.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnAtrasVerActividades, "/img/arrow.png");
			btnAtrasVerActividades.setBorder(null);
		}
		return btnAtrasVerActividades;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel.setLayout(new GridLayout(1, 7, 0, 1));
			panel.add(getScrollPnLunes());
			panel.add(getScrollPnMartes());
			panel.add(getScrollPnMiercoles());
			panel.add(getScrollPnJueves());
			panel.add(getScrollPnViernes());
			panel.add(getScrollPnSabado());
			panel.add(getScrollPnDomingo());
		}
		return panel;
	}

	private JPanel getPanelLunes() {
		if (panelLunes == null) {
			panelLunes = new JPanel();
			panelLunes.setBackground(Color.WHITE);
			panelLunes.setBorder(new TitledBorder(null, "Lunes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelLunes.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelLunes;
	}

	private JScrollPane getScrollPnLunes() {
		if (scrollPnLunes == null) {
			scrollPnLunes = new JScrollPane();
			scrollPnLunes.setBackground(Color.WHITE);
			scrollPnLunes.setViewportView(getPanelLunes());
		}
		return scrollPnLunes;
	}

	private JPanel getPanelMartes() {
		if (panelMartes == null) {
			panelMartes = new JPanel();
			panelMartes.setBackground(Color.WHITE);
			panelMartes.setBorder(new TitledBorder(null, "Martes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelMartes.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelMartes;
	}

	private JScrollPane getScrollPnMartes() {
		if (scrollPnMartes == null) {
			scrollPnMartes = new JScrollPane();
			scrollPnMartes.setBackground(Color.WHITE);
			scrollPnMartes.setViewportView(getPanelMartes());
		}
		return scrollPnMartes;
	}

	private JPanel getPanelMiercoles() {
		if (panelMiercoles == null) {
			panelMiercoles = new JPanel();
			panelMiercoles.setBackground(Color.WHITE);
			panelMiercoles.setBorder(
					new TitledBorder(null, "Mi\u00E9rcoles", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelMiercoles.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelMiercoles;
	}

	private JScrollPane getScrollPnMiercoles() {
		if (scrollPnMiercoles == null) {
			scrollPnMiercoles = new JScrollPane();
			scrollPnMiercoles.setBackground(Color.WHITE);
			scrollPnMiercoles.setViewportView(getPanelMiercoles());
		}
		return scrollPnMiercoles;
	}

	private JPanel getPanelJueves() {
		if (panelJueves == null) {
			panelJueves = new JPanel();
			panelJueves.setBackground(Color.WHITE);
			panelJueves.setBorder(new TitledBorder(null, "Jueves", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelJueves.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelJueves;
	}

	private JScrollPane getScrollPnJueves() {
		if (scrollPnJueves == null) {
			scrollPnJueves = new JScrollPane();
			scrollPnJueves.setBackground(Color.WHITE);
			scrollPnJueves.setViewportView(getPanelJueves());
		}
		return scrollPnJueves;
	}

	private JPanel getPanelViernes() {
		if (panelViernes == null) {
			panelViernes = new JPanel();
			panelViernes.setBackground(Color.WHITE);
			panelViernes
					.setBorder(new TitledBorder(null, "Viernes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelViernes.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelViernes;
	}

	private JScrollPane getScrollPnViernes() {
		if (scrollPnViernes == null) {
			scrollPnViernes = new JScrollPane();
			scrollPnViernes.setBackground(Color.WHITE);
			scrollPnViernes.setViewportView(getPanelViernes());
		}
		return scrollPnViernes;
	}

	private JPanel getPanelSabado() {
		if (panelSabado == null) {
			panelSabado = new JPanel();
			panelSabado.setBackground(Color.WHITE);
			panelSabado.setBorder(
					new TitledBorder(null, "S\u00E1bado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelSabado.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelSabado;
	}

	private JScrollPane getScrollPnSabado() {
		if (scrollPnSabado == null) {
			scrollPnSabado = new JScrollPane();
			scrollPnSabado.setBackground(Color.WHITE);
			scrollPnSabado.setViewportView(getPanelSabado());
		}
		return scrollPnSabado;
	}

	private JPanel getPanelDomingo() {
		if (panelDomingo == null) {
			panelDomingo = new JPanel();
			panelDomingo.setBackground(Color.WHITE);
			panelDomingo
					.setBorder(new TitledBorder(null, "Domingo", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panelDomingo.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return panelDomingo;
	}

	private JScrollPane getScrollPnDomingo() {
		if (scrollPnDomingo == null) {
			scrollPnDomingo = new JScrollPane();
			scrollPnDomingo.setBackground(Color.WHITE);
			scrollPnDomingo.setViewportView(getPanelDomingo());
		}
		return scrollPnDomingo;
	}

	private JButton getButtonComprobarVerActividades() {
		if (btnComprobarVerActividades == null) {
			btnComprobarVerActividades = new JButton("Comprobar");
			btnComprobarVerActividades.setBackground(Color.WHITE);
			btnComprobarVerActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					comprobarSemana();
				}
			});
		}
		return btnComprobarVerActividades;
	}

	private void comprobarSemana() {
		Calendar now = Calendar.getInstance();
		now.setTime(dateChooser.getDate());
		dateInicial = dateChooser.getDate();
		if (now.get(Calendar.DAY_OF_WEEK) == 2) {
			verLunes();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, 6);
			inicial = dateInicial;
			dateChooser.setDate(aux.getTime());
			finaL = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 3) {
			verMartes();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -1);
			dateChooser.setDate(aux.getTime());
			inicial = dateChooser.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooser.setDate(aux.getTime());
			finaL = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 4) {
			verMiercoles();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -2);
			dateChooser.setDate(aux.getTime());
			inicial = dateChooser.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooser.setDate(aux.getTime());
			finaL = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 5) {
			verJueves();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -3);
			dateChooser.setDate(aux.getTime());
			inicial = dateChooser.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooser.setDate(aux.getTime());
			finaL = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 6) {
			verViernes();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -4);
			dateChooser.setDate(aux.getTime());
			inicial = dateChooser.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooser.setDate(aux.getTime());
			finaL = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 7) {
			verSabado();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -5);
			dateChooser.setDate(aux.getTime());
			inicial = dateChooser.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooser.setDate(aux.getTime());
			finaL = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 1) {
			verDomingo();
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			finaL = dateInicial;
			aux.add(Calendar.DATE, -6);
			dateChooser.setDate(aux.getTime());
			inicial = dateChooser.getDate();
			dateChooser.setDate(dateInicial);
		}
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String fecha1 = df.format(inicial);
		String fecha2 = df.format(finaL);
		labelSemana.setText("Semana del " + fecha1 + " al " + fecha2);
	}

	private void actualizarDateChooser(int valor) {
		Calendar now = Calendar.getInstance();
		now.setTime(dateChooser.getDate());
		now.add(Calendar.DATE, valor);
		dateChooser.setDate(now.getTime());
	}

	private void verLunes() {
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
	}

	private void verMartes() {
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
		actualizarDateChooser(2);
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
	}

	private void verMiercoles() {
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(-2);
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(2);
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
	}

	private void verJueves() {
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
		actualizarDateChooser(-4);
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
	}

	private void verViernes() {
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
		actualizarDateChooser(-3);
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
	}

	private void verSabado() {
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(-1);
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
		actualizarDateChooser(6);
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
	}

	private void verDomingo() {
		showActivitiesOfTheDayVerActividades(panelDomingo, dateChooser.getDate());
		actualizarDateChooser(-6);
		showActivitiesOfTheDayVerActividades(panelLunes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelMartes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelMiercoles, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelJueves, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelViernes, dateChooser.getDate());
		actualizarDateChooser(1);
		showActivitiesOfTheDayVerActividades(panelSabado, dateChooser.getDate());
	}

	private void actualizarPaneles() {
		panelLunes.removeAll();
		getPanelLunes().repaint();
		panelMartes.removeAll();
		getPanelMartes().repaint();
		panelMiercoles.removeAll();
		getPanelMiercoles().repaint();
		panelJueves.removeAll();
		getPanelJueves().repaint();
		panelViernes.removeAll();
		getPanelViernes().repaint();
		panelSabado.removeAll();
		getPanelSabado().repaint();
		panelDomingo.removeAll();
		getPanelDomingo().repaint();
		dateChooser.setDate(new Date());
	}

	protected void showActivitiesOfTheDayVerActividades(JPanel panel, Date date) {
		panel.removeAll();
		panel.repaint();
		panel.paintAll(panel.getGraphics());
		List<String[]> list = null;
		list = manager.getAllActivitiesOfTheDay(date);

		if (list.size() > 0) {
			for (String[] strings : list) {
				showActivityOfTheDayUniqueVerActividades(panel, strings);
			}
		} else {
			JTextPane label = new JTextPane();
			label.setText("A�n no hay actividades para este \nd�a ni esta instalaci�n");
			label.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			label.setBounds(0, 0, 150, 50);
			label.setBackground(panel.getBackground());
			label.setEditable(false);
			panel.add(label);
		}
	}

	private void showActivityOfTheDayUniqueVerActividades(JPanel panel, String[] strings) {
		JTextPane label = new JTextPane();
		String text = "";
		text += "Clase: " + strings[0];
		text += "\nInstalaci�n: " + strings[1];
		text += "\nPer�odo: " + strings[2] + " - " + strings[3];
		label.setText(text);
		label.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		label.setBounds(0, 0, 150, 50);
		label.setBackground(panel.getBackground());
		label.setEditable(false);
		panel.add(label);
	}

	private JDateChooser getDateChooser() {
		if (dateChooser == null) {
			dateChooser = new JDateChooser();
			// dateChooser.getDateEditor().addPropertyChangeListener(new
			// PropertyChangeListener() {
			// public void propertyChange(PropertyChangeEvent arg0) {
			// try {
			// comprobarSemana();
			// }catch (Exception e){}
			// }
			// });
			dateChooser.setDate(new Date());
		}
		return dateChooser;
	}

	private JPanel getPnVerMisActividades() {
		if (pnVerMisActividades == null) {
			pnVerMisActividades = new JPanel();
			pnVerMisActividades.setBackground(Color.WHITE);
			GridBagLayout gbl_pnVerMisActividades = new GridBagLayout();
			gbl_pnVerMisActividades.columnWidths = new int[] { 106, 0, 291, 98, 226, 0 };
			gbl_pnVerMisActividades.rowHeights = new int[] { 87, 282, 0, 0 };
			gbl_pnVerMisActividades.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
			gbl_pnVerMisActividades.rowWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnVerMisActividades.setLayout(gbl_pnVerMisActividades);
			GridBagConstraints gbc_btnBackVerMisActividades = new GridBagConstraints();
			gbc_btnBackVerMisActividades.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackVerMisActividades.gridx = 0;
			gbc_btnBackVerMisActividades.gridy = 0;
			pnVerMisActividades.add(getBtnBackVerMisActividades(), gbc_btnBackVerMisActividades);
			GridBagConstraints gbc_labelActividadesApuntado = new GridBagConstraints();
			gbc_labelActividadesApuntado.insets = new Insets(0, 0, 5, 5);
			gbc_labelActividadesApuntado.gridx = 2;
			gbc_labelActividadesApuntado.gridy = 0;
			pnVerMisActividades.add(getLabelActividadesApuntado(), gbc_labelActividadesApuntado);
			GridBagConstraints gbc_pnActivitiesSelection = new GridBagConstraints();
			gbc_pnActivitiesSelection.insets = new Insets(0, 0, 5, 5);
			gbc_pnActivitiesSelection.fill = GridBagConstraints.BOTH;
			gbc_pnActivitiesSelection.gridx = 2;
			gbc_pnActivitiesSelection.gridy = 1;
			pnVerMisActividades.add(getPnActivitiesSelection(), gbc_pnActivitiesSelection);
		}
		return pnVerMisActividades;
	}

	private JPanel getPnActivitiesSelection() {
		if (pnActivitiesSelection == null) {
			pnActivitiesSelection = new JPanel();
			pnActivitiesSelection.setBackground(Color.WHITE);
			pnActivitiesSelection.setLayout(new BorderLayout(0, 0));
			pnActivitiesSelection.add(getScrollPnActivitiesSelection(), BorderLayout.CENTER);
		}
		return pnActivitiesSelection;
	}

	private JScrollPane getScrollPnActivitiesSelection() {
		if (scrollPnActivitiesSelection == null) {
			scrollPnActivitiesSelection = new JScrollPane();
			scrollPnActivitiesSelection.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesSelection
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesSelection.setBackground(new Color(255, 255, 255));
			scrollPnActivitiesSelection.setViewportView(getPnActivities());
		}
		return scrollPnActivitiesSelection;
	}

	private JPanel getPnActivities() {
		if (pnActivities == null) {
			pnActivities = new JPanel();
			pnActivities.setBackground(Color.WHITE);
			pnActivities.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnActivities;
	}

	private JLabel getLabelSemana() {
		if (labelSemana == null) {
			labelSemana = new JLabel();
			labelSemana.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return labelSemana;
	}

	private JButton getBtnBackVerMisActividades() {
		if (btnBackVerMisActividades == null) {
			btnBackVerMisActividades = new JButton();
			btnBackVerMisActividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
				}
			});
			btnBackVerMisActividades.setBackground(pnVerMisActividades.getBackground());
			btnBackVerMisActividades.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackVerMisActividades, "/img/arrow.png");
			btnBackVerMisActividades.setBorder(null);
		}
		return btnBackVerMisActividades;
	}

	private JLabel getLabelActividadesApuntado() {
		if (labelActividadesApuntado == null) {
			labelActividadesApuntado = new JLabel("Sus actividades reservadas son:");
			labelActividadesApuntado.setBackground(Color.WHITE);
			labelActividadesApuntado.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return labelActividadesApuntado;
	}

	// Alvaro
	private JButton getBtnDesapuntarSocioActividad() {
		if (btnDesapuntarSocioActividad == null) {
			btnDesapuntarSocioActividad = new JButton("Desapuntar a socio de actividad");
			btnDesapuntarSocioActividad.setBackground(Color.WHITE);
			btnDesapuntarSocioActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					pnActivitesAlvaro.removeAll();
					card.show(pnBase, "pnRemoveUserToActivity");
					showActivitiesToUser();
					pnActivitesAlvaro.paintAll(pnActivitesAlvaro.getGraphics());
					if (!manager.currentUserIsAdmin()) {
						txtApuntarDniSocio.setText(manager.getCurrentUser().getDNI());
						txtApuntarDniSocio.setEditable(false);
					} else {
						txtApuntarDniSocio.setText("");
						txtApuntarDniSocio.setEditable(true);
					}
				}
			});
			btnDesapuntarSocioActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnDesapuntarSocioActividad.setEnabled(false);
			btnDesapuntarSocioActividad.setVisible(false);
		}
		return btnDesapuntarSocioActividad;
	}

	private JComboBox<Integer> getCmbBoxChooseMonthTransferir() {
		if (cmbBoxChooseMonthTransferir == null) {
			cmbBoxChooseMonthTransferir = new JComboBox<Integer>();
			cmbBoxChooseMonthTransferir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (createdCombo) {
						fillComboBox();
						lblCuotaAPagarNumero.setText("");
						lblCuotaFijaNumero.setText("");
						pnInfoTransferir.removeAll();
						pnInfoTransferir.setVisible(false);
						pnInfoTransferir.setVisible(true);
						btnPasarTodosLos.setEnabled(false);

					}
				}
			});
		}
		return cmbBoxChooseMonthTransferir;
	}

	private JLabel getLblMes() {
		if (lblMes == null) {
			lblMes = new JLabel("Mes:");
			lblMes.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblMes;
	}

	private JRadioButton getRdbtnPuntual() {
		if (rdbtnPuntual == null) {
			rdbtnPuntual = new JRadioButton("Puntual");
			rdbtnPuntual.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			rdbtnPuntual.setBackground(Color.WHITE);
		}
		return rdbtnPuntual;
	}

	private JRadioButton getRdbtnPeriodica() {
		if (rdbtnPeriodica == null) {
			rdbtnPeriodica = new JRadioButton("Peri\u00F3dica");
			rdbtnPeriodica.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			rdbtnPeriodica.setBackground(Color.WHITE);
		}
		return rdbtnPeriodica;
	}

	private JLabel getLblPeriodicidad() {
		if (lblPeriodicidad == null) {
			lblPeriodicidad = new JLabel("Periodicidad:");
			lblPeriodicidad.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			lblPeriodicidad.setHorizontalAlignment(SwingConstants.RIGHT);
		}
		return lblPeriodicidad;
	}

	private JPanel getPnActividadPeriodicaCrear() {
		if (pnActividadPeriodicaCrear == null) {
			pnActividadPeriodicaCrear = new JPanel();
			pnActividadPeriodicaCrear.setBackground(pnBase.getBackground());
			pnActividadPeriodicaCrear.setLayout(new CardLayout(0, 0));
			pnActividadPeriodicaCrear.add(getPnDatosAcitividadPeriodica(), "datos");
			pnActividadPeriodicaCrear.add(getPnDataConflictsActivity(), "avisarTlfnos");
		}
		return pnActividadPeriodicaCrear;
	}

	private JLabel getLblDasEnLos() {
		if (lblDasEnLos == null) {
			lblDasEnLos = new JLabel("D\u00EDas en los que se realizar\u00E1 la actividad:");
			lblDasEnLos.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			lblDasEnLos.setHorizontalAlignment(SwingConstants.LEFT);
		}
		return lblDasEnLos;
	}

	private JPanel getPnDatosAcitividadPeriodica() {
		if (pnDatosAcitividadPeriodica == null) {
			pnDatosAcitividadPeriodica = new JPanel();
			pnDatosAcitividadPeriodica.setBackground(pnActividadPeriodicaCrear.getBackground());
			GridBagLayout gbl_pnDatosAcitividadPeriodica = new GridBagLayout();
			gbl_pnDatosAcitividadPeriodica.columnWidths = new int[] { 247, 0, 0, 0, 155, 21, 30, 109, 0, 0 };
			gbl_pnDatosAcitividadPeriodica.rowHeights = new int[] { 0, 16, 32, 16, 32, 16, 32, 16, 32, 16, 35, 0, 0 };
			gbl_pnDatosAcitividadPeriodica.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			gbl_pnDatosAcitividadPeriodica.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					1.0, 1.0, Double.MIN_VALUE };
			pnDatosAcitividadPeriodica.setLayout(gbl_pnDatosAcitividadPeriodica);
			GridBagConstraints gbc_pnInstalActividadPeriodicaAux = new GridBagConstraints();
			gbc_pnInstalActividadPeriodicaAux.fill = GridBagConstraints.BOTH;
			gbc_pnInstalActividadPeriodicaAux.insets = new Insets(0, 0, 0, 5);
			gbc_pnInstalActividadPeriodicaAux.gridheight = 11;
			gbc_pnInstalActividadPeriodicaAux.gridx = 0;
			gbc_pnInstalActividadPeriodicaAux.gridy = 1;
			pnDatosAcitividadPeriodica.add(getPnInstalActividadPeriodicaAux(), gbc_pnInstalActividadPeriodicaAux);
			GridBagConstraints gbc_lblDasEnLos = new GridBagConstraints();
			gbc_lblDasEnLos.fill = GridBagConstraints.BOTH;
			gbc_lblDasEnLos.insets = new Insets(0, 0, 5, 5);
			gbc_lblDasEnLos.gridwidth = 4;
			gbc_lblDasEnLos.gridx = 2;
			gbc_lblDasEnLos.gridy = 1;
			pnDatosAcitividadPeriodica.add(getLblDasEnLos(), gbc_lblDasEnLos);
			GridBagConstraints gbc_pnDiasParaActividadPeriodica = new GridBagConstraints();
			gbc_pnDiasParaActividadPeriodica.fill = GridBagConstraints.BOTH;
			gbc_pnDiasParaActividadPeriodica.insets = new Insets(0, 0, 5, 5);
			gbc_pnDiasParaActividadPeriodica.gridwidth = 6;
			gbc_pnDiasParaActividadPeriodica.gridx = 2;
			gbc_pnDiasParaActividadPeriodica.gridy = 2;
			pnDatosAcitividadPeriodica.add(getPnDiasParaActividadPeriodica(), gbc_pnDiasParaActividadPeriodica);
			GridBagConstraints gbc_lblDiaParaEmpezar = new GridBagConstraints();
			gbc_lblDiaParaEmpezar.gridwidth = 3;
			gbc_lblDiaParaEmpezar.fill = GridBagConstraints.BOTH;
			gbc_lblDiaParaEmpezar.insets = new Insets(0, 0, 5, 5);
			gbc_lblDiaParaEmpezar.gridx = 2;
			gbc_lblDiaParaEmpezar.gridy = 3;
			pnDatosAcitividadPeriodica.add(getLblDiaParaEmpezar(), gbc_lblDiaParaEmpezar);
			GridBagConstraints gbc_lblActividad = new GridBagConstraints();
			gbc_lblActividad.gridwidth = 3;
			gbc_lblActividad.fill = GridBagConstraints.BOTH;
			gbc_lblActividad.insets = new Insets(0, 0, 5, 5);
			gbc_lblActividad.gridx = 5;
			gbc_lblActividad.gridy = 3;
			pnDatosAcitividadPeriodica.add(getLblActividad(), gbc_lblActividad);
			GridBagConstraints gbc_dateActividadPeriodicaEmpezar = new GridBagConstraints();
			gbc_dateActividadPeriodicaEmpezar.gridwidth = 2;
			gbc_dateActividadPeriodicaEmpezar.fill = GridBagConstraints.BOTH;
			gbc_dateActividadPeriodicaEmpezar.insets = new Insets(0, 0, 5, 5);
			gbc_dateActividadPeriodicaEmpezar.gridx = 2;
			gbc_dateActividadPeriodicaEmpezar.gridy = 4;
			pnDatosAcitividadPeriodica.add(getDateActividadPeriodicaEmpezar(), gbc_dateActividadPeriodicaEmpezar);
			GridBagConstraints gbc_lblNombreActividadPeriodica = new GridBagConstraints();
			gbc_lblNombreActividadPeriodica.gridwidth = 3;
			gbc_lblNombreActividadPeriodica.fill = GridBagConstraints.BOTH;
			gbc_lblNombreActividadPeriodica.insets = new Insets(0, 0, 5, 5);
			gbc_lblNombreActividadPeriodica.gridx = 5;
			gbc_lblNombreActividadPeriodica.gridy = 4;
			pnDatosAcitividadPeriodica.add(getLblNombreActividadPeriodica(), gbc_lblNombreActividadPeriodica);
			GridBagConstraints gbc_lblNmeroDeSemanas = new GridBagConstraints();
			gbc_lblNmeroDeSemanas.gridwidth = 3;
			gbc_lblNmeroDeSemanas.fill = GridBagConstraints.BOTH;
			gbc_lblNmeroDeSemanas.insets = new Insets(0, 0, 5, 5);
			gbc_lblNmeroDeSemanas.gridx = 2;
			gbc_lblNmeroDeSemanas.gridy = 5;
			pnDatosAcitividadPeriodica.add(getLblNmeroDeSemanas(), gbc_lblNmeroDeSemanas);
			GridBagConstraints gbc_lblNmeroDePlazasPeriodica = new GridBagConstraints();
			gbc_lblNmeroDePlazasPeriodica.gridwidth = 3;
			gbc_lblNmeroDePlazasPeriodica.fill = GridBagConstraints.BOTH;
			gbc_lblNmeroDePlazasPeriodica.insets = new Insets(0, 0, 5, 5);
			gbc_lblNmeroDePlazasPeriodica.gridx = 5;
			gbc_lblNmeroDePlazasPeriodica.gridy = 5;
			pnDatosAcitividadPeriodica.add(getLblNmeroDePlazasPeriodica(), gbc_lblNmeroDePlazasPeriodica);
			GridBagConstraints gbc_dateActividadPeriodicaAcabar = new GridBagConstraints();
			gbc_dateActividadPeriodicaAcabar.gridwidth = 2;
			gbc_dateActividadPeriodicaAcabar.insets = new Insets(0, 0, 5, 5);
			gbc_dateActividadPeriodicaAcabar.fill = GridBagConstraints.BOTH;
			gbc_dateActividadPeriodicaAcabar.gridx = 2;
			gbc_dateActividadPeriodicaAcabar.gridy = 6;
			pnDatosAcitividadPeriodica.add(getDateActividadPeriodicaAcabar(), gbc_dateActividadPeriodicaAcabar);
			GridBagConstraints gbc_lblPlazasActividadPeriodica = new GridBagConstraints();
			gbc_lblPlazasActividadPeriodica.gridwidth = 3;
			gbc_lblPlazasActividadPeriodica.fill = GridBagConstraints.BOTH;
			gbc_lblPlazasActividadPeriodica.insets = new Insets(0, 0, 5, 5);
			gbc_lblPlazasActividadPeriodica.gridx = 5;
			gbc_lblPlazasActividadPeriodica.gridy = 6;
			pnDatosAcitividadPeriodica.add(getLblPlazasActividadPeriodica(), gbc_lblPlazasActividadPeriodica);
			GridBagConstraints gbc_lblHoraDeInicio = new GridBagConstraints();
			gbc_lblHoraDeInicio.gridwidth = 2;
			gbc_lblHoraDeInicio.fill = GridBagConstraints.BOTH;
			gbc_lblHoraDeInicio.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraDeInicio.gridx = 2;
			gbc_lblHoraDeInicio.gridy = 7;
			pnDatosAcitividadPeriodica.add(getLblHoraDeInicio(), gbc_lblHoraDeInicio);
			GridBagConstraints gbc_lblMonitor = new GridBagConstraints();
			gbc_lblMonitor.anchor = GridBagConstraints.WEST;
			gbc_lblMonitor.gridwidth = 3;
			gbc_lblMonitor.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonitor.gridx = 5;
			gbc_lblMonitor.gridy = 7;
			pnDatosAcitividadPeriodica.add(getLblMonitor(), gbc_lblMonitor);
			GridBagConstraints gbc_cmbBoxHoraDeInicio = new GridBagConstraints();
			gbc_cmbBoxHoraDeInicio.gridwidth = 2;
			gbc_cmbBoxHoraDeInicio.fill = GridBagConstraints.BOTH;
			gbc_cmbBoxHoraDeInicio.insets = new Insets(0, 0, 5, 5);
			gbc_cmbBoxHoraDeInicio.gridx = 2;
			gbc_cmbBoxHoraDeInicio.gridy = 8;
			pnDatosAcitividadPeriodica.add(getCmbBoxHoraDeInicio(), gbc_cmbBoxHoraDeInicio);
			GridBagConstraints gbc_cBMonitores = new GridBagConstraints();
			gbc_cBMonitores.gridwidth = 3;
			gbc_cBMonitores.insets = new Insets(0, 0, 5, 5);
			gbc_cBMonitores.fill = GridBagConstraints.BOTH;
			gbc_cBMonitores.gridx = 5;
			gbc_cBMonitores.gridy = 8;
			pnDatosAcitividadPeriodica.add(getCBMonitores(), gbc_cBMonitores);
			GridBagConstraints gbc_lblHoraDeFinal = new GridBagConstraints();
			gbc_lblHoraDeFinal.gridwidth = 2;
			gbc_lblHoraDeFinal.fill = GridBagConstraints.BOTH;
			gbc_lblHoraDeFinal.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraDeFinal.gridx = 2;
			gbc_lblHoraDeFinal.gridy = 9;
			pnDatosAcitividadPeriodica.add(getLblHoraDeFinal(), gbc_lblHoraDeFinal);
			GridBagConstraints gbc_cmbBoxHoraDeFinal = new GridBagConstraints();
			gbc_cmbBoxHoraDeFinal.gridwidth = 2;
			gbc_cmbBoxHoraDeFinal.fill = GridBagConstraints.BOTH;
			gbc_cmbBoxHoraDeFinal.insets = new Insets(0, 0, 5, 5);
			gbc_cmbBoxHoraDeFinal.gridx = 2;
			gbc_cmbBoxHoraDeFinal.gridy = 10;
			pnDatosAcitividadPeriodica.add(getCmbBoxHoraDeFinal(), gbc_cmbBoxHoraDeFinal);
			GridBagConstraints gbc_btnCrearActividad = new GridBagConstraints();
			gbc_btnCrearActividad.insets = new Insets(0, 0, 5, 5);
			gbc_btnCrearActividad.fill = GridBagConstraints.BOTH;
			gbc_btnCrearActividad.gridwidth = 3;
			gbc_btnCrearActividad.gridx = 5;
			gbc_btnCrearActividad.gridy = 10;
			pnDatosAcitividadPeriodica.add(getBtnCrearActividad(), gbc_btnCrearActividad);
		}
		return pnDatosAcitividadPeriodica;
	}

	private JPanel getPnInstalActividadPeriodicaAux() {
		if (pnInstalActividadPeriodicaAux == null) {
			pnInstalActividadPeriodicaAux = new JPanel();
			pnInstalActividadPeriodicaAux.setBackground(Color.WHITE);
			pnInstalActividadPeriodicaAux.setLayout(new BorderLayout(0, 0));
			pnInstalActividadPeriodicaAux.add(getScrollInstalActividadPeriodica(), BorderLayout.CENTER);
		}
		return pnInstalActividadPeriodicaAux;
	}

	private JScrollPane getScrollInstalActividadPeriodica() {
		if (scrollInstalActividadPeriodica == null) {
			scrollInstalActividadPeriodica = new JScrollPane();
			scrollInstalActividadPeriodica.setViewportView(getPnInstalacionActividadPeriodica());
		}
		return scrollInstalActividadPeriodica;
	}

	private JPanel getPnInstalacionActividadPeriodica() {
		if (pnInstalacionActividadPeriodica == null) {
			pnInstalacionActividadPeriodica = new JPanel();
			pnInstalacionActividadPeriodica.setBackground(Color.WHITE);
			pnInstalacionActividadPeriodica.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnInstalacionActividadPeriodica;
	}

	private JPanel getPnDiasParaActividadPeriodica() {
		if (pnDiasParaActividadPeriodica == null) {
			pnDiasParaActividadPeriodica = new JPanel();
			pnDiasParaActividadPeriodica.setBackground(Color.WHITE);
			pnDiasParaActividadPeriodica.setLayout(new GridLayout(1, 0, 0, 0));
			pnDiasParaActividadPeriodica.add(getChckbxLu());
			pnDiasParaActividadPeriodica.add(getChckbxMa());
			pnDiasParaActividadPeriodica.add(getChckbxMie());
			pnDiasParaActividadPeriodica.add(getChckbxJue());
			pnDiasParaActividadPeriodica.add(getChckbxVie());
			pnDiasParaActividadPeriodica.add(getChckbxSa());
			pnDiasParaActividadPeriodica.add(getChckbxDo());
		}
		return pnDiasParaActividadPeriodica;
	}

	private JCheckBox getChckbxLu() {
		if (chckbxLu == null) {
			chckbxLu = new JCheckBox("Lu");
			chckbxLu.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			chckbxLu.setToolTipText("Lunes");
			chckbxLu.setBackground(Color.WHITE);
		}
		return chckbxLu;
	}

	private JCheckBox getChckbxMa() {
		if (chckbxMa == null) {
			chckbxMa = new JCheckBox("Ma");
			chckbxMa.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			chckbxMa.setToolTipText("Martes");
			chckbxMa.setBackground(Color.WHITE);
		}
		return chckbxMa;
	}

	private JCheckBox getChckbxMie() {
		if (chckbxMie == null) {
			chckbxMie = new JCheckBox("Mie");
			chckbxMie.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			chckbxMie.setBackground(Color.WHITE);
			chckbxMie.setToolTipText("Mi\u00E9rcoles");
		}
		return chckbxMie;
	}

	private JCheckBox getChckbxJue() {
		if (chckbxJue == null) {
			chckbxJue = new JCheckBox("Jue");
			chckbxJue.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			chckbxJue.setToolTipText("Jueves");
			chckbxJue.setBackground(Color.WHITE);
		}
		return chckbxJue;
	}

	private JCheckBox getChckbxVie() {
		if (chckbxVie == null) {
			chckbxVie = new JCheckBox("Vie");
			chckbxVie.setBackground(Color.WHITE);
			chckbxVie.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			chckbxVie.setToolTipText("Viernes");
		}
		return chckbxVie;
	}

	private JCheckBox getChckbxSa() {
		if (chckbxSa == null) {
			chckbxSa = new JCheckBox("Sa");
			chckbxSa.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			chckbxSa.setToolTipText("S\u00E1bado");
			chckbxSa.setBackground(Color.WHITE);
		}
		return chckbxSa;
	}

	private JCheckBox getChckbxDo() {
		if (chckbxDo == null) {
			chckbxDo = new JCheckBox("Do");
			chckbxDo.setToolTipText("Domingo");
			chckbxDo.setBackground(Color.WHITE);
			chckbxDo.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return chckbxDo;
	}

	private JLabel getLblDiaParaEmpezar() {
		if (lblDiaParaEmpezar == null) {
			lblDiaParaEmpezar = new JLabel("D\u00EDa para empezar la actividad:");
			lblDiaParaEmpezar.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblDiaParaEmpezar;
	}

	private JDateChooser getDateActividadPeriodicaEmpezar() {
		if (dateActividadPeriodicaEmpezar == null) {
			dateActividadPeriodicaEmpezar = new JDateChooser();
			dateActividadPeriodicaEmpezar.setDateFormatString("dd-MM-yyyy");
			dateActividadPeriodicaEmpezar.setBackground(Color.WHITE);
			dateActividadPeriodicaEmpezar.setMinSelectableDate(new Date());
			dateActividadPeriodicaEmpezar.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent arg0) {
					dateActividadPeriodicaAcabar.setMinSelectableDate(dateActividadPeriodicaEmpezar.getDate());
					dateActividadPeriodicaAcabar.setEnabled(true);
				}
			});
		}
		return dateActividadPeriodicaEmpezar;
	}

	private JLabel getLblNmeroDeSemanas() {
		if (lblNmeroDeSemanas == null) {
			lblNmeroDeSemanas = new JLabel("D\u00EDa para acabar la actividad:");
			lblNmeroDeSemanas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblNmeroDeSemanas;
	}

	private JLabel getLblHoraDeInicio() {
		if (lblHoraDeInicio == null) {
			lblHoraDeInicio = new JLabel("Hora de inicio:");
			lblHoraDeInicio.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblHoraDeInicio;
	}

	private JLabel getLblHoraDeFinal() {
		if (lblHoraDeFinal == null) {
			lblHoraDeFinal = new JLabel("Hora de final:");
			lblHoraDeFinal.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblHoraDeFinal;
	}

	private JComboBox<String> getCmbBoxHoraDeInicio() {
		if (cmbBoxHoraDeInicio == null) {
			cmbBoxHoraDeInicio = new JComboBox<String>();
			cmbBoxHoraDeInicio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					generarHoras(cmbBoxHoraDeFinal, Integer.parseInt((String) cmbBoxHoraDeInicio.getSelectedItem()) + 1,
							23);
					cmbBoxHoraDeFinal.setEnabled(true);
				}
			});
			cmbBoxHoraDeInicio.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			cmbBoxHoraDeInicio.setMaximumRowCount(24);
			generarHoras(cmbBoxHoraDeInicio, 0, 23);
		}
		return cmbBoxHoraDeInicio;
	}

	private JComboBox<String> getCmbBoxHoraDeFinal() {
		if (cmbBoxHoraDeFinal == null) {
			cmbBoxHoraDeFinal = new JComboBox<String>();
			cmbBoxHoraDeFinal.setBackground(Color.WHITE);
			cmbBoxHoraDeFinal.setMaximumRowCount(24);
			cmbBoxHoraDeFinal.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			cmbBoxHoraDeFinal.setEnabled(false);
		}
		return cmbBoxHoraDeFinal;
	}

	private JLabel getLblActividad() {
		if (lblActividad == null) {
			lblActividad = new JLabel("Actividad:");
			lblActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblActividad;
	}

	private JLabel getLblNombreActividadPeriodica() {
		if (lblNombreActividadPeriodica == null) {
			lblNombreActividadPeriodica = new JLabel("");
			lblNombreActividadPeriodica.setBackground(Color.WHITE);
			lblNombreActividadPeriodica.setHorizontalAlignment(SwingConstants.CENTER);
			lblNombreActividadPeriodica.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblNombreActividadPeriodica;
	}

	private JLabel getLblNmeroDePlazasPeriodica() {
		if (lblNmeroDePlazasPeriodica == null) {
			lblNmeroDePlazasPeriodica = new JLabel("N\u00FAmero de plazas:");
			lblNmeroDePlazasPeriodica.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblNmeroDePlazasPeriodica;
	}

	private JLabel getLblPlazasActividadPeriodica() {
		if (lblPlazasActividadPeriodica == null) {
			lblPlazasActividadPeriodica = new JLabel("");
			lblPlazasActividadPeriodica.setBackground(Color.WHITE);
			lblPlazasActividadPeriodica.setHorizontalAlignment(SwingConstants.CENTER);
			lblPlazasActividadPeriodica.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblPlazasActividadPeriodica;
	}

	private JButton getBtnCrearActividad() {
		if (btnCrearActividad == null) {
			btnCrearActividad = new JButton("Crear Actividad");
			btnCrearActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (todoCorrecto()) {
						actividadPeriodica = true;
						String activityCode = generateCode();
						currentActivities = activityCode;
						currentActivitiesName = txtNombreAtividad.getText();
						String message = "Se ha creado la actividad: " + currentActivitiesName + "\nEn la instalaci�n: "
								+ currentInstallation + "\nDesde: " + cmbBoxHoraDeInicio.getSelectedItem() + "h hasta: "
								+ cmbBoxHoraDeFinal.getSelectedItem();
						message += "\nCon plazas ";
						int plazas = -1;
						if (rdbtnIlimitadas.isSelected()) {
							message += "ilimitadas\n";
							manager.crearActividad(currentActivitiesName, "-1", activityCode,
									getDNImonitor((String) getCBMonitores().getSelectedItem()));
						} else if (rdbtnLimitadas.isSelected()) {
							plazas = Integer.parseInt(txtNumeroDePlazas.getText());
							message += "limitadas a " + txtNumeroDePlazas.getText() + "participantes\n";
							manager.crearActividad(currentActivitiesName, txtNumeroDePlazas.getText(), activityCode,
									getDNImonitor((String) getCBMonitores().getSelectedItem()));
						}
						DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
						String fechaEmp = df.format(getDateActividadPeriodicaEmpezar().getDate());
						String fechaEnd = df.format(getDateActividadPeriodicaAcabar().getDate());
						message += "Desde el d�a: " + fechaEmp + "\n";
						message += "Hasta el d�a: " + fechaEnd + "\n";

						int conflicts = manager.checkConflictsWithTheNewActivity(
								getDateActividadPeriodicaEmpezar().getDate(),
								getDateActividadPeriodicaAcabar().getDate(), chckbxLu.isSelected(),
								chckbxMa.isSelected(), chckbxMie.isSelected(), chckbxJue.isSelected(),
								chckbxVie.isSelected(), chckbxSa.isSelected(), chckbxDo.isSelected(),
								Integer.parseInt((String) cmbBoxHoraDeInicio.getSelectedItem()),
								Integer.parseInt((String) cmbBoxHoraDeFinal.getSelectedItem()), currentInstallation,
								true);

						if (!checkMonitorOcupado()) {// Devuelve true si no hay
														// conflitos y false
														// cuando los hay
							JOptionPane.showMessageDialog(pnBase,
									"El monitor que ha seleccionado no esta disponible "
											+ "para alguno de los dias seleccionados",
									"Error", JOptionPane.ERROR_MESSAGE);
							manager.deleteclass(activityCode);
						} else {
							if (conflicts == 0) {
								manager.crearActividadPeriodica(currentActivitiesName, currentActivities,
										currentInstallation,
										Integer.parseInt((String) cmbBoxHoraDeInicio.getSelectedItem()),
										Integer.parseInt((String) cmbBoxHoraDeFinal.getSelectedItem()),
										getDNImonitor((String) getCBMonitores().getSelectedItem()), plazas);
								JOptionPane.showMessageDialog(pnBase, message,
										"Actividad periodica creada correctamente", JOptionPane.INFORMATION_MESSAGE);
								card.show(pnBase, "pnHomeView");
							} else {
								// rellenar datos
								fillUsuariosALosQueAvisar();
								cardPeriodica.show(pnActividadPeriodicaCrear, "avisarTlfnos");
							}
						}
					} else {
						JOptionPane.showMessageDialog(pnBase, "Falta alg�n campo por rellenar", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnCrearActividad.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnCrearActividad.setBackground(Color.WHITE);
		}
		return btnCrearActividad;
	}

	protected void fillUsuariosALosQueAvisar() {
		pnUsuariosAvisar.removeAll();
		List<String> list = manager.getUserConflictsString();
		for (String string : list) {
			JTextPane boton = new JTextPane();
			boton.setText(string);
			boton.setEditable(false);
			boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			pnUsuariosAvisar.add(boton);
		}
	}

	protected boolean todoCorrecto() {
		if (cmbBoxHoraDeFinal.isEnabled()
				&& (chckbxLu.isSelected() || chckbxMa.isSelected() || chckbxMie.isSelected() || chckbxJue.isSelected()
						|| chckbxVie.isSelected() || chckbxSa.isSelected() || chckbxDo.isSelected())
				&& currentInstallation != "") {
			return true;
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	private boolean fechaCoincideConSeleccionado() {
		int diaDeLaSemana = dateActividadPeriodicaEmpezar.getDate().getDay(); // 0
																				// domingo,
																				// 1
																				// lunes,
																				// 2
																				// martes
																				// ...
																				// 6
																				// sabado
		if (diaDeLaSemana == 1 && chckbxLu.isSelected()) {
			return true;
		} else if (diaDeLaSemana == 2 && chckbxMa.isSelected()) {
			return true;
		} else if (diaDeLaSemana == 3 && chckbxMie.isSelected()) {
			return true;
		} else if (diaDeLaSemana == 4 && chckbxJue.isSelected()) {
			return true;
		} else if (diaDeLaSemana == 5 && chckbxVie.isSelected()) {
			return true;
		} else if (diaDeLaSemana == 6 && chckbxSa.isSelected()) {
			return true;
		} else if (diaDeLaSemana == 0 && chckbxDo.isSelected()) {
			return true;
		}
		return false;
	}

	private JPanel getPnDataConflictsActivity() {
		if (pnDataConflictsActivity == null) {
			pnDataConflictsActivity = new JPanel();
			pnDataConflictsActivity.setBackground(Color.WHITE);
			GridBagLayout gbl_pnDataConflictsActivity = new GridBagLayout();
			gbl_pnDataConflictsActivity.columnWidths = new int[] { 0, 466, 0, 0, 0, 0 };
			gbl_pnDataConflictsActivity.rowHeights = new int[] { 0, 51, 296, 34, 24, 0 };
			gbl_pnDataConflictsActivity.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnDataConflictsActivity.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnDataConflictsActivity.setLayout(gbl_pnDataConflictsActivity);
			GridBagConstraints gbc_lblUsuariosQueSufrieron = new GridBagConstraints();
			gbc_lblUsuariosQueSufrieron.fill = GridBagConstraints.BOTH;
			gbc_lblUsuariosQueSufrieron.insets = new Insets(0, 0, 5, 5);
			gbc_lblUsuariosQueSufrieron.gridx = 1;
			gbc_lblUsuariosQueSufrieron.gridy = 1;
			pnDataConflictsActivity.add(getLblUsuariosQueSufrieron(), gbc_lblUsuariosQueSufrieron);
			GridBagConstraints gbc_pnUsuariosCancelacionesAux = new GridBagConstraints();
			gbc_pnUsuariosCancelacionesAux.gridwidth = 3;
			gbc_pnUsuariosCancelacionesAux.fill = GridBagConstraints.BOTH;
			gbc_pnUsuariosCancelacionesAux.insets = new Insets(0, 0, 5, 5);
			gbc_pnUsuariosCancelacionesAux.gridx = 1;
			gbc_pnUsuariosCancelacionesAux.gridy = 2;
			pnDataConflictsActivity.add(getPnUsuariosCancelacionesAux(), gbc_pnUsuariosCancelacionesAux);
			GridBagConstraints gbc_btnCancelarPeriodicaConflicto = new GridBagConstraints();
			gbc_btnCancelarPeriodicaConflicto.fill = GridBagConstraints.BOTH;
			gbc_btnCancelarPeriodicaConflicto.insets = new Insets(0, 0, 5, 5);
			gbc_btnCancelarPeriodicaConflicto.gridx = 2;
			gbc_btnCancelarPeriodicaConflicto.gridy = 3;
			pnDataConflictsActivity.add(getBtnCancelarPeriodicaConflicto(), gbc_btnCancelarPeriodicaConflicto);
			GridBagConstraints gbc_btnFinalizarPeriodicas = new GridBagConstraints();
			gbc_btnFinalizarPeriodicas.insets = new Insets(0, 0, 5, 5);
			gbc_btnFinalizarPeriodicas.fill = GridBagConstraints.BOTH;
			gbc_btnFinalizarPeriodicas.gridx = 3;
			gbc_btnFinalizarPeriodicas.gridy = 3;
			pnDataConflictsActivity.add(getBtnFinalizarPeriodicas(), gbc_btnFinalizarPeriodicas);
		}
		return pnDataConflictsActivity;
	}

	private JLabel getLblUsuariosQueSufrieron() {
		if (lblUsuariosQueSufrieron == null) {
			lblUsuariosQueSufrieron = new JLabel("Conflictos con reservas:");
			lblUsuariosQueSufrieron.setForeground(new Color(255, 69, 0));
			lblUsuariosQueSufrieron.setFont(new Font("Tw Cen MT", Font.BOLD, 18));
		}
		return lblUsuariosQueSufrieron;
	}

	private JPanel getPnUsuariosCancelacionesAux() {
		if (pnUsuariosCancelacionesAux == null) {
			pnUsuariosCancelacionesAux = new JPanel();
			pnUsuariosCancelacionesAux.setBackground(Color.WHITE);
			pnUsuariosCancelacionesAux.setLayout(new BorderLayout(0, 0));
			pnUsuariosCancelacionesAux.add(getScrollUsuariosAvisar(), BorderLayout.CENTER);
		}
		return pnUsuariosCancelacionesAux;
	}

	private JButton getBtnFinalizarPeriodicas() {
		if (btnFinalizarPeriodicas == null) {
			btnFinalizarPeriodicas = new JButton("Borrar y continuar");
			btnFinalizarPeriodicas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int plazas = -1;
					if (rdbtnLimitadas.isSelected()) {
						plazas = Integer.parseInt(txtNumeroDePlazas.getText());
					}
					int ans = JOptionPane.showConfirmDialog(pnBase,
							"�Desea continuar y por tanto, borrar estas reservas?", "Problema",
							JOptionPane.YES_NO_OPTION);
					if (actividadPeriodica) {

						if (ans == JOptionPane.YES_OPTION) {
							// cancelar las de conflicto
							manager.cancelAllTheBookingsConflict();
							// crearactividades
							manager.crearActividadPeriodica(currentActivitiesName, currentActivities,
									currentInstallation,
									Integer.parseInt((String) cmbBoxHoraDeInicio.getSelectedItem()),
									Integer.parseInt((String) cmbBoxHoraDeFinal.getSelectedItem()),
									getDNImonitor((String) getCBMonitores().getSelectedItem()), plazas);

							JOptionPane.showMessageDialog(pnBase, "�La clase se ha creado correctamente!");
							card.show(pnBase, "pnHomeView");
						}
					} else {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						String fecha = df.format(getCalendarActivities().getDate());
						if (ans == JOptionPane.YES_OPTION) {
							// cancelar las de conflicto
							manager.cancelAllTheBookingsConflict();
							// crearactividades
							manager.activityInstallationBookingAdm(currentActivitiesName, currentInstallation, fecha,
									Integer.parseInt((String) cbHourActivityInstallationI.getSelectedItem()),
									Integer.parseInt((String) cbHourActivityInstallationF.getSelectedItem()),
									manager.getCurrentUser().getDNI(), currentBookingCodePuntual, currentActivities,
									null, plazas, true);
							JOptionPane.showMessageDialog(null,
									"Se ha reservado correctamente la actividad: " + currentActivitiesName + ".\n"
											+ "En la instalaci�n: " + currentInstallation + ".\nDesde: "
											+ cbHourActivityInstallationI.getSelectedItem() + "h hasta: "
											+ cbHourActivityInstallationF.getSelectedItem() + "h.\n" + "El dia: "
											+ fecha + "." + currentMessagePuntual,
									"Informaci�n sobre reserva", JOptionPane.INFORMATION_MESSAGE);
							pnInstallations.removeAll();
							initializeAdmToActivityBooking();
							card.show(pnBase, "pnHomeView");
						}
					}
				}
			});
			btnFinalizarPeriodicas.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnFinalizarPeriodicas.setBackground(Color.WHITE);
		}
		return btnFinalizarPeriodicas;
	}

	private JScrollPane getScrollUsuariosAvisar() {
		if (scrollUsuariosAvisar == null) {
			scrollUsuariosAvisar = new JScrollPane();
			scrollUsuariosAvisar.setViewportView(getPnUsuariosAvisar());
		}
		return scrollUsuariosAvisar;
	}

	private JPanel getPnUsuariosAvisar() {
		if (pnUsuariosAvisar == null) {
			pnUsuariosAvisar = new JPanel();
			pnUsuariosAvisar.setBackground(Color.WHITE);
			pnUsuariosAvisar.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnUsuariosAvisar;
	}

	// 3 Sprint Saul
	private JLabel getLblMonitor() {
		if (lblMonitor == null) {
			lblMonitor = new JLabel("Monitor:");
			lblMonitor.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblMonitor;
	}

	private JComboBox<String> getCBMonitores() {
		if (cBMonitores == null) {
			cBMonitores = new JComboBox<String>();
			String[] model = manager.getDNIMonitores().toArray(new String[manager.getDNIMonitores().size()]);
			cBMonitores.setModel(new DefaultComboBoxModel<String>(model));
			cBMonitores.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return cBMonitores;
	}

	private boolean checkMonitorOcupado() {
		String dniMonitor = getDNImonitor((String) getCBMonitores().getSelectedItem());
		int horaInicio = Integer.parseInt((String) getCmbBoxHoraDeInicio().getSelectedItem());
		int horaFin = Integer.parseInt((String) getCmbBoxHoraDeFinal().getSelectedItem());
		return manager.checkMonitorOcupado(dniMonitor, horaInicio, horaFin);
	}

	private JLabel getLblMonitor_1() {
		if (lblMonitor_1 == null) {
			lblMonitor_1 = new JLabel("Monitor:");
			lblMonitor_1.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblMonitor_1;
	}

	private JComboBox<String> getCBMonitorPuntual() {
		if (cBMonitorPuntual == null) {
			cBMonitorPuntual = new JComboBox<String>();
			String[] model = manager.getDNIMonitores().toArray(new String[manager.getDNIMonitores().size()]);
			cBMonitorPuntual.setModel(new DefaultComboBoxModel<String>(model));
			cBMonitorPuntual.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return cBMonitorPuntual;
	}

	private String getDNImonitor(String txt) {
		return txt.split("<p>")[2].split("</p>")[0];
	}

	private JPanel getPnListaClasesMonitor() {
		if (pnListaClasesMonitor == null) {
			pnListaClasesMonitor = new JPanel();
			pnListaClasesMonitor.setBorder(new TitledBorder(new LineBorder(new Color(171, 173, 179)),
					"Clases a impartir", TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
			pnListaClasesMonitor.setBackground(Color.WHITE);
			pnListaClasesMonitor.setLayout(new BorderLayout(0, 0));
			pnListaClasesMonitor.add(getScrollPane());
		}
		return pnListaClasesMonitor;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getPnListaClasesMonitorView());
		}
		return scrollPane;
	}

	private JPanel getPnListaClasesMonitorView() {
		if (pnListaClasesMonitorView == null) {
			pnListaClasesMonitorView = new JPanel();
			pnListaClasesMonitorView.setBackground(Color.WHITE);
			pnListaClasesMonitorView.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnListaClasesMonitorView;
	}

	protected void showClassMonitorList() {
		List<String[]> clasesMonitor = manager.seeActMonitor(manager.getCurrentUser().getDNI());
		// [0]->idReserva [1]->Nombre instalaci�n [2]->Nombre clase [3]->fecha
		// [4]->horaI [5]->horaF
		for (String[] str : clasesMonitor) {
			String text = "<html><p>" + str[2] + "</p>" + "<p>El d�a: " + str[3] + "</p>" + "<p>En: " + str[1] + "</p>"
					+ "<p>De: " + str[4] + " a " + str[5] + " </p></html>";
			showClassMonitorUniqueToUser(text, str[0]);
		}
	}

	protected void showClassMonitorUniqueToUser(String text, String id_reserva) {
		JButton boton = new JButton();
		boton.setText(text);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
		boton.setActionCommand(id_reserva);
		boton.setBounds(0, 0, 100, 100);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boton.setBackground(new Color(135, 206, 250));
				Component[] comp = getPnListaClasesMonitorView().getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}

				currentClaseSeleccionadaParaPasarLista = boton.getActionCommand();
				HashMap<String, Object> info = manager.getInfoBookingActCancel(currentClaseSeleccionadaParaPasarLista);
				getLblLaClaseSeleccionada().setText("La clase de " + info.get("nombre_clase") + " se impartir� el d�a "
						+ info.get("fecha_usada") + " " + "desde las " + info.get("hora_usada_inicio")
						+ ":00 hasta las " + info.get("hora_usada_final") + ":00." + "\nCon el monitor "
						+ info.get("nombre_monitor") + " " + info.get("apellido_monitor") + " (con DNI "
						+ info.get("monitorDNI") + ").\nEn la instalaci�n " + info.get("nombre_inst") + ". Quedan "
						+ info.get("plazasLibres") + " plazas libres.");

				pnListAssists.removeAll();
				pnListAssists.repaint();
				showAssistantList();
				pnListAssists.paintAll(pnListAssists.getGraphics());

			}
		});
		boton.setBackground(getPnListAssists().getBackground());
		boton.setVisible(true);
		boton.setHorizontalAlignment(SwingConstants.LEFT);
		getPnListaClasesMonitorView().add(boton);
	}

	// private JPanel getPnCancelarActividad() {
	// if (pnCancelarActividad == null) {
	// pnCancelarActividad = new JPanel();
	// pnCancelarActividad.setBackground(Color.WHITE);
	// GridBagLayout gbl_pnCancelarActividad = new GridBagLayout();
	// gbl_pnCancelarActividad.columnWidths = new int[]{18, 64, 265, 0, 0, 21,
	// 150, 0, 148, 0, 0};
	// gbl_pnCancelarActividad.rowHeights = new int[]{41, 96, 22, 0, 54, 64, 0,
	// 0, 0, 38, 0, 0};
	// gbl_pnCancelarActividad.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0,
	// 0.0, 1.0, 1.0, 0.0, 1.0, 1.0,
	// Double.MIN_VALUE};
	// gbl_pnCancelarActividad.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0,
	// 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0,
	// Double.MIN_VALUE};
	// pnCancelarActividad.setLayout(gbl_pnCancelarActividad);
	// GridBagConstraints gbc_btnAtrs = new GridBagConstraints();
	// gbc_btnAtrs.fill = GridBagConstraints.BOTH;
	// gbc_btnAtrs.insets = new Insets(0, 0, 5, 5);
	// gbc_btnAtrs.gridx = 1;
	// gbc_btnAtrs.gridy = 1;
	// pnCancelarActividad.add(getBtnAtrs(), gbc_btnAtrs);
	// GridBagConstraints gbc_pnFiltros = new GridBagConstraints();
	// gbc_pnFiltros.gridwidth = 2;
	// gbc_pnFiltros.fill = GridBagConstraints.BOTH;
	// gbc_pnFiltros.insets = new Insets(0, 0, 5, 5);
	// gbc_pnFiltros.gridx = 2;
	// gbc_pnFiltros.gridy = 1;
	// pnCancelarActividad.add(getPnFiltros(), gbc_pnFiltros);
	// GridBagConstraints gbc_pnReservasCancelarAct = new GridBagConstraints();
	// gbc_pnReservasCancelarAct.gridheight = 2;
	// gbc_pnReservasCancelarAct.fill = GridBagConstraints.BOTH;
	// gbc_pnReservasCancelarAct.insets = new Insets(0, 0, 5, 5);
	// gbc_pnReservasCancelarAct.gridwidth = 5;
	// gbc_pnReservasCancelarAct.gridx = 4;
	// gbc_pnReservasCancelarAct.gridy = 1;
	// pnCancelarActividad.add(getPnReservasCancelarAct(),
	// gbc_pnReservasCancelarAct);
	// GridBagConstraints gbc_pnActividadesCanc = new GridBagConstraints();
	// gbc_pnActividadesCanc.fill = GridBagConstraints.BOTH;
	// gbc_pnActividadesCanc.insets = new Insets(0, 0, 5, 5);
	// gbc_pnActividadesCanc.gridheight = 8;
	// gbc_pnActividadesCanc.gridwidth = 2;
	// gbc_pnActividadesCanc.gridx = 1;
	// gbc_pnActividadesCanc.gridy = 2;
	// pnCancelarActividad.add(getPnActividadesCanc(), gbc_pnActividadesCanc);
	// GridBagConstraints gbc_pnInformacionReservaActCancelar = new
	// GridBagConstraints();
	// gbc_pnInformacionReservaActCancelar.gridheight = 3;
	// gbc_pnInformacionReservaActCancelar.fill = GridBagConstraints.BOTH;
	// gbc_pnInformacionReservaActCancelar.insets = new Insets(0, 0, 5, 5);
	// gbc_pnInformacionReservaActCancelar.gridwidth = 6;
	// gbc_pnInformacionReservaActCancelar.gridx = 3;
	// gbc_pnInformacionReservaActCancelar.gridy = 3;
	// pnCancelarActividad.add(getPnInformacionReservaActCancelar(),
	// gbc_pnInformacionReservaActCancelar);
	// GridBagConstraints gbc_pnBotonesActConcreta = new GridBagConstraints();
	// gbc_pnBotonesActConcreta.gridwidth = 4;
	// gbc_pnBotonesActConcreta.gridheight = 5;
	// gbc_pnBotonesActConcreta.insets = new Insets(0, 0, 0, 5);
	// gbc_pnBotonesActConcreta.fill = GridBagConstraints.BOTH;
	// gbc_pnBotonesActConcreta.gridx = 3;
	// gbc_pnBotonesActConcreta.gridy = 6;
	// pnCancelarActividad.add(getPnBotonesActConcreta(),
	// gbc_pnBotonesActConcreta);
	// groupCancelarActividadFiltroPlazas.add(getRdbtnPlazasLimitadas());
	// groupCancelarActividadFiltroPlazas.add(getRdbtnPlazasIlimitadas());
	// groupCancelarActividadFiltroPeriodicidad.add(getRdbtnActPuntual());
	// groupCancelarActividadFiltroPeriodicidad.add(getRdbtnActPeridica());
	// GridBagConstraints gbc_pnBotonesAct = new GridBagConstraints();
	// gbc_pnBotonesAct.gridwidth = 2;
	// gbc_pnBotonesAct.gridheight = 5;
	// gbc_pnBotonesAct.insets = new Insets(0, 0, 0, 5);
	// gbc_pnBotonesAct.fill = GridBagConstraints.BOTH;
	// gbc_pnBotonesAct.gridx = 7;
	// gbc_pnBotonesAct.gridy = 6;
	// pnCancelarActividad.add(getPnBotonesAct(), gbc_pnBotonesAct);
	// }
	// return pnCancelarActividad;
	// }
	// private JPanel getPnActividadesCanc() {
	// if (pnActividadesCanc == null) {
	// pnActividadesCanc = new JPanel();
	// pnActividadesCanc.setBorder(new TitledBorder(new LineBorder(new
	// Color(171, 173, 179)), "Actividades", TitledBorder.CENTER,
	// TitledBorder.TOP, null, new Color(0, 0, 0)));
	// pnActividadesCanc.setBackground(Color.WHITE);
	// pnActividadesCanc.setLayout(new BorderLayout(0, 0));
	// pnActividadesCanc.add(getScrollPane_1(), BorderLayout.CENTER);
	// }
	// return pnActividadesCanc;
	// }
	// private JScrollPane getScrollPane_1() {
	// if (scrollPane_1 == null) {
	// scrollPane_1 = new JScrollPane();
	// scrollPane_1.setBorder(null);
	// scrollPane_1.setViewportView(getPnActividadesCancView());
	// }
	// return scrollPane_1;
	// }
	// private JPanel getPnActividadesCancView() {
	// if (pnActividadesCancView == null) {
	// pnActividadesCancView = new JPanel();
	// pnActividadesCancView.setBorder(null);
	// pnActividadesCancView.setBackground(Color.WHITE);
	// pnActividadesCancView.setLayout(new GridLayout(0, 1, 0, 0));
	// }
	// return pnActividadesCancView;
	// }
	// private JPanel getPnFiltros() {
	// if (pnFiltros == null) {
	// pnFiltros = new JPanel();
	// pnFiltros.setBorder(new TitledBorder(new LineBorder(new Color(171, 173,
	// 179)), "Filtros", TitledBorder.CENTER,
	// TitledBorder.TOP, null, new Color(0, 0, 0)));
	// pnFiltros.setBackground(Color.WHITE);
	// pnFiltros.setLayout(new GridLayout(0, 1, 0, 0));
	// pnFiltros.add(getPnFiltrosTipoPlazas());
	// pnFiltros.add(getPnFiltrosPeriodicidad());
	// pnFiltros.add(getPnQuitarFiltros());
	// }
	// return pnFiltros;
	// }
	// private JPanel getPnFiltrosTipoPlazas() {
	// if (pnFiltrosTipoPlazas == null) {
	// pnFiltrosTipoPlazas = new JPanel();
	// pnFiltrosTipoPlazas.setBackground(Color.WHITE);
	// pnFiltrosTipoPlazas.setLayout(new GridLayout(1, 1, 0, 0));
	// pnFiltrosTipoPlazas.add(getRdbtnPlazasLimitadas());
	// pnFiltrosTipoPlazas.add(getRdbtnPlazasIlimitadas());
	// }
	// return pnFiltrosTipoPlazas;
	// }
	// private JPanel getPnFiltrosPeriodicidad() {
	// if (pnFiltrosPeriodicidad == null) {
	// pnFiltrosPeriodicidad = new JPanel();
	// pnFiltrosPeriodicidad.setLayout(new GridLayout(1, 0, 0, 0));
	// pnFiltrosPeriodicidad.add(getRdbtnActPuntual());
	// pnFiltrosPeriodicidad.add(getRdbtnActPeridica());
	// }
	// return pnFiltrosPeriodicidad;
	// }
	// private JRadioButton getRdbtnPlazasLimitadas() {
	// if (rdbtnPlazasLimitadas == null) {
	// rdbtnPlazasLimitadas = new JRadioButton("Plazas Limitadas");
	// rdbtnPlazasLimitadas.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent arg0) {
	// accionRadioBotonFiltroCancelarActividad();
	// }
	// });
	// rdbtnPlazasLimitadas.setBackground(Color.WHITE);
	// }
	// return rdbtnPlazasLimitadas;
	// }
	// private JRadioButton getRdbtnPlazasIlimitadas() {
	// if (rdbtnPlazasIlimitadas == null) {
	// rdbtnPlazasIlimitadas = new JRadioButton("Plazas Ilimitadas");
	// rdbtnPlazasIlimitadas.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent arg0) {
	// accionRadioBotonFiltroCancelarActividad();
	// }
	// });
	// rdbtnPlazasIlimitadas.setBackground(Color.WHITE);
	// }
	// return rdbtnPlazasIlimitadas;
	// }
	// private JRadioButton getRdbtnActPuntual() {
	// if (rdbtnActPuntual == null) {
	// rdbtnActPuntual = new JRadioButton("Act. Puntual");
	// rdbtnActPuntual.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent arg0) {
	// accionRadioBotonFiltroCancelarActividad();
	// }
	// });
	// rdbtnActPuntual.setBackground(Color.WHITE);
	// }
	// return rdbtnActPuntual;
	// }
	// private JRadioButton getRdbtnActPeridica() {
	// if (rdbtnActPeridica == null) {
	// rdbtnActPeridica = new JRadioButton("Act. Peri\u00F3dica");
	// rdbtnActPeridica.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent arg0) {
	// accionRadioBotonFiltroCancelarActividad();
	// }
	// });
	// rdbtnActPeridica.setBackground(Color.WHITE);
	// }
	// return rdbtnActPeridica;
	// }
	//
	// private void accionRadioBotonFiltroCancelarActividad() {
	//
	// getScrollPane_1().remove(getPnActividadesCancView());
	// pnActividadesCancView=null;
	//// getScrollPane_1().setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	// getPnActividadesCancView().repaint();
	// getPnActividadesCancView().paintAll(getPnActividadesCancView().getGraphics());
	// getScrollPane_1().setViewportView(getPnActividadesCancView());
	// showActivitiesToCancel();
	// getPnActividadesCancView().paintAll(getPnActividadesCancView().getGraphics());
	// currentReservaSeleccionadaParaCancelarAct = "";
	// getTxtInfoReservaConcreta().setText("");
	// currentClaseSeleccionadaParaCancelar = "";
	// getScrollPane_2().remove(getPnReservasCancelarActView());
	// getPnReservasCancelarActView().removeAll();
	// pnReservasCancelarActView=null;
	// getPnReservasCancelarActView().repaint();
	// getPnReservasCancelarActView().paintAll(getPnReservasCancelarActView().getGraphics());
	// getScrollPane_2().setViewportView(getPnReservasCancelarActView());
	//
	// Component[] comp2 =getPnBotonesActView().getComponents();
	// for (int i = 0; i < comp2.length; i++) {
	// JButton aux = (JButton) comp2[i];
	// aux.setEnabled(false);
	// }
	//
	// Component[] comp3 =getPnBotenesActConcretaView().getComponents();
	// for (int i = 0; i < comp3.length; i++) {
	// JButton aux = (JButton) comp3[i];
	// aux.setEnabled(false);
	// }
	//
	// }
	// private JPanel getPnReservasCancelarAct() {
	// if (pnReservasCancelarAct == null) {
	// pnReservasCancelarAct = new JPanel();
	// pnReservasCancelarAct.setBorder(new TitledBorder(new LineBorder(new
	// Color(171, 173, 179)), "Clases Concretas", TitledBorder.CENTER,
	// TitledBorder.TOP, null, new Color(0, 0, 0)));
	// pnReservasCancelarAct.setBackground(Color.WHITE);
	// pnReservasCancelarAct.setLayout(new BorderLayout(0, 0));
	// pnReservasCancelarAct.add(getScrollPane_2(), BorderLayout.CENTER);
	// }
	// return pnReservasCancelarAct;
	// }
	// private JScrollPane getScrollPane_2() {
	// if (scrollPane_2 == null) {
	// scrollPane_2 = new JScrollPane();
	// scrollPane_2.setBorder(null);
	// scrollPane_2.setViewportView(getPnReservasCancelarActView());
	// }
	// return scrollPane_2;
	// }
	// private JPanel getPnReservasCancelarActView() {
	// if (pnReservasCancelarActView == null) {
	// pnReservasCancelarActView = new JPanel();
	// pnReservasCancelarActView.setBorder(null);
	// pnReservasCancelarActView.setBackground(Color.WHITE);
	// pnReservasCancelarActView.setLayout(new GridLayout(1, 0, 0, 0));
	// }
	// return pnReservasCancelarActView;
	// }
	// private JPanel getPnInformacionReservaActCancelar() {
	// if (pnInformacionReservaActCancelar == null) {
	// pnInformacionReservaActCancelar = new JPanel();
	// pnInformacionReservaActCancelar.setBorder(new TitledBorder(new
	// LineBorder(new Color(171, 173, 179)),
	// "Informacion de Clase Concreta", TitledBorder.CENTER, TitledBorder.TOP,
	// null, new Color(0, 0, 0)));
	// pnInformacionReservaActCancelar.setBackground(Color.WHITE);
	// pnInformacionReservaActCancelar.setLayout(new BorderLayout(0, 0));
	// pnInformacionReservaActCancelar.add(getScrollPane_3(),
	// BorderLayout.CENTER);
	// }
	// return pnInformacionReservaActCancelar;
	// }
	// private JScrollPane getScrollPane_3() {
	// if (scrollPane_3 == null) {
	// scrollPane_3 = new JScrollPane();
	// scrollPane_3.setBorder(null);
	// scrollPane_3.setViewportView(getTxtInfoReservaConcreta());
	// }
	// return scrollPane_3;
	// }
	// private JTextArea getTxtInfoReservaConcreta() {
	// if (txtInfoReservaConcreta == null) {
	// txtInfoReservaConcreta = new JTextArea();
	// txtInfoReservaConcreta.setBackground(Color.WHITE);
	// txtInfoReservaConcreta.setBorder(new EmptyBorder(3, 3, 3, 3));
	// txtInfoReservaConcreta.setFont(new Font("Tw Cen MT", Font.BOLD |
	// Font.ITALIC, 14));
	// txtInfoReservaConcreta.setEditable(false);
	// }
	// return txtInfoReservaConcreta;
	// }
	// Gonzi YOLO
	private JButton getBtnCancelarActividad() {
		if (btnCancelarActividad == null) {
			btnCancelarActividad = new JButton("Cancelar Actividad/Clase");
			btnCancelarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String[] options = { "Cancelar actividad", "Cancelar clase concreta", "Atrás" };
					int seleccion = JOptionPane.showOptionDialog(null,
							"Seleccione la opción de cancelación que desea", "Atrás", JOptionPane.DEFAULT_OPTION,
							JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
					if (seleccion == 0) {
						// currentReservaSeleccionadaParaCancelarAct
						manager.cancelarActividad(currentClaseSeleccionadaHorarioIdClase);
						manager.borrarActividadFromDatabase(currentClaseSeleccionadaHorarioIdClase);
						JOptionPane.showMessageDialog(null, "Actividad cancelada con éxito");
						if (manager.getPlazasLibres(currentClaseSeleccionadaHorario) != -1) {
							List<String[]> lista = manager
									.obtenerAsistentesActividad(currentClaseSeleccionadaHorarioIdClase);
							if (lista.size() > 0) {
								card.show(pnBase, "pnCambiosCancelarActClass");
								cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnAvisarAsistentes");
								txtAreaAvisarAsistentes.setText("");
								String text = "";
								for (String[] str : lista) {

									text = text + str[0] + "   " + str[1] + " DNI: " + str[2] + "   Teléfono: "
											+ str[3] + "\n";
									txtAreaAvisarAsistentes.setText(text);
								}
							} else {
								card.show(pnBase, "pnHorarioActividadesCentro");
								comprobarSemanaHorarios(manager.getActParaHorarioCentro(
										(String) getCBInstalacionesAfiltrar().getSelectedItem()));
							}
						} else {
							card.show(pnBase, "pnHorarioActividadesCentro");
							comprobarSemanaHorarios(manager
									.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
						}
					} else if (seleccion == 1) {
						manager.cancelarClaseConcretaActividad(currentClaseSeleccionadaHorario);
						JOptionPane.showMessageDialog(null, "Clase cancelada con éxito");
						if (manager.getPlazasLibres(currentClaseSeleccionadaHorario) != -1) {
							List<String[]> lista = manager.obtenerAsistentesClase(currentClaseSeleccionadaHorario);
							if (lista.size() > 0) {
								card.show(pnBase, "pnCambiosCancelarActClass");
								cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnAvisarAsistentes");
								txtAreaAvisarAsistentes.setText("");
								String text = "";
								for (String[] str : lista) {

									text = text + str[0] + " " + str[1] + "  DNI: " + str[2] + "   Teléfono: " + str[3]
											+ "\n";
									txtAreaAvisarAsistentes.setText(text);
								}
							} else {
								card.show(pnBase, "pnHorarioActividadesCentro");
								comprobarSemanaHorarios(manager.getActParaHorarioCentro(
										(String) getCBInstalacionesAfiltrar().getSelectedItem()));
							}
						} else {
							card.show(pnBase, "pnHorarioActividadesCentro");
							comprobarSemanaHorarios(manager
									.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
						}
					}

				}
			});
			btnCancelarActividad.setBackground(Color.WHITE);
		}
		return btnCancelarActividad;
	}

	private JButton getBtnCancelarClaseConcreto() {
		if (btnCancelarClaseConcreto == null) {
			btnCancelarClaseConcreto = new JButton("Cancelar Clase Concreta");
			btnCancelarClaseConcreto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					HashMap<String, Object> str = getManager()
							.getInfoBookingActCancel(currentReservaSeleccionadaParaCancelarAct);
					String text = "";
					// for (HashMap<String, Object> str : acts) {
					text = "La clase se realizar� en la instalaci�n: \n\t" + (String) str.get("nombre_inst") + ". \n\n"
							+ "El d�a: " + (String) str.get("fecha_usada") + " desde las "
							+ String.valueOf((Integer) str.get("hora_usada_inicio")) + "h a "
							+ String.valueOf((Integer) str.get("hora_usada_final")) + "h. \n\n"
							+ "El monitor que impartir� la clase ser�: \n\t" + (String) str.get("nombre_monitor") + " "
							+ (String) str.get("apellido_monitor") + " con DNI: " + (String) str.get("monitorDNI");
					if ((Integer) str.get("plazasLibres") >= 0)
						text += "\n\nQuedan: " + (Integer) str.get("plazasLibres") + " plazas libres.";
					else
						text += "\n\nPlazas Ilimitadas.";
					// }
				}
			});
			btnCancelarClaseConcreto.setEnabled(false);
			btnCancelarClaseConcreto.setBackground(Color.WHITE);
		}
		return btnCancelarClaseConcreto;
	}
	// private JButton getBtnAtrs() {
	// if (btnAtrs == null) {
	// btnAtrs = new JButton("");
	// btnAtrs.setBackground(Color.WHITE);
	// btnAtrs.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent e) {
	// card.show(pnBase, "pnHomeView");
	// groupCancelarActividadFiltroPeriodicidad.clearSelection();
	// groupCancelarActividadFiltroPlazas.clearSelection();
	// accionRadioBotonFiltroCancelarActividad();
	// }
	// });
	// ;
	// btnAtrs.setBackground(pnCancelarActividad.getBackground());
	// btnAtrs.setBounds(0, 0, 70, 70);
	// setAdaptedImageButton(btnAtrs, "/img/arrow.png");
	// btnAtrs.setBorder(null);
	// }
	// return btnAtrs;
	// }
	// private JButton getBtnCancelarActividad_1() {
	// if (btnCancelarActividad_1 == null) {
	// btnCancelarActividad_1 = new JButton("Ver Detalles Actividades");
	// btnCancelarActividad_1.setBackground(Color.WHITE);
	// btnCancelarActividad_1.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
	// btnCancelarActividad_1.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent e) {
	//// clearCancelarActi();
	// card.show(pnBase, "pnCancelarActividad");
	//// showActivitiesToCancel();
	//
	// }
	// });
	// }
	// return btnCancelarActividad_1;
	// }

	// private void showActivitiesToCancel() {
	// List<HashMap<String, Object>> acts=null;
	// if(!getRdbtnActPeridica().isSelected() &&
	// !getRdbtnActPuntual().isSelected() &&
	// !getRdbtnPlazasIlimitadas().isSelected() &&
	// !getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookings();
	// else if(getRdbtnActPeridica().isSelected() &&
	// !getRdbtnActPuntual().isSelected() &&
	// !getRdbtnPlazasIlimitadas().isSelected() &&
	// !getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsPeriodicas();
	// else if(!getRdbtnActPeridica().isSelected() &&
	// getRdbtnActPuntual().isSelected() &&
	// !getRdbtnPlazasIlimitadas().isSelected() &&
	// !getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsPuntual();
	// else if(!getRdbtnActPeridica().isSelected() &&
	// !getRdbtnActPuntual().isSelected() &&
	// getRdbtnPlazasIlimitadas().isSelected() &&
	// !getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsIlimitadas();
	// else if(!getRdbtnActPeridica().isSelected() &&
	// !getRdbtnActPuntual().isSelected() &&
	// !getRdbtnPlazasIlimitadas().isSelected() &&
	// getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsLimitadas();
	// else if(!getRdbtnActPeridica().isSelected() &&
	// getRdbtnActPuntual().isSelected() &&
	// !getRdbtnPlazasIlimitadas().isSelected() &&
	// getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsLimitadasPuntual();
	// else if(getRdbtnActPeridica().isSelected() &&
	// !getRdbtnActPuntual().isSelected() &&
	// !getRdbtnPlazasIlimitadas().isSelected() &&
	// getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsLimitadasPeriodica();
	// else if(!getRdbtnActPeridica().isSelected() &&
	// getRdbtnActPuntual().isSelected() &&
	// getRdbtnPlazasIlimitadas().isSelected() &&
	// !getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsIlimitadasPuntual();
	// else if(getRdbtnActPeridica().isSelected() &&
	// !getRdbtnActPuntual().isSelected() &&
	// getRdbtnPlazasIlimitadas().isSelected() &&
	// !getRdbtnPlazasLimitadas().isSelected())
	// acts = getManager().getActivitiesWithFutureBookingsIlimitadasPeriodica();
	//
	// if(acts.isEmpty())
	// getPnActividadesCancView().add(getLblNoHayActividades_1());
	// else getPnActividadesCancView().remove(getLblNoHayActividades_1());
	//
	// for (HashMap<String, Object> str : acts) {
	// showActivitiesToCancelUnique((String) str.get("nombre"),
	// (String)str.get("id"));
	// }
	//
	// }

	// protected void showActivitiesToCancelUnique(String nombre, String
	// id_clase) {
	// JButton boton = new JButton();
	// boton.setText(nombre);
	// boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
	// boton.setActionCommand(id_clase);
	// boton.setHorizontalAlignment(SwingConstants.CENTER);
	// boton.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent arg0) {
	// boton.setBackground(new Color(135, 206, 250));
	//
	// Component[] comp =getPnActividadesCancView().getComponents();
	// for (int i = 0; i < comp.length; i++) {
	// JButton aux = (JButton) comp[i];
	// if (!aux.getActionCommand().equals(boton.getActionCommand()))
	// aux.setBackground(Color.WHITE);
	// }
	// currentReservaSeleccionadaParaCancelarAct = "";
	// getTxtInfoReservaConcreta().setText("");
	// currentClaseSeleccionadaParaCancelar = boton.getActionCommand();
	// getPnReservasCancelarActView().removeAll();
	//
	// getPnReservasCancelarActView().repaint();
	// getPnReservasCancelarActView().paintAll(getPnReservasCancelarActView().getGraphics());
	// showBookingActivitiesToCancel();
	// getPnReservasCancelarActView().paintAll(getPnReservasCancelarActView().getGraphics());
	//
	// Component[] comp2 =getPnBotonesActView().getComponents();
	// for (int i = 0; i < comp2.length; i++) {
	// JButton aux = (JButton) comp2[i];
	// aux.setEnabled(true);
	// }
	// }
	// });
	// boton.setBackground(getPnCancelarActividad().getBackground());
	// boton.setVisible(true);
	// getPnActividadesCancView().add(boton);
	// }

	// private void showBookingActivitiesToCancel() {
	// List<HashMap<String, Object>> acts =
	// getManager().getBookingsOfActivity(currentClaseSeleccionadaParaCancelar);
	//
	// for (HashMap<String, Object> str : acts) {
	// showBookingActivitiesToCancelUnique((String) str.get("nombre_inst"),
	// (String)str.get("id_reserva"),
	// (String)str.get("fecha_usada"));
	// }
	// }

	// protected void showBookingActivitiesToCancelUnique(String nombre, String
	// id_reserva, String fecha) {
	// JButton boton = new JButton();
	// boton.setText("<html><p>"+nombre+"</p><p>"+fecha+"</p></html>");
	// boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
	// boton.setActionCommand(id_reserva);
	// boton.setHorizontalAlignment(SwingConstants.CENTER);
	// boton.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent arg0) {
	// boton.setBackground(new Color(135, 206, 250));
	// Component[] comp =getPnReservasCancelarActView().getComponents();
	// for (int i = 0; i < comp.length; i++) {
	// JButton aux = (JButton) comp[i];
	// if (!aux.getActionCommand().equals(boton.getActionCommand()))
	// aux.setBackground(Color.WHITE);
	// }
	// getTxtInfoReservaConcreta().setText("");
	// currentReservaSeleccionadaParaCancelarAct = boton.getActionCommand();
	// showBookingInfoActivitiesToCancel();
	//
	// Component[] comp2 =getPnBotenesActConcretaView().getComponents();
	// for (int i = 0; i < comp2.length; i++) {
	// JButton aux = (JButton) comp2[i];
	// aux.setEnabled(true);
	// }
	//
	// }
	// });
	// boton.setBackground(getPnCancelarActividad().getBackground());
	// boton.setVisible(true);
	// getPnReservasCancelarActView().add(boton);
	// }

	// private void showBookingInfoActivitiesToCancel() {
	// List<HashMap<String, Object>> acts =
	// getManager().getInfoBookingActCancel(currentReservaSeleccionadaParaCancelarAct);
	// String text="";
	//
	// for (HashMap<String, Object> str : acts) {
	// text= "La clase se realizar� en la instalaci�n:
	// \n\t"+(String)str.get("nombre_inst")+". \n\n"
	// + "El d�a: "+ (String)str.get("fecha_usada")+" desde las "+
	// String.valueOf((Integer)str.get("hora_usada_inicio")) +"h a "+
	// String.valueOf((Integer)str.get("hora_usada_final"))+"h. \n\n"
	// + "El monitor que impartir� la clase ser�: \n\t" +
	// (String)str.get("nombre_monitor")+" "+
	// (String)str.get("apellido_monitor") + " con DNI: "+
	// (String)str.get("monitorDNI");
	// if((Integer)str.get("plazasLibres")>=0) text+="\n\nQuedan:
	// "+(Integer)str.get("plazasLibres")+" plazas libres.";
	// else text+= "\n\nPlazas Ilimitadas.";
	// }
	// getTxtInfoReservaConcreta().setText(text);
	// }
	// private JPanel getPnQuitarFiltros() {
	// if (pnQuitarFiltros == null) {
	// pnQuitarFiltros = new JPanel();
	// pnQuitarFiltros.setBackground(Color.WHITE);
	// pnQuitarFiltros.setLayout(new BorderLayout(0, 0));
	// pnQuitarFiltros.add(getBtnQuitarFiltros());
	// }
	// return pnQuitarFiltros;
	// }
	// private JButton getBtnQuitarFiltros() {
	// if (btnQuitarFiltros == null) {
	// btnQuitarFiltros = new JButton("Quitar Filtros");
	// btnQuitarFiltros.addActionListener(new ActionListener() {
	// public void actionPerformed(ActionEvent e) {
	// clearCancelarActi();
	// }
	// });
	// btnQuitarFiltros.setBackground(Color.WHITE);
	// }
	// return btnQuitarFiltros;
	// }

	// private void clearCancelarActi() {
	// groupCancelarActividadFiltroPeriodicidad.clearSelection();
	// groupCancelarActividadFiltroPlazas.clearSelection();
	// accionRadioBotonFiltroCancelarActividad();
	// }
	// private JLabel getLblNoHayActividades_1() {
	// if (lblNoHayActividades_1 == null) {
	// lblNoHayActividades_1 = new JLabel("No hay actividades");
	// lblNoHayActividades_1.setHorizontalAlignment(SwingConstants.CENTER);
	// }
	// return lblNoHayActividades_1;
	// }
	// private JPanel getPnBotonesActConcreta() {
	// if (pnBotonesActConcreta == null) {
	// pnBotonesActConcreta = new JPanel();
	// pnBotonesActConcreta.setBackground(Color.WHITE);
	// pnBotonesActConcreta.setBorder(new TitledBorder(null, "Acciones Clases
	// Concretas", TitledBorder.CENTER,
	// TitledBorder.TOP, null, null));
	// pnBotonesActConcreta.setLayout(new BorderLayout(0, 0));
	// pnBotonesActConcreta.add(getScrollPane_4());
	// }
	// return pnBotonesActConcreta;
	// }
	private JButton getBtnCambiarMonitorClase() {
		if (btnCambiarMonitorClase == null) {
			btnCambiarMonitorClase = new JButton("Cambiar Monitor Actividad");
			btnCambiarMonitorClase.setEnabled(false);
			btnCambiarMonitorClase.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String id_reserva = currentClaseSeleccionadaParaCancelar;
					List<HashMap<String, Object>> datos = manager.getFechaHoraIHoraF(id_reserva);
					String text = manager.getNombreApellidosMonitorDNI((String) datos.get(0).get("monitor"));
					getTxtNombreMonitor().setText(text);
					getLblNewLabel()
							.setText("Cambio de monitor para la actividad " + manager.getNombreActIDClase(id_reserva));
					card.show(pnBase, "pnCambiosCancelarActClass");
					cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnCambiarMonitorActividad");
				}
			});
			btnCambiarMonitorClase.setBackground(Color.WHITE);
		}
		return btnCambiarMonitorClase;
	}
	// private JPanel getPnBotonesAct() {
	// if (pnBotonesAct == null) {
	// pnBotonesAct = new JPanel();
	// pnBotonesAct.setBorder(new TitledBorder(null, "Acciones Actividad",
	// TitledBorder.CENTER, TitledBorder.TOP,
	// null, null));
	// pnBotonesAct.setBackground(Color.WHITE);
	// pnBotonesAct.setLayout(new BorderLayout(0, 0));
	// pnBotonesAct.add(getScrollPane_5());
	// }
	// return pnBotonesAct;
	// }
	// private JScrollPane getScrollPane_4() {
	// if (scrollPane_4 == null) {
	// scrollPane_4 = new JScrollPane();
	// scrollPane_4.setViewportView(getPnBotenesActConcretaView());
	// }
	// return scrollPane_4;
	// }
	// private JPanel getPnBotenesActConcretaView() {
	// if (pnBotenesActConcretaView == null) {
	// pnBotenesActConcretaView = new JPanel();
	// pnBotenesActConcretaView.setBackground(Color.WHITE);
	// pnBotenesActConcretaView.setLayout(new GridLayout(0, 1, 0, 0));
	// pnBotenesActConcretaView.add(getBtnCancelarClaseConcreto());
	// pnBotenesActConcretaView.add(getBtnCambiarHoraInicio());
	// pnBotenesActConcretaView.add(getBtnCambiarInstalacin());
	// }
	// return pnBotenesActConcretaView;
	// }
	// private JScrollPane getScrollPane_5() {
	// if (scrollPane_5 == null) {
	// scrollPane_5 = new JScrollPane();
	// scrollPane_5.setViewportView(getPnBotonesActView());
	// }
	// return scrollPane_5;
	// }
	// private JPanel getPnBotonesActView() {
	// if (pnBotonesActView == null) {
	// pnBotonesActView = new JPanel();
	// pnBotonesActView.setBackground(Color.WHITE);
	// pnBotonesActView.setLayout(new GridLayout(0, 1, 0, 0));
	// pnBotonesActView.add(getBtnCancelarActividad());
	// pnBotonesActView.add(getBtnCambiarMonitorClase());
	// }
	// return pnBotonesActView;
	// }
	// private JButton getBtnCambiarHoraInicio() {
	// if (btnCambiarHoraInicio == null) {
	// btnCambiarHoraInicio = new JButton("Cambiar Hora Inicio Clase Concreta");
	// btnCambiarHoraInicio.setEnabled(false);
	// btnCambiarHoraInicio.setBackground(Color.WHITE);
	// btnCambiarHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 10));
	// }
	// return btnCambiarHoraInicio;
	// }

	private JPanel getPnCambiosActANDClass() {
		if (pnCambiosActANDClass == null) {
			pnCambiosActANDClass = new JPanel();
			pnCambiosActANDClass.setBackground(Color.WHITE);
			pnCambiosActANDClass.setLayout(new CardLayout(0, 0));
			// pnCambiosActANDClass.add(getPnCambioMonitorNuevo(),
			// "pnCambiarMonitorActividad");
			// pnCambiosActANDClass.add(getPnCambioInstalacionNuevaClaseConcreta(),
			// "pnCambiarInstalacionClaseConcreta");
			pnCambiosActANDClass.add(getPnAvisarAsistentes(), "pnAvisarAsistentes");
		}
		return pnCambiosActANDClass;
	}

	private JPanel getPnCambioMonitorNuevo() {
		if (pnCambioMonitorNuevo == null) {
			pnCambioMonitorNuevo = new JPanel();
			pnCambioMonitorNuevo.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
					"Cambio de monitor para actividad", TitledBorder.CENTER, TitledBorder.TOP, null,
					new Color(0, 0, 0)));
			pnCambioMonitorNuevo.setBackground(Color.WHITE);
			GridBagLayout gbl_pnCambioMonitorNuevo = new GridBagLayout();
			gbl_pnCambioMonitorNuevo.columnWidths = new int[] { 100, 127, 4, 0 };
			gbl_pnCambioMonitorNuevo.rowHeights = new int[] { 40, 20, 31, 27, 32, 23, 0 };
			gbl_pnCambioMonitorNuevo.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnCambioMonitorNuevo.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnCambioMonitorNuevo.setLayout(gbl_pnCambioMonitorNuevo);
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.gridwidth = 3;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 0;
			pnCambioMonitorNuevo.add(getLblNewLabel(), gbc_lblNewLabel);
			GridBagConstraints gbc_lblMonitorAnterior = new GridBagConstraints();
			gbc_lblMonitorAnterior.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonitorAnterior.gridx = 0;
			gbc_lblMonitorAnterior.gridy = 1;
			pnCambioMonitorNuevo.add(getLblMonitorAnterior(), gbc_lblMonitorAnterior);
			GridBagConstraints gbc_txtNombreMonitor = new GridBagConstraints();
			gbc_txtNombreMonitor.fill = GridBagConstraints.BOTH;
			gbc_txtNombreMonitor.insets = new Insets(0, 0, 5, 0);
			gbc_txtNombreMonitor.gridwidth = 2;
			gbc_txtNombreMonitor.gridx = 1;
			gbc_txtNombreMonitor.gridy = 1;
			pnCambioMonitorNuevo.add(getTxtNombreMonitor(), gbc_txtNombreMonitor);
			GridBagConstraints gbc_lblMonitorNuevo = new GridBagConstraints();
			gbc_lblMonitorNuevo.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonitorNuevo.gridx = 0;
			gbc_lblMonitorNuevo.gridy = 3;
			pnCambioMonitorNuevo.add(getLblMonitorNuevo(), gbc_lblMonitorNuevo);
			GridBagConstraints gbc_cBNuevoMonitorCambio = new GridBagConstraints();
			gbc_cBNuevoMonitorCambio.fill = GridBagConstraints.BOTH;
			gbc_cBNuevoMonitorCambio.insets = new Insets(0, 0, 5, 0);
			gbc_cBNuevoMonitorCambio.gridwidth = 2;
			gbc_cBNuevoMonitorCambio.gridx = 1;
			gbc_cBNuevoMonitorCambio.gridy = 3;
			pnCambioMonitorNuevo.add(getCBNuevoMonitorCambio(), gbc_cBNuevoMonitorCambio);
			GridBagConstraints gbc_btnCambiarMonitor = new GridBagConstraints();
			gbc_btnCambiarMonitor.gridwidth = 3;
			gbc_btnCambiarMonitor.fill = GridBagConstraints.BOTH;
			gbc_btnCambiarMonitor.gridx = 0;
			gbc_btnCambiarMonitor.gridy = 5;
			pnCambioMonitorNuevo.add(getBtnCambiarMonitor(), gbc_btnCambiarMonitor);
		}
		return pnCambioMonitorNuevo;
	}

	private JLabel getLblMonitorAnterior() {
		if (lblMonitorAnterior == null) {
			lblMonitorAnterior = new JLabel("Monitor anterior:");
			lblMonitorAnterior.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblMonitorAnterior;
	}

	private JTextField getTxtNombreMonitor() {
		if (txtNombreMonitor == null) {
			txtNombreMonitor = new JTextField();
			txtNombreMonitor.setFont(new Font("Dialog", Font.ITALIC, 11));
			txtNombreMonitor.setBackground(Color.WHITE);
			txtNombreMonitor.setEditable(false);
			txtNombreMonitor.setColumns(10);
		}
		return txtNombreMonitor;
	}

	private JLabel getLblMonitorNuevo() {
		if (lblMonitorNuevo == null) {
			lblMonitorNuevo = new JLabel("Monitor nuevo:");
			lblMonitorNuevo.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblMonitorNuevo;
	}

	private JComboBox<String> getCBNuevoMonitorCambio() {
		if (cBNuevoMonitorCambio == null) {
			cBNuevoMonitorCambio = new JComboBox<String>();
			cBNuevoMonitorCambio.setBackground(Color.WHITE);
			String[] model = manager.getDNIMonitores().toArray(new String[manager.getDNIMonitores().size()]);
			cBNuevoMonitorCambio.setModel(new DefaultComboBoxModel<String>(model));
			cBNuevoMonitorCambio.setFont(new Font("Dialog", Font.ITALIC, 11));
		}
		return cBNuevoMonitorCambio;
	}

	private JButton getBtnCambiarMonitor() {
		if (btnCambiarMonitor == null) {
			btnCambiarMonitor = new JButton("Cambiar Monitor");
			btnCambiarMonitor.setFont(new Font("Dialog", Font.BOLD, 10));
			btnCambiarMonitor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String id_clase = manager.getIDclaseFromIDreserva(currentClaseSeleccionadaHorario);
					// HashMap<String, Object> datos =
					// manager.getFechaHoraIHoraF(id_clase);

					// String fecha=(String)datos.get("fecha");
					boolean a = true;
					for (HashMap<String, Object> datos : manager.getFechaHoraIHoraF(id_clase)) {
						int horaInicio = (Integer) datos.get("horaI");
						int horaFinal = (Integer) datos.get("horaF");
						if (!manager.checkMonitorOcupadoParaCambiar(
								getDNImonitor((String) getCBNuevoMonitorCambio().getSelectedItem()), horaInicio,
								horaFinal, id_clase)) {
							a = false;
							break;
						}
					}
					if (a) {

						manager.cambiarMonito(id_clase,
								(getDNImonitor((String) getCBNuevoMonitorCambio().getSelectedItem())));
						JOptionPane.showMessageDialog(null, "Monitor cambiado");
						// card.show(pnBase, "pnCancelarActividad");
						// accionRadioBotonFiltroCancelarActividad();
						showMonitorCambioActividad();

					} else {
						JOptionPane.showMessageDialog(null, "El monitor seleccionado est� ocupado a esta hora", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			btnCambiarMonitor.setBackground(Color.WHITE);
		}
		return btnCambiarMonitor;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("");
			lblNewLabel.setFont(new Font("Dialog", Font.ITALIC, 10));
		}
		return lblNewLabel;
	}

	private JButton getBtnCambiarInstalacin() {
		if (btnCambiarInstalacin == null) {
			btnCambiarInstalacin = new JButton("Cambiar Instalaci\u00F3n");
			btnCambiarInstalacin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String id_reserva = currentReservaSeleccionadaParaCancelarAct;
					String nombreInst = manager.getNombreInstWithID_Reserva(id_reserva);
					getTxtInstalacionAnterior().setText(nombreInst);
					String[] model = manager.getAllInst().toArray(new String[manager.getAllInst().size()]);
					getCBNuevasInstalacionesAEscoger().setModel(new DefaultComboBoxModel<String>(model));
					getLblCambioDeInstalacin().setText(manager.getFechaHorasWithID_Reserva(id_reserva)[0]);
					card.show(pnBase, "pnCambiosCancelarActClass");
					cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnCambiarInstalacionClaseConcreta");
				}
			});
			btnCambiarInstalacin.setEnabled(false);
			btnCambiarInstalacin.setBackground(Color.WHITE);
		}
		return btnCambiarInstalacin;
	}

	private JPanel getPnCambioInstalacionNuevaClaseConcreta() {
		if (pnCambioInstalacionNuevaClaseConcreta == null) {
			pnCambioInstalacionNuevaClaseConcreta = new JPanel();
			pnCambioInstalacionNuevaClaseConcreta
					.setBorder(new TitledBorder(null, "Cambio de Instalaci\u00F3n para clase concreta",
							TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnCambioInstalacionNuevaClaseConcreta.setBackground(Color.WHITE);
			GridBagLayout gbl_pnCambioInstalacionNuevaClaseConcreta = new GridBagLayout();
			gbl_pnCambioInstalacionNuevaClaseConcreta.columnWidths = new int[] { 100, 151, 0 };
			gbl_pnCambioInstalacionNuevaClaseConcreta.rowHeights = new int[] { 0, 27, 0, 27, 32, 23, 0 };
			gbl_pnCambioInstalacionNuevaClaseConcreta.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnCambioInstalacionNuevaClaseConcreta.rowWeights = new double[] { 1.0, 1.0, 0.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnCambioInstalacionNuevaClaseConcreta.setLayout(gbl_pnCambioInstalacionNuevaClaseConcreta);
			GridBagConstraints gbc_lblCambioDeInstalacin = new GridBagConstraints();
			gbc_lblCambioDeInstalacin.gridwidth = 2;
			gbc_lblCambioDeInstalacin.insets = new Insets(0, 0, 5, 0);
			gbc_lblCambioDeInstalacin.gridx = 0;
			gbc_lblCambioDeInstalacin.gridy = 0;
			pnCambioInstalacionNuevaClaseConcreta.add(getLblCambioDeInstalacin(), gbc_lblCambioDeInstalacin);
			GridBagConstraints gbc_lblInstalacinAnterior = new GridBagConstraints();
			gbc_lblInstalacinAnterior.fill = GridBagConstraints.VERTICAL;
			gbc_lblInstalacinAnterior.insets = new Insets(0, 0, 5, 5);
			gbc_lblInstalacinAnterior.gridx = 0;
			gbc_lblInstalacinAnterior.gridy = 1;
			pnCambioInstalacionNuevaClaseConcreta.add(getLblInstalacinAnterior(), gbc_lblInstalacinAnterior);
			GridBagConstraints gbc_txtInstalacionAnterior = new GridBagConstraints();
			gbc_txtInstalacionAnterior.fill = GridBagConstraints.BOTH;
			gbc_txtInstalacionAnterior.insets = new Insets(0, 0, 5, 0);
			gbc_txtInstalacionAnterior.gridx = 1;
			gbc_txtInstalacionAnterior.gridy = 1;
			pnCambioInstalacionNuevaClaseConcreta.add(getTxtInstalacionAnterior(), gbc_txtInstalacionAnterior);
			GridBagConstraints gbc_lblInstalacinNueva = new GridBagConstraints();
			gbc_lblInstalacinNueva.fill = GridBagConstraints.VERTICAL;
			gbc_lblInstalacinNueva.insets = new Insets(0, 0, 5, 5);
			gbc_lblInstalacinNueva.gridx = 0;
			gbc_lblInstalacinNueva.gridy = 3;
			pnCambioInstalacionNuevaClaseConcreta.add(getLblInstalacinNueva(), gbc_lblInstalacinNueva);
			GridBagConstraints gbc_cBNuevasInstalacionesAEscoger = new GridBagConstraints();
			gbc_cBNuevasInstalacionesAEscoger.fill = GridBagConstraints.BOTH;
			gbc_cBNuevasInstalacionesAEscoger.insets = new Insets(0, 0, 5, 0);
			gbc_cBNuevasInstalacionesAEscoger.gridx = 1;
			gbc_cBNuevasInstalacionesAEscoger.gridy = 3;
			pnCambioInstalacionNuevaClaseConcreta.add(getCBNuevasInstalacionesAEscoger(),
					gbc_cBNuevasInstalacionesAEscoger);
			GridBagConstraints gbc_btnCambiarInstalacin_1 = new GridBagConstraints();
			gbc_btnCambiarInstalacin_1.gridwidth = 2;
			gbc_btnCambiarInstalacin_1.fill = GridBagConstraints.BOTH;
			gbc_btnCambiarInstalacin_1.gridx = 0;
			gbc_btnCambiarInstalacin_1.gridy = 5;
			pnCambioInstalacionNuevaClaseConcreta.add(getBtnCambiarInstalacin_1(), gbc_btnCambiarInstalacin_1);
		}
		return pnCambioInstalacionNuevaClaseConcreta;
	}

	private JLabel getLblInstalacinAnterior() {
		if (lblInstalacinAnterior == null) {
			lblInstalacinAnterior = new JLabel("Instalaci\u00F3n anterior:");
			lblInstalacinAnterior.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblInstalacinAnterior;
	}

	private JLabel getLblInstalacinNueva() {
		if (lblInstalacinNueva == null) {
			lblInstalacinNueva = new JLabel("Instalaci\u00F3n nueva:");
			lblInstalacinNueva.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblInstalacinNueva;
	}

	private JTextField getTxtInstalacionAnterior() {
		if (txtInstalacionAnterior == null) {
			txtInstalacionAnterior = new JTextField();
			txtInstalacionAnterior.setBackground(Color.WHITE);
			txtInstalacionAnterior.setFont(new Font("Tw Cen MT", Font.ITALIC, 15));
			txtInstalacionAnterior.setHorizontalAlignment(SwingConstants.CENTER);
			txtInstalacionAnterior.setEditable(false);
			txtInstalacionAnterior.setColumns(10);
		}
		return txtInstalacionAnterior;
	}

	private JComboBox<String> getCBNuevasInstalacionesAEscoger() {
		if (cBNuevasInstalacionesAEscoger == null) {
			cBNuevasInstalacionesAEscoger = new JComboBox<String>();
			cBNuevasInstalacionesAEscoger.setBackground(Color.WHITE);
			cBNuevasInstalacionesAEscoger.setFont(new Font("Tw Cen MT", Font.ITALIC, 15));
		}
		return cBNuevasInstalacionesAEscoger;
	}

	private JButton getBtnCambiarInstalacin_1() {
		if (btnCambiarInstalacin_1 == null) {
			btnCambiarInstalacin_1 = new JButton("Cambiar Instalaci\u00F3n");
			btnCambiarInstalacin_1.setBackground(Color.WHITE);
			btnCambiarInstalacin_1.setFont(new Font("Dialog", Font.BOLD, 10));
			btnCambiarInstalacin_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String[] s = manager.getFechaHorasWithID_Reserva(currentClaseSeleccionadaHorario);
					if (manager.checkInstalacionOcupada((String) getCBNuevasInstalacionesAEscoger().getSelectedItem(),
							s[1], Integer.parseInt(s[2]), Integer.parseInt(s[3]))) {// True
																					// que
																					// no
																					// hay
																					// conflictos
						manager.cambiarInstalacion(currentClaseSeleccionadaHorario,
								(String) getCBNuevasInstalacionesAEscoger().getSelectedItem());
						JOptionPane.showMessageDialog(null, "Instalación cambiada");
						showClaseCambioInsta();
						if (manager.getPlazasLibres(currentClaseSeleccionadaHorario) != -1) {
							List<String[]> lista = manager.obtenerAsistentesClase(currentClaseSeleccionadaHorario);
							if (lista.size() > 0) {
								card.show(pnBase, "pnCambiosCancelarActClass");
								cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnAvisarAsistentes");
								txtAreaAvisarAsistentes.setText("");
								String text = "";
								for (String[] str : lista) {

									text = text + str[0] + " " + str[1] + "  DNI: " + str[2] + "   Teléfono: " + str[3]
											+ "\n";
									txtAreaAvisarAsistentes.setText(text);
								}
							}
						}
						// card.show(pnBase, "pnCancelarActividad");
						// accionRadioBotonFiltroCancelarActividad();
					} else {
						JOptionPane.showMessageDialog(null,
								"La instalación seleccionada está ocupada para esta fecha, y" + " horas", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnCambiarInstalacin_1;
	}

	private JLabel getLblCambioDeInstalacin() {
		if (lblCambioDeInstalacin == null) {
			lblCambioDeInstalacin = new JLabel("");
			lblCambioDeInstalacin.setFont(new Font("Dialog", Font.ITALIC, 11));
		}
		return lblCambioDeInstalacin;
	}

	// Alvaro
	private JPanel getPnActivitiesOfTheDayAdmin() {
		if (pnActivitiesOfTheDayAdmin == null) {
			pnActivitiesOfTheDayAdmin = new JPanel();
			pnActivitiesOfTheDayAdmin.setBackground(pnBase.getBackground());
			GridBagLayout gbl_pnActivitiesOfTheDayAdmin = new GridBagLayout();
			gbl_pnActivitiesOfTheDayAdmin.columnWidths = new int[] { 30, 70, 184, 30, 30, 240, 60, 50 };
			gbl_pnActivitiesOfTheDayAdmin.rowHeights = new int[] { 30, 60, 33, 153, 32, 0, 30, 30, 0 };
			gbl_pnActivitiesOfTheDayAdmin.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
			gbl_pnActivitiesOfTheDayAdmin.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnActivitiesOfTheDayAdmin.setLayout(gbl_pnActivitiesOfTheDayAdmin);
			GridBagConstraints gbc_btnBackActivitiesView = new GridBagConstraints();
			gbc_btnBackActivitiesView.fill = GridBagConstraints.BOTH;
			gbc_btnBackActivitiesView.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackActivitiesView.gridx = 1;
			gbc_btnBackActivitiesView.gridy = 1;
			pnActivitiesOfTheDayAdmin.add(getBtnBackActivitiesView(), gbc_btnBackActivitiesView);
			GridBagConstraints gbc_lblVerActividadesDel = new GridBagConstraints();
			gbc_lblVerActividadesDel.fill = GridBagConstraints.VERTICAL;
			gbc_lblVerActividadesDel.anchor = GridBagConstraints.EAST;
			gbc_lblVerActividadesDel.gridwidth = 2;
			gbc_lblVerActividadesDel.insets = new Insets(0, 0, 5, 5);
			gbc_lblVerActividadesDel.gridx = 2;
			gbc_lblVerActividadesDel.gridy = 1;
			pnActivitiesOfTheDayAdmin.add(getLblVerActividadesDel(), gbc_lblVerActividadesDel);
			GridBagConstraints gbc_lblFechaActivitiesView = new GridBagConstraints();
			gbc_lblFechaActivitiesView.fill = GridBagConstraints.BOTH;
			gbc_lblFechaActivitiesView.insets = new Insets(0, 0, 5, 5);
			gbc_lblFechaActivitiesView.gridx = 2;
			gbc_lblFechaActivitiesView.gridy = 2;
			pnActivitiesOfTheDayAdmin.add(getLblFechaActivitiesView(), gbc_lblFechaActivitiesView);
			GridBagConstraints gbc_calendarViewActivities = new GridBagConstraints();
			gbc_calendarViewActivities.anchor = GridBagConstraints.NORTHWEST;
			gbc_calendarViewActivities.insets = new Insets(0, 0, 5, 5);
			gbc_calendarViewActivities.gridx = 2;
			gbc_calendarViewActivities.gridy = 3;
			pnActivitiesOfTheDayAdmin.add(getCalendarViewActivities(), gbc_calendarViewActivities);
			GridBagConstraints gbc_pnActivitiesViewActivities = new GridBagConstraints();
			gbc_pnActivitiesViewActivities.gridheight = 3;
			gbc_pnActivitiesViewActivities.gridwidth = 2;
			gbc_pnActivitiesViewActivities.insets = new Insets(0, 0, 5, 5);
			gbc_pnActivitiesViewActivities.fill = GridBagConstraints.BOTH;
			gbc_pnActivitiesViewActivities.gridx = 5;
			gbc_pnActivitiesViewActivities.gridy = 1;
			pnActivitiesOfTheDayAdmin.add(getPnActivitiesViewActivities(), gbc_pnActivitiesViewActivities);
			GridBagConstraints gbc_cmbBoxInstActivitiesView = new GridBagConstraints();
			gbc_cmbBoxInstActivitiesView.insets = new Insets(0, 0, 5, 5);
			gbc_cmbBoxInstActivitiesView.fill = GridBagConstraints.BOTH;
			gbc_cmbBoxInstActivitiesView.gridx = 2;
			gbc_cmbBoxInstActivitiesView.gridy = 5;
			pnActivitiesOfTheDayAdmin.add(getCmbBoxInstActivitiesView(), gbc_cmbBoxInstActivitiesView);
			GridBagConstraints gbc_pnModificar = new GridBagConstraints();
			gbc_pnModificar.gridheight = 2;
			gbc_pnModificar.gridwidth = 2;
			gbc_pnModificar.insets = new Insets(0, 0, 5, 5);
			gbc_pnModificar.fill = GridBagConstraints.BOTH;
			gbc_pnModificar.gridx = 5;
			gbc_pnModificar.gridy = 5;
			pnActivitiesOfTheDayAdmin.add(getPnModificar(), gbc_pnModificar);
			GridBagConstraints gbc_btnEnsearAvtividades = new GridBagConstraints();
			gbc_btnEnsearAvtividades.fill = GridBagConstraints.BOTH;
			gbc_btnEnsearAvtividades.insets = new Insets(0, 0, 5, 5);
			gbc_btnEnsearAvtividades.gridx = 2;
			gbc_btnEnsearAvtividades.gridy = 6;
			pnActivitiesOfTheDayAdmin.add(getBtnEnsearAvtividades(), gbc_btnEnsearAvtividades);
		}
		return pnActivitiesOfTheDayAdmin;
	}

	private JCalendar getCalendarViewActivities() {
		if (calendarViewActivities == null) {
			calendarViewActivities = new JCalendar();
			calendarViewActivities.setMinSelectableDate(new Date());
		}
		return calendarViewActivities;
	}

	private JButton getBtnEnsearAvtividades() {
		if (btnEnsearAvtividades == null) {
			btnEnsearAvtividades = new JButton("Ense\u00F1ar actividades");
			btnEnsearAvtividades.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					pnActivitiesViewInfo.removeAll();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String fecha = df.format(calendarViewActivities.getDate());
					lblFechaActivitiesView.setText(fecha);
					showActivitiesOfTheDay(calendarViewActivities.getDate(),
							String.valueOf(cmbBoxInstActivitiesView.getSelectedItem()));
				}
			});
			btnEnsearAvtividades.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnEnsearAvtividades.setBackground(Color.WHITE);
		}
		return btnEnsearAvtividades;
	}

	protected void showActivitiesOfTheDay(Date date, String inst) {

		List<String[]> list = null;
		if (inst.equals("Todas")) {
			list = manager.getAllActivitiesOfTheDayChoose(date);
		} else {
			list = manager.getAllActivitiesOfTheDayChoose(date, inst);
		}
		for (String act[] : list) {
			showActivityOfTheDayUnique("<html><p>Clase: " + act[0] + "</p>" + "<p>Instalacion: " + act[1] + "</p>"
					+ "<p>Periodo: " + act[2] + "-" + act[3] + "</p></html>", act[4]);
		}
		if (list.isEmpty()) {
			JTextPane label = new JTextPane();
			label.setText("A�n no hay actividades para este \nd�a ni esta instalaci�n");
			pnActivitiesViewInfo.add(label);
		}
	}

	protected void showActivityOfTheDayUnique(String act, String id_reserva) {
		JButton boton = new JButton();
		boton.setText(act);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(id_reserva);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentActivitiesToUser = boton.getActionCommand();
				boton.setBackground(new Color(135, 206, 250));
				Component[] comp = getPnActivitiesViewInfo().getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(getPnActivitiesViewInfo().getBackground());
		getPnActivitiesViewInfo().add(boton);

	}

	private JScrollPane getScrollPnActivitiesView() {
		if (scrollPnActivitiesView == null) {
			scrollPnActivitiesView = new JScrollPane();
			scrollPnActivitiesView.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesView.setViewportView(getPnActivitiesViewInfo());
		}
		return scrollPnActivitiesView;
	}

	private JPanel getPnActivitiesViewInfo() {
		if (pnActivitiesViewInfo == null) {
			pnActivitiesViewInfo = new JPanel();
			pnActivitiesViewInfo.setBackground(pnActivitiesOfTheDayAdmin.getBackground());
			pnActivitiesViewInfo.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnActivitiesViewInfo;
	}

	private JButton getBtnBackActivitiesView() {
		if (btnBackActivitiesView == null) {
			btnBackActivitiesView = new JButton("");
			btnBackActivitiesView.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
				}
			});
			;
			btnBackActivitiesView.setBackground(pnActivitiesOfTheDayAdmin.getBackground());
			btnBackActivitiesView.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackActivitiesView, "/img/arrow.png");
			btnBackActivitiesView.setBorder(null);
		}
		return btnBackActivitiesView;
	}

	private JLabel getLblFechaActivitiesView() {
		if (lblFechaActivitiesView == null) {
			lblFechaActivitiesView = new JLabel("");
			lblFechaActivitiesView.setHorizontalAlignment(SwingConstants.CENTER);
			lblFechaActivitiesView.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblFechaActivitiesView;
	}

	private JComboBox<String> getCmbBoxInstActivitiesView() {
		if (cmbBoxInstActivitiesView == null) {
			cmbBoxInstActivitiesView = new JComboBox<String>();
			cmbBoxInstActivitiesView.setFont(new Font("Tw Cen MT", Font.PLAIN, 16));
			cmbBoxInstActivitiesView.setMaximumRowCount(20);
		}
		return cmbBoxInstActivitiesView;
	}

	private JLabel getLblVerActividadesDel() {
		if (lblVerActividadesDel == null) {
			lblVerActividadesDel = new JLabel("Ver actividades del centro:");
			lblVerActividadesDel.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return lblVerActividadesDel;
	}

	private JPanel getPnActivitiesViewActivities() {
		if (pnActivitiesViewActivities == null) {
			pnActivitiesViewActivities = new JPanel();
			pnActivitiesViewActivities.setLayout(new BorderLayout(0, 0));
			pnActivitiesViewActivities.add(getScrollPnActivitiesView());
		}
		return pnActivitiesViewActivities;
	}

	private JButton getBtnDisminuirPlazas() {
		if (btnDisminuirPlazas == null) {
			btnDisminuirPlazas = new JButton("Disminuir Plazas");
			btnDisminuirPlazas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
			});
			btnDisminuirPlazas.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnDisminuirPlazas.setBackground(Color.WHITE);
		}
		return btnDisminuirPlazas;
	}

	private JButton getBtnAumentarPlazas() {
		if (btnAumentarPlazas == null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = df.format(getCalendar().getDate());
			btnAumentarPlazas = new JButton("Aumentar Plazas");
			btnAumentarPlazas.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnAumentarPlazas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (!currentActivitiesToUser.equals("")) {
						if (manager.getPlazasActividad(currentActivitiesToUser) != -1) {
							if (!getTxModificarPlazas().getText().equals("")) {
								if (Integer.parseInt(getTxModificarPlazas().getText()) > 0) {
									if (manager.seeActivitiesFecha(currentActivitiesToUser, fecha)) {
										if (manager.modificarPlazas(currentActivitiesToUser,
												getTxModificarPlazas().getText())) {
											JOptionPane.showMessageDialog(null,
													"La operacion se ha completado con exito");
											currentActivitiesToUser = "";
											txModificarPlazas.setText("");
										}
									} else
										JOptionPane.showMessageDialog(null,
												"Esta actividad ya se ha llevado a cabo, por lo que no puede modificar el numero de dias");
								} else
									JOptionPane.showMessageDialog(null,
											"El numero de dias a modificar debe ser mayor a 0");
							} else
								JOptionPane.showMessageDialog(null,
										"El campo de seleccionar plazas esta vacio, inserte un numero");
						} else
							JOptionPane.showMessageDialog(null,
									"No se puede modificar las plazas de esta actividad ya que es una actividad ilimitada");
					} else
						JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna actividad");
				}
			});
			btnAumentarPlazas.setBackground(Color.WHITE);
		}
		return btnAumentarPlazas;
	}

	private JLabel getLblPlazasAModificar() {
		if (lblPlazasAModificar == null) {
			lblPlazasAModificar = new JLabel("N�mero de plazas a aumentar:");
			lblPlazasAModificar.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lblPlazasAModificar;
	}

	private JTextField getTxModificarPlazas() {
		if (txModificarPlazas == null) {
			txModificarPlazas = new JTextField();
			txModificarPlazas.setColumns(10);
		}
		return txModificarPlazas;
	}

	private JPanel getPnModificar() {
		if (pnModificar == null) {
			pnModificar = new JPanel();
			pnModificar.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Modificador de plazas",
					TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
			pnModificar.setBackground(Color.WHITE);
			GridBagLayout gbl_pnModificar = new GridBagLayout();
			gbl_pnModificar.columnWidths = new int[] { 148, 148, 0 };
			gbl_pnModificar.rowHeights = new int[] { 32, 32, 0 };
			gbl_pnModificar.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
			gbl_pnModificar.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
			pnModificar.setLayout(gbl_pnModificar);
			GridBagConstraints gbc_lblPlazasAModificar = new GridBagConstraints();
			gbc_lblPlazasAModificar.anchor = GridBagConstraints.EAST;
			gbc_lblPlazasAModificar.fill = GridBagConstraints.VERTICAL;
			gbc_lblPlazasAModificar.insets = new Insets(0, 0, 5, 5);
			gbc_lblPlazasAModificar.gridx = 0;
			gbc_lblPlazasAModificar.gridy = 0;
			pnModificar.add(getLblPlazasAModificar(), gbc_lblPlazasAModificar);
			GridBagConstraints gbc_txModificarPlazas = new GridBagConstraints();
			gbc_txModificarPlazas.insets = new Insets(0, 0, 5, 0);
			gbc_txModificarPlazas.gridx = 1;
			gbc_txModificarPlazas.gridy = 0;
			pnModificar.add(getTxModificarPlazas(), gbc_txModificarPlazas);
			GridBagConstraints gbc_btnAumentarPlazas = new GridBagConstraints();
			gbc_btnAumentarPlazas.anchor = GridBagConstraints.EAST;
			gbc_btnAumentarPlazas.fill = GridBagConstraints.VERTICAL;
			gbc_btnAumentarPlazas.insets = new Insets(0, 0, 0, 5);
			gbc_btnAumentarPlazas.gridx = 0;
			gbc_btnAumentarPlazas.gridy = 1;
			pnModificar.add(getBtnAumentarPlazas(), gbc_btnAumentarPlazas);
			GridBagConstraints gbc_btnDisminuirPlazas = new GridBagConstraints();
			gbc_btnDisminuirPlazas.anchor = GridBagConstraints.EAST;
			gbc_btnDisminuirPlazas.fill = GridBagConstraints.VERTICAL;
			gbc_btnDisminuirPlazas.gridx = 1;
			gbc_btnDisminuirPlazas.gridy = 1;
			pnModificar.add(getBtnDisminuirPlazas(), gbc_btnDisminuirPlazas);
		}
		return pnModificar;
	}

	private JPanel getPnAvisarAsistentes() {
		if (pnAvisarAsistentes == null) {
			pnAvisarAsistentes = new JPanel();
			pnAvisarAsistentes.setBackground(Color.WHITE);
			pnAvisarAsistentes.setLayout(null);
			pnAvisarAsistentes.add(getLblAvisarALos());
			pnAvisarAsistentes.add(getPnAvisarAsis());
			pnAvisarAsistentes.add(getBtnVolverAvisarAsistentes());
		}
		return pnAvisarAsistentes;
	}

	private JLabel getLblAvisarALos() {
		if (lblAvisarALos == null) {
			lblAvisarALos = new JLabel(
					"Avisar a los siguientes usuarios de que la actividad ha sido cancelada / modificada:");
			lblAvisarALos.setFont(new Font("Times New Roman", Font.BOLD, 14));
			lblAvisarALos.setBounds(51, 40, 560, 46);
		}
		return lblAvisarALos;
	}

	private JPanel getPnAvisarAsis() {
		if (pnAvisarAsis == null) {
			pnAvisarAsis = new JPanel();
			pnAvisarAsis.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Usuarios afectados",
					TitledBorder.CENTER, TitledBorder.TOP, null, Color.BLACK));
			pnAvisarAsis.setBounds(122, 97, 461, 344);
			pnAvisarAsis.setLayout(new BorderLayout(0, 0));
			pnAvisarAsis.add(getTxtAreaAvisarAsistentes(), BorderLayout.CENTER);
		}
		return pnAvisarAsis;
	}

	private JTextArea getTxtAreaAvisarAsistentes() {
		if (txtAreaAvisarAsistentes == null) {
			txtAreaAvisarAsistentes = new JTextArea();
			txtAreaAvisarAsistentes.setEditable(false);
		}
		return txtAreaAvisarAsistentes;
	}

	private JButton getBtnVolverAvisarAsistentes() {
		if (btnVolverAvisarAsistentes == null) {
			btnVolverAvisarAsistentes = new JButton("Volver");
			btnVolverAvisarAsistentes.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnVolverAvisarAsistentes.setBounds(589, 444, 126, 36);
			btnVolverAvisarAsistentes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHorarioActividadesCentro");
					comprobarSemanaHorarios(
							manager.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
				}
			});
		}
		return btnVolverAvisarAsistentes;
	}// GonziYOLO

	private JButton getBtnCancelarPeriodicaConflicto() {
		if (btnCancelarPeriodicaConflicto == null) {
			btnCancelarPeriodicaConflicto = new JButton("Atr\u00E1s");
			btnCancelarPeriodicaConflicto.setEnabled(true);
			btnCancelarPeriodicaConflicto.setBackground(Color.WHITE);
			btnCancelarPeriodicaConflicto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (actividadPeriodica) {
						cardPeriodica.show(pnActividadPeriodicaCrear, "datos");
					} else {
						card.show(pnBase, "pnInstallationView");
						cardInstallation.show(pnInstallationBookingView, "pnActivity");
					}
				}
			});
			btnCancelarPeriodicaConflicto.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnCancelarPeriodicaConflicto;
	}

	private JDateChooser getDateActividadPeriodicaAcabar() {
		if (dateActividadPeriodicaAcabar == null) {
			dateActividadPeriodicaAcabar = new JDateChooser();
		}
		return dateActividadPeriodicaAcabar;
	}

	private JScrollPane getScrollPaneHorarioCentral() {
		if (scrollPaneHorarioCentral == null) {
			scrollPaneHorarioCentral = new JScrollPane();
			scrollPaneHorarioCentral.setViewportView(getTableHorarioCentro());
		}
		return scrollPaneHorarioCentral;
	}

	private JTable getTableHorarioCentro() {
		if (tableHorarioCentro == null) {
			String[] diasSemana = { "", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
			ModeloTablaActividades mt = new ModeloTablaActividades(diasSemana);
			tableHorarioCentro = new JTable(mt) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
					Component c = super.prepareRenderer(renderer, row, column);
					if (column > 0) {
						try {
							if (!isCellSelected(row, column)) {
								if (column > 0) {
									((JComponent) c).setOpaque(true);
									Object type = getModel().getValueAt(row, column);

									if (type == null) {
										c.setBackground(Color.WHITE);
									} else {

										c.setBackground(new Color(173, 216, 230));

									}
								}

							}

							return c;
						} catch (Exception e) {

						}
					}
					return c;
				}
			};
			tableHorarioCentro.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					Object celda = tableHorarioCentro.getValueAt(tableHorarioCentro.getSelectedRow(),
							tableHorarioCentro.getSelectedColumn());
					if (celda != null) {
						String contenido = celda.toString();
						while (contenido == "") {
							tableHorarioCentro.setColumnSelectionInterval(tableHorarioCentro.getSelectedColumn(),
									tableHorarioCentro.getSelectedColumn());
							tableHorarioCentro.setRowSelectionInterval(tableHorarioCentro.getSelectedRow() - 1,
									tableHorarioCentro.getSelectedRow() - 1);
							celda = tableHorarioCentro.getValueAt(tableHorarioCentro.getSelectedRow(),
									tableHorarioCentro.getSelectedColumn());
							contenido = celda.toString();
						}

						String act = tableHorarioCentro
								.getValueAt(tableHorarioCentro.getSelectedRow(), tableHorarioCentro.getSelectedColumn())
								.toString().split("</p>")[0].split("<p>")[1];
						String inst = tableHorarioCentro
								.getValueAt(tableHorarioCentro.getSelectedRow(), tableHorarioCentro.getSelectedColumn())
								.toString().split("</p>")[1].split("<p>")[1];

						int horaI = Integer.parseInt(
								((String) tableHorarioCentro.getValueAt(tableHorarioCentro.getSelectedRow(), 0)
										.toString()).split(":")[0].split("<p>")[1]);

						currentClaseSeleccionadaHorario = getIDreserva(act, inst,
								semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1],
								semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1],
								semanaEnInts.get(2)[tableHorarioCentro.getSelectedColumn() - 1], horaI);
						currentClaseSeleccionadaHorarioIdClase = getIDclase(act, inst,
								semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1],
								semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1],
								semanaEnInts.get(2)[tableHorarioCentro.getSelectedColumn() - 1], horaI);

						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date ahora = new Date();

						Calendar calendario = Calendar.getInstance();
						int horaAhora = calendario.get(Calendar.HOUR_OF_DAY);

						String fechaSeleccionada = "";
						if (semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1] >= 10
								&& semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1] >= 10)
							fechaSeleccionada = semanaEnInts.get(2)[tableHorarioCentro.getSelectedColumn() - 1] + "-"
									+ semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1] + "-"
									+ semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1];
						else if (semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1] < 10)
							fechaSeleccionada = semanaEnInts.get(2)[tableHorarioCentro.getSelectedColumn() - 1] + "-"
									+ semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1] + "-0"
									+ semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1];
						else if (semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1] < 10)
							fechaSeleccionada = semanaEnInts.get(2)[tableHorarioCentro.getSelectedColumn() - 1] + "-0"
									+ semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1] + "-"
									+ semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1];
						else
							fechaSeleccionada = semanaEnInts.get(2)[tableHorarioCentro.getSelectedColumn() - 1] + "-0"
									+ semanaEnInts.get(1)[tableHorarioCentro.getSelectedColumn() - 1] + "-0"
									+ semanaEnInts.get(0)[tableHorarioCentro.getSelectedColumn() - 1];

						String fechaAhora = df.format(ahora);
						try {
							Date parse = df.parse(fechaSeleccionada);
							if (fechaAhora.compareTo(fechaSeleccionada) == 0) {
								if (horaI >= horaAhora) {
									btnCambiar.setEnabled(true);
								} else {
									btnCambiar.setEnabled(false);
								}
							} else {
								if (parse.compareTo(ahora) > 0) {
									btnCambiar.setEnabled(true);
								} else
									btnCambiar.setEnabled(false);
							}

						} catch (ParseException e1) {
						}

						// btnCambiar.setEnabled(true);

						HashMap<String, Object> info = manager.getInfoBookingActCancel(currentClaseSeleccionadaHorario);
						if (((Integer) info.get("plazasLibres")) > 0) {
							JOptionPane.showMessageDialog(null, "La clase de " + info.get("nombre_clase")
									+ " se impartir� el d�a " + info.get("fecha_usada") + " " + "desde las "
									+ info.get("hora_usada_inicio") + ":00 hasta las " + info.get("hora_usada_final")
									+ ":00." + "\nCon el monitor " + info.get("nombre_monitor") + " "
									+ info.get("apellido_monitor") + " (con DNI " + info.get("monitorDNI")
									+ ").\nEn la instalaci�n " + info.get("nombre_inst") + ". Quedan "
									+ info.get("plazasLibres") + " plazas libres.",
									"Informaci�n de la clase seleccionada", JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(null, "La clase de " + info.get("nombre_clase")
									+ " se impartir� el d�a " + info.get("fecha_usada") + " " + "desde las "
									+ info.get("hora_usada_inicio") + ":00 hasta las " + info.get("hora_usada_final")
									+ ":00" + ".\nCon el monitor : " + info.get("nombre_monitor") + " "
									+ info.get("apellido_monitor") + " (con DNI " + info.get("monitorDNI")
									+ ").\nEn la instalaci�n " + info.get("nombre_inst") + ". Plazas ilimitadas.",
									"Informaci�n de la clase seleccionada", JOptionPane.INFORMATION_MESSAGE);
						}

					} else {
						currentClaseSeleccionadaHorario = "";
						currentClaseSeleccionadaHorarioIdClase = "";
						btnCambiar.setEnabled(false);
					}
				}
			});

			tableHorarioCentro.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableHorarioCentro.setRowSelectionAllowed(false);
			tableHorarioCentro.setCellSelectionEnabled(true);
			tableHorarioCentro.setBorder(new EmptyBorder(5, 5, 5, 5));
			tableHorarioCentro.setSelectionForeground(Color.DARK_GRAY);
			tableHorarioCentro.getTableHeader().setReorderingAllowed(false);
			tableHorarioCentro.getTableHeader().setResizingAllowed(false);
			tableHorarioCentro.setSelectionBackground(new Color(211, 211, 211));
			tableHorarioCentro.setRowHeight(80);
			tableHorarioCentro.setRowMargin(0);
			tableHorarioCentro.setGridColor(Color.LIGHT_GRAY);
			tableHorarioCentro.getColumnModel().setColumnMargin(0);

			// Ponemos las columnas de un mismo tamaño, y que el texto se
			// centre
			DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
			tcr.setHorizontalAlignment(JLabel.CENTER);
			for (int i = 1; i < 8; i++) {
				tableHorarioCentro.getColumnModel().getColumn(i).setPreferredWidth(60);
				tableHorarioCentro.getColumnModel().getColumn(i).setCellRenderer(tcr);
			}
			tableHorarioCentro.setDefaultRenderer(String.class, tcr);
			tableHorarioCentro.getColumnModel().getColumn(0).setCellRenderer(tcr);
			tableHorarioCentro.getColumnModel().getColumn(0).setPreferredWidth(60);
			tableHorarioCentro.getColumnModel().getColumn(0).setMaxWidth(60);
			tableHorarioCentro.getColumnModel().getColumn(0).setCellRenderer(null);

			tableHorarioCentro.getTableHeader().setSize(60, 20);
			Font fuente = new Font("Verdana", Font.BOLD, 15);
			tableHorarioCentro.getTableHeader().setFont(fuente);
			tableHorarioCentro.getTableHeader().setForeground(Color.WHITE);
			tableHorarioCentro.getTableHeader().setBackground(Color.BLACK);

		}
		return tableHorarioCentro;
	}

	private String getIDreserva(String a, String ins, int dia, int mes, int anio, int horaI) {
		return manager.getIDreserva(a, ins, dia, mes, anio, horaI);
	}

	// GonziYOLO
	private String getIDclase(String a, String ins, int dia, int mes, int anio, int horaI) {
		return manager.getIDclase(a, ins, dia, mes, anio, horaI);
	}

	private class ModeloTablaActividades extends DefaultTableModel {
		private static final long serialVersionUID = 1L;

		public ModeloTablaActividades(Object[] columnNames) {
			super(columnNames, 24); // Siempre 24 horas
			for (int i = 0; i < 24; i++) {
				if (i + 1 == 24) {
					this.setValueAt("<html><p>" + i + ":00 a</p> 23:59</html>", i, 0);
				} else if (i < 10 && i + 1 < 10) {
					this.setValueAt("<html><p>0" + i + ":00 a</p> 0" + (i + 1) + ":00</html>", i, 0);
				} else if (i < 10) {
					this.setValueAt("<html><p>0" + i + ":00 a</p> " + (i + 1) + ":00</html>", i, 0);
				} else {
					this.setValueAt("<html><p>" + i + ":00 a</p> " + (i + 1) + ":00</html>", i, 0);
				}

			}

		}

		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

	}

	public void introduccionDatos(int[] days, int[] meses, int[] anios,
			ArrayList<HashMap<String, Object>> clasesSemana) {
		ArrayList<int[]> semanaEnInt = new ArrayList<int[]>();

		ArrayList<HashMap<String, Object>> clasesL = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> clasesM = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> clasesX = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> clasesJ = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> clasesV = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> clasesS = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> clasesD = new ArrayList<HashMap<String, Object>>();

		for (HashMap<String, Object> act : clasesSemana) {
			int diaAct = Integer.parseInt(((String) act.get("fecha")).split("-")[2]);
			int mesAct = Integer.parseInt(((String) act.get("fecha")).split("-")[1]);
			int anioAct = Integer.parseInt(((String) act.get("fecha")).split("-")[0]);
			if (diaAct == days[0] && mesAct == meses[0] && anioAct == anios[0]) {// Lines
				clasesL.add(act);
			} else if (diaAct == days[1] && mesAct == meses[1] && anioAct == anios[1]) {// Martes
				clasesM.add(act);
			} else if (diaAct == days[2] && mesAct == meses[2] && anioAct == anios[2]) {// Miercoles
				clasesX.add(act);
			} else if (diaAct == days[3] && mesAct == meses[3] && anioAct == anios[3]) {// Jueves
				clasesJ.add(act);
			} else if (diaAct == days[4] && mesAct == meses[4] && anioAct == anios[4]) {// Viernes
				clasesV.add(act);
			} else if (diaAct == days[5] && mesAct == meses[5] && anioAct == anios[5]) {// Sabado
				clasesS.add(act);
			} else if (diaAct == days[6] && mesAct == meses[6] && anioAct == anios[6]) {// Domingo
				clasesD.add(act);
			}

		}

		for (int i = 0; i < tableHorarioCentro.getRowCount(); i++) {
			for (int j = 1; j < tableHorarioCentro.getColumnCount(); j++) {
				getTableHorarioCentro().setValueAt(null, i, j);
			}
		}

		pintarColumna(1, clasesL);
		pintarColumna(2, clasesM);
		pintarColumna(3, clasesX);
		pintarColumna(4, clasesJ);
		pintarColumna(5, clasesV);
		pintarColumna(6, clasesS);
		pintarColumna(7, clasesD);

		getTableHorarioCentro().getTableHeader().setVisible(false);
		getTableHorarioCentro().getColumnModel().getColumn(1)
				.setHeaderValue("Lunes " + days[0] + " - " + getNombreMes(meses[0]));
		getTableHorarioCentro().getColumnModel().getColumn(2)
				.setHeaderValue("Martes " + days[1] + " - " + getNombreMes(meses[1]));
		getTableHorarioCentro().getColumnModel().getColumn(3)
				.setHeaderValue("Miercoles " + days[2] + " - " + getNombreMes(meses[2]));
		getTableHorarioCentro().getColumnModel().getColumn(4)
				.setHeaderValue("Jueves " + days[3] + " - " + getNombreMes(meses[3]));
		getTableHorarioCentro().getColumnModel().getColumn(5)
				.setHeaderValue("Viernes " + days[4] + " - " + getNombreMes(meses[4]));
		getTableHorarioCentro().getColumnModel().getColumn(6)
				.setHeaderValue("Sabado " + days[5] + " - " + getNombreMes(meses[5]));
		getTableHorarioCentro().getColumnModel().getColumn(7)
				.setHeaderValue("Domingo " + days[6] + " - " + getNombreMes(meses[6]));
		getTableHorarioCentro().getTableHeader().setVisible(true);

		semanaEnInt.add(days);
		semanaEnInt.add(meses);
		semanaEnInt.add(anios);
		semanaEnInts = semanaEnInt;

	}

	private String getNombreMes(int mes) {
		String result = "";

		switch (mes) {
		case 1: {
			result = "Ene";
			break;
		}
		case 2: {
			result = "Feb";
			break;
		}
		case 3: {
			result = "Mar";
			break;
		}
		case 4: {
			result = "Abr";
			break;
		}
		case 5: {
			result = "May";
			break;
		}
		case 6: {
			result = "Jun";
			break;
		}
		case 7: {
			result = "Jul";
			break;
		}
		case 8: {
			result = "Ago";
			break;
		}
		case 9: {
			result = "Sept";
			break;
		}
		case 10: {
			result = "Oct";
			break;
		}
		case 11: {
			result = "Nov";
			break;
		}
		case 12: {
			result = "Dic";
			break;
		}
		}
		return result;
	}

	private void pintarColumna(int colum, ArrayList<HashMap<String, Object>> clases) {
		int horaI = -1;
		int horaF = -1;
		for (HashMap<String, Object> act : clases) {
			horaI = Integer.parseInt(((String) act.get("horas")).split("-")[0]);
			horaF = Integer.parseInt(((String) act.get("horas")).split("-")[1]);
			if (horaF - horaI == 1) {
				getTableHorarioCentro().setValueAt(act.get("actividad"), horaI, colum);
			} else {
				for (int i = horaI; i < horaF; i++) {
					if (i == horaI)
						getTableHorarioCentro().setValueAt(act.get("actividad"), horaI, colum);
					else
						getTableHorarioCentro().setValueAt("", i, colum);
				}
			}
		}
	}

	private void comprobarSemanaHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		Calendar now = Calendar.getInstance();
		now.setTime(dateChooserHorarioCentro.getDate());
		dateInicial = dateChooserHorarioCentro.getDate();
		if (now.get(Calendar.DAY_OF_WEEK) == 2) {
			verLunesHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, 6);
			inicialHorarioCentro = dateInicial;
			dateChooserHorarioCentro.setDate(aux.getTime());
			finalHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 3) {
			verMartesHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -1);
			dateChooserHorarioCentro.setDate(aux.getTime());
			inicialHorarioCentro = dateChooserHorarioCentro.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooserHorarioCentro.setDate(aux.getTime());
			finalHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 4) {
			verMiercolesHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -2);
			dateChooserHorarioCentro.setDate(aux.getTime());
			inicialHorarioCentro = dateChooserHorarioCentro.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooserHorarioCentro.setDate(aux.getTime());
			finalHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 5) {
			verJuevesHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -3);
			dateChooserHorarioCentro.setDate(aux.getTime());
			inicialHorarioCentro = dateChooserHorarioCentro.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooserHorarioCentro.setDate(aux.getTime());
			finalHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 6) {
			verViernesHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -4);
			dateChooserHorarioCentro.setDate(aux.getTime());
			inicialHorarioCentro = dateChooserHorarioCentro.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooserHorarioCentro.setDate(aux.getTime());
			finalHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 7) {
			verSabadoHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			aux.add(Calendar.DATE, -5);
			dateChooserHorarioCentro.setDate(aux.getTime());
			inicialHorarioCentro = dateChooserHorarioCentro.getDate();
			aux.add(Calendar.DATE, 6);
			dateChooserHorarioCentro.setDate(aux.getTime());
			finalHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}
		if (now.get(Calendar.DAY_OF_WEEK) == 1) {
			verDomingoHorarios(clasesSemana);
			Calendar aux = Calendar.getInstance();
			aux.setTime(dateInicial);
			finalHorarioCentro = dateInicial;
			aux.add(Calendar.DATE, -6);
			dateChooserHorarioCentro.setDate(aux.getTime());
			inicialHorarioCentro = dateChooserHorarioCentro.getDate();
			dateChooserHorarioCentro.setDate(dateInicial);
		}

	}

	private void verLunesHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia;
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void verMartesHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia;
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(2);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia;

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void verMiercolesHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;// Miercoles

		actualizarDateChooserCentroHorario(-2);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia; // Lunes
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;// Martes

		actualizarDateChooserCentroHorario(2);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;// Miercoles

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia;// Jueves

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia;// Sabado

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;// Domingo
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void verJuevesHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;// Jueves

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia; // Viernes

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia;// Sabado

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;// Domingo
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(-4);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;// Miercoles

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;// Martes

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia;// Lunes
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void verViernesHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia;// V

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia; // S

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;// D
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(-3);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;// J

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;// Miercoles

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;// Martes

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia;// Lunes
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void verSabadoHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia;// S

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia; // V

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;// J

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;// X

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;// M

		actualizarDateChooserCentroHorario(-1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia;// L
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(6);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;// D
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void verDomingoHorarios(ArrayList<HashMap<String, Object>> clasesSemana) {
		int[] dias = new int[7];
		int[] meses = new int[7];
		int[] anios = new int[7];
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		int mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		int anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[6] = anio;
		meses[6] = mes;
		dias[6] = dia;// D
		diaFinalSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(-6);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[0] = anio;
		meses[0] = mes;
		dias[0] = dia; // L
		diaInicialSemanaHorario = dateChooserHorarioCentro.getDate();

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[1] = anio;
		meses[1] = mes;
		dias[1] = dia;// M

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[2] = anio;
		meses[2] = mes;
		dias[2] = dia;// X

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[3] = anio;
		meses[3] = mes;
		dias[3] = dia;// J

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[4] = anio;
		meses[4] = mes;
		dias[4] = dia;// V

		actualizarDateChooserCentroHorario(1);
		dia = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[2]));
		mes = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[1]));
		anio = Integer.parseInt((df.format(dateChooserHorarioCentro.getDate()).split("-")[0]));
		anios[5] = anio;
		meses[5] = mes;
		dias[5] = dia;// S

		introduccionDatos(dias, meses, anios, clasesSemana);
		lbSemanaHorario.setText("Semana del " + dias[0] + "/" + meses[0] + "/" + anios[0] + " al " + dias[6] + "/"
				+ meses[6] + "/" + anios[6]);
	}

	private void actualizarDateChooserCentroHorario(int valor) {
		Calendar now = Calendar.getInstance();
		now.setTime(dateChooserHorarioCentro.getDate());
		now.add(Calendar.DATE, valor);
		dateChooserHorarioCentro.setDate(now.getTime());
	}

	private JButton getButtonComprobarVerActividadesCentro() {
		if (btnComprobarVerActividadesHorarioCentro == null) {
			btnComprobarVerActividadesHorarioCentro = new JButton("Comprobar");
			btnComprobarVerActividadesHorarioCentro.setBackground(new Color(255, 255, 255));
			btnComprobarVerActividadesHorarioCentro.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (isHorarioMonitoroAdm == 0)
						comprobarSemanaHorarios(manager
								.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
					else if (isHorarioMonitoroAdm == 1)
						comprobarSemanaHorarios(manager.getActParaHorarioMonitor(manager.getCurrentUser().getDNI()));

					btnSemanaSiguiente.setEnabled(true);
					btnSemanaAnterior.setEnabled(true);
				}
			});
		}
		return btnComprobarVerActividadesHorarioCentro;
	}

	private JDateChooser getDateChooserHorarioCentro() {
		if (dateChooserHorarioCentro == null) {
			dateChooserHorarioCentro = new JDateChooser();
			dateChooserHorarioCentro.setBackground(Color.WHITE);
			dateChooserHorarioCentro.setDate(new Date());
			dateChooserHorarioCentro.setMinSelectableDate(new Date());
		}
		return dateChooserHorarioCentro;
	}

	private JPanel getPnNorteHorarioActCentro() {
		if (pnNorteHorarioActCentro == null) {
			pnNorteHorarioActCentro = new JPanel();
			pnNorteHorarioActCentro.setBackground(Color.WHITE);
			pnNorteHorarioActCentro.setLayout(new BorderLayout(0, 0));
			pnNorteHorarioActCentro.add(getBtnAtrasDesdeHorarioCentro(), BorderLayout.WEST);
			pnNorteHorarioActCentro.add(getPnCenter(), BorderLayout.CENTER);
			pnNorteHorarioActCentro.add(getCBInstalacionesAfiltrar(), BorderLayout.NORTH);
			getCBInstalacionesAfiltrar().setSelectedIndex(0);
		}
		return pnNorteHorarioActCentro;
	}

	private JLabel getLbSemanaHorario() {
		if (lbSemanaHorario == null) {
			lbSemanaHorario = new JLabel();
			lbSemanaHorario.setBackground(Color.WHITE);
			lbSemanaHorario.setHorizontalAlignment(SwingConstants.CENTER);
			lbSemanaHorario.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		}
		return lbSemanaHorario;
	}

	private JPanel getPnBotonesHorarioCentro() {
		if (pnBotonesHorarioCentro == null) {
			pnBotonesHorarioCentro = new JPanel();
			pnBotonesHorarioCentro.setBackground(Color.WHITE);
			pnBotonesHorarioCentro.setLayout(new BorderLayout(0, 0));
			pnBotonesHorarioCentro.add(getBtnSemanaSiguiente(), BorderLayout.EAST);
			pnBotonesHorarioCentro.add(getBtnSemanaAnterior(), BorderLayout.WEST);
			// pnBotonesHorarioCentro.add(getBtnCambiar(), BorderLayout.CENTER);
		}
		return pnBotonesHorarioCentro;
	}

	private JButton getBtnSemanaSiguiente() {
		if (btnSemanaSiguiente == null) {
			btnSemanaSiguiente = new JButton(">");
			btnSemanaSiguiente.setBackground(Color.WHITE);
			btnSemanaSiguiente.setEnabled(false);
			btnSemanaSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Calendar now = Calendar.getInstance();
					now.setTime(diaFinalSemanaHorario);
					now.add(Calendar.DATE, 1);
					dateChooserHorarioCentro.setDate(now.getTime());
					if (isHorarioMonitoroAdm == 0)
						comprobarSemanaHorarios(manager
								.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
					else if (isHorarioMonitoroAdm == 1)
						comprobarSemanaHorarios(manager.getActParaHorarioMonitor(manager.getCurrentUser().getDNI()));

					getBtnCambiar().setEnabled(false);
					currentClaseSeleccionadaHorario = "";
					currentClaseSeleccionadaHorarioIdClase = "";
				}
			});
		}
		return btnSemanaSiguiente;
	}

	private JButton getBtnSemanaAnterior() {
		if (btnSemanaAnterior == null) {
			btnSemanaAnterior = new JButton("<");
			btnSemanaAnterior.setBackground(Color.WHITE);
			btnSemanaAnterior.setEnabled(false);
			btnSemanaAnterior.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Calendar now = Calendar.getInstance();
					now.setTime(diaInicialSemanaHorario);
					now.add(Calendar.DATE, -1);

					Date min = dateChooserHorarioCentro.getMinSelectableDate();
					Date nueva = now.getTime();
					if (min.compareTo(nueva) <= 0) {
						dateChooserHorarioCentro.setDate(now.getTime());
						if (isHorarioMonitoroAdm == 0)
							comprobarSemanaHorarios(manager
									.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
						else if (isHorarioMonitoroAdm == 1)
							comprobarSemanaHorarios(
									manager.getActParaHorarioMonitor(manager.getCurrentUser().getDNI()));

						getBtnCambiar().setEnabled(false);
						currentClaseSeleccionadaHorario = "";
						currentClaseSeleccionadaHorarioIdClase = "";
					} else
						JOptionPane.showMessageDialog(null, "No puede mirar semanas que han pasado", "Error",
								JOptionPane.ERROR_MESSAGE);
				}
			});
		}
		return btnSemanaAnterior;
	}

	private JButton getBtnCambiar() {
		if (btnCambiar == null) {
			btnCambiar = new JButton("Modificar");
			btnCambiar.setBackground(Color.WHITE);
			btnCambiar.setVisible(true);
			btnCambiar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// JOptionPane.showMessageDialog(null,
					// currentClaseSeleccionadaHorario);
					if (isHorarioMonitoroAdm == 0) {
						showMonitorCambioActividad();
						showClaseCambioInsta();
						showHoraInicioClase();
						tfAumentarPlazas
								.setText(String.valueOf(manager.getPlazasActividad(currentClaseSeleccionadaHorario)));
						int horaActual = manager.getHoras(currentClaseSeleccionadaHorario);
						tfHoraActual.setText(String.valueOf(horaActual));
						tfInstalacionActual.setText(manager.getInstalacion(currentClaseSeleccionadaHorario));
						tfActividadEscogida.setText(manager.getActividad(currentClaseSeleccionadaHorario));
						card.show(pnBase, "pnModificacionesActClase");
					} else if (isHorarioMonitoroAdm == 1) {
						// JOptionPane.showMessageDialog(null,
						// currentClaseSeleccionadaHorario);
						HashMap<String, Object> info = manager.getInfoBookingActCancel(currentClaseSeleccionadaHorario);
						if (((Integer) info.get("plazasLibres")) > 0) {
							card.show(pnBase, "pnSeeAssistantsList");
							pnListAssists.repaint();
							showClassMonitorList();

							pnListAssists.paintAll(pnListAssists.getGraphics());
							currentClaseSeleccionadaParaPasarLista = currentClaseSeleccionadaHorario;

							Component[] comp = getPnListaClasesMonitorView().getComponents();
							for (int i = 0; i < comp.length; i++) {
								JButton aux = (JButton) comp[i];
								if (!aux.getActionCommand().equals(currentClaseSeleccionadaHorario))
									aux.setBackground(Color.WHITE);
								else {
									aux.setBackground(new Color(135, 206, 250));
								}
							}

							getLblLaClaseSeleccionada().setText("La clase de " + info.get("nombre_clase")
									+ " se impartir� el d�a " + info.get("fecha_usada") + " " + "desde las "
									+ info.get("hora_usada_inicio") + ":00 hasta las " + info.get("hora_usada_final")
									+ ":00." + "\nCon el monitor " + info.get("nombre_monitor") + " "
									+ info.get("apellido_monitor") + " (con DNI " + info.get("monitorDNI")
									+ ").\nEn la instalaci�n " + info.get("nombre_inst") + ". Quedan "
									+ info.get("plazasLibres") + " plazas libres.");

							pnListAssists.removeAll();
							pnListAssists.setVisible(false);
							showAssistantList();
							pnListAssists.setVisible(true);
						} else {
							JOptionPane.showMessageDialog(null, "Se trata de una clase con plazas ilimitadas",
									"No hay listas de asistentes", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			});
			btnCambiar.setEnabled(false);
		}
		return btnCambiar;
	}

	private void showClaseCambioInsta() {
		String id_reserva = currentClaseSeleccionadaHorario;
		String nombreInst = manager.getNombreInstWithID_Reserva(id_reserva);
		getTxtInstalacionAnterior().setText(nombreInst);
		String[] model = manager.getAllInst().toArray(new String[manager.getAllInst().size()]);
		getCBNuevasInstalacionesAEscoger().setModel(new DefaultComboBoxModel<String>(model));
		getLblCambioDeInstalacin().setText(manager.getFechaHorasWithID_Reserva(id_reserva)[0]);
	}

	private void showHoraInicioClase() {
		String id_reserva = currentClaseSeleccionadaHorario;
		String horaI = manager.getHoraInicioWithID_Reserva(id_reserva);
		String horaF = manager.getHoraFinalWithID_Reserva(id_reserva);
		generarHoras(cbNuevaHoraInicio, 0, Integer.parseInt(horaF) - 1);
		getTxtHoraInicioAnterior().setText(horaI);
		getLblPrueba().setText(manager.getFechaHorasWithID_Reserva(id_reserva)[4]);
	}

	private void showMonitorCambioActividad() {
		String id_reserva = currentClaseSeleccionadaHorario;
		List<HashMap<String, Object>> datos = manager.getFechaHoraIHoraF(manager.getIDclaseFromIDreserva(id_reserva));
		String text = manager.getNombreApellidosMonitorDNI((String) datos.get(0).get("monitor"));
		getTxtNombreMonitor().setText(text);
		getLblNewLabel().setText("Esta cambiando el monitor para la actividad "
				+ manager.getNombreActIDClase(manager.getIDclaseFromIDreserva(id_reserva)));
	}

	private JPanel getPnHorarioActividadesCentro() {
		if (pnHorarioActividadesCentro == null) {
			pnHorarioActividadesCentro = new JPanel();
			pnHorarioActividadesCentro.setBackground(Color.WHITE);
			pnHorarioActividadesCentro.setLayout(new BorderLayout(0, 0));
			pnHorarioActividadesCentro.add(getPnBotonesHorarioCentro(), BorderLayout.SOUTH);
			pnHorarioActividadesCentro.add(getPnNorteHorarioActCentro(), BorderLayout.NORTH);
			pnHorarioActividadesCentro.add(getScrollPaneHorarioCentral(), BorderLayout.CENTER);
		}
		return pnHorarioActividadesCentro;
	}

	private JButton getBtnHorariosActividadCentro() {
		if (btnVerHorarioAct == null) {
			btnVerHorarioAct = new JButton("Ver Horarios Actividades");
			btnVerHorarioAct.setBackground(Color.WHITE);
			btnVerHorarioAct.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnVerHorarioAct.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					pnBotonesHorarioCentro.add(getBtnCambiar(), BorderLayout.CENTER);

					getBtnCambiar().setVisible(true);

					getBtnCambiar().setText("Modificar");
					getBtnCambiar().setEnabled(false);

					currentClaseSeleccionadaHorario = "";
					currentClaseSeleccionadaHorarioIdClase = "";
					Calendar now = Calendar.getInstance();
					now.setTime(new Date());
					now.add(Calendar.DATE, 1);
					dateChooserHorarioCentro.setDate(now.getTime());
					comprobarSemanaHorarios(
							manager.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));

					isHorarioMonitoroAdm = 0;

					card.show(pnBase, "pnHorarioActividadesCentro");
					btnSemanaSiguiente.setEnabled(true);
					btnSemanaAnterior.setEnabled(true);
					fillComboBoxPlazas();
					fillComboBoxHoras();

				}
			});
		}
		return btnVerHorarioAct;
	}

	private JButton getBtnHorariosActividadMonitor() {
		if (btnVerHorarioMonitor == null) {
			btnVerHorarioMonitor = new JButton("Ver Horarios Actividades");
			btnVerHorarioMonitor.setBackground(Color.WHITE);
			btnVerHorarioMonitor.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
			btnVerHorarioMonitor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					pnBotonesHorarioCentro.add(getBtnCambiar(), BorderLayout.CENTER);

					getBtnCambiar().setVisible(true);

					getBtnCambiar().setText("Ver lista de asistentes");
					getBtnCambiar().setEnabled(false);

					currentClaseSeleccionadaHorario = "";
					currentClaseSeleccionadaHorarioIdClase = "";

					Calendar now = Calendar.getInstance();
					now.setTime(new Date());
					now.add(Calendar.DATE, 1);
					dateChooserHorarioCentro.setDate(now.getTime());

					comprobarSemanaHorarios(manager.getActParaHorarioMonitor(manager.getCurrentUser().getDNI()));

					isHorarioMonitoroAdm = 1;
					card.show(pnBase, "pnHorarioActividadesCentro");
					btnSemanaSiguiente.setEnabled(true);
					btnSemanaAnterior.setEnabled(true);

				}
			});
		}
		return btnVerHorarioMonitor;
	}

	private JButton getBtnAtrasDesdeHorarioCentro() {
		if (btnAtrasDesdeHorarioCentro == null) {
			btnAtrasDesdeHorarioCentro = new JButton("");
			btnAtrasDesdeHorarioCentro.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHomeView");
				}
			});
			btnAtrasDesdeHorarioCentro.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnAtrasDesdeHorarioCentro.setBackground(Color.WHITE);
			btnAtrasDesdeHorarioCentro.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnAtrasDesdeHorarioCentro, "/img/arrow.png");
			btnAtrasDesdeHorarioCentro.setBorder(null);
		}
		return btnAtrasDesdeHorarioCentro;
	}

	private JPanel getPnCenter() {
		if (pnCenter == null) {
			pnCenter = new JPanel();
			pnCenter.setBackground(Color.WHITE);
			pnCenter.setLayout(new BorderLayout(0, 0));
			pnCenter.add(getDateChooserHorarioCentro(), BorderLayout.NORTH);
			pnCenter.add(getButtonComprobarVerActividadesCentro(), BorderLayout.CENTER);
			pnCenter.add(getLbSemanaHorario(), BorderLayout.SOUTH);
		}
		return pnCenter;
	}

	private JPanel getPnCambiosDesdeHorario() {
		if (pnCambiosDesdeHorario == null) {
			pnCambiosDesdeHorario = new JPanel();
			pnCambiosDesdeHorario.setBackground(Color.WHITE);
			pnCambiosDesdeHorario.setLayout(new BorderLayout(0, 0));
			pnCambiosDesdeHorario.add(getBtnVolverAHorario(), BorderLayout.SOUTH);
			pnCambiosDesdeHorario.add(getScrollPaneCambiosClaseConcreta(), BorderLayout.CENTER);
		}
		return pnCambiosDesdeHorario;
	}

	private JButton getBtnVolverAHorario() {
		if (btnVolverAHorario == null) {
			btnVolverAHorario = new JButton("Volver a horario");
			btnVolverAHorario.setBackground(Color.WHITE);
			btnVolverAHorario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					card.show(pnBase, "pnHorarioActividadesCentro");
					comprobarSemanaHorarios(
							manager.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
				}
			});
		}
		return btnVolverAHorario;
	}

	private JPanel getPnCambios() {
		if (pnCambios == null) {
			pnCambios = new JPanel();
			pnCambios.setBorder(null);
			pnCambios.setBackground(Color.WHITE);
			pnCambios.setLayout(new GridLayout(0, 1, 0, 0));
			pnCambios.add(getBtnCancelarActividad());
			pnCambios.add(getPnCambioHoraInicio());
			pnCambios.add(getPnCambioMonitorNuevo());
			pnCambios.add(getPnCambioInstalacionNuevaClaseConcreta());
			pnCambios.add(getPnModificarPlazas());
			pnCambios.add(getPnModificarHoras());
			pnCambios.add(getPnInstalacionActividad());

		}
		return pnCambios;
	}

	private JScrollPane getScrollPaneCambiosClaseConcreta() {
		if (scrollPaneCambiosClaseConcreta == null) {
			scrollPaneCambiosClaseConcreta = new JScrollPane();
			scrollPaneCambiosClaseConcreta.setViewportView(getPnCambios());
		}
		return scrollPaneCambiosClaseConcreta;
	}

	private JPanel getPnContenedorDeTablaCrearActividad() {
		if (pnContenedorDeTablaCrearActividad == null) {
			pnContenedorDeTablaCrearActividad = new JPanel();
			pnContenedorDeTablaCrearActividad.setLayout(new BorderLayout(0, 0));
			pnContenedorDeTablaCrearActividad.add(getScrollParaTablaCrearActividadPuntual(), BorderLayout.CENTER);
		}
		return pnContenedorDeTablaCrearActividad;
	}

	private JScrollPane getScrollParaTablaCrearActividadPuntual() {
		if (scrollParaTablaCrearActividadPuntual == null) {
			scrollParaTablaCrearActividadPuntual = new JScrollPane();
		}
		return scrollParaTablaCrearActividadPuntual;
	}

	private JTable getTableAgendaActividades() {
		if (tableAgendaActividades == null) {
			tableAgendaActividades = new JTable();
			tableAgendaActividades.setVisible(true);
			tableAgendaActividades.setOpaque(true);
		}
		return tableAgendaActividades;
	}

	/**
	 * This method shows in the table, all the schedule of the installation
	 * 
	 * @param boton
	 *            the installation clicked
	 */
	protected void showScheduleInstActividadPuntual(String boton) {
		ArrayList<Activity> list = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(calendarActivities.getDate());

		ArrayList<Activity> auxlist = manager.getScheduleInstallation(boton, fecha);

		String[] columns = { "Hora", "      " };
		Object[][] data = new Object[24][2];

		if (auxlist.size() > 0) {
			for (Activity a : auxlist) {
				list.add(a);
			}
			// putting data into the table when it has any kind of activity
			// scheduled
			for (int i = 0; i < 24; i++) {
				data[i][0] = i;
				for (int j = 0; j < list.size(); j++) {
					Activity ac = list.get(j);
					if (ac.getHoraInit() <= i && ac.getHoraFin() > i) {
						if (ac.getType().compareTo("a") == 0) { // admin
							data[i][1] = ">" + ac.getClaseNombre();
						} else if (manager.checkIfBookingIsCancelled(currentInstallation, fecha, i)) {// cancelada
							data[i][1] = "*" + "Cancelada";
						} else if (ac.getType().compareTo("n") == 0) {// not
																		// member
							data[i][1] = "-" + ac.getClaseNombre();
						} else if (ac.getType().compareTo("m") == 0) {// member
							data[i][1] = "+" + ac.getClaseNombre();
						}
						j = list.size() - 1;
					} else {
						data[i][1] = "";
					}
				}
			}
		} else if (auxlist.size() == 0) {
			// putting data into the table when it is empty
			for (int i = 0; i < 24; i++) {
				data[i][0] = i;
				data[i][1] = new String("");
			}
		}

		DefaultTableModel model = new DefaultTableModel(data, columns) {

			private static final long serialVersionUID = 1L;

			@Override
			public Class<? extends Object> getColumnClass(int column) {
				return getValueAt(0, column).getClass();
			}
		};

		tableAgendaActividades = new JTable(model) {
			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				if (!isRowSelected(row)) {
					if (tableAgendaActividades.getColumnCount() >= 0) {
						String type = (String) getModel().getValueAt(row, 1);
						// colores en funcion del tipo de actividad que haya en
						// la instalacion y dependiendo del usuario que este
						// conectado, si es admin o si es un member
						if (manager.currentUserIsAdmin()) {
							if (type.equals("")) {
								c.setBackground(Color.white);
							} else if (type.substring(0, 1).compareTo(">") == 0) {
								c.setBackground(Color.yellow);
							} else if (type.substring(0, 1).compareTo("+") == 0) {
								c.setBackground(Color.lightGray);// si es de
																	// socio
							} else if (type.substring(0, 1).compareTo("-") == 0) {
								c.setBackground(SystemColor.activeCaption);
							} else if (type.substring(0, 1).compareTo("*") == 0) {
								c.setBackground(Color.red);
							}

						} else {// cuando el loggeado es un usuario verde para
								// sus cosas, y naranja para los del centro
							if (type.equals("")) {
								c.setBackground(Color.white);
							} else if (type.substring(0, 1).compareTo(">") == 0) {
								c.setBackground(Color.lightGray);
							} else if (type.substring(0, 1).compareTo("+") == 0) {
								c.setBackground(Color.yellow);// si es de socio
							} else if (type.substring(0, 1).compareTo("-") == 0) {
								c.setBackground(Color.lightGray);
							} else if (type.substring(0, 1).compareTo("*") == 0) {
								c.setBackground(Color.red);
							}
						}
					}
				}
				return c;
			}
		};
		// centrar el texto de dentro de la tabla
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		tableAgendaActividades.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		tableAgendaActividades.setDefaultRenderer(String.class, centerRenderer);

		// a�adir al scroll pane la tabla
		scrollParaTablaCrearActividadPuntual.setViewportView(getTableAgendaActividades());

	}

	private JTextArea getLblLaClaseSeleccionada() {
		if (lblLaClaseSeleccionada == null) {
			lblLaClaseSeleccionada = new JTextArea("");
			lblLaClaseSeleccionada.setEditable(false);
		}
		return lblLaClaseSeleccionada;
	}

	private JPanel getPnCambioHoraInicio() {
		if (pnCambioHoraInicio == null) {
			pnCambioHoraInicio = new JPanel();
			pnCambioHoraInicio.setBackground(Color.WHITE);
			pnCambioHoraInicio.setBorder(new TitledBorder(new LineBorder(new Color(171, 173, 179)),
					"Cambio de hora de inicio para clase concreta", TitledBorder.CENTER, TitledBorder.TOP, null,
					new Color(0, 0, 0)));
			pnCambioHoraInicio.setLayout(null);
			pnCambioHoraInicio.add(getLblCambioHoraInicio());
			pnCambioHoraInicio.add(getLblHoraInicioAnterior());
			pnCambioHoraInicio.add(getTxtHoraInicioAnterior());
			txtHoraInicioAnterior.setColumns(10);

			cbNuevaHoraInicio = new JComboBox<String>();
			cbNuevaHoraInicio.setFont(new Font("Dialog", Font.ITALIC, 11));
			cbNuevaHoraInicio.setBackground(Color.WHITE);
			cbNuevaHoraInicio.setBounds(281, 125, 154, 23);
			pnCambioHoraInicio.add(cbNuevaHoraInicio);

			JLabel lblNuevaHoraDe = new JLabel("Nueva Hora de inicio:");
			lblNuevaHoraDe.setFont(new Font("Dialog", Font.BOLD, 12));
			lblNuevaHoraDe.setBounds(83, 127, 150, 19);
			pnCambioHoraInicio.add(lblNuevaHoraDe);
			pnCambioHoraInicio.add(getBtnCambiarHoraInicio());
			pnCambioHoraInicio.add(getLblPrueba());
		}
		return pnCambioHoraInicio;
	}

	private JLabel getLblCambioHoraInicio() {
		if (lblCambioHoraInicio == null) {
			lblCambioHoraInicio = new JLabel("");
			lblCambioHoraInicio.setBounds(245, 57, 406, -31);
		}
		return lblCambioHoraInicio;
	}

	private JLabel getLblHoraInicioAnterior() {
		if (lblHoraInicioAnterior == null) {
			lblHoraInicioAnterior = new JLabel("Hora de inicio anterior:");
			lblHoraInicioAnterior.setFont(new Font("Dialog", Font.BOLD, 12));
			lblHoraInicioAnterior.setBounds(83, 72, 183, 19);
		}
		return lblHoraInicioAnterior;
	}

	private JTextField getTxtHoraInicioAnterior() {
		if (txtHoraInicioAnterior == null) {
			txtHoraInicioAnterior = new JTextField();
			txtHoraInicioAnterior.setBackground(Color.WHITE);
			txtHoraInicioAnterior.setEditable(false);
			txtHoraInicioAnterior.setBounds(276, 73, 159, 19);
		}
		return txtHoraInicioAnterior;
	}

	private JButton getBtnCambiarHoraInicio() {
		if (btnCambiarHoraInicio == null) {
			btnCambiarHoraInicio = new JButton("Cambiar Hora Inicio");
			btnCambiarHoraInicio.setBounds(316, 170, 149, 21);
			btnCambiarHoraInicio.setFont(new Font("Dialog", Font.BOLD, 10));
			btnCambiarHoraInicio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					HashMap<String, Object> datos = manager.getInfoBookingActCancel(currentClaseSeleccionadaHorario);
					String nombreI = (String) datos.get("nombre_inst");
					String fecha = (String) datos.get("fecha_usada");
					int horaI = (Integer) datos.get("hora_usada_inicio");
					int horaF = (Integer) datos.get("hora_usada_final");

					if (Integer.parseInt((String) cbNuevaHoraInicio.getSelectedItem()) <= horaI) {
						if (manager.checkInstalacionOcupada(nombreI, fecha,
								Integer.parseInt((String) cbNuevaHoraInicio.getSelectedItem()), horaI)) {
							manager.cambiarHoraInicio(Integer.parseInt((String) cbNuevaHoraInicio.getSelectedItem()),
									currentClaseSeleccionadaHorario);
							JOptionPane.showMessageDialog(null, "Hora de inicio de clase cambiada con éxito");
							showHoraInicioClase();
							if (manager.getPlazasLibres(currentClaseSeleccionadaHorario) != -1) {
								List<String[]> lista = manager.obtenerAsistentesClase(currentClaseSeleccionadaHorario);
								if (lista.size() > 0) {
									card.show(pnBase, "pnCambiosCancelarActClass");
									cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnAvisarAsistentes");
									txtAreaAvisarAsistentes.setText("");
									String text = "";
									for (String[] str : lista) {

										text = text + str[0] + " " + str[1] + "  DNI: " + str[2] + "   Teléfono: "
												+ str[3] + "\n";
										txtAreaAvisarAsistentes.setText(text);
									}
								}
							}
						} else {
							JOptionPane.showMessageDialog(null,
									"Imposible cambiar la hora de inicio\nInstalación no disponible para la hora selecionada");
						}
					} else {
						manager.cambiarHoraInicio(Integer.parseInt((String) cbNuevaHoraInicio.getSelectedItem()),
								currentClaseSeleccionadaHorario);
						JOptionPane.showMessageDialog(null, "Hora de inicio de clase cambiada con éxito");
						showHoraInicioClase();
						if (manager.getPlazasLibres(currentClaseSeleccionadaHorario) != -1) {
							List<String[]> lista = manager.obtenerAsistentesClase(currentClaseSeleccionadaHorario);
							if (lista.size() > 0) {
								card.show(pnBase, "pnCambiosCancelarActClass");
								cardCancelarCambiarActClass.show(pnCambiosActANDClass, "pnAvisarAsistentes");
								txtAreaAvisarAsistentes.setText("");
								String text = "";
								for (String[] str : lista) {
									text = text + str[0] + " " + str[1] + "  DNI: " + str[2] + "   Teléfono: " + str[3]
											+ "\n";
									txtAreaAvisarAsistentes.setText(text);
								}
							}
						}

					}
				}
			});
			btnCambiarHoraInicio.setBackground(Color.WHITE);
		}
		return btnCambiarHoraInicio;
	}

	private JLabel getLblPrueba() {
		if (lblPrueba == null) {
			lblPrueba = new JLabel("");
			lblPrueba.setBounds(113, 24, 487, 36);
		}
		return lblPrueba;
	}

	private JLabel getLblTicketDeDevolucion() {
		if (lblTicketDeDevolucion == null) {
			lblTicketDeDevolucion = new JLabel("Ticket de devoluci\u00F3n");
			lblTicketDeDevolucion.setForeground(Color.RED);
			lblTicketDeDevolucion.setFont(new Font("Tw Cen MT", Font.BOLD, 40));
		}
		return lblTicketDeDevolucion;
	}

	private JTextPane getTxtPaneTicketDevolucion() {
		if (txtPaneTicketDevolucion == null) {
			txtPaneTicketDevolucion = new JTextPane();
			txtPaneTicketDevolucion.setFont(new Font("Tw Cen MT", Font.PLAIN, 18));
		}
		return txtPaneTicketDevolucion;
	}

	private JButton getBtnFinalizarTicketDevolucion() {
		if (btnFinalizarTicketDevolucion == null) {
			btnFinalizarTicketDevolucion = new JButton("Finalizar");
			btnFinalizarTicketDevolucion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					card.show(pnBase, "pnHomeView");
				}
			});
			btnFinalizarTicketDevolucion.setFont(new Font("Tw Cen MT", Font.BOLD, 12));
		}
		return btnFinalizarTicketDevolucion;
	}

	// Alvaro desapuntarSocio
	private JPanel getPnRemoveUserToActivity() {
		if (pnRemoveUserToActivity == null) {
			pnRemoveUserToActivity = new JPanel();
			pnRemoveUserToActivity.setBackground(Color.WHITE);
			GridBagLayout gbl_pnRemoveUserToActivity = new GridBagLayout();
			gbl_pnRemoveUserToActivity.columnWidths = new int[] { 70, 100, 198, 149, 0 };
			gbl_pnRemoveUserToActivity.rowHeights = new int[] { 41, 21, 50, 30, 30, 5, 30, 0 };
			gbl_pnRemoveUserToActivity.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnRemoveUserToActivity.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
					Double.MIN_VALUE };
			pnRemoveUserToActivity.setLayout(gbl_pnRemoveUserToActivity);
			GridBagConstraints gbc_btnComprobarActividadesRemoveUser = new GridBagConstraints();
			gbc_btnComprobarActividadesRemoveUser.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnComprobarActividadesRemoveUser.anchor = GridBagConstraints.SOUTH;
			gbc_btnComprobarActividadesRemoveUser.insets = new Insets(0, 0, 5, 5);
			gbc_btnComprobarActividadesRemoveUser.gridx = 2;
			gbc_btnComprobarActividadesRemoveUser.gridy = 0;
			pnRemoveUserToActivity.add(getBtnComprobarActividadesRemoveUser(), gbc_btnComprobarActividadesRemoveUser);
			GridBagConstraints gbc_btnBackHomeForRemoveUserToActivity = new GridBagConstraints();
			gbc_btnBackHomeForRemoveUserToActivity.fill = GridBagConstraints.BOTH;
			gbc_btnBackHomeForRemoveUserToActivity.gridheight = 2;
			gbc_btnBackHomeForRemoveUserToActivity.insets = new Insets(0, 0, 5, 5);
			gbc_btnBackHomeForRemoveUserToActivity.gridx = 0;
			gbc_btnBackHomeForRemoveUserToActivity.gridy = 0;
			pnRemoveUserToActivity.add(getBtnBackHomeForRemoveUserToActivity(), gbc_btnBackHomeForRemoveUserToActivity);
			GridBagConstraints gbc_pnActivitiesSelection = new GridBagConstraints();
			gbc_pnActivitiesSelection.fill = GridBagConstraints.BOTH;
			gbc_pnActivitiesSelection.gridheight = 5;
			gbc_pnActivitiesSelection.insets = new Insets(0, 0, 5, 5);
			gbc_pnActivitiesSelection.gridx = 2;
			gbc_pnActivitiesSelection.gridy = 1;
			pnRemoveUserToActivity.add(getPnActivitiesSelectionAlvaro(), gbc_pnActivitiesSelection);
			GridBagConstraints gbc_txtRemovePlazasUser = new GridBagConstraints();
			gbc_txtRemovePlazasUser.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtRemovePlazasUser.insets = new Insets(0, 0, 5, 0);
			gbc_txtRemovePlazasUser.gridx = 3;
			gbc_txtRemovePlazasUser.gridy = 1;
			pnRemoveUserToActivity.add(getTxtRemovePlazasUser(), gbc_txtRemovePlazasUser);
			GridBagConstraints gbc_calendarRemoveUserToActivity = new GridBagConstraints();
			gbc_calendarRemoveUserToActivity.gridheight = 6;
			gbc_calendarRemoveUserToActivity.insets = new Insets(0, 0, 5, 5);
			gbc_calendarRemoveUserToActivity.fill = GridBagConstraints.BOTH;
			gbc_calendarRemoveUserToActivity.gridx = 1;
			gbc_calendarRemoveUserToActivity.gridy = 0;
			// pnRemoveUserToActivity.add(getCalendarRemoveUserToActivity(),
			// gbc_calendarRemoveUserToActivity);
			GridBagConstraints gbc_lblRemoveUserToActivity = new GridBagConstraints();
			gbc_lblRemoveUserToActivity.anchor = GridBagConstraints.EAST;
			gbc_lblRemoveUserToActivity.fill = GridBagConstraints.BOTH;
			gbc_lblRemoveUserToActivity.insets = new Insets(0, 0, 5, 5);
			gbc_lblRemoveUserToActivity.gridx = 0;
			gbc_lblRemoveUserToActivity.gridy = 2;
			pnRemoveUserToActivity.add(getLblRemoveUserToActivity(), gbc_lblRemoveUserToActivity);
			GridBagConstraints gbc_txtApuntarDniSocio = new GridBagConstraints();
			gbc_txtApuntarDniSocio.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtApuntarDniSocio.insets = new Insets(0, 0, 5, 5);
			gbc_txtApuntarDniSocio.gridx = 1;
			gbc_txtApuntarDniSocio.gridy = 2;
			pnRemoveUserToActivity.add(getTxtApuntarDniSocio(), gbc_txtApuntarDniSocio);
			GridBagConstraints gbc_btnDesapuntarSocio = new GridBagConstraints();
			gbc_btnDesapuntarSocio.fill = GridBagConstraints.BOTH;
			gbc_btnDesapuntarSocio.insets = new Insets(0, 0, 5, 0);
			gbc_btnDesapuntarSocio.gridx = 3;
			gbc_btnDesapuntarSocio.gridy = 5;
			pnRemoveUserToActivity.add(getBtnDesapuntarSocio(), gbc_btnDesapuntarSocio);
		}
		return pnRemoveUserToActivity;
	}

	private JButton getBtnBackHomeForRemoveUserToActivity() {
		if (btnBackHomeForRemoveUserToActivity == null) {
			btnBackHomeForRemoveUserToActivity = new JButton("");
			btnBackHomeForRemoveUserToActivity.setBackground(Color.WHITE);

			btnBackHomeForRemoveUserToActivity.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					resetPanelActivitiesForUsers();
					card.show(pnBase, "pnHomeView");
				}
			});
			btnBackHomeForRemoveUserToActivity.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
			btnBackHomeForRemoveUserToActivity.setBackground(pnNotMemberInstallationBooking.getBackground());
			btnBackHomeForRemoveUserToActivity.setBounds(0, 0, 70, 70);
			setAdaptedImageButton(btnBackHomeForRemoveUserToActivity, "/img/arrow.png");
			btnBackHomeForRemoveUserToActivity.setBorder(null);
		}
		return btnBackHomeForRemoveUserToActivity;
	}

	private JButton getBtnComprobarActividadesRemoveUser() {
		if (btnComprobarActividadesRemoveUser == null) {
			btnComprobarActividadesRemoveUser = new JButton("Comprobar actividades");
			btnComprobarActividadesRemoveUser.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {// Comprobar
					resetPanelActivitiesForUsersRemove();
					pnActivitesAlvaro.removeAll();
					getPnActivitesAlvaro().repaint();
					showActivitiesToUserRemove(getTxtApuntarDniSocio().getText());
					pnActivitesAlvaro.paintAll(pnActivitesAlvaro.getGraphics());
				}
			});
			btnComprobarActividadesRemoveUser.setBackground(Color.WHITE);
			btnComprobarActividadesRemoveUser.setFont(new Font("Tw Cen MT", Font.BOLD, 14));
		}
		return btnComprobarActividadesRemoveUser;
	}

	private JPanel getPnActivitiesSelectionAlvaro() {
		if (pnActivitiesSelectionAlvaro == null) {
			pnActivitiesSelectionAlvaro = new JPanel();
			pnActivitiesSelectionAlvaro.setBackground(Color.WHITE);
			pnActivitiesSelectionAlvaro.setLayout(new BorderLayout(0, 0));
			pnActivitiesSelectionAlvaro.add(getScrollPnActivitiesSelectionAlvaro(), BorderLayout.CENTER);
		}
		return pnActivitiesSelectionAlvaro;
	}

	private JScrollPane getScrollPnActivitiesSelectionAlvaro() {
		if (scrollPnActivitiesSelectionAlvaro == null) {
			scrollPnActivitiesSelectionAlvaro = new JScrollPane();
			scrollPnActivitiesSelectionAlvaro.setBackground(pnActivitiesSelectionAlvaro.getBackground());
			scrollPnActivitiesSelectionAlvaro
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesSelectionAlvaro
					.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPnActivitiesSelectionAlvaro.setViewportView(getPnActivitesAlvaro());
		}
		return scrollPnActivitiesSelectionAlvaro;
	}

	private JPanel getPnActivitesAlvaro() {
		if (pnActivitesAlvaro == null) {
			pnActivitesAlvaro = new JPanel();
			pnActivitesAlvaro.setBackground(Color.WHITE);
			// pnActivites.setBorder(new EmptyBorder(5, 5, 5, 5));
			pnActivitesAlvaro.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return pnActivitesAlvaro;
	}

	private JTextField getTxtRemovePlazasUser() {
		if (txtRemovePlazasUser == null) {
			txtRemovePlazasUser = new JTextField();
			txtRemovePlazasUser.setForeground(Color.BLACK);
			txtRemovePlazasUser.setBackground(Color.WHITE);
			txtRemovePlazasUser.setHorizontalAlignment(SwingConstants.CENTER);
			txtRemovePlazasUser.setFont(new Font("Tw Cen MT", Font.BOLD | Font.ITALIC, 14));
			txtRemovePlazasUser.setEditable(false);
			txtRemovePlazasUser.setColumns(10);
		}
		return txtRemovePlazasUser;
	}

	private JLabel getLblRemoveUserToActivity() {
		if (lblRemoveUserToActivity == null) {
			lblRemoveUserToActivity = new JLabel("DNI del socio:");
			lblRemoveUserToActivity.setHorizontalAlignment(SwingConstants.RIGHT);
			lblRemoveUserToActivity.setFont(new Font("Tahoma", Font.BOLD, 15));
		}
		return lblRemoveUserToActivity;
	}

	private JTextField getTxtApuntarDniSocio() {
		if (txtApuntarDniSocio == null) {
			txtApuntarDniSocio = new JTextField();
			txtApuntarDniSocio.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent a) {
					char c = a.getKeyChar();
					if (Character.isWhitespace(c) || (countNumbersDNI(txtApuntarDniSocio) == 8 && Character.isDigit(c))
							|| (countLettersDNI(txtApuntarDniSocio) == 1 && Character.isLetter(c))
							|| notCorrectDNI(txtApuntarDniSocio)) {
						a.consume();
					}
				}
			});
			txtApuntarDniSocio.setColumns(10);
		}
		return txtApuntarDniSocio;
	}

	private JButton getBtnDesapuntarSocio() {
		if (btnDesapuntarSocio == null) {
			btnDesapuntarSocio = new JButton("Desapuntar");
			btnDesapuntarSocio.setFont(new Font("Tw Cen MT", Font.BOLD, 15));
			btnDesapuntarSocio.setBackground(Color.WHITE);
			btnDesapuntarSocio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!getTxtApuntarDniSocio().getText().equals("")) {
						if (manager.isDniUser(getTxtApuntarDniSocio().getText())) {
							if (!currentActivitiesToUser.equals("")) {
								if (manager.isUserRegisterToThisActivity(getTxtApuntarDniSocio().getText(),
										currentActivitiesToUser)) {
									if (manager.removeUserInListAssistants(currentActivitiesToUser,
											getTxtApuntarDniSocio().getText())) {
										JOptionPane.showMessageDialog(null, "Se ha completado correctamente", "",
												JOptionPane.INFORMATION_MESSAGE);
										resetPanelActivitiesForUsersRemove();
										card.show(pnBase, "pnHomeView");

									}
								} else
									JOptionPane.showMessageDialog(null, "Este socio no esta apuntado a esta actividad",
											"Error", JOptionPane.ERROR_MESSAGE);
							} else
								JOptionPane.showMessageDialog(null, "No ha seleccionado ninguna actividad", "Error",
										JOptionPane.ERROR_MESSAGE);
						} else
							JOptionPane.showMessageDialog(null, "El DNI no pertenece a ning�n socio", "Error",
									JOptionPane.ERROR_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null, "No ha insertado ningun dni", "Error",
								JOptionPane.ERROR_MESSAGE);
				}
			});
		}
		return btnDesapuntarSocio;
	}

	private void resetPanelActivitiesForUsersRemove() {
		pnActivitesAlvaro.removeAll();
		getPnActivitesAlvaro().repaint();
		pnActivitesAlvaro.paintAll(pnActivitesAlvaro.getGraphics());
		currentActivitiesToUser = "";
		getTxtRemovePlazasUser().setText("");
		getTxtRemovePlazasUser().setForeground(Color.BLACK);
		getBtnDesapuntarSocio().setText("Desapuntar");
	}

	protected void showActivitiesToUserRemove(String dni) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String fecha = df.format(getCalendar().getDate());
		ArrayList<String> reservas = manager.getIdReservaDesapuntarSocio(dni);
		for (String id_reserva : reservas) {
			ArrayList<String[]> activities = manager.getActivitiesSocio(id_reserva);
			for (String act[] : activities) {
				if (manager.seeActivitiesFecha(act[1], fecha))
					showActivityUniqueToUserRemove(
							"<html><p>" + act[0] + "</p>" + "<p>Fecha: " + manager.getBookingDate(act[1]) + "</p>"
									+ "<p>Hora de inicio: " + manager.getBookingHourI(act[1]) + "h</p></html>",
							act[1]);
			}
			if (activities.isEmpty())
				pnActivitesAlvaro.add(getLblNoHayActividades());
		}
	}

	protected void showActivityUniqueToUserRemove(String act, String id_reserva) {
		JButton boton = new JButton();
		boton.setText(act);
		boton.setFont(new Font("Tw Cen MT", Font.BOLD, 16));
		boton.setActionCommand(id_reserva);
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentActivitiesToUser = boton.getActionCommand(); // Saul
				boton.setBackground(new Color(135, 206, 250));
				Component[] comp = getPnActivitesAlvaro().getComponents();
				for (int i = 0; i < comp.length; i++) {
					JButton aux = (JButton) comp[i];
					if (!aux.getActionCommand().equals(boton.getActionCommand()))
						aux.setBackground(Color.WHITE);
				}

				if (manager.getPlazasLibresAct(currentActivitiesToUser) > 0) {
					getTxtRemovePlazasUser()
							.setText("Quedan: " + manager.getPlazasLibresAct(currentActivitiesToUser) + " plazas");
				} else {
					getTxtRemovePlazasUser().setText("No quedan plazas");
					getTxtRemovePlazasUser().setForeground(Color.RED);
					getBtnDesapuntarSocio().setText("Apuntar a lista de espera");
				}
			}
		});
		boton.setBounds(0, 0, 100, 100);
		boton.setBackground(getPnActivitesAlvaro().getBackground());
		getPnActivitesAlvaro().add(boton);
	}

	// Alvaro ModificarPlazas
	private JPanel getPnModificarPlazas() {
		if (pnModificarPlazas == null) {
			pnModificarPlazas = new JPanel();
			pnModificarPlazas.setBorder(
					new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Aumentar Plazas para clase concreta",
							TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnModificarPlazas.setBackground(Color.WHITE);
			GridBagLayout gbl_pnModificarPlazas = new GridBagLayout();
			gbl_pnModificarPlazas.columnWidths = new int[] { 100, 151, 0 };
			gbl_pnModificarPlazas.rowHeights = new int[] { 0, 27, 0, 27, 32, 23, 0 };
			gbl_pnModificarPlazas.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnModificarPlazas.rowWeights = new double[] { 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnModificarPlazas.setLayout(gbl_pnModificarPlazas);
			GridBagConstraints gbc_tfAumentarPlazas = new GridBagConstraints();
			gbc_tfAumentarPlazas.insets = new Insets(0, 0, 5, 5);
			gbc_tfAumentarPlazas.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfAumentarPlazas.gridx = 1;
			gbc_tfAumentarPlazas.gridy = 1;
			pnModificarPlazas.add(getTfAumentarPlazas(), gbc_tfAumentarPlazas);
			GridBagConstraints gbc_lblPlazasActuales = new GridBagConstraints();
			gbc_lblPlazasActuales.insets = new Insets(0, 0, 5, 5);
			gbc_lblPlazasActuales.gridx = 0;
			gbc_lblPlazasActuales.gridy = 1;
			pnModificarPlazas.add(getLblPlazasActuales(), gbc_lblPlazasActuales);
			GridBagConstraints gbc_lblModificarPlazas = new GridBagConstraints();
			gbc_lblModificarPlazas.fill = GridBagConstraints.VERTICAL;
			gbc_lblModificarPlazas.insets = new Insets(0, 0, 5, 5);
			gbc_lblModificarPlazas.gridx = 0;
			gbc_lblModificarPlazas.gridy = 3;
			pnModificarPlazas.add(getLblModificarPlazas(), gbc_lblModificarPlazas);
			GridBagConstraints gbc_cbxPlazas = new GridBagConstraints();
			gbc_cbxPlazas.fill = GridBagConstraints.BOTH;
			gbc_cbxPlazas.insets = new Insets(0, 0, 5, 0);
			gbc_cbxPlazas.gridx = 1;
			gbc_cbxPlazas.gridy = 3;
			pnModificarPlazas.add(getCbxPlazas(), gbc_cbxPlazas);
			GridBagConstraints gbc_btnModificarPlazas = new GridBagConstraints();
			gbc_btnModificarPlazas.fill = GridBagConstraints.BOTH;
			gbc_btnModificarPlazas.gridwidth = 2;
			gbc_btnModificarPlazas.gridx = 0;
			gbc_btnModificarPlazas.gridy = 5;
			pnModificarPlazas.add(getBtnModificarPlazas(), gbc_btnModificarPlazas);
		}
		return pnModificarPlazas;
	}

	private JLabel getLblModificarPlazas() {
		if (lblModificarPlazas == null) {
			lblModificarPlazas = new JLabel("N�mero de plazas a aumentar:");
			lblModificarPlazas.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblModificarPlazas;
	}

	private JComboBox<String> getCbxPlazas() {
		if (cbxPlazas == null) {
			cbxPlazas = new JComboBox<String>();
			cbxPlazas.setFont(new Font("Tw Cen MT", Font.ITALIC, 15));
			cbxPlazas.setBackground(Color.WHITE);
		}
		return cbxPlazas;
	}

	protected void fillComboBoxPlazas() {
		cbxPlazas.removeAllItems();
		for (int i = 1; i < 100; i++) {
			cbxPlazas.addItem(String.valueOf(i));
		}
	}

	private JButton getBtnModificarPlazas() {
		if (btnModificarPlazas == null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = df.format(getCalendar().getDate());
			btnModificarPlazas = new JButton("Aumentar plazas");
			btnModificarPlazas.setFont(new Font("Dialog", Font.BOLD, 10));
			btnModificarPlazas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (manager.getPlazasActividad(currentClaseSeleccionadaHorario) != -1) {
						if (manager.seeActivitiesFecha(currentClaseSeleccionadaHorario, fecha)) {
							// if
							// (Integer.parseInt(String.valueOf(cbxPlazas.getSelectedItem()))
							// > manager
							// .getPlazasActividad(currentClaseSeleccionadaHorario))
							// {
							if (manager.modificarPlazas(currentClaseSeleccionadaHorario,
									String.valueOf(getCbxPlazas().getSelectedItem()))) {
								JOptionPane.showMessageDialog(null,
										"Se ha aumentado el n�mero de plazas en: "
												+ String.valueOf(getCbxPlazas().getSelectedItem())
												+ "\nLa operacion se ha completado con exito");
								tfAumentarPlazas.setText(
										String.valueOf(manager.getPlazasActividad(currentClaseSeleccionadaHorario)));
							}
							// } else
							// JOptionPane.showMessageDialog(null,
							// "Las plazas deben ser mas de las que hay
							// actualmente", "Error",
							// JOptionPane.ERROR_MESSAGE);
						} else
							JOptionPane.showMessageDialog(null,
									"Esta actividad ya se ha llevado a cabo, por lo que no puede modificar el numero de dias",
									"Error", JOptionPane.ERROR_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null,
								"No se puede modificar las plazas de esta actividad ya que es una actividad ilimitada",
								"Error", JOptionPane.ERROR_MESSAGE);
				}
			});
			btnModificarPlazas.setBackground(Color.WHITE);
		}
		return btnModificarPlazas;
	}

	private JLabel getLblPlazasActuales() {
		if (lblPlazasActuales == null) {
			lblPlazasActuales = new JLabel("Plazas actuales:");
			lblPlazasActuales.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblPlazasActuales;
	}

	private JTextField getTfAumentarPlazas() {
		if (tfAumentarPlazas == null) {
			tfAumentarPlazas = new JTextField();
			tfAumentarPlazas.setEditable(false);
			tfAumentarPlazas.setColumns(10);
		}
		return tfAumentarPlazas;
	}

	// Alvaro ModificarHoras
	private JPanel getPnModificarHoras() {
		if (pnModificarHoras == null) {
			pnModificarHoras = new JPanel();
			pnModificarHoras.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					"Cambio de Hora Final para clase concreta", TitledBorder.CENTER, TitledBorder.TOP, null,
					new Color(0, 0, 0)));
			pnModificarHoras.setBackground(Color.WHITE);
			GridBagLayout gbl_pnModificarHoras = new GridBagLayout();
			gbl_pnModificarHoras.columnWidths = new int[] { 100, 151, 0 };
			gbl_pnModificarHoras.rowHeights = new int[] { 0, 27, 0, 27, 32, 23, 0 };
			gbl_pnModificarHoras.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnModificarHoras.rowWeights = new double[] { 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnModificarHoras.setLayout(gbl_pnModificarHoras);
			GridBagConstraints gbc_tfHoraActual = new GridBagConstraints();
			gbc_tfHoraActual.insets = new Insets(0, 0, 5, 5);
			gbc_tfHoraActual.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfHoraActual.gridx = 1;
			gbc_tfHoraActual.gridy = 1;
			pnModificarHoras.add(getTfHoraActual(), gbc_tfHoraActual);
			GridBagConstraints gbc_cbxHoraFinal = new GridBagConstraints();
			gbc_cbxHoraFinal.insets = new Insets(0, 0, 5, 0);
			gbc_cbxHoraFinal.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbxHoraFinal.gridx = 1;
			gbc_cbxHoraFinal.gridy = 3;
			pnModificarHoras.add(getCbxHoraFinal(), gbc_cbxHoraFinal);
			GridBagConstraints gbc_lblNuevaHoraFinal = new GridBagConstraints();
			gbc_lblNuevaHoraFinal.insets = new Insets(0, 0, 5, 5);
			gbc_lblNuevaHoraFinal.gridx = 0;
			gbc_lblNuevaHoraFinal.gridy = 3;
			pnModificarHoras.add(getLblNuevaHoraFinal(), gbc_lblNuevaHoraFinal);
			GridBagConstraints gbc_lblHoraFinalAnterior = new GridBagConstraints();
			gbc_lblHoraFinalAnterior.fill = GridBagConstraints.VERTICAL;
			gbc_lblHoraFinalAnterior.insets = new Insets(0, 0, 5, 5);
			gbc_lblHoraFinalAnterior.gridx = 0;
			gbc_lblHoraFinalAnterior.gridy = 1;
			pnModificarHoras.add(getLblHoraFinalAnterior(), gbc_lblHoraFinalAnterior);
			GridBagConstraints gbc_btnCambiarHora = new GridBagConstraints();
			gbc_btnCambiarHora.fill = GridBagConstraints.BOTH;
			gbc_btnCambiarHora.gridwidth = 2;
			gbc_btnCambiarHora.gridx = 0;
			gbc_btnCambiarHora.gridy = 5;
			pnModificarHoras.add(getBtnCambiarHora(), gbc_btnCambiarHora);
		}
		return pnModificarHoras;
	}

	private JLabel getLblHoraFinalAnterior() {
		if (lblHoraFinalAnterior == null) {
			lblHoraFinalAnterior = new JLabel("Hora final actual:");
			lblHoraFinalAnterior.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblHoraFinalAnterior;
	}

	private JButton getBtnCambiarHora() {
		if (btnCambiarHora == null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = df.format(getCalendar().getDate());
			btnCambiarHora = new JButton("Cambiar Hora");
			btnCambiarHora.setFont(new Font("Dialog", Font.BOLD, 10));
			btnCambiarHora.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String instalacion = manager.getInstalacion(currentClaseSeleccionadaHorario);
					String fechaInstalacion = manager.getFechaInstalacion(currentClaseSeleccionadaHorario);
					ArrayList<Integer> listaHorasOcupadas = manager.getHorasInstalacion(instalacion, fechaInstalacion);
					int horaActual = manager.getHoras(currentClaseSeleccionadaHorario);
					if (manager.seeActivitiesFecha(currentClaseSeleccionadaHorario, fecha)) {
						if (Integer.parseInt(String.valueOf(cbxHoraFinal.getSelectedItem())) > horaActual) {
							for (int i = 0; i < listaHorasOcupadas.size(); i++) {
								if (Integer.parseInt(
										String.valueOf(cbxHoraFinal.getSelectedItem())) != listaHorasOcupadas.get(i)) {
									if (manager.modificarHoras(currentClaseSeleccionadaHorario,
											String.valueOf(getCbxHoraFinal().getSelectedItem()))) {
										JOptionPane.showMessageDialog(null, "La operacion se ha completado con exito");
										tfHoraActual.setText(
												String.valueOf(manager.getHoras(currentClaseSeleccionadaHorario)));
										break;
									}
								} else {
									JOptionPane.showMessageDialog(null, "Esta instalacion esta ocupada para esta hora",
											"Error", JOptionPane.ERROR_MESSAGE);
									break;
								}
							}
						} else
							JOptionPane.showMessageDialog(null,
									"La hora final no puede ser menor igual que la hora final actual", "Error",
									JOptionPane.ERROR_MESSAGE);
					}

					else
						JOptionPane.showMessageDialog(null,
								"Esta actividad ya se ha llevado a cabo, por lo que no puede modificar el numero de Horas",
								"Error", JOptionPane.ERROR_MESSAGE);
				}
			});
			btnCambiarHora.setBackground(Color.WHITE);
		}
		return btnCambiarHora;
	}

	private JLabel getLblNuevaHoraFinal() {
		if (lblNuevaHoraFinal == null) {
			lblNuevaHoraFinal = new JLabel("Nueva hora final");
		}
		return lblNuevaHoraFinal;
	}

	private JComboBox<String> getCbxHoraFinal() {
		if (cbxHoraFinal == null) {
			cbxHoraFinal = new JComboBox<String>();
			cbxHoraFinal.setFont(new Font("Tw Cen MT", Font.ITALIC, 15));
			cbxHoraFinal.setBackground(Color.WHITE);
		}
		return cbxHoraFinal;
	}

	protected void fillComboBoxHoras() {
		cbxHoraFinal.removeAllItems();
		for (int i = 0; i < 24; i++) {
			cbxHoraFinal.addItem(String.valueOf(i));
		}
	}

	private JTextField getTfHoraActual() {
		if (tfHoraActual == null) {
			tfHoraActual = new JTextField();
			tfHoraActual.setEditable(false);
			tfHoraActual.setColumns(10);
		}
		return tfHoraActual;
	}

	private JComboBox<String> getCBInstalacionesAfiltrar() {
		if (cBInstalacionesAfiltrar == null) {
			cBInstalacionesAfiltrar = new JComboBox();
			cBInstalacionesAfiltrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						comprobarSemanaHorarios(manager
								.getActParaHorarioCentro((String) getCBInstalacionesAfiltrar().getSelectedItem()));
						getBtnCambiar().setEnabled(false);
					} catch (Exception eew) {
						// TODO: handle exception
					}
				}
			});
			String[] model = manager.getAllInst().toArray(new String[manager.getAllInst().size()]);
			cBInstalacionesAfiltrar.setModel(new DefaultComboBoxModel<String>(model));

		}
		return cBInstalacionesAfiltrar;
	}

	// Alvaro modificarInstlacion
	private JPanel getPnInstalacionActividad() {
		if (pnInstalacionActividad == null) {
			pnInstalacionActividad = new JPanel();
			pnInstalacionActividad.setBorder(
					new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Cambio de Instalacion para actividad",
							TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
			pnInstalacionActividad.setBackground(Color.WHITE);
			GridBagLayout gbl_pnInstalacionActividad = new GridBagLayout();
			gbl_pnInstalacionActividad.columnWidths = new int[] { 100, 151, 0 };
			gbl_pnInstalacionActividad.rowHeights = new int[] { 0, 27, 0, 27, 32, 23, 0 };
			gbl_pnInstalacionActividad.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
			gbl_pnInstalacionActividad.rowWeights = new double[] { 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			pnInstalacionActividad.setLayout(gbl_pnInstalacionActividad);
			GridBagConstraints gbc_tfInstalacionActual = new GridBagConstraints();
			gbc_tfInstalacionActual.insets = new Insets(0, 0, 5, 5);
			gbc_tfInstalacionActual.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfInstalacionActual.gridx = 1;
			gbc_tfInstalacionActual.gridy = 2;
			pnInstalacionActividad.add(getTfInstalacionActual(), gbc_tfInstalacionActual);
			GridBagConstraints gbc_lblInstalacionActual = new GridBagConstraints();
			gbc_lblInstalacionActual.insets = new Insets(0, 0, 5, 5);
			gbc_lblInstalacionActual.gridx = 0;
			gbc_lblInstalacionActual.gridy = 2;
			pnInstalacionActividad.add(getLblInstalacionActual(), gbc_lblInstalacionActual);
			GridBagConstraints gbc_lblActividadEscogida = new GridBagConstraints();
			gbc_lblActividadEscogida.fill = GridBagConstraints.VERTICAL;
			gbc_lblActividadEscogida.insets = new Insets(0, 0, 5, 5);
			gbc_lblActividadEscogida.gridx = 0;
			gbc_lblActividadEscogida.gridy = 1;
			pnInstalacionActividad.add(getLblActividadEscogida(), gbc_lblActividadEscogida);
			GridBagConstraints gbc_tfActividadEscogida = new GridBagConstraints();
			gbc_tfActividadEscogida.fill = GridBagConstraints.HORIZONTAL;
			gbc_tfActividadEscogida.insets = new Insets(0, 0, 5, 0);
			gbc_tfActividadEscogida.gridx = 1;
			gbc_tfActividadEscogida.gridy = 1;
			pnInstalacionActividad.add(getTfActividadEscogida(), gbc_tfActividadEscogida);
			GridBagConstraints gbc_lblNuevaInstalacion = new GridBagConstraints();
			gbc_lblNuevaInstalacion.insets = new Insets(0, 0, 5, 5);
			gbc_lblNuevaInstalacion.gridx = 0;
			gbc_lblNuevaInstalacion.gridy = 3;
			pnInstalacionActividad.add(getLblNuevaInstalacion(), gbc_lblNuevaInstalacion);
			GridBagConstraints gbc_cbxNuevaInstalacion = new GridBagConstraints();
			gbc_cbxNuevaInstalacion.fill = GridBagConstraints.HORIZONTAL;
			gbc_cbxNuevaInstalacion.insets = new Insets(0, 0, 5, 0);
			gbc_cbxNuevaInstalacion.gridx = 1;
			gbc_cbxNuevaInstalacion.gridy = 3;
			pnInstalacionActividad.add(getCbxNuevaInstalacion(), gbc_cbxNuevaInstalacion);
			GridBagConstraints gbc_btnCambiarInstalacionDe = new GridBagConstraints();
			gbc_btnCambiarInstalacionDe.fill = GridBagConstraints.BOTH;
			gbc_btnCambiarInstalacionDe.gridwidth = 2;
			gbc_btnCambiarInstalacionDe.gridx = 0;
			gbc_btnCambiarInstalacionDe.gridy = 5;
			pnInstalacionActividad.add(getBtnCambiarInstalacionDe(), gbc_btnCambiarInstalacionDe);
		}
		return pnInstalacionActividad;
	}

	private JTextField getTfActividadEscogida() {
		if (tfActividadEscogida == null) {
			tfActividadEscogida = new JTextField();
			tfActividadEscogida.setEditable(false);
			tfActividadEscogida.setColumns(10);
		}
		return tfActividadEscogida;
	}

	private JComboBox<String> getCbxNuevaInstalacion() {
		if (cbxNuevaInstalacion == null) {
			cbxNuevaInstalacion = new JComboBox<String>();
			cbxNuevaInstalacion.setFont(new Font("Tw Cen MT", Font.ITALIC, 15));
			cbxNuevaInstalacion.setBackground(Color.WHITE);
			String[] model = manager.getInstallations().toArray(new String[manager.getInstallations().size()]);
			cbxNuevaInstalacion.setModel(new DefaultComboBoxModel<String>(model));
		}
		return cbxNuevaInstalacion;
	}

	private JLabel getLblNuevaInstalacion() {
		if (lblNuevaInstalacion == null) {
			lblNuevaInstalacion = new JLabel("Nueva Instalacion:");
		}
		return lblNuevaInstalacion;
	}

	private JLabel getLblActividadEscogida() {
		if (lblActividadEscogida == null) {
			lblActividadEscogida = new JLabel("Actividad escogida:");
			lblActividadEscogida.setFont(new Font("Dialog", Font.BOLD, 12));
		}
		return lblActividadEscogida;
	}

	private JButton getBtnCambiarInstalacionDe() {
		if (btnCambiarInstalacionDe == null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String fecha = df.format(getCalendar().getDate());
			btnCambiarInstalacionDe = new JButton("Cambiar Instalacion de actividad");
			btnCambiarInstalacionDe.setFont(new Font("Dialog", Font.BOLD, 10));
			btnCambiarInstalacionDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String actividad = manager.getActividad(currentClaseSeleccionadaHorario);
					if (manager.getModInst(String.valueOf(cbxNuevaInstalacion.getSelectedItem()), actividad, fecha)) {
						tfInstalacionActual.setText(String.valueOf(cbxNuevaInstalacion.getSelectedItem()));
						JOptionPane.showMessageDialog(null, "La operacion se ha completado con exito");
					}
				}
			});
			btnCambiarInstalacionDe.setBackground(Color.WHITE);
		}
		return btnCambiarInstalacionDe;
	}

	private JLabel getLblInstalacionActual() {
		if (lblInstalacionActual == null) {
			lblInstalacionActual = new JLabel("Instalaci�n actual:");
		}
		return lblInstalacionActual;
	}

	private JTextField getTfInstalacionActual() {
		if (tfInstalacionActual == null) {
			tfInstalacionActual = new JTextField();
			tfInstalacionActual.setEditable(false);
			tfInstalacionActual.setColumns(10);
		}
		return tfInstalacionActual;
	}
}
